package com.mx.controlsoft.Cajas.Model;

public class DetdatHaber_DetalleframedBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public long getINidDet() {
		return INidDet;
	}
	public void setINidDet(long iNidDet) {
		INidDet = iNidDet;
	}
	public String getServicioDescripcion() {
		return servicioDescripcion;
	}
	public void setServicioDescripcion(String servicioDescripcion) {
		this.servicioDescripcion = servicioDescripcion;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public String getSociopromo() {
		return sociopromo;
	}
	public void setSociopromo(String sociopromo) {
		this.sociopromo = sociopromo;
	}
	public String gethRef() {
		return hRef;
	}
	public void sethRef(String hRef) {
		this.hRef = hRef;
	}
	private long  INid;
    private long INidDet;
    private String servicioDescripcion;
    private String saldo;
    private String sociopromo;
    private String hRef;
    
	@Override
	public String toString() {
		return "ttdetdatHaber_DetalleframedBE [INid=" + INid + ", INidDet=" + INidDet + ", servicioDescripcion="
				+ servicioDescripcion + ", saldo=" + saldo + ", sociopromo=" + sociopromo + ", hRef=" + hRef + "]";
	}
}
