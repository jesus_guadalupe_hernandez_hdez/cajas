package com.mx.controlsoft.Cajas.Model;

public class EncdatosBuscaSocioAceptarBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsIrPagina() {
		return INsIrPagina;
	}
	public void setINsIrPagina(String iNsIrPagina) {
		INsIrPagina = iNsIrPagina;
	}
	public String getINVerSaldos() {
		return INVerSaldos;
	}
	public void setINVerSaldos(String iNVerSaldos) {
		INVerSaldos = iNVerSaldos;
	}
	public String getINSeValidoHuella() {
		return INSeValidoHuella;
	}
	public void setINSeValidoHuella(String iNSeValidoHuella) {
		INSeValidoHuella = iNSeValidoHuella;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getNuevaFicha() {
		return NuevaFicha;
	}
	public void setNuevaFicha(String nuevaFicha) {
		NuevaFicha = nuevaFicha;
	}
	public String getPrimeraPagina() {
		return PrimeraPagina;
	}
	public void setPrimeraPagina(String primeraPagina) {
		PrimeraPagina = primeraPagina;
	}
	public String getsNombreSocio() {
		return sNombreSocio;
	}
	public void setsNombreSocio(String sNombreSocio) {
		this.sNombreSocio = sNombreSocio;
	}
	public boolean isAcceperftran() {
		return acceperftran;
	}
	public void setAcceperftran(boolean acceperftran) {
		this.acceperftran = acceperftran;
	}
	
	public long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String respuesta;
    private String INsIrPagina;
    private String INVerSaldos;
    private String INSeValidoHuella;
    private String INsNumeroSocio; 
    private String INsTransaccion;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String NuevaFicha;
    private String PrimeraPagina;
    private String sNombreSocio;
    private boolean acceperftran;
    
	@Override
	public String toString() {
		return "EncdatosBuscaSocioAceptarBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", respuesta=" + respuesta + ", INsIrPagina=" + INsIrPagina + ", INVerSaldos="
				+ INVerSaldos + ", INSeValidoHuella=" + INSeValidoHuella + ", INsNumeroSocio=" + INsNumeroSocio
				+ ", INsTransaccion=" + INsTransaccion + ", INsUnionSocio=" + INsUnionSocio + ", INsCajaSocio="
				+ INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio + ", NuevaFicha=" + NuevaFicha
				+ ", PrimeraPagina=" + PrimeraPagina + ", sNombreSocio=" + sNombreSocio + ", acceperftran="
				+ acceperftran + "]";
	}
}
