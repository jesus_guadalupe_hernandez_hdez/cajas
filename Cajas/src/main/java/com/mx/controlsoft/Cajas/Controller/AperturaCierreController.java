package com.mx.controlsoft.Cajas.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralAperturaCierre;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Controller
@RequestMapping(value = "/apertura")
public class AperturaCierreController {
	
	@Autowired
	ConfigProperties config;
	
	@RequestMapping(value = "/aperturaCierre",method = RequestMethod.GET)
	public String mostrarPrincipal(Model model ,Login  login, HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			
			RequestGralAperturaCierre req = consumeAperturaCaja();
			model.addAttribute("requestAperturaCierre", req.getRequest().getTtserapertura());
			model.addAttribute("requestCaja", req.getRequest().getTthistapertura());
			model.addAttribute("requestDato", req.getRequest().getTtdato());
			
			return "apertura_cierre/aperturaCierreList";
		}
		return "login";
		
		
	}
	
	@RequestMapping(value = "/aperturaEdit",method = RequestMethod.GET)
	public String mostrarAperturaEdit(Model model ,Login  login, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "apertura_cierre/abrirAperturaCierre";
		}
		return "login";
		
	}
	
	private RequestGralAperturaCierre consumeAperturaCaja () {
		try {
			RequestGralAperturaCierre request = null;
			String json = "{\r\n" + 
					"  \"request\": {\r\n" + 
					"    \"ttdato\": [\r\n" + 
					"      {\r\n" + 
					"        \"IDUnion\": \"1\",\r\n" + 
					"        \"IDCajaPopular\": \"8\",\r\n" + 
					"        \"IDSucursal\": \"1\",\r\n" + 
					"        \"IDUsuario\": \"cpcomala\",\r\n" + 
					"        \"fecha\": \"30-01-2020\",\r\n" + 
					"        \"tituloseccion\": \"\",\r\n" + 
					"        \"NombreSucursal\": \"\",\r\n" + 
					"        \"titulohistorico\": \"\",\r\n" + 
					"        \"opchrRespuesta\": \"\"\r\n" + 
					"      }\r\n" + 
					"    ],\r\n" + 
					"    \"tthistapertura\": [\r\n" + 
					"      \r\n" + 
					"    ],\r\n" + 
					"    \"ttserapertura\": [\r\n" + 
					"      \r\n" + 
					"    ],\r\n" + 
					"    \"ttpendiente\": [\r\n" + 
					"      \r\n" + 
					"    ]\r\n" + 
					"  }\r\n" + 
					"}";
            URL url = new URL(ConfigProperties.getConex() +"/wscajapService/aperturaBE");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	Gson gson = new Gson();
            	request  = gson.fromJson(output,
            			RequestGralAperturaCierre.class);
            	//System.out.println(request);
            }
            
            conn.disconnect();
            return request;

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
            return null;
        }
	}

}