package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetFicha_FichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasconHorarioxSucBE;

public class RequestFichasconHorarioxSucBE {
	
	public List<Encdatos_FichasconHorarioxSucBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<Encdatos_FichasconHorarioxSucBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DetFicha_FichasconHorarioxSucBE> getTtDetFicha() {
		return ttDetFicha;
	}
	public void setTtDetFicha(List<DetFicha_FichasconHorarioxSucBE> ttDetFicha) {
		this.ttDetFicha = ttDetFicha;
	}
	
	private List<Encdatos_FichasconHorarioxSucBE> ttEncdatos;
	private List<DetFicha_FichasconHorarioxSucBE> ttDetFicha;
	
	@Override
	public String toString() {
		return "RequestFichasconHorarioxSucBE [ttEncdatos=" + ttEncdatos + ", ttDetFicha=" + ttDetFicha + "]";
	}
}
