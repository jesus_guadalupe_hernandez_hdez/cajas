package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.Login;

public class RequestLogin {
	
	private List<Login> ttlogin;

	public List<Login> getTtlogin() {
		return ttlogin;
	}

	public void setTtlogin(List<Login> ttlogin) {
		this.ttlogin = ttlogin;
	}

	@Override
	public String toString() {
		return "RequestLogin [ttlogin=" + ttlogin + "]";
	}
}
