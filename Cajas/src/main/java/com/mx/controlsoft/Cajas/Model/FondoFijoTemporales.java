package com.mx.controlsoft.Cajas.Model;

public class FondoFijoTemporales {
	public String getEstadoTransaccion() {
		return estadoTransaccion;
	}
	public void setEstadoTransaccion(String estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}
	public String getCajaCajero() {
		return cajaCajero;
	}
	public void setCajaCajero(String cajaCajero) {
		this.cajaCajero = cajaCajero;
	}
	public String getTxtImporte() {
		return txtImporte;
	}
	public void setTxtImporte(String txtImporte) {
		this.txtImporte = txtImporte;
	}
	public String getTtMovimiento() {
		return ttMovimiento;
	}
	public void setTtMovimiento(String ttMovimiento) {
		this.ttMovimiento = ttMovimiento;
	}
	public String getTtTipoMoneda() {
		return ttTipoMoneda;
	}
	public void setTtTipoMoneda(String ttTipoMoneda) {
		this.ttTipoMoneda = ttTipoMoneda;
	}
	public String getTtTipoEfec() {
		return ttTipoEfec;
	}
	public void setTtTipoEfec(String ttTipoEfec) {
		this.ttTipoEfec = ttTipoEfec;
	}
	public String getTtCantidad() {
		return ttCantidad;
	}
	public void setTtCantidad(String ttCantidad) {
		this.ttCantidad = ttCantidad;
	}
	public String getTtDenominacion() {
		return ttDenominacion;
	}
	public void setTtDenominacion(String ttDenominacion) {
		this.ttDenominacion = ttDenominacion;
	}
	public String getTtImporte() {
		return ttImporte;
	}
	public void setTtImporte(String ttImporte) {
		this.ttImporte = ttImporte;
	}
	
	private String estadoTransaccion;
    private String cajaCajero;
    private String txtImporte;
	private String ttMovimiento;
    private String ttTipoMoneda;
    private String ttTipoEfec;
    private String ttCantidad;
    private String ttDenominacion;
    private String ttImporte;
    
    
    
	@Override
	public String toString() {
		return "FondoFijoTemporales [estadoTransaccion=" + estadoTransaccion + ", cajaCajero=" + cajaCajero
				+ ", txtImporte=" + txtImporte + "]";
	}
}
