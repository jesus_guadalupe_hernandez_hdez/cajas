package com.mx.controlsoft.Cajas.Model;

public class DetMensajesoc_DetalleFichaBE {
	
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getIdDet() {
		return idDet;
	}
	public void setIdDet(String idDet) {
		this.idDet = idDet;
	}
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public String getsTexto() {
		return sTexto;
	}
	public void setsTexto(String sTexto) {
		this.sTexto = sTexto;
	}
	public String getnColor() {
		return nColor;
	}
	public void setnColor(String nColor) {
		this.nColor = nColor;
	}
	public String getChrColor() {
		return chrColor;
	}
	public void setChrColor(String chrColor) {
		this.chrColor = chrColor;
	}
	
	private long INid;
    private String idDet;
    private String tipoMensaje;
    private String sTexto;
    private String nColor;
    private String chrColor;
    
	@Override
	public String toString() {
		return "DetMensajesoc_DetalleFichaBE [INid=" + INid + ", idDet=" + idDet + ", tipoMensaje=" + tipoMensaje
				+ ", sTexto=" + sTexto + ", nColor=" + nColor + ", chrColor=" + chrColor + "]";
	}
     
}
