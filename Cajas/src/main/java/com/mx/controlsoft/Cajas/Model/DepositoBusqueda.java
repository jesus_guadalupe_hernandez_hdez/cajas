package com.mx.controlsoft.Cajas.Model;

public class DepositoBusqueda {
	
	public String getUnion() {
		return union;
	}
	public void setUnion(String union) {
		this.union = union;
	}
	public String getCajasocio() {
		return cajasocio;
	}
	public void setCajasocio(String cajasocio) {
		this.cajasocio = cajasocio;
	}
	public String getSucursalsocio() {
		return sucursalsocio;
	}
	public void setSucursalsocio(String sucursalsocio) {
		this.sucursalsocio = sucursalsocio;
	}
	public String getSocio() {
		return socio;
	}
	public void setSocio(String socio) {
		this.socio = socio;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	
	
	private String union;
	private String cajasocio;
	private String sucursalsocio;
	private String socio;
	private String apellido1;
	private String apellido2;
	
	
	@Override
	public String toString() {
		return "DepositoBusqueda [union=" + union + ", cajasocio=" + cajasocio + ", sucursalsocio=" + sucursalsocio
				+ ", socio=" + socio + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + "]";
	}
	
}
