package com.mx.controlsoft.Cajas.Model;

public class DetalleCorteCajaConsultacortecajaBE {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIDDetalleCorteCaja() {
		return IDDetalleCorteCaja;
	}
	public void setIDDetalleCorteCaja(String iDDetalleCorteCaja) {
		IDDetalleCorteCaja = iDDetalleCorteCaja;
	}
	public String getIDTipoMoneda() {
		return IDTipoMoneda;
	}
	public void setIDTipoMoneda(String iDTipoMoneda) {
		IDTipoMoneda = iDTipoMoneda;
	}
	public String getTipoEfectivo() {
		return TipoEfectivo;
	}
	public void setTipoEfectivo(String tipoEfectivo) {
		TipoEfectivo = tipoEfectivo;
	}
	public String getDenominacion() {
		return Denominacion;
	}
	public void setDenominacion(String denominacion) {
		Denominacion = denominacion;
	}
	public String getCantidad() {
		return Cantidad;
	}
	public void setCantidad(String cantidad) {
		Cantidad = cantidad;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	private long id;
    private String IDDetalleCorteCaja;
    private String IDTipoMoneda;
    private String TipoEfectivo;
    private String Denominacion;
    private String Cantidad;
    private String Importe;
	@Override
	public String toString() {
		return "DetalleCorteCajaConsultacortecajaBE [id=" + id + ", IDDetalleCorteCaja=" + IDDetalleCorteCaja
				+ ", IDTipoMoneda=" + IDTipoMoneda + ", TipoEfectivo=" + TipoEfectivo + ", Denominacion=" + Denominacion
				+ ", Cantidad=" + Cantidad + ", Importe=" + Importe + "]";
	}
}
