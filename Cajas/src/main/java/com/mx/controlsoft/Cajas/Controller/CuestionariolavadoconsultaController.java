package com.mx.controlsoft.Cajas.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.controlsoft.Cajas.Model.Login;

@Controller
@RequestMapping(value = "/cuestionarioLavado")
public class CuestionariolavadoconsultaController {
	
	@GetMapping ("/getPostCuestionariLavado")
	public String getPostCuestionariLavado(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "util/cuestionarioLavado";
		}
		return "login";
	}
}
