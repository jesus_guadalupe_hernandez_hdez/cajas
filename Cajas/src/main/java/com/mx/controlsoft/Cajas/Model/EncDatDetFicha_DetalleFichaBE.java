package com.mx.controlsoft.Cajas.Model;

public class EncDatDetFicha_DetalleFichaBE {
	
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINUsuarioOrigen() {
		return INUsuarioOrigen;
	}
	public void setINUsuarioOrigen(String iNUsuarioOrigen) {
		INUsuarioOrigen = iNUsuarioOrigen;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getINRefreshHead() {
		return INRefreshHead;
	}
	public void setINRefreshHead(String iNRefreshHead) {
		INRefreshHead = iNRefreshHead;
	}
	public String getINNuevaFicha() {
		return INNuevaFicha;
	}
	public void setINNuevaFicha(String iNNuevaFicha) {
		INNuevaFicha = iNNuevaFicha;
	}
	public String getINDespliegaMensaje() {
		return INDespliegaMensaje;
	}
	public void setINDespliegaMensaje(String iNDespliegaMensaje) {
		INDespliegaMensaje = iNDespliegaMensaje;
	}
	public String getINsFolioFicha() {
		return INsFolioFicha;
	}
	public void setINsFolioFicha(String iNsFolioFicha) {
		INsFolioFicha = iNsFolioFicha;
	}
	public String getINsServicio() {
		return INsServicio;
	}
	public void setINsServicio(String iNsServicio) {
		INsServicio = iNsServicio;
	}
	public String getINsCajaAsignada() {
		return INsCajaAsignada;
	}
	public void setINsCajaAsignada(String iNsCajaAsignada) {
		INsCajaAsignada = iNsCajaAsignada;
	}
	public String getINsTransaccion2() {
		return INsTransaccion2;
	}
	public void setINsTransaccion2(String iNsTransaccion2) {
		INsTransaccion2 = iNsTransaccion2;
	}
	public String getINsCadenaFicha() {
		return INsCadenaFicha;
	}
	public void setINsCadenaFicha(String iNsCadenaFicha) {
		INsCadenaFicha = iNsCadenaFicha;
	}
	public String getsPageStatus() {
		return sPageStatus;
	}
	public void setsPageStatus(String sPageStatus) {
		this.sPageStatus = sPageStatus;
	}
	public String getINMaintOption1() {
		return INMaintOption1;
	}
	public void setINMaintOption1(String iNMaintOption1) {
		INMaintOption1 = iNMaintOption1;
	}
	public String getINMaintOption() {
		return INMaintOption;
	}
	public void setINMaintOption(String iNMaintOption) {
		INMaintOption = iNMaintOption;
	}
	public String getINServicio() {
		return INServicio;
	}
	public void setINServicio(String iNServicio) {
		INServicio = iNServicio;
	}
	public String getINvalidapago() {
		return INvalidapago;
	}
	public void setINvalidapago(String iNvalidapago) {
		INvalidapago = iNvalidapago;
	}
	public String getINImporteTotal() {
		return INImporteTotal;
	}
	public void setINImporteTotal(String iNImporteTotal) {
		INImporteTotal = iNImporteTotal;
	}
	public String getINsCallType() {
		return INsCallType;
	}
	public void setINsCallType(String iNsCallType) {
		INsCallType = iNsCallType;
	}
	public String getsCallerProcedure() {
		return sCallerProcedure;
	}
	public void setsCallerProcedure(String sCallerProcedure) {
		this.sCallerProcedure = sCallerProcedure;
	}
	public String gethRef() {
		return hRef;
	}
	public void sethRef(String hRef) {
		this.hRef = hRef;
	}
	public String getIDDetalleFicha() {
		return IDDetalleFicha;
	}
	public void setIDDetalleFicha(String iDDetalleFicha) {
		IDDetalleFicha = iDDetalleFicha;
	}
	public String getImporteTotalficha() {
		return importeTotalficha;
	}
	public void setImporteTotalficha(String importeTotalficha) {
		this.importeTotalficha = importeTotalficha;
	}
	public String getImportefichasacum() {
		return importefichasacum;
	}
	public void setImportefichasacum(String importefichasacum) {
		this.importefichasacum = importefichasacum;
	}
	public String getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getBTNContinuar() {
		return BTNContinuar;
	}
	public void setBTNContinuar(String bTNContinuar) {
		BTNContinuar = bTNContinuar;
	}
	public String getINEntregoCheque() {
		return INEntregoCheque;
	}
	public void setINEntregoCheque(String iNEntregoCheque) {
		INEntregoCheque = iNEntregoCheque;
	}
	public String getINIrPagina() {
		return INIrPagina;
	}
	public void setINIrPagina(String iNIrPagina) {
		INIrPagina = iNIrPagina;
	}
	public String getBTNCancelarMv() {
		return BTNCancelarMv;
	}
	public void setBTNCancelarMv(String bTNCancelarMv) {
		BTNCancelarMv = bTNCancelarMv;
	}
	public String getnOcultarBoton() {
		return nOcultarBoton;
	}
	public void setnOcultarBoton(String nOcultarBoton) {
		this.nOcultarBoton = nOcultarBoton;
	}
	public String getBTNPago() {
		return BTNPago;
	}
	public void setBTNPago(String bTNPago) {
		BTNPago = bTNPago;
	}
	public String getBTNAcumularficha() {
		return BTNAcumularficha;
	}
	public void setBTNAcumularficha(String bTNAcumularficha) {
		BTNAcumularficha = bTNAcumularficha;
	}
	public String getBTNAceptar() {
		return BTNAceptar;
	}
	public void setBTNAceptar(String bTNAceptar) {
		BTNAceptar = bTNAceptar;
	}
	public String getBTNModificar() {
		return BTNModificar;
	}
	public void setBTNModificar(String bTNModificar) {
		BTNModificar = bTNModificar;
	}
	public String getBTNBorrar() {
		return BTNBorrar;
	}
	public void setBTNBorrar(String bTNBorrar) {
		BTNBorrar = bTNBorrar;
	}
	public String getBTNCancelar() {
		return BTNCancelar;
	}
	public void setBTNCancelar(String bTNCancelar) {
		BTNCancelar = bTNCancelar;
	}
	public String getBTNlimpiar() {
		return BTNlimpiar;
	}
	public void setBTNlimpiar(String bTNlimpiar) {
		BTNlimpiar = bTNlimpiar;
	}
	public String getsType() {
		return sType;
	}
	public void setsType(String sType) {
		this.sType = sType;
	}
	public String getsMode() {
		return sMode;
	}
	public void setsMode(String sMode) {
		this.sMode = sMode;
	}
	public String getsFilter() {
		return sFilter;
	}
	public void setsFilter(String sFilter) {
		this.sFilter = sFilter;
	}
	public String getsFilterAux() {
		return sFilterAux;
	}
	public void setsFilterAux(String sFilterAux) {
		this.sFilterAux = sFilterAux;
	}
	public String getNavigate() {
		return Navigate;
	}
	public void setNavigate(String navigate) {
		Navigate = navigate;
	}
	public String getNavRowid() {
		return NavRowid;
	}
	public void setNavRowid(String navRowid) {
		NavRowid = navRowid;
	}
	public String getLinkRowid() {
		return LinkRowid;
	}
	public void setLinkRowid(String linkRowid) {
		LinkRowid = linkRowid;
	}
	public String getsWhere() {
		return sWhere;
	}
	public void setsWhere(String sWhere) {
		this.sWhere = sWhere;
	}
	public String getsWhereAux() {
		return sWhereAux;
	}
	public void setsWhereAux(String sWhereAux) {
		this.sWhereAux = sWhereAux;
	}
	public String getsPk() {
		return sPk;
	}
	public void setsPk(String sPk) {
		this.sPk = sPk;
	}
	public String getsOrder() {
		return sOrder;
	}
	public void setsOrder(String sOrder) {
		this.sOrder = sOrder;
	}
	public String getnYaSeAgregoComision() {
		return nYaSeAgregoComision;
	}
	public void setnYaSeAgregoComision(String nYaSeAgregoComision) {
		this.nYaSeAgregoComision = nYaSeAgregoComision;
	}
	public String getJsFolioPromo() {
		return jsFolioPromo;
	}
	public void setJsFolioPromo(String jsFolioPromo) {
		this.jsFolioPromo = jsFolioPromo;
	}
	public String getINcancelar() {
		return INcancelar;
	}
	public void setINcancelar(String iNcancelar) {
		INcancelar = iNcancelar;
	}
	public String getINacepta() {
		return INacepta;
	}
	public void setINacepta(String iNacepta) {
		INacepta = iNacepta;
	}
	public String getINConcepto() {
		return INConcepto;
	}
	public void setINConcepto(String iNConcepto) {
		INConcepto = iNConcepto;
	}
	public String getINAhRNDepositante() {
		return INAhRNDepositante;
	}
	public void setINAhRNDepositante(String iNAhRNDepositante) {
		INAhRNDepositante = iNAhRNDepositante;
	}
	public String getINAhRNFolio() {
		return INAhRNFolio;
	}
	public void setINAhRNFolio(String iNAhRNFolio) {
		INAhRNFolio = iNAhRNFolio;
	}
	public String getINAReferencia() {
		return INAReferencia;
	}
	public void setINAReferencia(String iNAReferencia) {
		INAReferencia = iNAReferencia;
	}
	public String getINAContrato() {
		return INAContrato;
	}
	public void setINAContrato(String iNAContrato) {
		INAContrato = iNAContrato;
	}
	public String getINACajero() {
		return INACajero;
	}
	public void setINACajero(String iNACajero) {
		INACajero = iNACajero;
	}
	public String getINAUCSCajero() {
		return INAUCSCajero;
	}
	public void setINAUCSCajero(String iNAUCSCajero) {
		INAUCSCajero = iNAUCSCajero;
	}
	public String getINANomCajero() {
		return INANomCajero;
	}
	public void setINANomCajero(String iNANomCajero) {
		INANomCajero = iNANomCajero;
	}
	public String getINAhRNComision() {
		return INAhRNComision;
	}
	public void setINAhRNComision(String iNAhRNComision) {
		INAhRNComision = iNAhRNComision;
	}
	public String getAviso() {
		return aviso;
	}
	public void setAviso(String aviso) {
		this.aviso = aviso;
	}
	public String getPregunta() {
		return pregunta;
	}
	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}
	public String getINModificar() {
		return INModificar;
	}
	public void setINModificar(String iNModificar) {
		INModificar = iNModificar;
	}
	public String getINDetalleFicha() {
		return INDetalleFicha;
	}
	public void setINDetalleFicha(String iNDetalleFicha) {
		INDetalleFicha = iNDetalleFicha;
	}
	public String getINInteresNormal() {
		return INInteresNormal;
	}
	public void setINInteresNormal(String iNInteresNormal) {
		INInteresNormal = iNInteresNormal;
	}
	public String getINInteresMora() {
		return INInteresMora;
	}
	public void setINInteresMora(String iNInteresMora) {
		INInteresMora = iNInteresMora;
	}
	public String getINDescuento() {
		return INDescuento;
	}
	public void setINDescuento(String iNDescuento) {
		INDescuento = iNDescuento;
	}
	public String getINServicioIntNormal() {
		return INServicioIntNormal;
	}
	public void setINServicioIntNormal(String iNServicioIntNormal) {
		INServicioIntNormal = iNServicioIntNormal;
	}
	public String getINAbono() {
		return INAbono;
	}
	public void setINAbono(String iNAbono) {
		INAbono = iNAbono;
	}
	public String getINBBorrar() {
		return INBBorrar;
	}
	public void setINBBorrar(String iNBBorrar) {
		INBBorrar = iNBBorrar;
	}
	public String getINServicioIntMora() {
		return INServicioIntMora;
	}
	public void setINServicioIntMora(String iNServicioIntMora) {
		INServicioIntMora = iNServicioIntMora;
	}
	public String getINBsref() {
		return INBsref;
	}
	public void setINBsref(String iNBsref) {
		INBsref = iNBsref;
	}
	public String getINBanco() {
		return INBanco;
	}
	public void setINBanco(String iNBanco) {
		INBanco = iNBanco;
	}
	public String getINCuenta() {
		return INCuenta;
	}
	public void setINCuenta(String iNCuenta) {
		INCuenta = iNCuenta;
	}
	public String getINCheque() {
		return INCheque;
	}
	public void setINCheque(String iNCheque) {
		INCheque = iNCheque;
	}
	public String getINImporte() {
		return INImporte;
	}
	public void setINImporte(String iNImporte) {
		INImporte = iNImporte;
	}
	public String getINComisionBancaria() {
		return INComisionBancaria;
	}
	public void setINComisionBancaria(String iNComisionBancaria) {
		INComisionBancaria = iNComisionBancaria;
	}
	public String getINIndemnizacion() {
		return INIndemnizacion;
	}
	public void setINIndemnizacion(String iNIndemnizacion) {
		INIndemnizacion = iNIndemnizacion;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String INsTransaccion;
    private String respuesta;
    private String INUsuarioOrigen;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INsNumeroSocio;
    private String INRefreshHead;
    private String INNuevaFicha;
    private String INDespliegaMensaje;
    private String INsFolioFicha;
    private String INsServicio;
    private String INsCajaAsignada;
    private String INsTransaccion2;
    private String INsCadenaFicha;
    private String sPageStatus;
    private String INMaintOption1;
    private String INMaintOption;
    private String INServicio;
    private String INvalidapago;
    private String INImporteTotal;
    private String INsCallType;
    private String sCallerProcedure;
    private String hRef;
    private String IDDetalleFicha;
    private String importeTotalficha;
    private String importefichasacum;
    private String importeTotal;
    private String BTNContinuar;
    private String INEntregoCheque;
    private String INIrPagina;
    private String BTNCancelarMv;
    private String nOcultarBoton;
    private String BTNPago;
    private String BTNAcumularficha;
    private String BTNAceptar;
    private String BTNModificar;
    private String BTNBorrar;
    private String BTNCancelar;
    private String BTNlimpiar;
    private String sType;
    private String sMode;
    private String sFilter;
    private String sFilterAux;
    private String Navigate;
    private String NavRowid;
    private String LinkRowid;
    private String sWhere;
    private String sWhereAux;
    private String sPk;
    private String sOrder;
    private String nYaSeAgregoComision;
    private String jsFolioPromo;
    private String INcancelar;
    private String INacepta;
    private String INConcepto;
    private String INAhRNDepositante;
    private String INAhRNFolio;
    private String INAReferencia;
    private String INAContrato;
    private String INACajero;
    private String INAUCSCajero;
    private String INANomCajero;
    private String INAhRNComision;
    private String aviso;
    private String pregunta;
    private String INModificar;
    private String INDetalleFicha;
    private String INInteresNormal;
    private String INInteresMora;
    private String INDescuento;
    private String INServicioIntNormal;
    private String INAbono;
    private String INBBorrar;
    private String INServicioIntMora;
    private String INBsref;
    private String INBanco;
    private String INCuenta;
    private String INCheque;
    private String INImporte;
    private String INComisionBancaria;
    private String INIndemnizacion;
    
	@Override
	public String toString() {
		return "EncDatDetFicha_DetalleFichaBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", INsTransaccion=" + INsTransaccion + ", respuesta=" + respuesta
				+ ", INUsuarioOrigen=" + INUsuarioOrigen + ", INsUnionSocio=" + INsUnionSocio + ", INsCajaSocio="
				+ INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio + ", INsNumeroSocio=" + INsNumeroSocio
				+ ", INRefreshHead=" + INRefreshHead + ", INNuevaFicha=" + INNuevaFicha + ", INDespliegaMensaje="
				+ INDespliegaMensaje + ", INsFolioFicha=" + INsFolioFicha + ", INsServicio=" + INsServicio
				+ ", INsCajaAsignada=" + INsCajaAsignada + ", INsTransaccion2=" + INsTransaccion2 + ", INsCadenaFicha="
				+ INsCadenaFicha + ", sPageStatus=" + sPageStatus + ", INMaintOption1=" + INMaintOption1
				+ ", INMaintOption=" + INMaintOption + ", INServicio=" + INServicio + ", INvalidapago=" + INvalidapago
				+ ", INImporteTotal=" + INImporteTotal + ", INsCallType=" + INsCallType + ", sCallerProcedure="
				+ sCallerProcedure + ", hRef=" + hRef + ", IDDetalleFicha=" + IDDetalleFicha + ", importeTotalficha="
				+ importeTotalficha + ", importefichasacum=" + importefichasacum + ", importeTotal=" + importeTotal
				+ ", BTNContinuar=" + BTNContinuar + ", INEntregoCheque=" + INEntregoCheque + ", INIrPagina="
				+ INIrPagina + ", BTNCancelarMv=" + BTNCancelarMv + ", nOcultarBoton=" + nOcultarBoton + ", BTNPago="
				+ BTNPago + ", BTNAcumularficha=" + BTNAcumularficha + ", BTNAceptar=" + BTNAceptar + ", BTNModificar="
				+ BTNModificar + ", BTNBorrar=" + BTNBorrar + ", BTNCancelar=" + BTNCancelar + ", BTNlimpiar="
				+ BTNlimpiar + ", sType=" + sType + ", sMode=" + sMode + ", sFilter=" + sFilter + ", sFilterAux="
				+ sFilterAux + ", Navigate=" + Navigate + ", NavRowid=" + NavRowid + ", LinkRowid=" + LinkRowid
				+ ", sWhere=" + sWhere + ", sWhereAux=" + sWhereAux + ", sPk=" + sPk + ", sOrder=" + sOrder
				+ ", nYaSeAgregoComision=" + nYaSeAgregoComision + ", jsFolioPromo=" + jsFolioPromo + ", INcancelar="
				+ INcancelar + ", INacepta=" + INacepta + ", INConcepto=" + INConcepto + ", INAhRNDepositante="
				+ INAhRNDepositante + ", INAhRNFolio=" + INAhRNFolio + ", INAReferencia=" + INAReferencia
				+ ", INAContrato=" + INAContrato + ", INACajero=" + INACajero + ", INAUCSCajero=" + INAUCSCajero
				+ ", INANomCajero=" + INANomCajero + ", INAhRNComision=" + INAhRNComision + ", aviso=" + aviso
				+ ", pregunta=" + pregunta + ", INModificar=" + INModificar + ", INDetalleFicha=" + INDetalleFicha
				+ ", INInteresNormal=" + INInteresNormal + ", INInteresMora=" + INInteresMora + ", INDescuento="
				+ INDescuento + ", INServicioIntNormal=" + INServicioIntNormal + ", INAbono=" + INAbono + ", INBBorrar="
				+ INBBorrar + ", INServicioIntMora=" + INServicioIntMora + ", INBsref=" + INBsref + ", INBanco="
				+ INBanco + ", INCuenta=" + INCuenta + ", INCheque=" + INCheque + ", INImporte=" + INImporte
				+ ", INComisionBancaria=" + INComisionBancaria + ", INIndemnizacion=" + INIndemnizacion + "]";
	}
    
}
