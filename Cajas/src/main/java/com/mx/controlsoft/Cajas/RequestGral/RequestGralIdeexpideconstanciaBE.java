package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestIdeexpideconstanciaBE;

public class RequestGralIdeexpideconstanciaBE {
	private RequestIdeexpideconstanciaBE request;

	public RequestIdeexpideconstanciaBE getRequest() {
		return request;
	}

	public void setRequest(RequestIdeexpideconstanciaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralIdeexpideconstanciaBE [request=" + request + "]";
	}
	
}
