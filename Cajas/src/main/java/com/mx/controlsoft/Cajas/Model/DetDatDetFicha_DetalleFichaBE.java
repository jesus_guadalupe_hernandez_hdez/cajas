package com.mx.controlsoft.Cajas.Model;

public class DetDatDetFicha_DetalleFichaBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public long getINidDet() {
		return INidDet;
	}
	public void setINidDet(long iNidDet) {
		INidDet = iNidDet;
	}
	public String getIDDetalleFicha() {
		return IDDetalleFicha;
	}
	public void setIDDetalleFicha(String iDDetalleFicha) {
		IDDetalleFicha = iDDetalleFicha;
	}
	public String getIDServicio() {
		return IDServicio;
	}
	public void setIDServicio(String iDServicio) {
		IDServicio = iDServicio;
	}
	public String getIDServicioForaneo() {
		return IDServicioForaneo;
	}
	public void setIDServicioForaneo(String iDServicioForaneo) {
		IDServicioForaneo = iDServicioForaneo;
	}
	public String getConcepto() {
		return Concepto;
	}
	public void setConcepto(String concepto) {
		Concepto = concepto;
	}
	public String getSaldoAnterior() {
		return SaldoAnterior;
	}
	public void setSaldoAnterior(String saldoAnterior) {
		SaldoAnterior = saldoAnterior;
	}
	public String getCargo() {
		return Cargo;
	}
	public void setCargo(String cargo) {
		Cargo = cargo;
	}
	public String getAbono() {
		return Abono;
	}
	public void setAbono(String abono) {
		Abono = abono;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	public String getnSaldoActual() {
		return nSaldoActual;
	}
	public void setnSaldoActual(String nSaldoActual) {
		this.nSaldoActual = nSaldoActual;
	}
	public String gethRef() {
		return hRef;
	}
	public void sethRef(String hRef) {
		this.hRef = hRef;
	}
	
	private long INid;
    private long INidDet;
    private String IDDetalleFicha;
    private String IDServicio;
    private String IDServicioForaneo;
    private String Concepto;
    private String SaldoAnterior;
    private String Cargo;
    private String Abono;
    private String Importe;
    private String nSaldoActual;
    private String hRef;
    
	@Override
	public String toString() {
		return "DetDatDetFicha_DetalleFichaBE [INid=" + INid + ", INidDet=" + INidDet + ", IDDetalleFicha="
				+ IDDetalleFicha + ", IDServicio=" + IDServicio + ", IDServicioForaneo=" + IDServicioForaneo
				+ ", Concepto=" + Concepto + ", SaldoAnterior=" + SaldoAnterior + ", Cargo=" + Cargo + ", Abono="
				+ Abono + ", Importe=" + Importe + ", nSaldoActual=" + nSaldoActual + ", hRef=" + hRef + "]";
	}
    
}
