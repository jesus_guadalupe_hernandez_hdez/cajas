package com.mx.controlsoft.Cajas.Model;

public class DatosDeberesBuscaSocioBE {
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getSaldo() {
		return Saldo;
	}
	public void setSaldo(String saldo) {
		Saldo = saldo;
	}
	
	private long id;
    private String Descripcion;
    private String Saldo;
    
	@Override
	public String toString() {
		return "DatosDeberesBuscaSocioBE [id=" + id + ", Descripcion=" + Descripcion + ", Saldo=" + Saldo + "]";
	}
 
}
