package com.mx.controlsoft.Cajas.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.mx.controlsoft.Cajas.Model.Login;

@Controller
public class AceptacheqsalvobuencoController {

	@GetMapping ("/Aceptacheque/salvoBuenCobro")
	public String getAceptacheqsalvobuencobro(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "aceptaCheque/salvoBuenCobro";
		}
		return "login";
		
	}
	
	@PostMapping ("/Aceptacheque/salvoBuenCobroFilter")
	public String getAceptacheqsalvobuencobroFilter(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "aceptaCheque/salvoBuenCobroFilter";
		}
		return "login";
		
	}
}
