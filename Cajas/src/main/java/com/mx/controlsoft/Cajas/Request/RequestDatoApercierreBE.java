package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DatoApercierreBE;

public class RequestDatoApercierreBE {
	
	private List<DatoApercierreBE> ttdato;

	public List<DatoApercierreBE> getTtdato() {
		return ttdato;
	}

	public void setTtdato(List<DatoApercierreBE> ttdato) {
		this.ttdato = ttdato;
	}

	@Override
	public String toString() {
		return "RequestDatoApercierreBE [ttdato=" + ttdato + "]";
	}
	
}
