package com.mx.controlsoft.Cajas.Model;

public class DetSabanaEFBuscasabanaBE {
	public String getIDTipoTransaccion() {
		return IDTipoTransaccion;
	}
	public void setIDTipoTransaccion(String iDTipoTransaccion) {
		IDTipoTransaccion = iDTipoTransaccion;
	}
	public String getIDFicha() {
		return IDFicha;
	}
	public void setIDFicha(String iDFicha) {
		IDFicha = iDFicha;
	}
	public String getIDEfectivo() {
		return IDEfectivo;
	}
	public void setIDEfectivo(String iDEfectivo) {
		IDEfectivo = iDEfectivo;
	}
	public String getFlujo() {
		return Flujo;
	}
	public void setFlujo(String flujo) {
		Flujo = flujo;
	}
	public String getIDTipoMoneda() {
		return IDTipoMoneda;
	}
	public void setIDTipoMoneda(String iDTipoMoneda) {
		IDTipoMoneda = iDTipoMoneda;
	}
	public String getTipoEfectivo() {
		return TipoEfectivo;
	}
	public void setTipoEfectivo(String tipoEfectivo) {
		TipoEfectivo = tipoEfectivo;
	}
	public String getCantidad() {
		return Cantidad;
	}
	public void setCantidad(String cantidad) {
		Cantidad = cantidad;
	}
	public String getDenominacion() {
		return Denominacion;
	}
	public void setDenominacion(String denominacion) {
		Denominacion = denominacion;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	private String IDTipoTransaccion;
    private String IDFicha;
    private String IDEfectivo;
    private String Flujo;
    private String IDTipoMoneda;
    private String TipoEfectivo;
    private String Cantidad;
    private String Denominacion;
    private String Importe;
	@Override
	public String toString() {
		return "DetSabanaEFBuscasabanaBE [IDTipoTransaccion=" + IDTipoTransaccion + ", IDFicha=" + IDFicha
				+ ", IDEfectivo=" + IDEfectivo + ", Flujo=" + Flujo + ", IDTipoMoneda=" + IDTipoMoneda
				+ ", TipoEfectivo=" + TipoEfectivo + ", Cantidad=" + Cantidad + ", Denominacion=" + Denominacion
				+ ", Importe=" + Importe + "]";
	}
}
