package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestFacturacionelectronicajsBE;

public class RequestGralFacturacionelectronicajsBE {
	
	private RequestFacturacionelectronicajsBE request;

	public RequestFacturacionelectronicajsBE getRequest() {
		return request;
	}

	public void setRequest(RequestFacturacionelectronicajsBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralFacturacionelectronicajsBE [request=" + request + "]";
	}
	
}
