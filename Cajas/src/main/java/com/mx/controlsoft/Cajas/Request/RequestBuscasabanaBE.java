package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncSabanaBuscasabanaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosBuscasabanaBE;

public class RequestBuscasabanaBE {
	public List<EncdatosBuscasabanaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosBuscasabanaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<EncSabanaBuscasabanaBE> getTtEncSabana() {
		return ttEncSabana;
	}
	public void setTtEncSabana(List<EncSabanaBuscasabanaBE> ttEncSabana) {
		this.ttEncSabana = ttEncSabana;
	}
	
	private List<EncdatosBuscasabanaBE> ttEncdatos;
	private List<EncSabanaBuscasabanaBE> ttEncSabana;
	
	@Override
	public String toString() {
		return "RequestBuscasabanaBE [ttEncdatos=" + ttEncdatos + ", ttEncSabana=" + ttEncSabana + "]";
	}
}
