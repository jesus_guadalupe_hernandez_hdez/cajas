package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.CorteCajaBuscacortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosBuscacortecajaBE;

public class RequestBuscacortecajaBE {
	public List<EncdatosBuscacortecajaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosBuscacortecajaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<CorteCajaBuscacortecajaBE> getTtCorteCaja() {
		return ttCorteCaja;
	}
	public void setTtCorteCaja(List<CorteCajaBuscacortecajaBE> ttCorteCaja) {
		this.ttCorteCaja = ttCorteCaja;
	}
	private List<EncdatosBuscacortecajaBE> ttEncdatos;
	private List<CorteCajaBuscacortecajaBE> ttCorteCaja;
	@Override
	public String toString() {
		return "RequestBuscacortecajaBE [ttEncdatos=" + ttEncdatos + ", ttCorteCaja=" + ttCorteCaja + "]";
	}
}
