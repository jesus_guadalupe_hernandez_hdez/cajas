package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestFichasconHorarioxSucBE;

public class RequestGralFichasconHorarioxSucBE {
	
	private RequestFichasconHorarioxSucBE request;

	public RequestFichasconHorarioxSucBE getRequest() {
		return request;
	}

	public void setRequest(RequestFichasconHorarioxSucBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralFichasconHorarioxSucBE [request=" + request + "]";
	}
}
