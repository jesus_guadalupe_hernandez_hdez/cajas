package com.mx.controlsoft.Cajas.Model;

public class CajaAsignada_FichasxServicioenSucursalBE {
	public String getIDCajaAsignada() {
		return IDCajaAsignada;
	}
	public void setIDCajaAsignada(String iDCajaAsignada) {
		IDCajaAsignada = iDCajaAsignada;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	private String IDCajaAsignada;
    private String Descripcion;
    
	@Override
	public String toString() {
		return "CajaAsignada_FichasxServicioenSucursalBE [IDCajaAsignada=" + IDCajaAsignada + ", Descripcion="
				+ Descripcion + "]";
	}
}
