package com.mx.controlsoft.Cajas.Controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.controlsoft.Cajas.Dao.DepositosDao;
import com.mx.controlsoft.Cajas.Model.EncafichasListadoficha;
import com.mx.controlsoft.Cajas.Model.EncdatosCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultasociorecomendadoconsultaBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Model.objectTem;
import com.mx.controlsoft.Cajas.Request.RequestCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.Request.RequestFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Request.RequestFichasxServicioenSucursalBE_Inicio;
import com.mx.controlsoft.Cajas.Request.RequestReporteListadoficha;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralReporteListadoficha;
import com.mx.controlsoft.Cajas.Service.ReporteFichasService;
import com.mx.controlsoft.Cajas.Service.ReporteFichasxservicioensucursalBE_Service;

@Controller
@RequestMapping(value = "/ajax_")
public class Ajax_MetodosController {
	
	private static final Logger logger = LogManager.getLogger(DepositosDao.class);
	
	@Autowired
	ReporteFichasxservicioensucursalBE_Service service;
	
	@Autowired
	ReporteFichasService service_reportes;
	
	@ResponseBody
	@RequestMapping(value = "/getFichasxservicioensucursalBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralFichasxServicioenSucursalBE mostrarPrincipal(@RequestBody fechasRecibe  fe ,HttpServletRequest request, HttpServletResponse response) {
		RequestGralFichasxServicioenSucursalBE req = new RequestGralFichasxServicioenSucursalBE();
		//Encdatos_FichasxServicioenSucursalBE encFichaServicio = new Encdatos_FichasxServicioenSucursalBE();
		Encdatos_FichasxServicioenSucursalBE  encFichaServicio = new Encdatos_FichasxServicioenSucursalBE();
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
		encFichaServicio.setiNid(1);
		encFichaServicio.setiNtoken("");
		encFichaServicio.setiNnIDUnion(logginn.getnUnion());
		encFichaServicio.setiNnIDCajaPopular(logginn.getnCaja());
		encFichaServicio.setiNnIDSucursal(logginn.getnSucursal());
		encFichaServicio.setiNsIDUsuario(logginn.getUe());
		encFichaServicio.setRespuesta("");
		encFichaServicio.setiNGenerar("Generar el Reporte");
		encFichaServicio.setiNMenu("");
		                    
		encFichaServicio.setiNCajero(fe.getCaja());
		encFichaServicio.setnIDCajeroInicio("1");
		encFichaServicio.setiNServicio(fe.getServicio());
		encFichaServicio.setiNsFechaInicial(fe.getInicia());
		encFichaServicio.setiNsFechaFinal(fe.getTermina());
		encFichaServicio.setsNombreCaja("");
		encFichaServicio.setsNombreSucursal("");
		encFichaServicio.setFecha("");
		encFichaServicio.setTermino("");
		List<Encdatos_FichasxServicioenSucursalBE> lstFichasxServicio = new LinkedList<>();
		lstFichasxServicio.add(encFichaServicio);
		RequestFichasxServicioenSucursalBE_Inicio reqFicha = new RequestFichasxServicioenSucursalBE_Inicio();
		reqFicha.setTtEncdatos(lstFichasxServicio);
		req.setRequest(reqFicha);
		RequestGralFichasxServicioenSucursalBE requestGralFichaSucursal= service.getFichasxservicioensucursalBE_ReportesInicio(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFichasconhorarioxsucBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralFichasconHorarioxSucBE mostrarFichasconHorarios(@RequestBody Encdatos_FichasconHorarioxSucBE  enc ) {
		RequestGralFichasconHorarioxSucBE req = new RequestGralFichasconHorarioxSucBE();
		
		enc.setiNid(1);
		enc.setiNtoken("");
		enc.setiNnIDUnion("1");
		enc.setiNnIDCajaPopular("8");
		enc.setiNnIDSucursal("1");
		enc.setiNsIDUsuario("ROOT");
		enc.setiNsAccion("Genera");
		enc.setsNombreCaja("");
		enc.setsRFCCaja("");
		enc.setFecha("");
		enc.setRespuesta("");
		
		List<Encdatos_FichasconHorarioxSucBE> lstFichasxHorario = new LinkedList<>();
		lstFichasxHorario.add(enc);
		RequestFichasconHorarioxSucBE reqFicha = new RequestFichasconHorarioxSucBE();
		reqFicha.setTtEncdatos(lstFichasxHorario);
		req.setRequest(reqFicha);
		RequestGralFichasconHorarioxSucBE requestGralFichaSucursal= service_reportes.getFichasconHorarioxSucBE(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getConsultasociorecomendadoconsultaBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralConsultaSocioRecomendadoConsultaBE mostrarConsultaSocioRecomendadoConsultaBE(@RequestBody fechasRecibe fe ) {
		RequestGralConsultaSocioRecomendadoConsultaBE req = new RequestGralConsultaSocioRecomendadoConsultaBE();
		
		EncdatosConsultasociorecomendadoconsultaBE  enc = new EncdatosConsultasociorecomendadoconsultaBE();
		
		System.out.println("valor fecha ini: " + enc.getINsFechaIni());
		System.out.println("valor fecha fin: " + enc.getINsFechaIni());
		
		enc.setINid(1);
		enc.setINtoken("");
		enc.setINnIDUnion("1");
		enc.setINnIDCajaPopular("8");
		enc.setINnIDSucursal("1");
		enc.setINsIDUsuario("root");
		enc.setRespuesta("");
		enc.setINsFechaIni(fe.getInicia());
		enc.setINsFechafin(fe.getTermina());
		//enc.setINsFechaIni("10/01/2020");
		//enc.setINsFechafin("29/04/2020");
		enc.setTitulo("");
		
		List<EncdatosConsultasociorecomendadoconsultaBE> lstConsultasociorecomendadoconsultaBE = new LinkedList<>();
		lstConsultasociorecomendadoconsultaBE.add(enc);
		RequestConsultaSocioRecomendadoConsultaBE reqFicha = new RequestConsultaSocioRecomendadoConsultaBE();
		reqFicha.setTtEncdatos(lstConsultasociorecomendadoconsultaBE);
		req.setRequest(reqFicha);
		RequestGralConsultaSocioRecomendadoConsultaBE requestGralFichaSucursal= service_reportes.getConsultaSocioRecomendadoConsultaBE(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDataListadoFichas",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralReporteListadoficha mostrarPrincipal(@RequestBody fechasRecibe  encFichaServicio ) {
		RequestGralReporteListadoficha req = new RequestGralReporteListadoficha();
		
		EncafichasListadoficha enc = new EncafichasListadoficha(); 
		enc.setId(1);
		enc.setnUnion("1");
		enc.setnCaja("8");
		enc.setnSucursal("1");
		enc.setFechaReporte(encFichaServicio.getInicia());
		enc.setCajaAsignadaReporte(encFichaServicio.getCaja());
		enc.setsCaja("");
		enc.setsSucursal("");
		enc.setTitulo("");
		enc.setFiltro("");
		
		List<EncafichasListadoficha> lstFichasxServicio = new LinkedList<>();
		lstFichasxServicio.add(enc);
		
		RequestReporteListadoficha reqFicha = new RequestReporteListadoficha();
		reqFicha.setTtencafichas  (lstFichasxServicio);
		req.setRequest(reqFicha);
		
		RequestGralReporteListadoficha requestGralFichaSucursal= service_reportes.getConsultaListadoficha(req) ; //(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getDataCancelaFichaCorreoPost",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralCancelaFichaCorreoConsultaBE mostrarDataCancelaFichaCorreo(@RequestBody objectTem  objecttem ) {
		
		logger.info("Valor recibido caja" + objecttem);
		
		RequestGralCancelaFichaCorreoConsultaBE req = new RequestGralCancelaFichaCorreoConsultaBE();
		EncdatosCancelaFichaCorreoConsultaBE enc = new EncdatosCancelaFichaCorreoConsultaBE(); 
		enc.setINid(1);
		enc.setINtoken("");
		enc.setINnIDUnion(objecttem.getUnion());
		enc.setINnIDCajaPopular(objecttem.getCajaPupular());
		enc.setINnIDSucursal(objecttem.getSucursal());
		enc.setINsIDUsuario("lClzdbkdcaaFaJkl");
		enc.setINnEjercisio(objecttem.getEjercisio());
		enc.setINsAccion("realizarConsulta");
		enc.setINnIDCajaAsignadaCorreo(objecttem.getCajaAsignada());
		enc.setINnIDFichacorreo("");
		enc.setINnIDTipoTransaccionCorreo("");
		enc.setRespuesta("");
		
		List<EncdatosCancelaFichaCorreoConsultaBE> lstcancelaFichaCorreo = new LinkedList<>();
		lstcancelaFichaCorreo.add(enc);
		
		RequestCancelaFichaCorreoConsultaBE reqFicha = new RequestCancelaFichaCorreoConsultaBE();
		reqFicha.setTtEncdatos(lstcancelaFichaCorreo);
		
		req.setRequest(reqFicha);
		
		RequestGralCancelaFichaCorreoConsultaBE requestGralFichaSucursal= service_reportes.getConsultaCancelaFichaCorreoConsultaBE(req) ; //(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
	}
	
}

class fechasRecibe  {
	public String getInicia() {
		return inicia;
	}
	public void setInicia(String inicia) {
		this.inicia = inicia;
	}
	public String getTermina() {
		return termina;
	}
	public void setTermina(String termina) {
		this.termina = termina;
	}
	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String inicia;
	public String termina;
	public String caja;
	public String servicio;
	public String nombreArchivo;
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
}
