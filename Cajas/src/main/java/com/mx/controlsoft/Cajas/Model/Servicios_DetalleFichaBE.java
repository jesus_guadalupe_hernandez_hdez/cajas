package com.mx.controlsoft.Cajas.Model;

public class Servicios_DetalleFichaBE {
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public String getsServicio() {
		return sServicio;
	}
	public void setsServicio(String sServicio) {
		this.sServicio = sServicio;
	}
	public String getsDescripcionServicio() {
		return sDescripcionServicio;
	}
	public void setsDescripcionServicio(String sDescripcionServicio) {
		this.sDescripcionServicio = sDescripcionServicio;
	}
	public String getsTipoServicio() {
		return sTipoServicio;
	}
	public void setsTipoServicio(String sTipoServicio) {
		this.sTipoServicio = sTipoServicio;
	}
	
	private long ID;
    private String sServicio;
    private String sDescripcionServicio;
    private String sTipoServicio;
    
	@Override
	public String toString() {
		return "Servicios_DetalleFichaBE [ID=" + ID + ", sServicio=" + sServicio + ", sDescripcionServicio="
				+ sDescripcionServicio + ", sTipoServicio=" + sTipoServicio + "]";
	}
     
}
