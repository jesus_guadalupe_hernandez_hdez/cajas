package com.mx.controlsoft.Cajas.Model;

public class CajRecepDetallefondofijoBE {
	 public long getTtNid() {
		return ttNid;
	}
	public void setTtNid(long ttNid) {
		this.ttNid = ttNid;
	}
	public String getTtCajAsig() {
		return ttCajAsig;
	}
	public void setTtCajAsig(String ttCajAsig) {
		this.ttCajAsig = ttCajAsig;
	}
	public String getTtIDUser() {
		return ttIDUser;
	}
	public void setTtIDUser(String ttIDUser) {
		this.ttIDUser = ttIDUser;
	}
	public String getTtNomUser() {
		return ttNomUser;
	}
	public void setTtNomUser(String ttNomUser) {
		this.ttNomUser = ttNomUser;
	}
	private long ttNid;
    private String ttCajAsig;
    private String ttIDUser;
    private String ttNomUser;
	@Override
	public String toString() {
		return "CajRecepDetallefondofijoBE [ttNid=" + ttNid + ", ttCajAsig=" + ttCajAsig + ", ttIDUser=" + ttIDUser
				+ ", ttNomUser=" + ttNomUser + "]";
	}
}
