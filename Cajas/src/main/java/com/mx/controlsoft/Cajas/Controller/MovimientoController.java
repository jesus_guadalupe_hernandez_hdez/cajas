package com.mx.controlsoft.Cajas.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.mx.controlsoft.Cajas.Model.Login;

@Controller
@RequestMapping(value = "/movimiento")
public class MovimientoController {

	@RequestMapping(value = "/movimientoList",method = RequestMethod.GET)
	public String mostrarPrincipal(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "movimiento/movimientoLst";
		}
		return "login";
		
	}
	
	@GetMapping ("/serviciosAyuntamiento")
	public String getServiciosAyuntamiento(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/capitalSocialAportVoluntaria")
	public String getCapitalSocialVoluntaria(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/cobroAcumulado")
	public String getCobroAcumulado(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/otrosServicios")
	public String getPtrosServicios(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/inscripcionAsamblea")
	public String getInscripcionAsamblea(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/asistenciaAsamblea")
	public String getAsistenciaAsamblea(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/prestamoPersonal")
	public String getPrestamoPersonal(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/entragaPrestamoPreautorizado")
	public String getEntregaPrestamoPreautorizado(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@GetMapping ("/autorizacionDeCheques")
	public String getAutorizacionDeCheques(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@RequestMapping(value = "/aperturaEdit",method = RequestMethod.GET)
	public String mostrarAperturaEdit(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "apertura_cierre/abrirAperturaCierre";
		}
		return "login";
	}
	
	@RequestMapping(value = "/retirosCheques",method = RequestMethod.GET)
	public String mostrarRetirosConCheque(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "movimiento/retiroCheque";
		}
		return "login";
	}
	
	@RequestMapping(value = "/transpasosCuentas",method = RequestMethod.GET)
	public String mostrarTranspasoCuentas(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "movimiento/transpasoCuentas";
		}
		return "login";
	}
	
	@RequestMapping(value = "/abonoParcialInteres",method = RequestMethod.GET)
	public String mostrarAbonoParcialInteres(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "movimiento/abonoParcialInteres";
		}
		return "login";
		
	}

}
