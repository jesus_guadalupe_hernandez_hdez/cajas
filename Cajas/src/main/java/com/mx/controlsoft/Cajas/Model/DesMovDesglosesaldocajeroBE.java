package com.mx.controlsoft.Cajas.Model;

public class DesMovDesglosesaldocajeroBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDesMovCaja() {
		return DesMovCaja;
	}
	public void setDesMovCaja(String desMovCaja) {
		DesMovCaja = desMovCaja;
	}
	public String getDesMovFichaDep() {
		return DesMovFichaDep;
	}
	public void setDesMovFichaDep(String desMovFichaDep) {
		DesMovFichaDep = desMovFichaDep;
	}
	public String getDesMovFichaRet() {
		return DesMovFichaRet;
	}
	public void setDesMovFichaRet(String desMovFichaRet) {
		DesMovFichaRet = desMovFichaRet;
	}
	public String getDesMovFondoDep() {
		return DesMovFondoDep;
	}
	public void setDesMovFondoDep(String desMovFondoDep) {
		DesMovFondoDep = desMovFondoDep;
	}
	public String getDesMovFondoRet() {
		return DesMovFondoRet;
	}
	public void setDesMovFondoRet(String desMovFondoRet) {
		DesMovFondoRet = desMovFondoRet;
	}
	public String getDesMovSumaDep() {
		return DesMovSumaDep;
	}
	public void setDesMovSumaDep(String desMovSumaDep) {
		DesMovSumaDep = desMovSumaDep;
	}
	public String getDesMovSumaRet() {
		return DesMovSumaRet;
	}
	public void setDesMovSumaRet(String desMovSumaRet) {
		DesMovSumaRet = desMovSumaRet;
	}
	public String getDesMovResConsSalIni() {
		return DesMovResConsSalIni;
	}
	public void setDesMovResConsSalIni(String desMovResConsSalIni) {
		DesMovResConsSalIni = desMovResConsSalIni;
	}
	public String getDesMovSaldCajSalIni() {
		return DesMovSaldCajSalIni;
	}
	public void setDesMovSaldCajSalIni(String desMovSaldCajSalIni) {
		DesMovSaldCajSalIni = desMovSaldCajSalIni;
	}
	public String getDesMovDifSalIni() {
		return DesMovDifSalIni;
	}
	public void setDesMovDifSalIni(String desMovDifSalIni) {
		DesMovDifSalIni = desMovDifSalIni;
	}
	public String getDesMovResConsTotIng() {
		return DesMovResConsTotIng;
	}
	public void setDesMovResConsTotIng(String desMovResConsTotIng) {
		DesMovResConsTotIng = desMovResConsTotIng;
	}
	public String getDesMovSaldCajTotIng() {
		return DesMovSaldCajTotIng;
	}
	public void setDesMovSaldCajTotIng(String desMovSaldCajTotIng) {
		DesMovSaldCajTotIng = desMovSaldCajTotIng;
	}
	public String getDesMovDifTotIng() {
		return DesMovDifTotIng;
	}
	public void setDesMovDifTotIng(String desMovDifTotIng) {
		DesMovDifTotIng = desMovDifTotIng;
	}
	public String getDesMovResConsTotEge() {
		return DesMovResConsTotEge;
	}
	public void setDesMovResConsTotEge(String desMovResConsTotEge) {
		DesMovResConsTotEge = desMovResConsTotEge;
	}
	public String getDesMovSaldCajTotEge() {
		return DesMovSaldCajTotEge;
	}
	public void setDesMovSaldCajTotEge(String desMovSaldCajTotEge) {
		DesMovSaldCajTotEge = desMovSaldCajTotEge;
	}
	public String getDesMovDifTotEge() {
		return DesMovDifTotEge;
	}
	public void setDesMovDifTotEge(String desMovDifTotEge) {
		DesMovDifTotEge = desMovDifTotEge;
	}
	public String getDesMovResConsSalAct() {
		return DesMovResConsSalAct;
	}
	public void setDesMovResConsSalAct(String desMovResConsSalAct) {
		DesMovResConsSalAct = desMovResConsSalAct;
	}
	public String getDesMovSaldCajSalAct() {
		return DesMovSaldCajSalAct;
	}
	public void setDesMovSaldCajSalAct(String desMovSaldCajSalAct) {
		DesMovSaldCajSalAct = desMovSaldCajSalAct;
	}
	public String getDesMovDifSalAct() {
		return DesMovDifSalAct;
	}
	public void setDesMovDifSalAct(String desMovDifSalAct) {
		DesMovDifSalAct = desMovDifSalAct;
	}
	public String getDesMovDesCajNumCheq() {
		return DesMovDesCajNumCheq;
	}
	public void setDesMovDesCajNumCheq(String desMovDesCajNumCheq) {
		DesMovDesCajNumCheq = desMovDesCajNumCheq;
	}
	public String getDesMovDesCajTotCheq() {
		return DesMovDesCajTotCheq;
	}
	public void setDesMovDesCajTotCheq(String desMovDesCajTotCheq) {
		DesMovDesCajTotCheq = desMovDesCajTotCheq;
	}
	public String getDesMovDesCajNumDoc() {
		return DesMovDesCajNumDoc;
	}
	public void setDesMovDesCajNumDoc(String desMovDesCajNumDoc) {
		DesMovDesCajNumDoc = desMovDesCajNumDoc;
	}
	public String getDesMovDesCajTotDoc() {
		return DesMovDesCajTotDoc;
	}
	public void setDesMovDesCajTotDoc(String desMovDesCajTotDoc) {
		DesMovDesCajTotDoc = desMovDesCajTotDoc;
	}
	public String getDesMovDesCajEfec() {
		return DesMovDesCajEfec;
	}
	public void setDesMovDesCajEfec(String desMovDesCajEfec) {
		DesMovDesCajEfec = desMovDesCajEfec;
	}
	private long id;
    private String DesMovCaja;
    private String DesMovFichaDep;
    private String DesMovFichaRet;
    private String DesMovFondoDep;
    private String DesMovFondoRet;
    private String DesMovSumaDep;
    private String DesMovSumaRet;
    private String DesMovResConsSalIni;
    private String DesMovSaldCajSalIni;
    private String DesMovDifSalIni;
    private String DesMovResConsTotIng;
    private String DesMovSaldCajTotIng;
    private String DesMovDifTotIng;
    private String DesMovResConsTotEge;
    private String DesMovSaldCajTotEge;
    private String DesMovDifTotEge;
    private String DesMovResConsSalAct;
    private String DesMovSaldCajSalAct;
    private String DesMovDifSalAct;
    private String DesMovDesCajNumCheq;
    private String DesMovDesCajTotCheq;
    private String DesMovDesCajNumDoc;
    private String DesMovDesCajTotDoc;
    private String DesMovDesCajEfec;
    
	@Override
	public String toString() {
		return "DesMovDesglosesaldocajeroBE [id=" + id + ", DesMovCaja=" + DesMovCaja + ", DesMovFichaDep="
				+ DesMovFichaDep + ", DesMovFichaRet=" + DesMovFichaRet + ", DesMovFondoDep=" + DesMovFondoDep
				+ ", DesMovFondoRet=" + DesMovFondoRet + ", DesMovSumaDep=" + DesMovSumaDep + ", DesMovSumaRet="
				+ DesMovSumaRet + ", DesMovResConsSalIni=" + DesMovResConsSalIni + ", DesMovSaldCajSalIni="
				+ DesMovSaldCajSalIni + ", DesMovDifSalIni=" + DesMovDifSalIni + ", DesMovResConsTotIng="
				+ DesMovResConsTotIng + ", DesMovSaldCajTotIng=" + DesMovSaldCajTotIng + ", DesMovDifTotIng="
				+ DesMovDifTotIng + ", DesMovResConsTotEge=" + DesMovResConsTotEge + ", DesMovSaldCajTotEge="
				+ DesMovSaldCajTotEge + ", DesMovDifTotEge=" + DesMovDifTotEge + ", DesMovResConsSalAct="
				+ DesMovResConsSalAct + ", DesMovSaldCajSalAct=" + DesMovSaldCajSalAct + ", DesMovDifSalAct="
				+ DesMovDifSalAct + ", DesMovDesCajNumCheq=" + DesMovDesCajNumCheq + ", DesMovDesCajTotCheq="
				+ DesMovDesCajTotCheq + ", DesMovDesCajNumDoc=" + DesMovDesCajNumDoc + ", DesMovDesCajTotDoc="
				+ DesMovDesCajTotDoc + ", DesMovDesCajEfec=" + DesMovDesCajEfec + "]";
	}
}
