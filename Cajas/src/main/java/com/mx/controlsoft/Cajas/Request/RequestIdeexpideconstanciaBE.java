package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncdatcompIdeexpideconstanciaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosIdeexpideconstanciaBE;
import com.mx.controlsoft.Cajas.Model.SucursalIdeexpideconstanciaBE;

public class RequestIdeexpideconstanciaBE {
	public List<EncdatosIdeexpideconstanciaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosIdeexpideconstanciaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<SucursalIdeexpideconstanciaBE> getTtSucursal() {
		return ttSucursal;
	}
	public void setTtSucursal(List<SucursalIdeexpideconstanciaBE> ttSucursal) {
		this.ttSucursal = ttSucursal;
	}
	public List<EncdatcompIdeexpideconstanciaBE> getTtEncdatcomp() {
		return ttEncdatcomp;
	}
	public void setTtEncdatcomp(List<EncdatcompIdeexpideconstanciaBE> ttEncdatcomp) {
		this.ttEncdatcomp = ttEncdatcomp;
	}
	private List<EncdatosIdeexpideconstanciaBE> ttEncdatos;
	private List<SucursalIdeexpideconstanciaBE> ttSucursal;
	private List<EncdatcompIdeexpideconstanciaBE> ttEncdatcomp;
	@Override
	public String toString() {
		return "RequestIdeexpideconstanciaBE [ttEncdatos=" + ttEncdatos + ", ttSucursal=" + ttSucursal
				+ ", ttEncdatcomp=" + ttEncdatcomp + "]";
	}
}
