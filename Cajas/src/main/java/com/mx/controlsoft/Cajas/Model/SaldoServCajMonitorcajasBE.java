package com.mx.controlsoft.Cajas.Model;

public class SaldoServCajMonitorcajasBE {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTotSalIniServ() {
		return totSalIniServ;
	}
	public void setTotSalIniServ(String totSalIniServ) {
		this.totSalIniServ = totSalIniServ;
	}
	public String getTotDepoServ() {
		return totDepoServ;
	}
	public void setTotDepoServ(String totDepoServ) {
		this.totDepoServ = totDepoServ;
	}
	public String getTotRetiServ() {
		return totRetiServ;
	}
	public void setTotRetiServ(String totRetiServ) {
		this.totRetiServ = totRetiServ;
	}
	public String getTotSaldAcuServ() {
		return totSaldAcuServ;
	}
	public void setTotSaldAcuServ(String totSaldAcuServ) {
		this.totSaldAcuServ = totSaldAcuServ;
	}
	public String getDiferencia() {
		return diferencia;
	}
	public void setDiferencia(String diferencia) {
		this.diferencia = diferencia;
	}
	
	private long id;
    private String totSalIniServ;
    private String totDepoServ;
    private String totRetiServ;
    private String totSaldAcuServ;
    private String diferencia;
    
	@Override
	public String toString() {
		return "SaldoServCajMonitorcajasBE [id=" + id + ", totSalIniServ=" + totSalIniServ + ", totDepoServ="
				+ totDepoServ + ", totRetiServ=" + totRetiServ + ", totSaldAcuServ=" + totSaldAcuServ + ", diferencia="
				+ diferencia + "]";
	}
}
