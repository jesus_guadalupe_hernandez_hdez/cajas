package com.mx.controlsoft.Cajas.Model;

public class DetFicha_FichasconHorarioxSucBE {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getiDSucursal() {
		return iDSucursal;
	}
	public void setiDSucursal(String iDSucursal) {
		this.iDSucursal = iDSucursal;
	}
	public String getiDCajaAsignada() {
		return iDCajaAsignada;
	}
	public void setiDCajaAsignada(String iDCajaAsignada) {
		this.iDCajaAsignada = iDCajaAsignada;
	}
	public String getiDTipotransaccion() {
		return iDTipotransaccion;
	}
	public void setiDTipotransaccion(String iDTipotransaccion) {
		this.iDTipotransaccion = iDTipotransaccion;
	}
	public String getiDFicha() {
		return iDFicha;
	}
	public void setiDFicha(String iDFicha) {
		this.iDFicha = iDFicha;
	}
	public String getFechaFicha() {
		return fechaFicha;
	}
	public void setFechaFicha(String fechaFicha) {
		this.fechaFicha = fechaFicha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	private long id;
    private String iDSucursal;
    private String iDCajaAsignada;
    private String iDTipotransaccion;
    private String iDFicha;
    private String fechaFicha;
    private String hora;
    
	@Override
	public String toString() {
		return "DetFicha_FichasconHorarioxSucBE [id=" + id + ", iDSucursal=" + iDSucursal + ", iDCajaAsignada="
				+ iDCajaAsignada + ", iDTipotransaccion=" + iDTipotransaccion + ", iDFicha=" + iDFicha + ", fechaFicha="
				+ fechaFicha + ", hora=" + hora + "]";
	}
}
