package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DatosGeneCuestionariolavadoconsultaBE;
import com.mx.controlsoft.Cajas.Model.EncDatosCuestionariolavadoconsultaBE;

public class RequestCuestionariolavadoconsultaBE {
	public List<EncDatosCuestionariolavadoconsultaBE> getTtEncDatos() {
		return ttEncDatos;
	}
	public void setTtEncDatos(List<EncDatosCuestionariolavadoconsultaBE> ttEncDatos) {
		this.ttEncDatos = ttEncDatos;
	}
	public List<DatosGeneCuestionariolavadoconsultaBE> getTtDatosGene() {
		return ttDatosGene;
	}
	public void setTtDatosGene(List<DatosGeneCuestionariolavadoconsultaBE> ttDatosGene) {
		this.ttDatosGene = ttDatosGene;
	}
	
	private List<EncDatosCuestionariolavadoconsultaBE> ttEncDatos;
	private List<DatosGeneCuestionariolavadoconsultaBE> ttDatosGene;
	
	@Override
	public String toString() {
		return "RequestCuestionariolavadoconsultaBE [ttEncDatos=" + ttEncDatos + ", ttDatosGene=" + ttDatosGene + "]";
	}
}
