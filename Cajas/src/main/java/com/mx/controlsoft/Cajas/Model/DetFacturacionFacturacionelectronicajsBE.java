package com.mx.controlsoft.Cajas.Model;

public class DetFacturacionFacturacionelectronicajsBE {
	public String getCancelar() {
		return Cancelar;
	}
	public void setCancelar(String cancelar) {
		Cancelar = cancelar;
	}
	public String getCancelarHREF() {
		return CancelarHREF;
	}
	public void setCancelarHREF(String cancelarHREF) {
		CancelarHREF = cancelarHREF;
	}
	public String getIDSucursal() {
		return IDSucursal;
	}
	public void setIDSucursal(String iDSucursal) {
		IDSucursal = iDSucursal;
	}
	public String getFechaFactura() {
		return FechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		FechaFactura = fechaFactura;
	}
	public String getIDCMDI() {
		return IDCMDI;
	}
	public void setIDCMDI(String iDCMDI) {
		IDCMDI = iDCMDI;
	}
	public String getIDSucursalSocio() {
		return IDSucursalSocio;
	}
	public void setIDSucursalSocio(String iDSucursalSocio) {
		IDSucursalSocio = iDSucursalSocio;
	}
	public String getIDSocio() {
		return IDSocio;
	}
	public void setIDSocio(String iDSocio) {
		IDSocio = iDSocio;
	}
	public String getRFC() {
		return RFC;
	}
	public void setRFC(String rFC) {
		RFC = rFC;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	public String getIVA() {
		return IVA;
	}
	public void setIVA(String iVA) {
		IVA = iVA;
	}
	public String getsIDCMDI() {
		return sIDCMDI;
	}
	public void setsIDCMDI(String sIDCMDI) {
		this.sIDCMDI = sIDCMDI;
	}
	public String getEstatus() {
		return Estatus;
	}
	public void setEstatus(String estatus) {
		Estatus = estatus;
	}
	public String getEstatusHREF() {
		return EstatusHREF;
	}
	public void setEstatusHREF(String estatusHREF) {
		EstatusHREF = estatusHREF;
	}
	public String getPDF() {
		return PDF;
	}
	public void setPDF(String pDF) {
		PDF = pDF;
	}
	public String getPDFHREF() {
		return PDFHREF;
	}
	public void setPDFHREF(String pDFHREF) {
		PDFHREF = pDFHREF;
	}
	public String getXML() {
		return XML;
	}
	public void setXML(String xML) {
		XML = xML;
	}
	public String getXMLHREF() {
		return XMLHREF;
	}
	public void setXMLHREF(String xMLHREF) {
		XMLHREF = xMLHREF;
	}
	
	private String Cancelar;
    private String CancelarHREF;
    private String IDSucursal;
    private String FechaFactura;
    private String IDCMDI;
    private String IDSucursalSocio;
    private String IDSocio;
    private String RFC;
    private String Importe;
    private String IVA;
    private String sIDCMDI;
    private String Estatus;
    private String EstatusHREF;
    private String PDF;
    private String PDFHREF;
    private String XML;
    private String XMLHREF;
    
	@Override
	public String toString() {
		return "DetFacturacionFacturacionelectronicajsBE [Cancelar=" + Cancelar + ", CancelarHREF=" + CancelarHREF
				+ ", IDSucursal=" + IDSucursal + ", FechaFactura=" + FechaFactura + ", IDCMDI=" + IDCMDI
				+ ", IDSucursalSocio=" + IDSucursalSocio + ", IDSocio=" + IDSocio + ", RFC=" + RFC + ", Importe="
				+ Importe + ", IVA=" + IVA + ", sIDCMDI=" + sIDCMDI + ", Estatus=" + Estatus + ", EstatusHREF="
				+ EstatusHREF + ", PDF=" + PDF + ", PDFHREF=" + PDFHREF + ", XML=" + XML + ", XMLHREF=" + XMLHREF + "]";
	}
}
