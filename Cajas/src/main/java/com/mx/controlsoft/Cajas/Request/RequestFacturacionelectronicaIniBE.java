package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncFacturaElecFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.Model.SucursalConsultaSocioRecomendadoBE;

public class RequestFacturacionelectronicaIniBE {
	public List<EncFacturaElecFacturacionelectronicaIniBE> getTt_EncFacturaElec() {
		return tt_EncFacturaElec;
	}
	public void setTt_EncFacturaElec(List<EncFacturaElecFacturacionelectronicaIniBE> tt_EncFacturaElec) {
		this.tt_EncFacturaElec = tt_EncFacturaElec;
	}
	public List<SucursalConsultaSocioRecomendadoBE> getTt_Sucursal() {
		return tt_Sucursal;
	}
	public void setTt_Sucursal(List<SucursalConsultaSocioRecomendadoBE> tt_Sucursal) {
		this.tt_Sucursal = tt_Sucursal;
	}
	private List<EncFacturaElecFacturacionelectronicaIniBE> tt_EncFacturaElec;
	private List<SucursalConsultaSocioRecomendadoBE> tt_Sucursal;
}
