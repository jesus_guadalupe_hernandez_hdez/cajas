package com.mx.controlsoft.Cajas.Model;

public class DetCajaAsignadaFichas {
	 public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getIDCajaasignada() {
		return IDCajaasignada;
	}
	public void setIDCajaasignada(String iDCajaasignada) {
		IDCajaasignada = iDCajaasignada;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	private long INid;
    private String IDCajaasignada;
    private String nombre;
    
	@Override
	public String toString() {
		return "DetCajaAsignada [INid=" + INid + ", IDCajaasignada=" + IDCajaasignada + ", nombre=" + nombre + "]";
	} 
}
