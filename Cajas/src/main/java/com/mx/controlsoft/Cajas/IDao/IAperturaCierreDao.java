package com.mx.controlsoft.Cajas.IDao;

public interface IAperturaCierreDao<T> {
	public T getDataAperturaCierreSuc(T request);
}
