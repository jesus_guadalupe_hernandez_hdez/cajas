package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.CajaAsignada_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.Detficha_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.Servicio_FichasxServicioenSucursalBE;

public class RequestFichasxServicioenSucursalBE_Inicio {

	public List<Encdatos_FichasxServicioenSucursalBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<Encdatos_FichasxServicioenSucursalBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<CajaAsignada_FichasxServicioenSucursalBE> getTtCajaAsignada() {
		return ttCajaAsignada;
	}
	public void setTtCajaAsignada(List<CajaAsignada_FichasxServicioenSucursalBE> ttCajaAsignada) {
		this.ttCajaAsignada = ttCajaAsignada;
	}
	public List<Servicio_FichasxServicioenSucursalBE> getTtServicio() {
		return ttServicio;
	}
	public void setTtServicio(List<Servicio_FichasxServicioenSucursalBE> ttServicio) {
		this.ttServicio = ttServicio;
	}
	private List<Encdatos_FichasxServicioenSucursalBE> ttEncdatos;
	private List<CajaAsignada_FichasxServicioenSucursalBE> ttCajaAsignada;
	private List<Servicio_FichasxServicioenSucursalBE> ttServicio;
	private List<Detficha_FichasxServicioenSucursalBE> ttDetficha;
	
	public List<Detficha_FichasxServicioenSucursalBE> getTtDetficha() {
		return ttDetficha;
	}
	public void setTtDetficha(List<Detficha_FichasxServicioenSucursalBE> ttDetficha) {
		this.ttDetficha = ttDetficha;
	}
	@Override
	public String toString() {
		return "RequestFichasxServicioenSucursalBE_Inicio [ttEncdatos=" + ttEncdatos + ", ttCajaAsignada="
				+ ttCajaAsignada + ", ttServicio=" + ttServicio + ", ttDetficha=" + ttDetficha + "]";
	}
	
}
