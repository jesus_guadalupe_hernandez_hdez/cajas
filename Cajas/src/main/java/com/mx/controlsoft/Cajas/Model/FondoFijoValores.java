package com.mx.controlsoft.Cajas.Model;

public class FondoFijoValores {
	public String getiNsServicio() {
		return iNsServicio;
	}
	public void setiNsServicio(String iNsServicio) {
		this.iNsServicio = iNsServicio;
	}
	public String getiNServicio() {
		return iNServicio;
	}
	public void setiNServicio(String iNServicio) {
		this.iNServicio = iNServicio;
	}
	public String getiNImporte() {
		return iNImporte;
	}
	public void setiNImporte(String iNImporte) {
		this.iNImporte = iNImporte;
	}
	public String getiNAbono() {
		return iNAbono;
	}
	public void setiNAbono(String iNAbono) {
		this.iNAbono = iNAbono;
	}
	public String getiNsImporteSubTotal() {
		return iNsImporteSubTotal;
	}
	public void setiNsImporteSubTotal(String iNsImporteSubTotal) {
		this.iNsImporteSubTotal = iNsImporteSubTotal;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public String getTipoMonedaBilleteMoneda() {
		return tipoMonedaBilleteMoneda;
	}
	public void setTipoMonedaBilleteMoneda(String tipoMonedaBilleteMoneda) {
		this.tipoMonedaBilleteMoneda = tipoMonedaBilleteMoneda;
	}
	private String iNsServicio;
	private String iNServicio;
	private String iNImporte;
	private String iNAbono;
	private String iNsImporteSubTotal;
	private String tipoMoneda;
	private String tipoMonedaBilleteMoneda;
	@Override
	public String toString() {
		return "FondoFijoValores [iNsServicio=" + iNsServicio + ", iNServicio=" + iNServicio + ", iNImporte="
				+ iNImporte + ", iNAbono=" + iNAbono + ", iNsImporteSubTotal=" + iNsImporteSubTotal + ", tipoMoneda="
				+ tipoMoneda + ", tipoMonedaBilleteMoneda=" + tipoMonedaBilleteMoneda + "]";
	}
}
