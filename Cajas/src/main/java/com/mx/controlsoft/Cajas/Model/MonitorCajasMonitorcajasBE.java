package com.mx.controlsoft.Cajas.Model;

public class MonitorCajasMonitorcajasBE {

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCajero() {
		return cajero;
	}
	public void setCajero(String cajero) {
		this.cajero = cajero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSaldoIni() {
		return saldoIni;
	}
	public void setSaldoIni(String saldoIni) {
		this.saldoIni = saldoIni;
	}
	public String getDepositos() {
		return depositos;
	}
	public void setDepositos(String depositos) {
		this.depositos = depositos;
	}
	public String getRetiros() {
		return retiros;
	}
	public void setRetiros(String retiros) {
		this.retiros = retiros;
	}
	public String getSaldoDia() {
		return saldoDia;
	}
	public void setSaldoDia(String saldoDia) {
		this.saldoDia = saldoDia;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getSaldoAcum() {
		return saldoAcum;
	}
	public void setSaldoAcum(String saldoAcum) {
		this.saldoAcum = saldoAcum;
	}
	
	private long id;
    private String nombre;
    private String cajero;
    private String tipo;
    private String saldoIni;
    private String depositos;
    private String retiros;
    private String saldoDia;
    private String Estado;
    private String saldoAcum;
    
	@Override
	public String toString() {
		return "MonitorCajasMonitorcajasBE [id=" + id + ", nombre=" + nombre + ", cajero=" + cajero + ", tipo=" + tipo
				+ ", saldoIni=" + saldoIni + ", depositos=" + depositos + ", retiros=" + retiros + ", saldoDia="
				+ saldoDia + ", Estado=" + Estado + ", saldoAcum=" + saldoAcum + "]";
	}
}
