package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DesMovDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDesglosesaldocajeroBE;

public class RequestDesglosesaldocajeroBE {
	public List<EncdatosDesglosesaldocajeroBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosDesglosesaldocajeroBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DesMovDesglosesaldocajeroBE> getTtdesMov() {
		return ttdesMov;
	}
	public void setTtdesMov(List<DesMovDesglosesaldocajeroBE> ttdesMov) {
		this.ttdesMov = ttdesMov;
	}
	private List<EncdatosDesglosesaldocajeroBE> ttEncdatos;
	private List<DesMovDesglosesaldocajeroBE> ttdesMov;
	
	@Override
	public String toString() {
		return "RequestDesglosesaldocajeroBE [ttEncdatos=" + ttEncdatos + ", ttdesMov=" + ttdesMov + "]";
	}
}
