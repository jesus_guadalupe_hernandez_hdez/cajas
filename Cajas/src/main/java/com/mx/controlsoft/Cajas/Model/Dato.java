package com.mx.controlsoft.Cajas.Model;

public class Dato {

    public String getIdUnion() {
		return idUnion;
	}
	public void setIdUnion(String idUnion) {
		this.idUnion = idUnion;
	}
	public String getIdCajaPopular() {
		return idCajaPopular;
	}
	public void setIdCajaPopular(String idCajaPopular) {
		this.idCajaPopular = idCajaPopular;
	}
	public String getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTituloSeccion() {
		return tituloSeccion;
	}
	public void setTituloSeccion(String tituloSeccion) {
		this.tituloSeccion = tituloSeccion;
	}
	public String getNombreSucursal() {
		return NombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		NombreSucursal = nombreSucursal;
	}
	public String getTitulohistorico() {
		return titulohistorico;
	}
	public void setTitulohistorico(String titulohistorico) {
		this.titulohistorico = titulohistorico;
	}
	public String getOpchrRespuesta() {
		return opchrRespuesta;
	}
	public void setOpchrRespuesta(String opchrRespuesta) {
		this.opchrRespuesta = opchrRespuesta;
	}
	
	private String  idUnion; 
	private String  idCajaPopular;
    private String  idSucursal;
    private String idUsuario;
    private String fecha;
    private String tituloSeccion;
    private String NombreSucursal;
    private String titulohistorico;
    private String opchrRespuesta;
    
	@Override
	public String toString() {
		return "Dato [idUnion=" + idUnion + ", idCajaPopular=" + idCajaPopular + ", idSucursal=" + idSucursal
				+ ", idUsuario=" + idUsuario + ", fecha=" + fecha + ", tituloSeccion=" + tituloSeccion
				+ ", NombreSucursal=" + NombreSucursal + ", titulohistorico=" + titulohistorico + ", opchrRespuesta="
				+ opchrRespuesta + "]";
	}
}
