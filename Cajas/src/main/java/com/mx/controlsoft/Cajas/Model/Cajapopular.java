package com.mx.controlsoft.Cajas.Model;

public class Cajapopular {
	
	public String getnUnion() {
		return nUnion;
	}
	public void setnUnion(String nUnion) {
		this.nUnion = nUnion;
	}
	public String getnCaja() {
		return nCaja;
	}
	public void setnCaja(String nCaja) {
		this.nCaja = nCaja;
	}
	public String getsCaja() {
		return sCaja;
	}
	public void setsCaja(String sCaja) {
		this.sCaja = sCaja;
	}
	
	@Override
	public String toString() {
		return "Cajapopular [nUnion=" + nUnion + ", nCaja=" + nCaja + ", sCaja=" + sCaja + "]";
	}

	private String nUnion;
	private String nCaja;
	private String sCaja;
	
}
