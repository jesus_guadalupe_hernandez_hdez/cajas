package com.mx.controlsoft.Cajas.Model;

public class DetFichaFondoFijoDetallefondofijoBE {
	public long getTtDetNid() {
		return ttDetNid;
	}
	public void setTtDetNid(long ttDetNid) {
		this.ttDetNid = ttDetNid;
	}
	public String getTtsIDReg() {
		return ttsIDReg;
	}
	public void setTtsIDReg(String ttsIDReg) {
		this.ttsIDReg = ttsIDReg;
	}
	public String getTtDetCajEmis() {
		return ttDetCajEmis;
	}
	public void setTtDetCajEmis(String ttDetCajEmis) {
		this.ttDetCajEmis = ttDetCajEmis;
	}
	public String getTtDetUsuario() {
		return ttDetUsuario;
	}
	public void setTtDetUsuario(String ttDetUsuario) {
		this.ttDetUsuario = ttDetUsuario;
	}
	public String getTtDetFolFicha() {
		return ttDetFolFicha;
	}
	public void setTtDetFolFicha(String ttDetFolFicha) {
		this.ttDetFolFicha = ttDetFolFicha;
	}
	public String getTtDetHora() {
		return ttDetHora;
	}
	public void setTtDetHora(String ttDetHora) {
		this.ttDetHora = ttDetHora;
	}
	public String getTtDetTransaccion() {
		return ttDetTransaccion;
	}
	public void setTtDetTransaccion(String ttDetTransaccion) {
		this.ttDetTransaccion = ttDetTransaccion;
	}
	public String getTtDetEstado() {
		return ttDetEstado;
	}
	public void setTtDetEstado(String ttDetEstado) {
		this.ttDetEstado = ttDetEstado;
	}
	private long ttDetNid;
    private String ttsIDReg;
    private String ttDetCajEmis;
    private String ttDetUsuario;
    private String ttDetFolFicha;
    private String ttDetHora;
    private String ttDetTransaccion;
    private String ttDetEstado;
	@Override
	public String toString() {
		return "DetFichaFondoFijoDetallefondofijoBE [ttDetNid=" + ttDetNid + ", ttsIDReg=" + ttsIDReg
				+ ", ttDetCajEmis=" + ttDetCajEmis + ", ttDetUsuario=" + ttDetUsuario + ", ttDetFolFicha="
				+ ttDetFolFicha + ", ttDetHora=" + ttDetHora + ", ttDetTransaccion=" + ttDetTransaccion
				+ ", ttDetEstado=" + ttDetEstado + "]";
	}
}
