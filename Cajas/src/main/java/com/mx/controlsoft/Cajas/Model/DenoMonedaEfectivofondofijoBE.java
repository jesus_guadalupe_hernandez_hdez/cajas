package com.mx.controlsoft.Cajas.Model;

public class DenoMonedaEfectivofondofijoBE {
	 public long getTtNid() {
		return ttNid;
	}
	public void setTtNid(long ttNid) {
		this.ttNid = ttNid;
	}
	public String getTtTipoMoneda() {
		return ttTipoMoneda;
	}
	public void setTtTipoMoneda(String ttTipoMoneda) {
		this.ttTipoMoneda = ttTipoMoneda;
	}
	public String getTtDescMoneda() {
		return ttDescMoneda;
	}
	public void setTtDescMoneda(String ttDescMoneda) {
		this.ttDescMoneda = ttDescMoneda;
	}
	public String getTtTipDescMon() {
		return ttTipDescMon;
	}
	public void setTtTipDescMon(String ttTipDescMon) {
		this.ttTipDescMon = ttTipDescMon;
	}
	private long ttNid;
     private String ttTipoMoneda;
     private String ttDescMoneda;
     private String ttTipDescMon;
	@Override
	public String toString() {
		return "DenoMonedaEfectivofondofijoBE [ttNid=" + ttNid + ", ttTipoMoneda=" + ttTipoMoneda + ", ttDescMoneda="
				+ ttDescMoneda + ", ttTipDescMon=" + ttTipDescMon + "]";
	}
     
}
