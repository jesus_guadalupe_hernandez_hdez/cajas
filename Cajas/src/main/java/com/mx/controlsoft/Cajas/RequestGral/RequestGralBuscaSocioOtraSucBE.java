package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestBuscasociootrasucBE;

public class RequestGralBuscaSocioOtraSucBE {
	private RequestBuscasociootrasucBE request;

	public RequestBuscasociootrasucBE getRequest() {
		return request;
	}

	public void setRequest(RequestBuscasociootrasucBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralBuscaSocioOtraSucBE [request=" + request + "]";
	}
	
}
