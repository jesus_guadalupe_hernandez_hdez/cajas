package com.mx.controlsoft.Cajas.Model;

public class Detficha_FichasxServicioenSucursalBE {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getIdCajaAsignada() {
		return idCajaAsignada;
	}
	public void setIdCajaAsignada(String idCajaAsignada) {
		this.idCajaAsignada = idCajaAsignada;
	}
	public String getIdTipoTransaccion() {
		return idTipoTransaccion;
	}
	public void setIdTipoTransaccion(String idTipoTransaccion) {
		this.idTipoTransaccion = idTipoTransaccion;
	}
	public String getiDFicha() {
		return iDFicha;
	}
	public void setiDFicha(String iDFicha) {
		this.iDFicha = iDFicha;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getSucsocio() {
		return sucsocio;
	}
	public void setSucsocio(String sucsocio) {
		this.sucsocio = sucsocio;
	}
	public String getiDSocio() {
		return iDSocio;
	}
	public void setiDSocio(String iDSocio) {
		this.iDSocio = iDSocio;
	}
	public String getNomsocio() {
		return nomsocio;
	}
	public void setNomsocio(String nomsocio) {
		this.nomsocio = nomsocio;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getAbono() {
		return abono;
	}
	public void setAbono(String abono) {
		this.abono = abono;
	}
	public String getaHref() {
		return aHref;
	}
	public void setaHref(String aHref) {
		this.aHref = aHref;
	}
	private long id;
    private String idSucursal;
    private String idCajaAsignada;
    private String idTipoTransaccion;
    private String iDFicha;
    private String usuario;
    private String fecha;
    private String hora;
    private String sucsocio;
    private String iDSocio;
    private String nomsocio;
    private String servicio;
    private String cargo;
    private String abono;
    private String aHref;
    
	@Override
	public String toString() {
		return "Detficha_FichasxServicioenSucursalBE [id=" + id + ", idSucursal=" + idSucursal + ", idCajaAsignada="
				+ idCajaAsignada + ", idTipoTransaccion=" + idTipoTransaccion + ", iDFicha=" + iDFicha + ", usuario="
				+ usuario + ", fecha=" + fecha + ", hora=" + hora + ", sucsocio=" + sucsocio + ", iDSocio=" + iDSocio
				+ ", nomsocio=" + nomsocio + ", servicio=" + servicio + ", cargo=" + cargo + ", abono=" + abono
				+ ", aHref=" + aHref + "]";
	} 
    
}
