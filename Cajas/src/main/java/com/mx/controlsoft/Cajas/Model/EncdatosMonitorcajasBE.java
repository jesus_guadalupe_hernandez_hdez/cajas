package com.mx.controlsoft.Cajas.Model;

public class EncdatosMonitorcajasBE {
	 public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getTtEncPrincCaj() {
		return ttEncPrincCaj;
	}
	public void setTtEncPrincCaj(String ttEncPrincCaj) {
		this.ttEncPrincCaj = ttEncPrincCaj;
	}
	public String getTtEncSecCaj() {
		return ttEncSecCaj;
	}
	public void setTtEncSecCaj(String ttEncSecCaj) {
		this.ttEncSecCaj = ttEncSecCaj;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getnSucursalVER() {
		return nSucursalVER;
	}
	public void setnSucursalVER(String nSucursalVER) {
		this.nSucursalVER = nSucursalVER;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String ttEncPrincCaj;
    private String ttEncSecCaj;
    private String respuesta;
    private String nSucursalVER;
    
	@Override
	public String toString() {
		return "EncdatosMonitorcajasBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", ttEncPrincCaj=" + ttEncPrincCaj + ", ttEncSecCaj=" + ttEncSecCaj + ", respuesta="
				+ respuesta + ", nSucursalVER=" + nSucursalVER + "]";
	}
}
