package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestFichasSucursal;

public class RequestGralFichasSucursal {
	private RequestFichasSucursal request;

	public RequestFichasSucursal getRequest() {
		return request;
	}

	public void setRequest(RequestFichasSucursal request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralFichasSucursal [request=" + request + "]";
	}
	
}
