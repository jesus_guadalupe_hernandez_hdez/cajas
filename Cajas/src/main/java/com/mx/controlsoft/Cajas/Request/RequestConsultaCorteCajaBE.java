package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.CorteCajaConsultacortecajaBE;
import com.mx.controlsoft.Cajas.Model.DetalleCorteCajaConsultacortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultacortecajaBE;

public class RequestConsultaCorteCajaBE {
	public List<EncdatosConsultacortecajaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosConsultacortecajaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<CorteCajaConsultacortecajaBE> getTtCorteCaja() {
		return ttCorteCaja;
	}
	public void setTtCorteCaja(List<CorteCajaConsultacortecajaBE> ttCorteCaja) {
		this.ttCorteCaja = ttCorteCaja;
	}
	public List<DetalleCorteCajaConsultacortecajaBE> getTtDetalleCorteCaja() {
		return ttDetalleCorteCaja;
	}
	public void setTtDetalleCorteCaja(List<DetalleCorteCajaConsultacortecajaBE> ttDetalleCorteCaja) {
		this.ttDetalleCorteCaja = ttDetalleCorteCaja;
	}
	private List<EncdatosConsultacortecajaBE> ttEncdatos;
	private List<CorteCajaConsultacortecajaBE> ttCorteCaja;
	private List<DetalleCorteCajaConsultacortecajaBE> ttDetalleCorteCaja;
	@Override
	public String toString() {
		return "RequestConsultaCorteCajaBE [ttEncdatos=" + ttEncdatos + ", ttCorteCaja=" + ttCorteCaja
				+ ", ttDetalleCorteCaja=" + ttDetalleCorteCaja + "]";
	}
}
