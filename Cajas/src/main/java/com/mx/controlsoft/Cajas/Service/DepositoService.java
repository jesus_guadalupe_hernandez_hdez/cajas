package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IDepositosDao;
import com.mx.controlsoft.Cajas.Request.RequestBuscasocioAceptarBE;
import com.mx.controlsoft.Cajas.Request.RequestEncdatosBuscaSocioBE_Inicio;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscaSocioOtraSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleFichaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleframedBE_One;

@Service
public class DepositoService {
	
	@Autowired
	IDepositosDao <RequestGralBuscaSocioOtraSucBE,RequestEncdatosBuscaSocioBE_Inicio,RequestGralDetalleframedBE_One,RequestGralDetalleFichaBE,RequestBuscasocioAceptarBE> req;
	
	public DepositoService(IDepositosDao <RequestGralBuscaSocioOtraSucBE,RequestEncdatosBuscaSocioBE_Inicio,RequestGralDetalleframedBE_One,RequestGralDetalleFichaBE,RequestBuscasocioAceptarBE> req) {
		this.req = req;
	}
	
	public RequestGralBuscaSocioOtraSucBE getBuscaSocioOtraSucBE (RequestGralBuscaSocioOtraSucBE request) {
		return  this.req.getBuscaSocioOtraSucBE(request) ;
	}
	
	public RequestEncdatosBuscaSocioBE_Inicio getBuscaSocioBE_Inicio (RequestEncdatosBuscaSocioBE_Inicio request) {
		return  this.req.getBuscaSocioBE_Inicio(request) ;
	}
	
	public RequestGralDetalleframedBE_One getDepositoDetalleFramedBE (RequestGralDetalleframedBE_One request) {
		return  this.req.getDepositoDetalleFramedBE(request) ;
	}
	
	public RequestGralDetalleFichaBE getDepositoDetallefichaBE_Dat (RequestGralDetalleFichaBE request) {
		return  this.req.getDepositoDetallefichaBE_Dat(request) ;
	}

	public RequestBuscasocioAceptarBE getBuscaSocioAceptarBE (RequestBuscasocioAceptarBE request) {
		return  this.req.getBuscaSocioAceptarBE(request) ;
	}
}
