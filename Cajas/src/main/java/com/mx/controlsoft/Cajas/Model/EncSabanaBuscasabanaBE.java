package com.mx.controlsoft.Cajas.Model;

import java.util.List;

public class EncSabanaBuscasabanaBE {
	public String getIDTipoTransaccion() {
		return IDTipoTransaccion;
	}
	public void setIDTipoTransaccion(String iDTipoTransaccion) {
		IDTipoTransaccion = iDTipoTransaccion;
	}
	public String getIDFicha() {
		return IDFicha;
	}
	public void setIDFicha(String iDFicha) {
		IDFicha = iDFicha;
	}
	public String getFichaCancelada() {
		return FichaCancelada;
	}
	public void setFichaCancelada(String fichaCancelada) {
		FichaCancelada = fichaCancelada;
	}
	public String getTotalF() {
		return TotalF;
	}
	public void setTotalF(String totalF) {
		TotalF = totalF;
	}
	public String getRecibido() {
		return Recibido;
	}
	public void setRecibido(String recibido) {
		Recibido = recibido;
	}
	public String getEntregado() {
		return Entregado;
	}
	public void setEntregado(String entregado) {
		Entregado = entregado;
	}
	public String getDiferencia() {
		return Diferencia;
	}
	public void setDiferencia(String diferencia) {
		Diferencia = diferencia;
	}
	public List<DetSabanaEFBuscasabanaBE> getTtDetSabanaEF() {
		return ttDetSabanaEF;
	}
	public void setTtDetSabanaEF(List<DetSabanaEFBuscasabanaBE> ttDetSabanaEF) {
		this.ttDetSabanaEF = ttDetSabanaEF;
	}
	
	private String IDTipoTransaccion;
    private String IDFicha;
    private String FichaCancelada;
    private String TotalF;
    private String Recibido;
    private String Entregado;
    private String Diferencia;
    private List<DetSabanaEFBuscasabanaBE> ttDetSabanaEF;
    
	@Override
	public String toString() {
		return "EncSabanaBuscasabanaBE [IDTipoTransaccion=" + IDTipoTransaccion + ", IDFicha=" + IDFicha
				+ ", FichaCancelada=" + FichaCancelada + ", TotalF=" + TotalF + ", Recibido=" + Recibido
				+ ", Entregado=" + Entregado + ", Diferencia=" + Diferencia + ", ttDetSabanaEF=" + ttDetSabanaEF + "]";
	}
}
