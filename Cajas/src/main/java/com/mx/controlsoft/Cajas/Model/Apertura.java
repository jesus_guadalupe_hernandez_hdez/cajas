package com.mx.controlsoft.Cajas.Model;

public class Apertura {
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServicio() {
		return Servicio;
	}

	public void setServicio(String servicio) {
		Servicio = servicio;
	}

	public String getSaldoActual() {
		return SaldoActual;
	}

	public void setSaldoActual(String saldoActual) {
		SaldoActual = saldoActual;
	}

	public String getCargo() {
		return Cargo;
	}

	public void setCargo(String cargo) {
		Cargo = cargo;
	}

	public String getAbono() {
		return Abono;
	}

	public void setAbono(String abono) {
		Abono = abono;
	}

	private long id;
	private String Servicio;
	private String SaldoActual;
	private String Cargo;
	private String Abono;
	
	
	@Override
	public String toString() {
		return "Apertura [id=" + id + ", Servicio=" + Servicio + ", SaldoActual=" + SaldoActual + ", Cargo=" + Cargo
				+ ", Abono=" + Abono + "]";
	}
}
