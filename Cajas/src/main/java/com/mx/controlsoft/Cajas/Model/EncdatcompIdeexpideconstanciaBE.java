package com.mx.controlsoft.Cajas.Model;

public class EncdatcompIdeexpideconstanciaBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursa() {
		return iNnIDSucursa;
	}
	public void setiNnIDSucursa(String iNnIDSucursa) {
		this.iNnIDSucursa = iNnIDSucursa;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getiNsMontoD() {
		return iNsMontoD;
	}
	public void setiNsMontoD(String iNsMontoD) {
		this.iNsMontoD = iNsMontoD;
	}
	public String getiNsMontoE() {
		return iNsMontoE;
	}
	public void setiNsMontoE(String iNsMontoE) {
		this.iNsMontoE = iNsMontoE;
	}
	public String getiNsMontoR() {
		return iNsMontoR;
	}
	public void setiNsMontoR(String iNsMontoR) {
		this.iNsMontoR = iNsMontoR;
	}
	public String getiNsMontoPR() {
		return iNsMontoPR;
	}
	public void setiNsMontoPR(String iNsMontoPR) {
		this.iNsMontoPR = iNsMontoPR;
	}
	public String getiNsMes() {
		return iNsMes;
	}
	public void setiNsMes(String iNsMes) {
		this.iNsMes = iNsMes;
	}
	public String getiNsSocio() {
		return iNsSocio;
	}
	public void setiNsSocio(String iNsSocio) {
		this.iNsSocio = iNsSocio;
	}
	public String getiNsSucursal() {
		return iNsSucursal;
	}
	public void setiNsSucursal(String iNsSucursal) {
		this.iNsSucursal = iNsSucursal;
	}
	public String getiNsmontoide() {
		return iNsmontoide;
	}
	public void setiNsmontoide(String iNsmontoide) {
		this.iNsmontoide = iNsmontoide;
	}
	public String getDmontoficha() {
		return dmontoficha;
	}
	public void setDmontoficha(String dmontoficha) {
		this.dmontoficha = dmontoficha;
	}
	public String getsNombre() {
		return sNombre;
	}
	public void setsNombre(String sNombre) {
		this.sNombre = sNombre;
	}
	public String getsMeses() {
		return sMeses;
	}
	public void setsMeses(String sMeses) {
		this.sMeses = sMeses;
	}
	public String getsYear() {
		return sYear;
	}
	public void setsYear(String sYear) {
		this.sYear = sYear;
	}
	public String getsRFC() {
		return sRFC;
	}
	public void setsRFC(String sRFC) {
		this.sRFC = sRFC;
	}
	public String getsCURP() {
		return sCURP;
	}
	public void setsCURP(String sCURP) {
		this.sCURP = sCURP;
	}
	public String getsPrimerApellido() {
		return sPrimerApellido;
	}
	public void setsPrimerApellido(String sPrimerApellido) {
		this.sPrimerApellido = sPrimerApellido;
	}
	public String getsSegundoApellido() {
		return sSegundoApellido;
	}
	public void setsSegundoApellido(String sSegundoApellido) {
		this.sSegundoApellido = sSegundoApellido;
	}
	public String getsDenominacion() {
		return sDenominacion;
	}
	public void setsDenominacion(String sDenominacion) {
		this.sDenominacion = sDenominacion;
	}
	public String getsRFCCaja() {
		return sRFCCaja;
	}
	public void setsRFCCaja(String sRFCCaja) {
		this.sRFCCaja = sRFCCaja;
	}
	public String getsRazonSocial() {
		return sRazonSocial;
	}
	public void setsRazonSocial(String sRazonSocial) {
		this.sRazonSocial = sRazonSocial;
	}
	public String getsMontoD() {
		return sMontoD;
	}
	public void setsMontoD(String sMontoD) {
		this.sMontoD = sMontoD;
	}
	public String getsMontoE() {
		return sMontoE;
	}
	public void setsMontoE(String sMontoE) {
		this.sMontoE = sMontoE;
	}
	public String getsMontoR() {
		return sMontoR;
	}
	public void setsMontoR(String sMontoR) {
		this.sMontoR = sMontoR;
	}
	public String getsMontoPR() {
		return sMontoPR;
	}
	public void setsMontoPR(String sMontoPR) {
		this.sMontoPR = sMontoPR;
	}
	private long iNid ;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursa;
    private String iNsIDUsuario;
    private String respuesta;
    private String iNsMontoD;
    private String iNsMontoE;
    private String iNsMontoR;
    private String iNsMontoPR;
    private String iNsMes;
    private String iNsSocio;
    private String iNsSucursal;
    private String iNsmontoide;
    private String dmontoficha;
    private String sNombre;
    private String sMeses;
    private String sYear;
    private String sRFC;
    private String sCURP;
    private String sPrimerApellido;
    private String sSegundoApellido;
    private String sDenominacion;
    private String sRFCCaja;
    private String sRazonSocial;
    private String sMontoD;
    private String sMontoE;
    private String sMontoR;
    private String sMontoPR;
	@Override
	public String toString() {
		return "EncdatcompIdeexpideconstanciaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursa=" + iNnIDSucursa + ", iNsIDUsuario="
				+ iNsIDUsuario + ", respuesta=" + respuesta + ", iNsMontoD=" + iNsMontoD + ", iNsMontoE=" + iNsMontoE
				+ ", iNsMontoR=" + iNsMontoR + ", iNsMontoPR=" + iNsMontoPR + ", iNsMes=" + iNsMes + ", iNsSocio="
				+ iNsSocio + ", iNsSucursal=" + iNsSucursal + ", iNsmontoide=" + iNsmontoide + ", dmontoficha="
				+ dmontoficha + ", sNombre=" + sNombre + ", sMeses=" + sMeses + ", sYear=" + sYear + ", sRFC=" + sRFC
				+ ", sCURP=" + sCURP + ", sPrimerApellido=" + sPrimerApellido + ", sSegundoApellido=" + sSegundoApellido
				+ ", sDenominacion=" + sDenominacion + ", sRFCCaja=" + sRFCCaja + ", sRazonSocial=" + sRazonSocial
				+ ", sMontoD=" + sMontoD + ", sMontoE=" + sMontoE + ", sMontoR=" + sMontoR + ", sMontoPR=" + sMontoPR
				+ "]";
	}
}
