package com.mx.controlsoft.Cajas.Model;

public class Union {
	
	public String getnUnion() {
		return nUnion;
	}
	public void setnUnion(String nUnion) {
		this.nUnion = nUnion;
	}
	public String getsUnion() {
		return sUnion;
	}
	public void setsUnion(String sUnion) {
		this.sUnion = sUnion;
	}
	
	private String nUnion;
	private String sUnion;
	
	@Override
	public String toString() {
		return "Union [nUnion=" + nUnion + ", sUnion=" + sUnion + "]";
	}

}
