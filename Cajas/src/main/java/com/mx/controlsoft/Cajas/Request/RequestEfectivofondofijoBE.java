package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DenoMonedaEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.DetEfecEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.TotDetEfecEfectivofondofijoBE;

public class RequestEfectivofondofijoBE {
	
	private List<EncdatosEfectivofondofijoBE> ttEncdatos;
	private List<DenoMonedaEfectivofondofijoBE> ttDenoMoneda;
	private List<TotDetEfecEfectivofondofijoBE> ttTotDetEfec;
	private List<DetEfecEfectivofondofijoBE> ttDetEfec;
	
	public List<DetEfecEfectivofondofijoBE> getTtDetEfec() {
		return ttDetEfec;
	}
	public void setTtDetEfec(List<DetEfecEfectivofondofijoBE> ttDetEfec) {
		this.ttDetEfec = ttDetEfec;
	}
	public List<EncdatosEfectivofondofijoBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosEfectivofondofijoBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DenoMonedaEfectivofondofijoBE> getTtDenoMoneda() {
		return ttDenoMoneda;
	}
	public void setTtDenoMoneda(List<DenoMonedaEfectivofondofijoBE> ttDenoMoneda) {
		this.ttDenoMoneda = ttDenoMoneda;
	}
	public List<TotDetEfecEfectivofondofijoBE> getTtTotDetEfec() {
		return ttTotDetEfec;
	}
	public void setTtTotDetEfec(List<TotDetEfecEfectivofondofijoBE> ttTotDetEfec) {
		this.ttTotDetEfec = ttTotDetEfec;
	}
	
	@Override
	public String toString() {
		return "RequestEfectivofondofijoBE [ttEncdatos=" + ttEncdatos + ", ttDenoMoneda=" + ttDenoMoneda
				+ ", ttTotDetEfec=" + ttTotDetEfec + ", ttDetEfec=" + ttDetEfec + "]";
	}
}
