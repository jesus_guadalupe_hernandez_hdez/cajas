package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.IConsultaCorteCajaDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscacortecajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralChequescortecajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaCorteCajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDocumentoscortecajaBE;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class ConsultaCorteCaja implements IConsultaCorteCajaDao <RequestGralDocumentoscortecajaBE,RequestGralChequescortecajaBE,RequestGralConsultaCorteCajaBE,RequestGralBuscacortecajaBE> {
	
	private static final Logger logger = LogManager.getLogger(ConsultaCorteCaja.class);
	
	public RequestGralDocumentoscortecajaBE getDocumentoscortecajaBE(RequestGralDocumentoscortecajaBE request) {
		try {
			RequestGralDocumentoscortecajaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/documentoscortecajaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralDocumentoscortecajaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	public RequestGralChequescortecajaBE getChequesCorteCajaBE(RequestGralChequescortecajaBE request) {
		try {
			RequestGralChequescortecajaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/chequescortecajaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralChequescortecajaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	public RequestGralConsultaCorteCajaBE getConsultacortecajaBE(RequestGralConsultaCorteCajaBE request) {
		try {
			RequestGralConsultaCorteCajaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/consultacortecajaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralConsultaCorteCajaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	} 
	public RequestGralBuscacortecajaBE getBuscacortecajaBE(RequestGralBuscacortecajaBE request) {
		try {
			RequestGralBuscacortecajaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/buscacortecajaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralBuscacortecajaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
}
