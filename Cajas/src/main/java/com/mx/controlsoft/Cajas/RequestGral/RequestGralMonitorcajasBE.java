package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestMonitorcajasBE;

public class RequestGralMonitorcajasBE {
	private RequestMonitorcajasBE request;

	public RequestMonitorcajasBE getRequest() {
		return request;
	}

	public void setRequest(RequestMonitorcajasBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralMonitorcajasBE [request=" + request + "]";
	}
	
}
