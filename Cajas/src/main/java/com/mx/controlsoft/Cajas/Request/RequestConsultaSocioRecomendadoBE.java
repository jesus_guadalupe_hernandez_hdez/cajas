package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncdatosConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.Model.SucursalConsultaSocioRecomendadoBE;

public class RequestConsultaSocioRecomendadoBE {
	public List<EncdatosConsultaSocioRecomendadoBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosConsultaSocioRecomendadoBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<SucursalConsultaSocioRecomendadoBE> getTtSucursal() {
		return ttSucursal;
	}
	public void setTtSucursal(List<SucursalConsultaSocioRecomendadoBE> ttSucursal) {
		this.ttSucursal = ttSucursal;
	}
	
	private List<EncdatosConsultaSocioRecomendadoBE> ttEncdatos;
	private List<SucursalConsultaSocioRecomendadoBE> ttSucursal;
	
	@Override
	public String toString() {
		return "RequestConsultaSocioRecomendadoBE [ttEncdatos=" + ttEncdatos + ", ttSucursal=" + ttSucursal + "]";
	}
}
