package com.mx.controlsoft.Cajas.Model;

public class objectTem {
	public String getUnion() {
		return union;
	}
	public void setUnion(String union) {
		this.union = union;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getCajaAsignada() {
		return cajaAsignada;
	}
	public void setCajaAsignada(String cajaAsignada) {
		this.cajaAsignada = cajaAsignada;
	}
	public String getFichaCorreo() {
		return fichaCorreo;
	}
	public void setFichaCorreo(String fichaCorreo) {
		this.fichaCorreo = fichaCorreo;
	}
	public String getCajaPupular() {
		return cajaPupular;
	}
	public void setCajaPupular(String cajaPupular) {
		this.cajaPupular = cajaPupular;
	}
	public String getEjercisio() {
		return ejercisio;
	}
	public void setEjercisio(String ejercisio) {
		this.ejercisio = ejercisio;
	}
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}
	private String union;
	private String sucursal;
	private String cajaAsignada;
	private String fichaCorreo;
	private String cajaPupular;
	private String ejercisio;
	private String tipoTransaccion ;
	@Override
	public String toString() {
		return "objectTem [union=" + union + ", sucursal=" + sucursal + ", cajaAsignada=" + cajaAsignada
				+ ", fichaCorreo=" + fichaCorreo + ", cajaPupular=" + cajaPupular + ", ejercisio=" + ejercisio
				+ ", tipoTransaccion=" + tipoTransaccion + "]";
	}
	
	
}
