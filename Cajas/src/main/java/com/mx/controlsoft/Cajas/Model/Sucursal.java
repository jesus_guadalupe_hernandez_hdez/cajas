package com.mx.controlsoft.Cajas.Model;

public class Sucursal {
	public String getnUnion() {
		return nUnion;
	}
	public void setnUnion(String nUnion) {
		this.nUnion = nUnion;
	}
	public String getnCaja() {
		return nCaja;
	}
	public void setnCaja(String nCaja) {
		this.nCaja = nCaja;
	}
	public String getnSucursal() {
		return nSucursal;
	}
	public void setnSucursal(String nSucursal) {
		this.nSucursal = nSucursal;
	}
	public String getsSucursal() {
		return sSucursal;
	}
	public void setsSucursal(String sSucursal) {
		this.sSucursal = sSucursal;
	}
	private String nUnion;
	private String nCaja;
	private String nSucursal;
	private String sSucursal;
	
	@Override
	public String toString() {
		return "Sucursal [nUnion=" + nUnion + ", nCaja=" + nCaja + ", nSucursal=" + nSucursal + ", sSucursal="
				+ sSucursal + "]";
	}

}
