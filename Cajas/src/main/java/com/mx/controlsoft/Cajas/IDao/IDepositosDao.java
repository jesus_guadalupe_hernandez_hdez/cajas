package com.mx.controlsoft.Cajas.IDao;

public interface IDepositosDao<T,U,V,O, X> {
	
	public T getBuscaSocioOtraSucBE(T request);
	
	public U getBuscaSocioBE_Inicio(U request);
	
	public V getDepositoDetalleFramedBE (V request); 
	
	public O getDepositoDetallefichaBE_Dat (O request);
	
	public X  getBuscaSocioAceptarBE (X request);
}
