package com.mx.controlsoft.Cajas.Model;

import java.util.List;

public class ListaFondoFijo {
	public List<FondoFijoValores> getListaFijo() {
		return listaFijo;
	}
	public void setListaFijo(List<FondoFijoValores> listaFijo) {
		this.listaFijo = listaFijo;
	}
	public FondoFijoTemporales getTemVal() {
		return temVal;
	}
	public void setTemVal(FondoFijoTemporales temVal) {
		this.temVal = temVal;
	}
	
	private List<FondoFijoValores> listaFijo;
	private FondoFijoTemporales temVal;
	
	@Override
	public String toString() {
		return "ListaFondoFijo [listaFijo=" + listaFijo + ", temVal=" + temVal + "]";
	}
}
