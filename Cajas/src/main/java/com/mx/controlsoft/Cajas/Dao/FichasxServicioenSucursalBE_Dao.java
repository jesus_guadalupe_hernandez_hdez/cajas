package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.IFichasxServicioenSucursalBE_Dao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class FichasxServicioenSucursalBE_Dao implements IFichasxServicioenSucursalBE_Dao<RequestGralFichasxServicioenSucursalBE> {
	
	private static final Logger logger = LogManager.getLogger(FichasxServicioenSucursalBE_Dao.class);
	
	public RequestGralFichasxServicioenSucursalBE getFichasxservicioensucursalBE_ReportesInicio (RequestGralFichasxServicioenSucursalBE request) {
		try {
			RequestGralFichasxServicioenSucursalBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/fichasxservicioensucursalBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralFichasxServicioenSucursalBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }

	}

	
}
