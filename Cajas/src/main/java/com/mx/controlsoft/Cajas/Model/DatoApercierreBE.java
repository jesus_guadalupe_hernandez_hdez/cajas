package com.mx.controlsoft.Cajas.Model;

public class DatoApercierreBE {
	public long getIdttdato() {
		return idttdato;
	}
	public void setIdttdato(long idttdato) {
		this.idttdato = idttdato;
	}
	public String getIDUnion() {
		return IDUnion;
	}
	public void setIDUnion(String iDUnion) {
		IDUnion = iDUnion;
	}
	public String getIDCajaPopular() {
		return IDCajaPopular;
	}
	public void setIDCajaPopular(String iDCajaPopular) {
		IDCajaPopular = iDCajaPopular;
	}
	public String getIDSucursal() {
		return IDSucursal;
	}
	public void setIDSucursal(String iDSucursal) {
		IDSucursal = iDSucursal;
	}
	public String getIDUsuario() {
		return IDUsuario;
	}
	public void setIDUsuario(String iDUsuario) {
		IDUsuario = iDUsuario;
	}
	public String getTipoEventoAPE_CIE() {
		return TipoEventoAPE_CIE;
	}
	public void setTipoEventoAPE_CIE(String tipoEventoAPE_CIE) {
		TipoEventoAPE_CIE = tipoEventoAPE_CIE;
	}
	public String getOpchrRespuesta() {
		return opchrRespuesta;
	}
	public void setOpchrRespuesta(String opchrRespuesta) {
		this.opchrRespuesta = opchrRespuesta;
	}
	
	private long idttdato;
    private String IDUnion;
    private String IDCajaPopular;
    private String IDSucursal;
    private String IDUsuario;
    private String TipoEventoAPE_CIE;
    private String opchrRespuesta;
    
	@Override
	public String toString() {
		return "DatoApercierreBE [idttdato=" + idttdato + ", IDUnion=" + IDUnion + ", IDCajaPopular=" + IDCajaPopular
				+ ", IDSucursal=" + IDSucursal + ", IDUsuario=" + IDUsuario + ", TipoEventoAPE_CIE=" + TipoEventoAPE_CIE
				+ ", opchrRespuesta=" + opchrRespuesta + "]";
	}
    
}
