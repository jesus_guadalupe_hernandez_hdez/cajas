package com.mx.controlsoft.Cajas.Model;

public class EncdatosIdeexpideconstanciaBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursa() {
		return iNnIDSucursa;
	}
	public void setiNnIDSucursa(String iNnIDSucursa) {
		this.iNnIDSucursa = iNnIDSucursa;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getiNPUnion1() {
		return iNPUnion1;
	}
	public void setiNPUnion1(String iNPUnion1) {
		this.iNPUnion1 = iNPUnion1;
	}
	public String getiNPCaja1() {
		return iNPCaja1;
	}
	public void setiNPCaja1(String iNPCaja1) {
		this.iNPCaja1 = iNPCaja1;
	}
	public String getiNPSucursal1() {
		return iNPSucursal1;
	}
	public void setiNPSucursal1(String iNPSucursal1) {
		this.iNPSucursal1 = iNPSucursal1;
	}
	public String getiNPFechaInicial1() {
		return iNPFechaInicial1;
	}
	public void setiNPFechaInicial1(String iNPFechaInicial1) {
		this.iNPFechaInicial1 = iNPFechaInicial1;
	}
	public String getiNPFechaFinal1() {
		return iNPFechaFinal1;
	}
	public void setiNPFechaFinal1(String iNPFechaFinal1) {
		this.iNPFechaFinal1 = iNPFechaFinal1;
	}
	public String getiNPOperadorInicial1() {
		return iNPOperadorInicial1;
	}
	public void setiNPOperadorInicial1(String iNPOperadorInicial1) {
		this.iNPOperadorInicial1 = iNPOperadorInicial1;
	}
	public String getiNPOperadorFinal1() {
		return iNPOperadorFinal1;
	}
	public void setiNPOperadorFinal1(String iNPOperadorFinal1) {
		this.iNPOperadorFinal1 = iNPOperadorFinal1;
	}
	public String getiNPNumSocio() {
		return iNPNumSocio;
	}
	public void setiNPNumSocio(String iNPNumSocio) {
		this.iNPNumSocio = iNPNumSocio;
	}
	public String getiNListaSucursal() {
		return iNListaSucursal;
	}
	public void setiNListaSucursal(String iNListaSucursal) {
		this.iNListaSucursal = iNListaSucursal;
	}
	public String getiNGeneraReporte() {
		return iNGeneraReporte;
	}
	public void setiNGeneraReporte(String iNGeneraReporte) {
		this.iNGeneraReporte = iNGeneraReporte;
	}
	public String getiNOperadorInicial() {
		return iNOperadorInicial;
	}
	public void setiNOperadorInicial(String iNOperadorInicial) {
		this.iNOperadorInicial = iNOperadorInicial;
	}
	public String getiNlmes() {
		return iNlmes;
	}
	public void setiNlmes(String iNlmes) {
		this.iNlmes = iNlmes;
	}
	public String getiNNumSocio() {
		return iNNumSocio;
	}
	public void setiNNumSocio(String iNNumSocio) {
		this.iNNumSocio = iNNumSocio;
	}
	public String getiNResultado() {
		return iNResultado;
	}
	public void setiNResultado(String iNResultado) {
		this.iNResultado = iNResultado;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursa;
    private String iNsIDUsuario;
    private String respuesta;
    private String iNPUnion1;
    private String iNPCaja1;
    private String iNPSucursal1;
    private String iNPFechaInicial1;
    private String iNPFechaFinal1;
    private String iNPOperadorInicial1;
    private String iNPOperadorFinal1;
    private String iNPNumSocio;
    private String iNListaSucursal;
    private String iNGeneraReporte;
    private String iNOperadorInicial;
    private String iNlmes;
    private String iNNumSocio;
    private String iNResultado;
    private String titulo;
	@Override
	public String toString() {
		return "EncdatosIdeexpideconstanciaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursa=" + iNnIDSucursa + ", iNsIDUsuario="
				+ iNsIDUsuario + ", respuesta=" + respuesta + ", iNPUnion1=" + iNPUnion1 + ", iNPCaja1=" + iNPCaja1
				+ ", iNPSucursal1=" + iNPSucursal1 + ", iNPFechaInicial1=" + iNPFechaInicial1 + ", iNPFechaFinal1="
				+ iNPFechaFinal1 + ", iNPOperadorInicial1=" + iNPOperadorInicial1 + ", iNPOperadorFinal1="
				+ iNPOperadorFinal1 + ", iNPNumSocio=" + iNPNumSocio + ", iNListaSucursal=" + iNListaSucursal
				+ ", iNGeneraReporte=" + iNGeneraReporte + ", iNOperadorInicial=" + iNOperadorInicial + ", iNlmes="
				+ iNlmes + ", iNNumSocio=" + iNNumSocio + ", iNResultado=" + iNResultado + ", titulo=" + titulo + "]";
	}
}
