package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IIdeexpideconstanciaBEDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralIdeexpideconstanciaBE;

@Service
public class IdeexpideconstanciaBEService {
	
	@Autowired
	private IIdeexpideconstanciaBEDao<RequestGralIdeexpideconstanciaBE> rep;
	
	public RequestGralIdeexpideconstanciaBE getDataIdeexpideconstanciaBE (RequestGralIdeexpideconstanciaBE request) {
		return rep.getDataIdeexpideconstanciaBE(request);
	}
}
