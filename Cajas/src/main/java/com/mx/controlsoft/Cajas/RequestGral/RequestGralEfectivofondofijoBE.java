package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestEfectivofondofijoBE;

public class RequestGralEfectivofondofijoBE {
	private RequestEfectivofondofijoBE request;

	public RequestEfectivofondofijoBE getRequest() {
		return request;
	}

	public void setRequest(RequestEfectivofondofijoBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralEfectivofondofijoBE [request=" + request + "]";
	}
	
}
