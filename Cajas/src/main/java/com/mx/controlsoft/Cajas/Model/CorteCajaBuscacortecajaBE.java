package com.mx.controlsoft.Cajas.Model;

public class CorteCajaBuscacortecajaBE {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getiDUnion() {
		return iDUnion;
	}
	public void setiDUnion(String iDUnion) {
		this.iDUnion = iDUnion;
	}
	public String getiDCajaPopular() {
		return iDCajaPopular;
	}
	public void setiDCajaPopular(String iDCajaPopular) {
		this.iDCajaPopular = iDCajaPopular;
	}
	public String getiDSucursal() {
		return iDSucursal;
	}
	public void setiDSucursal(String iDSucursal) {
		this.iDSucursal = iDSucursal;
	}
	public String getiDCajaAsignada() {
		return iDCajaAsignada;
	}
	public void setiDCajaAsignada(String iDCajaAsignada) {
		this.iDCajaAsignada = iDCajaAsignada;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String gethRef() {
		return hRef;
	}
	public void sethRef(String hRef) {
		this.hRef = hRef;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	private long id;
    private String iDUnion;
    private String iDCajaPopular;
    private String iDSucursal;
    private String iDCajaAsignada;
    private String fecha;
    private String hRef;
    private String nombre;
    private String monto;
    private String resultado;
    
	@Override
	public String toString() {
		return "CorteCajaBuscacortecajaBE [id=" + id + ", iDUnion=" + iDUnion + ", iDCajaPopular=" + iDCajaPopular
				+ ", iDSucursal=" + iDSucursal + ", iDCajaAsignada=" + iDCajaAsignada + ", fecha=" + fecha + ", hRef="
				+ hRef + ", nombre=" + nombre + ", monto=" + monto + ", resultado=" + resultado + "]";
	}
}
