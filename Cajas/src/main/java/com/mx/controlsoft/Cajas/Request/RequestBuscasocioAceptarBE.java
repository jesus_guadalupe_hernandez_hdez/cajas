package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncdatosBuscaSocioAceptarBE;

public class RequestBuscasocioAceptarBE {
	
	private List< EncdatosBuscaSocioAceptarBE> ttEncdatos;

	public List<EncdatosBuscaSocioAceptarBE> getTtEncdatos() {
		return ttEncdatos;
	}

	public void setTtEncdatos(List<EncdatosBuscaSocioAceptarBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}

	@Override
	public String toString() {
		return "RequestBuscasocioAceptarBE [ttEncdatos=" + ttEncdatos + "]";
	}
	
}
