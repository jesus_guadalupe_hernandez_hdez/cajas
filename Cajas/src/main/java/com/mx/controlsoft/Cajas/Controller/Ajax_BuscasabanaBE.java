package com.mx.controlsoft.Cajas.Controller;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.controlsoft.Cajas.Model.EncdatosBuscasabanaBE;
import com.mx.controlsoft.Cajas.Request.RequestBuscasabanaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscasabanaBE;
import com.mx.controlsoft.Cajas.Service.BuscasabanaBEService;


@Controller
@RequestMapping(value = "/ajax_BuscaSabana")
public class Ajax_BuscasabanaBE {
	
	//private static final Logger logger = LogManager.getLogger(Ajax_BuscasabanaBE.class);
	
	@Autowired
	BuscasabanaBEService service;
	
	@ResponseBody
	@RequestMapping(value = "/getBuscaSabanasBEPost",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralBuscasabanaBE mostrarPrincipal(@RequestBody BuscaSab  tem ) {
		RequestGralBuscasabanaBE req = new RequestGralBuscasabanaBE();
		EncdatosBuscasabanaBE enc = new EncdatosBuscasabanaBE();
		enc.setiNid(1);
		enc.setiNtoken("");
		enc.setiNnIDUnion(tem.getiNnIDUnion());
		enc.setiNnIDCajaPopular(tem.getiNnIDCajaPopular());
		enc.setiNnIDSucursal(tem.getiNnIDSucursal());
		enc.setiNsIDUsuario("lClzdbkdcaaFaJkl");
		enc.setiNmensaje("");
		enc.setiNsTransaccion("");
		enc.setiNsUnionSocio(tem.getiNsUnionSocio());
		enc.setiNsCajaSocio(tem.getiNsCajaSocio());
		enc.setiNsSucursalSocio(tem.getiNsSucursalSocio());
		enc.setiNsNumeroSocio("");
		enc.setiNIDCaja(tem.getiNIDCaja());
		enc.setiNsFecha(tem.getiNsFecha());
		enc.setiNsFichaInicial("");
		enc.setiNsOperadorInicial("");
		enc.setiNsFichaFinal("");
		enc.setiNsOperadorFinal("");
		enc.setOUTsTitulo("");
		enc.setOUTsNombreCaja("");
		enc.setOUTsFecha("");
		enc.setRespuesta("");
		
		List<EncdatosBuscasabanaBE> lstBuscaSabanas = new LinkedList<>();
		lstBuscaSabanas.add(enc);
		RequestBuscasabanaBE reqSabana = new RequestBuscasabanaBE();
		reqSabana.setTtEncdatos(lstBuscaSabanas);
		req.setRequest(reqSabana);
		RequestGralBuscasabanaBE requestGralSabana= service.getBuscasabanaBE(req) ;  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralSabana;
	}
}

class BuscaSab {
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursal() {
		return iNnIDSucursal;
	}
	public void setiNnIDSucursal(String iNnIDSucursal) {
		this.iNnIDSucursal = iNnIDSucursal;
	}
	public String getiNsUnionSocio() {
		return iNsUnionSocio;
	}
	public void setiNsUnionSocio(String iNsUnionSocio) {
		this.iNsUnionSocio = iNsUnionSocio;
	}
	public String getiNsCajaSocio() {
		return iNsCajaSocio;
	}
	public void setiNsCajaSocio(String iNsCajaSocio) {
		this.iNsCajaSocio = iNsCajaSocio;
	}
	public String getiNsSucursalSocio() {
		return iNsSucursalSocio;
	}
	public void setiNsSucursalSocio(String iNsSucursalSocio) {
		this.iNsSucursalSocio = iNsSucursalSocio;
	}
	public String getiNIDCaja() {
		return iNIDCaja;
	}
	public void setiNIDCaja(String iNIDCaja) {
		this.iNIDCaja = iNIDCaja;
	}
	public String getiNsFecha() {
		return iNsFecha;
	}
	public void setiNsFecha(String iNsFecha) {
		this.iNsFecha = iNsFecha;
	}
	private String iNnIDUnion;
	private String iNnIDCajaPopular;
	private String iNnIDSucursal;
	private String iNsUnionSocio;
	private String iNsCajaSocio;
	private String iNsSucursalSocio;
	private String iNIDCaja;
	private String iNsFecha;
	@Override
	public String toString() {
		return "BuscaSab [iNnIDUnion=" + iNnIDUnion + ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursal="
				+ iNnIDSucursal + ", iNsUnionSocio=" + iNsUnionSocio + ", iNsCajaSocio=" + iNsCajaSocio
				+ ", iNsSucursalSocio=" + iNsSucursalSocio + ", iNIDCaja=" + iNIDCaja + ", iNsFecha=" + iNsFecha + "]";
	}
}
