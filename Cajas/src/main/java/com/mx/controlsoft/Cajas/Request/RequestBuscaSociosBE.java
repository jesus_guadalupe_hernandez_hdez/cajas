package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DatosDeberesBuscaSocioBE;
import com.mx.controlsoft.Cajas.Model.DatosHaberesBuscaSocioBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_BuscaSocioBE;
import com.mx.controlsoft.Cajas.Model.PrestamosBuscaSocioBE;

public class RequestBuscaSociosBE {
	
	public List<Encdatos_BuscaSocioBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<Encdatos_BuscaSocioBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DatosHaberesBuscaSocioBE> getTtdatoshaberes() {
		return ttdatoshaberes;
	}
	public void setTtdatoshaberes(List<DatosHaberesBuscaSocioBE> ttdatoshaberes) {
		this.ttdatoshaberes = ttdatoshaberes;
	}
	public List<DatosDeberesBuscaSocioBE> getTtdatosdeberes() {
		return ttdatosdeberes;
	}
	public void setTtdatosdeberes(List<DatosDeberesBuscaSocioBE> ttdatosdeberes) {
		this.ttdatosdeberes = ttdatosdeberes;
	}
	public List<PrestamosBuscaSocioBE> getTtprestamos() {
		return ttprestamos;
	}
	public void setTtprestamos(List<PrestamosBuscaSocioBE> ttprestamos) {
		this.ttprestamos = ttprestamos;
	}
	
	private List<Encdatos_BuscaSocioBE> ttEncdatos;
	private List<DatosHaberesBuscaSocioBE> ttdatoshaberes;
	private List<DatosDeberesBuscaSocioBE> ttdatosdeberes;
	private List<PrestamosBuscaSocioBE> ttprestamos;
	
	@Override
	public String toString() {
		return "RequestBuscaSociosBE [ttEncdatos=" + ttEncdatos + ", ttdatoshaberes=" + ttdatoshaberes
				+ ", ttdatosdeberes=" + ttdatosdeberes + ", ttprestamos=" + ttprestamos + "]";
	}

}
