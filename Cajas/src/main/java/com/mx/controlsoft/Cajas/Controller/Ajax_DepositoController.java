package com.mx.controlsoft.Cajas.Controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.controlsoft.Cajas.Dao.DepositosDao;
import com.mx.controlsoft.Cajas.Model.EncDatDetFicha_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosBuscaSocioAceptarBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestBuscasocioAceptarBE;
import com.mx.controlsoft.Cajas.Request.RequestDetalleframedBE_One;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleFichaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleframedBE_One;
import com.mx.controlsoft.Cajas.Service.DepositoService;

@Controller
@RequestMapping(value = "/ajax_Deposito")
public class Ajax_DepositoController {
	
	@Autowired 
	private DepositoService depServce;
	
	private static final Logger logger = LogManager.getLogger(DepositosDao.class);
	
	/*
	@ResponseBody
	@RequestMapping(value = "/getBuscaSocioAceptarBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralDetalleframedBE_One getGralDetalleFichaBe(EncDatDetFicha_DetalleFichaBE encDetalle, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			Encdatos_DetalleframedBE encDetalleFrame = new Encdatos_DetalleframedBE();
			encDetalleFrame.setINid(1);
			encDetalleFrame.setINtoken("");
			encDetalleFrame.setINnIDUnion(logginn.getnUnion());
			encDetalleFrame.setINnIDCajaPopular(logginn.getnCaja());
			encDetalleFrame.setINnIDSucursa( logginn.getnSucursal());
			encDetalleFrame.setINsIDUsuario(logginn.getUe());
			encDetalleFrame.setRespuesta("");
			encDetalleFrame.setINsTransaccion("D");
			encDetalleFrame.setINsUnionSocio(logginn.getnUnion());
			encDetalleFrame.setINsCajaSocio(encDetalle.getINsCajaSocio());
			encDetalleFrame.setINsSucursalSocio(encDetalle.getINsSucursalSocio());
			encDetalleFrame.setINsNumeroSocio(encDetalle.getINsNumeroSocio());
			encDetalleFrame.setINVerSaldos("SI");
			encDetalleFrame.setINValidoHuella("SI");
			encDetalleFrame.setINDespliegaMensaje("SI");
			encDetalleFrame.setINnIDUnion( logginn.getnUnion());
			encDetalleFrame.setINnIDCajaPopular( logginn.getnCaja() );
			encDetalleFrame.setINnIDSucursa( logginn.getnSucursal() );
			encDetalleFrame.setINsIDUsuario("lClzdbkdcaaFaJkl");
			encDetalleFrame.setRespuesta("");
			encDetalleFrame.setINsTransaccion("D");
			encDetalleFrame.setINsUnionSocio(encDetalle.getINsUnionSocio() );
			//encDetalleFrame.setINsCajaSocio(encDetalle.getINsSucursalSocio());
			//encDetalleFrame.setINsSucursalSocio(encDetalle.getINsCajaSocio());
			//encDetalleFrame.setINsNumeroSocio(encDetalle.getINsNumeroSocio());
			
			 
			List<Encdatos_DetalleframedBE> lstDetalle = new LinkedList<>();
			lstDetalle.add(encDetalleFrame);
			//RequestGralDetalleframedBE_One
			
			RequestGralDetalleframedBE_One gralRequest = new RequestGralDetalleframedBE_One();
			RequestDetalleframedBE_One re = new RequestDetalleframedBE_One();
			re.setTtEncdatos(lstDetalle);
			gralRequest.setRequest(re);
			logger.info("antes de enviar funcion " + gralRequest);
			
			RequestGralDetalleframedBE_One requestGral =  new  DepositoService(new DepositosDao()).getDepositoDetalleFramedBE(gralRequest) ;
			
			return requestGral;
			/*
			EncDatDetFicha_DetalleFichaBE encDetalleFicha = new EncDatDetFicha_DetalleFichaBE();
			encDetalleFicha.setINid(1);
			encDetalleFicha.setINtoken("");
			encDetalleFicha.setINnIDUnion(logginn.getnUnion());
			encDetalleFicha.setINnIDCajaPopular(logginn.getnCaja());
			encDetalleFicha.setINnIDSucursa(logginn.getnSucursal());
			encDetalleFicha.setINsIDUsuario(logginn.getUe());
			encDetalleFicha.setINsTransaccion("D");
			//sustituir por los que se cargaron al inicio en las cajas de texto
			encDetalleFicha.setINsUnionSocio("");
			encDetalleFicha.setINsCajaSocio("");
			encDetalleFicha.setINsSucursalSocio("");
			encDetalleFicha.setINsNumeroSocio("");
			encDetalleFicha.setINsServicio("AHCC");
			encDetalleFicha.setINServicio("AHCC");
			encDetalleFicha.setINImporte("1000");
			encDetalleFicha.setINAbono("1000");
			encDetalleFicha.setINsCajaAsignada("099"); //cajero logueado
			//termina de lo que se cargo en cajas de texto
			encDetalleFicha.setINRefreshHead("NO");
			encDetalleFicha.setINNuevaFicha("");
			encDetalleFicha.setINDespliegaMensaje("SI");
			encDetalleFicha.setINsFolioFicha("");
			encDetalleFicha.setsPageStatus("OUT");
			encDetalleFicha.setsType("dialog");
			encDetalleFicha.setsMode("CON");
	    }
		
		
		return null;
	}
	*/
	
	@ResponseBody
	@RequestMapping(value = "/getBuscaSocioAceptarBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestBuscasocioAceptarBE mostrarPrincipal(@RequestBody Metadata tem  ) {
		
		EncdatosBuscaSocioAceptarBE enc = new EncdatosBuscaSocioAceptarBE();
		
		enc.setINid(1);
		enc.setINtoken("");
		enc.setINnIDUnion(tem.getUnion());
		enc.setINnIDCajaPopular(tem.getPolular());
		enc.setINnIDSucursa(tem.getCaja());
		enc.setINsNumeroSocio(tem.getSocio());
		enc.setINsIDUsuario("lClzdbkdcaaFaJkl");
		enc.setRespuesta("");
		enc.setINsIrPagina("detalleframed");
		enc.setINVerSaldos("SI");
		enc.setINSeValidoHuella("SI");
		enc.setINsTransaccion("D");
		enc.setINsUnionSocio("1");
		enc.setINsCajaSocio("8");
		enc.setINsSucursalSocio("1");
		enc.setNuevaFicha("SI");
		enc.setPrimeraPagina("");
		enc.setsNombreSocio("");
		enc.setAcceperftran(false);
		
		List<EncdatosBuscaSocioAceptarBE> lstenc = new LinkedList<>();
		lstenc.add(enc);
		RequestBuscasocioAceptarBE reqEnc = new RequestBuscasocioAceptarBE();
		reqEnc.setTtEncdatos(lstenc);

		RequestBuscasocioAceptarBE requestGralFichaSucursal= depServce.getBuscaSocioAceptarBE(reqEnc);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
		return requestGralFichaSucursal;
	}
}

class Metadata{
	
	public String getUnion() {
		return union;
	}
	public void setUnion(String union) {
		this.union = union;
	}
	public String getPolular() {
		return polular;
	}
	public void setPolular(String polular) {
		this.polular = polular;
	}
	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String getSocio() {
		return socio;
	}
	public void setSocio(String socio) {
		this.socio = socio;
	}
	
	private String union;
	private String polular;
	private String caja;
	private String socio;
	
}
