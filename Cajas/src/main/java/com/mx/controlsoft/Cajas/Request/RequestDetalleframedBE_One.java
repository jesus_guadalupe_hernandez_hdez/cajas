package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetdatDebe_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.DetdatHaber_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.EncdatDebe_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.EncdatHaber_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.Encdatosficha_DetalleframedBE;

public class RequestDetalleframedBE_One {
	public List<Encdatos_DetalleframedBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<Encdatos_DetalleframedBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<Encdatosficha_DetalleframedBE> getTtEncdatosficha() {
		return ttEncdatosficha;
	}
	public void setTtEncdatosficha(List<Encdatosficha_DetalleframedBE> ttEncdatosficha) {
		this.ttEncdatosficha = ttEncdatosficha;
	}
	public List<EncdatHaber_DetalleframedBE> getTtEncdatHaber() {
		return ttEncdatHaber;
	}
	public void setTtEncdatHaber(List<EncdatHaber_DetalleframedBE> ttEncdatHaber) {
		this.ttEncdatHaber = ttEncdatHaber;
	}
	public List<DetdatHaber_DetalleframedBE> getTtdetdatHaber() {
		return ttdetdatHaber;
	}
	public void setTtdetdatHaber(List<DetdatHaber_DetalleframedBE> ttdetdatHaber) {
		this.ttdetdatHaber = ttdetdatHaber;
	}
	public List<EncdatDebe_DetalleframedBE> getTtEncdatDebe() {
		return ttEncdatDebe;
	}
	public void setTtEncdatDebe(List<EncdatDebe_DetalleframedBE> ttEncdatDebe) {
		this.ttEncdatDebe = ttEncdatDebe;
	}
	public List<DetdatDebe_DetalleframedBE> getTtdetdatDebe() {
		return ttdetdatDebe;
	}
	public void setTtdetdatDebe(List<DetdatDebe_DetalleframedBE> ttdetdatDebe) {
		this.ttdetdatDebe = ttdetdatDebe;
	}
	List< Encdatos_DetalleframedBE> ttEncdatos;
	List<Encdatosficha_DetalleframedBE> ttEncdatosficha;
	List<EncdatHaber_DetalleframedBE> ttEncdatHaber;
	List<DetdatHaber_DetalleframedBE> ttdetdatHaber;
	List<EncdatDebe_DetalleframedBE> ttEncdatDebe;
	List<DetdatDebe_DetalleframedBE>  ttdetdatDebe;
	
	@Override
	public String toString() {
		return "RequestDetalleframedBE_One [ttEncdatos=" + ttEncdatos + ", ttEncdatosficha=" + ttEncdatosficha
				+ ", ttEncdatHaber=" + ttEncdatHaber + ", ttdetdatHaber=" + ttdetdatHaber + ", ttEncdatDebe="
				+ ttEncdatDebe + ", ttdetdatDebe=" + ttdetdatDebe + "]";
	}

}
