package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequesDetalleFichaBE;

public class RequestGralDetalleFichaBE {
	private RequesDetalleFichaBE request;

	public RequesDetalleFichaBE getRequest() {
		return request;
	}

	public void setRequest(RequesDetalleFichaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralDetalleFichaBE [request=" + request + "]";
	}
	
}
