package com.mx.controlsoft.Cajas.Model;

public class EncdatosConsultaSocioRecomendadoBE {
	 public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursal() {
		return INnIDSucursal;
	}
	public void setINnIDSucursal(String iNnIDSucursal) {
		INnIDSucursal = iNnIDSucursal;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursal;
    private String INsIDUsuario;
    private String respuesta;
    
	@Override
	public String toString() {
		return "EncdatosConsultaSocioRecomendadoBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion="
				+ INnIDUnion + ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursal=" + INnIDSucursal
				+ ", INsIDUsuario=" + INsIDUsuario + ", respuesta=" + respuesta + "]";
	}
}
