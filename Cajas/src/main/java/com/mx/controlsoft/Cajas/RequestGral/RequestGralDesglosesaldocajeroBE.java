package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestDesglosesaldocajeroBE;

public class RequestGralDesglosesaldocajeroBE {
	
	private RequestDesglosesaldocajeroBE request;

	public RequestDesglosesaldocajeroBE getRequest() {
		return request;
	}

	public void setRequest(RequestDesglosesaldocajeroBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralDesglosesaldocajeroBE [request=" + request + "]";
	}
	
}
