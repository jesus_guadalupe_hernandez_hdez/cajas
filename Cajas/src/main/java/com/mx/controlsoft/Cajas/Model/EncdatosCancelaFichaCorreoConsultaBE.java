package com.mx.controlsoft.Cajas.Model;

public class EncdatosCancelaFichaCorreoConsultaBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursal() {
		return INnIDSucursal;
	}
	public void setINnIDSucursal(String iNnIDSucursal) {
		INnIDSucursal = iNnIDSucursal;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getINnEjercisio() {
		return INnEjercisio;
	}
	public void setINnEjercisio(String iNnEjercisio) {
		INnEjercisio = iNnEjercisio;
	}
	public String getINsAccion() {
		return INsAccion;
	}
	public void setINsAccion(String iNsAccion) {
		INsAccion = iNsAccion;
	}
	public String getINnIDCajaAsignadaCorreo() {
		return INnIDCajaAsignadaCorreo;
	}
	public void setINnIDCajaAsignadaCorreo(String iNnIDCajaAsignadaCorreo) {
		INnIDCajaAsignadaCorreo = iNnIDCajaAsignadaCorreo;
	}
	public String getINnIDFichacorreo() {
		return INnIDFichacorreo;
	}
	public void setINnIDFichacorreo(String iNnIDFichacorreo) {
		INnIDFichacorreo = iNnIDFichacorreo;
	}
	public String getINnIDTipoTransaccionCorreo() {
		return INnIDTipoTransaccionCorreo;
	}
	public void setINnIDTipoTransaccionCorreo(String iNnIDTipoTransaccionCorreo) {
		INnIDTipoTransaccionCorreo = iNnIDTipoTransaccionCorreo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursal;
    private String INsIDUsuario;
    private String INnEjercisio;
    private String INsAccion;
    private String INnIDCajaAsignadaCorreo;
    private String INnIDFichacorreo;
    private String INnIDTipoTransaccionCorreo;
    private String respuesta;
    
	@Override
	public String toString() {
		return "EncdatosCancelaFichaCorreoConsultaBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion="
				+ INnIDUnion + ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursal=" + INnIDSucursal
				+ ", INsIDUsuario=" + INsIDUsuario + ", INnEjercisio=" + INnEjercisio + ", INsAccion=" + INsAccion
				+ ", INnIDCajaAsignadaCorreo=" + INnIDCajaAsignadaCorreo + ", INnIDFichacorreo=" + INnIDFichacorreo
				+ ", INnIDTipoTransaccionCorreo=" + INnIDTipoTransaccionCorreo + ", respuesta=" + respuesta + "]";
	}
}
