package com.mx.controlsoft.Cajas.Request;
import java.util.List;

import com.mx.controlsoft.Cajas.Model.Cajapopular;
import com.mx.controlsoft.Cajas.Model.EncDatosBuscaSocioOtraSucBE;
import com.mx.controlsoft.Cajas.Model.Sucursal;
import com.mx.controlsoft.Cajas.Model.Union;

public class RequestBuscasociootrasucBE {
	
	public List<EncDatosBuscaSocioOtraSucBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncDatosBuscaSocioOtraSucBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<Union> getTtUnion() {
		return ttUnion;
	}
	public void setTtUnion(List<Union> ttUnion) {
		this.ttUnion = ttUnion;
	}
	public List<Cajapopular> getTtCajapopular() {
		return ttCajapopular;
	}
	public void setTtCajapopular(List<Cajapopular> ttCajapopular) {
		this.ttCajapopular = ttCajapopular;
	}
	public List<Sucursal> getTtSucursal() {
		return ttSucursal;
	}
	public void setTtSucursal(List<Sucursal> ttSucursal) {
		this.ttSucursal = ttSucursal;
	}
	
	private List<EncDatosBuscaSocioOtraSucBE> ttEncdatos;
	private List<Union> ttUnion ;
	private List<Cajapopular> ttCajapopular;
	private List<Sucursal> ttSucursal;
	
	@Override
	public String toString() {
		return "RequestBuscasociootrasucBE [ttEncdatos=" + ttEncdatos + ", ttUnion=" + ttUnion + ", ttCajapopular="
				+ ttCajapopular + ", ttSucursal=" + ttSucursal + "]";
	}
}
