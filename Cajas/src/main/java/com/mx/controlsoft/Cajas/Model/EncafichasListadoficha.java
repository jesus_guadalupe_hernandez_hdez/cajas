package com.mx.controlsoft.Cajas.Model;

public class EncafichasListadoficha {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getnUnion() {
		return nUnion;
	}
	public void setnUnion(String nUnion) {
		this.nUnion = nUnion;
	}
	public String getnCaja() {
		return nCaja;
	}
	public void setnCaja(String nCaja) {
		this.nCaja = nCaja;
	}
	public String getnSucursal() {
		return nSucursal;
	}
	public void setnSucursal(String nSucursal) {
		this.nSucursal = nSucursal;
	}
	public String getFechaReporte() {
		return FechaReporte;
	}
	public void setFechaReporte(String fechaReporte) {
		FechaReporte = fechaReporte;
	}
	public String getCajaAsignadaReporte() {
		return CajaAsignadaReporte;
	}
	public void setCajaAsignadaReporte(String cajaAsignadaReporte) {
		CajaAsignadaReporte = cajaAsignadaReporte;
	}
	public String getsCaja() {
		return sCaja;
	}
	public void setsCaja(String sCaja) {
		this.sCaja = sCaja;
	}
	public String getsSucursal() {
		return sSucursal;
	}
	public void setsSucursal(String sSucursal) {
		this.sSucursal = sSucursal;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getFiltro() {
		return Filtro;
	}
	public void setFiltro(String filtro) {
		Filtro = filtro;
	}
	
	private long id;
    private String nUnion;
    private String nCaja;
    private String nSucursal;
    private String FechaReporte;
    private String CajaAsignadaReporte;
    private String sCaja;
    private String sSucursal;
    private String titulo;
    private String Filtro;
    
	@Override
	public String toString() {
		return "EncafichasListadoficha [id=" + id + ", nUnion=" + nUnion + ", nCaja=" + nCaja + ", nSucursal="
				+ nSucursal + ", FechaReporte=" + FechaReporte + ", CajaAsignadaReporte=" + CajaAsignadaReporte
				+ ", sCaja=" + sCaja + ", sSucursal=" + sSucursal + ", titulo=" + titulo + ", Filtro=" + Filtro + "]";
	}
    
}
