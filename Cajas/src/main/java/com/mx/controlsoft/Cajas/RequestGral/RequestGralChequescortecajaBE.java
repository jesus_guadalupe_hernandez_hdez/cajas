package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestChequescortecajaBE;

public class RequestGralChequescortecajaBE {
	private RequestChequescortecajaBE request;

	public RequestChequescortecajaBE getRequest() {
		return request;
	}

	public void setRequest(RequestChequescortecajaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralChequescortecajaBE [request=" + request + "]";
	}
}
