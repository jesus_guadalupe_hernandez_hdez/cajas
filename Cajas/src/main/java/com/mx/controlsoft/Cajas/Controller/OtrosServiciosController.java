package com.mx.controlsoft.Cajas.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.controlsoft.Cajas.Model.Login;

@Controller
@RequestMapping(value = "/otrosServicios")
public class OtrosServiciosController {

	@RequestMapping(value = "/otrosServicios",method = RequestMethod.GET)
	public String mostrarOtrosServicios(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "otrosServicios/otrosServicios";
		}
		return "login";
	}
	
	@RequestMapping(value = "/correo",method = RequestMethod.GET)
	public String mostrarCorreo(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "otrosServicios/correo";
		}
		return "login";
	}
	
	@RequestMapping(value = "/cancelarFichaCorreoAplicada",method = RequestMethod.GET)
	public String mostrarCancelarFichaCorreo(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "otrosServicios/cancelarFichaCorreo";
		}
		return "login";
	}
	
	@RequestMapping(value = "/socioDependenciaGobierno",method = RequestMethod.GET)
	public String mostrarSocioDependenciaGobierno(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "otrosServicios/socioDependenciaGobierno";
		}
		return "login";
	}
}
