package com.mx.controlsoft.Cajas.Controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mx.controlsoft.Cajas.Dao.DepositosDao;
import com.mx.controlsoft.Cajas.Model.DepositoBusqueda;
import com.mx.controlsoft.Cajas.Model.EncDatDetFicha_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.EncDatosBuscaSocioOtraSucBE;
import com.mx.controlsoft.Cajas.Model.EncDatos_buscaSocioBE_Inicio;
import com.mx.controlsoft.Cajas.Model.Encdatos_DetalleframedBE;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequesDetalleFichaBE;
import com.mx.controlsoft.Cajas.Request.RequestBuscasociootrasucBE;
import com.mx.controlsoft.Cajas.Request.RequestDetalleframedBE_One;
import com.mx.controlsoft.Cajas.Request.RequestEncdatosBuscaSocioBE_Inicio;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscaSocioOtraSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleFichaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleframedBE_One;
import com.mx.controlsoft.Cajas.Service.DepositoService;


@Controller
@RequestMapping(value = "/deposito")
public class DepositoController {
	
	@Autowired 
	private DepositoService depServce;
	
	
	private static final Logger logger = LogManager.getLogger(DepositoController.class);
	
	@RequestMapping(value = "/movimiento",method = RequestMethod.GET)
	public String mostrarPrincipal(Model model ,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "deposito/depositoLst";
		}
		return "login";
		
		
	}
	
	@GetMapping ("/inversion")
	public String getBuzonQuejasPLD(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "construccion";
		}
		return "login";
	}
	
	@RequestMapping(value = "/deposito",method = RequestMethod.GET)
	public String mostrarDeposito(Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {

			if (getReadyIniAcceso().getTtEncdatos().get(0).getRespuesta().equals("SUCCESS")) {
				
				model.addAttribute("usuario",(Login) sesion.getAttribute("usuario"));
				model.addAttribute("opcionPantalla", getReadyIniAcceso().getTtEncdatos().get(0).getsDescripcionTransaccion());
				return "deposito/depositoFrm";
			}
			
			//model.addAttribute("requestInicio", requestInicio);
			return "deposito/depositoFrm";
		}
		return "login";
		
	}
	
	@RequestMapping(value = "/otraSucursal",method = RequestMethod.GET)
	public String mostrarOtraSucursal(Model model  ,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			model.addAttribute("requestData", getBuscaSocioOtraSucBE());
			return "deposito/otraSuc";

		}
		return "login";
		
		
	}
	
	private RequestGralBuscaSocioOtraSucBE getBuscaSocioOtraSucBE () {
		RequestGralBuscaSocioOtraSucBE otraSucRequest = new RequestGralBuscaSocioOtraSucBE();
		RequestBuscasociootrasucBE reqOtraSuc = new RequestBuscasociootrasucBE();
		EncDatosBuscaSocioOtraSucBE encDatosOtraSuc = new EncDatosBuscaSocioOtraSucBE();
		encDatosOtraSuc.setINid(1);
		encDatosOtraSuc.setINtoken("");
		encDatosOtraSuc.setINnIDUnion("1");
		encDatosOtraSuc.setINnIDCajaPopular("8");
		encDatosOtraSuc.setINnIDSucursa("1");
		encDatosOtraSuc.setINsIDUsuario("lClzdbkdcaaFaJkl");
		encDatosOtraSuc.setRespuesta("");
		
		List<EncDatosBuscaSocioOtraSucBE> lstEncDatosBuscaSocioOtraSucBE = new LinkedList<>();
		lstEncDatosBuscaSocioOtraSucBE.add(encDatosOtraSuc);
		
		reqOtraSuc.setTtEncdatos(lstEncDatosBuscaSocioOtraSucBE);
		otraSucRequest.setRequest(reqOtraSuc);
		
		RequestGralBuscaSocioOtraSucBE requestOtraSuc = depServce.getBuscaSocioOtraSucBE(otraSucRequest) ; // new DepositoService(new DepositosDao()).getBuscaSocioOtraSucBE(otraSucRequest) ;
		
		return requestOtraSuc;
	}
	
	private RequestEncdatosBuscaSocioBE_Inicio getReadyIniAcceso() {
		RequestEncdatosBuscaSocioBE_Inicio iniReq = new RequestEncdatosBuscaSocioBE_Inicio();
		List<EncDatos_buscaSocioBE_Inicio> lstIniDto = new LinkedList<>();
		EncDatos_buscaSocioBE_Inicio ini = new EncDatos_buscaSocioBE_Inicio();
		
		ini.setINid(1);
		ini.setINtoken("");
		ini.setINnIDUnion("1");
		ini.setINnIDCajaPopular("8");
		ini.setINnIDSucursa("1");
		ini.setINsIDUsuario("lClzdbkdcaaFaJkl");
		ini.setINsTransaccion("D");
		ini.setRespuesta("");
		
		lstIniDto.add(ini);
		
		iniReq.setTtEncdatos(lstIniDto);
		
		//Empieza otro metodo
		RequestEncdatosBuscaSocioBE_Inicio requestInicio =  depServce.getBuscaSocioBE_Inicio(iniReq);  // new DepositoService(new DepositosDao()).getBuscaSocioBE_Inicio(iniReq);
		
		return requestInicio;
	}
	
	
	private RequestGralDetalleframedBE_One getRequestOneDetalleFramedBE ( Encdatos_DetalleframedBE enct)  {
		
		
		List<Encdatos_DetalleframedBE> lstDetalle = new LinkedList<>();
		lstDetalle.add(enct);
		
		RequestGralDetalleframedBE_One gralRequest = new RequestGralDetalleframedBE_One();
		RequestDetalleframedBE_One re = new RequestDetalleframedBE_One();
		re.setTtEncdatos(lstDetalle);
		gralRequest.setRequest(re);
		logger.info("antes de enviar funcion " + gralRequest);
		
		RequestGralDetalleframedBE_One requestGral =  new  DepositoService(new DepositosDao()).getDepositoDetalleFramedBE(gralRequest) ;
		return requestGral;
	}
	
	private RequestGralDetalleFichaBE getRequestDetalleFichaBE(EncDatDetFicha_DetalleFichaBE encDetalle) {
		 
		encDetalle.setINid(1);
		encDetalle.setINtoken("");
        encDetalle.setINsIDUsuario("lClzdbkdcaaFaJkl");
        encDetalle.setINsTransaccion("D");
        encDetalle.setRespuesta("");
        //encDetalle.setINsUnionSocio("1");
        //encDetalle.setINsCajaSocio("8");
        //encDetalle.setINsSucursalSocio("1");
        //encDetalle.setINsNumeroSocio("128");
        encDetalle.setINRefreshHead("NO");
        encDetalle.setINNuevaFicha("");
        encDetalle.setINDespliegaMensaje("SI");
        encDetalle.setINsFolioFicha("");
        encDetalle.setINsServicio("AHCC");
        encDetalle.setINServicio("AHCC");
        encDetalle.setINImporte("1000");
        encDetalle.setINAbono("100");
        encDetalle.setINsCajaAsignada("99");
        encDetalle.setsPageStatus("OUT");
        encDetalle.setsType("dialog");
        encDetalle.setsMode("CON");
        encDetalle.setINMaintOption1("");
        encDetalle.setINMaintOption("Aceptar");
        encDetalle.setINacepta("Aceptar");
        
        List<EncDatDetFicha_DetalleFichaBE> lstDetalle = new LinkedList<>();
        lstDetalle.add(encDetalle);
        
        RequestGralDetalleFichaBE request = new RequestGralDetalleFichaBE();
        RequesDetalleFichaBE req = new RequesDetalleFichaBE();
        req.setTtEncdatdetficha(lstDetalle);
        request.setRequest(req);
		
        RequestGralDetalleFichaBE requestGral =  new  DepositoService(new DepositosDao()).getDepositoDetallefichaBE_Dat(request) ;
		return requestGral;
	}
	
	@RequestMapping(value = "/detallefichaBE/{dataLista}",method = {RequestMethod.GET, RequestMethod.POST})
	//public String mostrarDetallefichaBE (@ModelAttribute("encDetalle") EncDatDetFicha_DetalleFichaBE encDetalle, Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	public String mostrarDetallefichaBE (@PathVariable ("dataLista") String  dataLista, Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		logger.info("valor recibido: " + dataLista);
		String[] arrSplit = dataLista.split("-"); 
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		
		
		if (logginn != null) {
			
			model.addAttribute("usuario", logginn);
			
			Encdatos_DetalleframedBE encDetalleFrame = new Encdatos_DetalleframedBE();
			encDetalleFrame.setINid(1);
			encDetalleFrame.setINtoken("");
			encDetalleFrame.setINnIDUnion(logginn.getnUnion());
			encDetalleFrame.setINnIDCajaPopular(logginn.getnCaja());
			encDetalleFrame.setINnIDSucursa( logginn.getnSucursal());
			encDetalleFrame.setINsIDUsuario(logginn.getUe());
			encDetalleFrame.setRespuesta("");
			encDetalleFrame.setINsTransaccion("D");
			encDetalleFrame.setINsUnionSocio(arrSplit[0]);
			encDetalleFrame.setINsCajaSocio(arrSplit[1]);
			encDetalleFrame.setINsSucursalSocio(arrSplit[2]);
			encDetalleFrame.setINsNumeroSocio(arrSplit[3]);
			encDetalleFrame.setINVerSaldos("SI");
			encDetalleFrame.setINValidoHuella("SI");
			encDetalleFrame.setINDespliegaMensaje("SI");
			encDetalleFrame.setINnIDUnion( logginn.getnUnion());
			encDetalleFrame.setINnIDCajaPopular( logginn.getnCaja() );
			encDetalleFrame.setINnIDSucursa( logginn.getnSucursal() );
			encDetalleFrame.setINsIDUsuario("lClzdbkdcaaFaJkl");
			encDetalleFrame.setRespuesta("");
			encDetalleFrame.setINsTransaccion("D");
			//encDetalleFrame.setINsUnionSocio(encDetalle.getINsUnionSocio() );
			//encDetalleFrame.setINsCajaSocio(encDetalle.getINsSucursalSocio());
			//encDetalleFrame.setINsSucursalSocio(encDetalle.getINsCajaSocio());
			//encDetalleFrame.setINsNumeroSocio(encDetalle.getINsNumeroSocio());
			
			EncDatDetFicha_DetalleFichaBE encDetalle = new EncDatDetFicha_DetalleFichaBE();
			
			encDetalle.setINnIDUnion(logginn.getnUnion());
	        encDetalle.setINnIDCajaPopular(logginn.getnCaja());
	        encDetalle.setINnIDSucursa(logginn.getnSucursal());
	        
	        encDetalle.setINsUnionSocio(arrSplit[0]);
	        encDetalle.setINsCajaSocio(arrSplit[1]);
	        encDetalle.setINsSucursalSocio(arrSplit[2]);
	        encDetalle.setINsNumeroSocio(arrSplit[3]);
	        
	        model.addAttribute("requestDataHead", getRequestOneDetalleFramedBE(encDetalleFrame));
			model.addAttribute("requestData", getRequestDetalleFichaBE(encDetalle));
			model.addAttribute("endDetalleFrame",encDetalleFrame);
			
			return "deposito/detallefichabe";

		}
		return "login";
	}
	
	@RequestMapping(value = "/retiroEfectivo",method = RequestMethod.GET)
	public String mostrarRetiroEfectivo(Model model ,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			model.addAttribute("requestData", getBuscaSocioOtraSucBE());
			return "deposito/retiroEfectivo";

		}
		return "login";		
	}
	
	@RequestMapping(value = "/buscar",method = RequestMethod.GET)
	public String mostrarBusqueda(Model model ,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			model.addAttribute("requestData", getBuscaSocioOtraSucBE());
			return "deposito/busqueda";

		}
		return "login";
		
	}
	
	@RequestMapping(value = "/buscar",method = RequestMethod.POST)
	public String mostrarBusquedaPost(@ModelAttribute("depositoBusqueda1") DepositoBusqueda depositoBusqueda1,Model model ,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			model.addAttribute("depositoBusqueda", depositoBusqueda1.getSocio());
			return "deposito/busqueda";
			

		}
		return "login";
		
	}
}
