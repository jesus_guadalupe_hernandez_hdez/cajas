package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.ICajeroPrincipalDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicajsBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralMonitorcajasBE;

@Service
public class CajeroPrincipalBEService {
	
	@Autowired
	ICajeroPrincipalDao <RequestGralFacturacionelectronicaIniBE , RequestGralFacturacionelectronicajsBE,RequestGralMonitorcajasBE,RequestGralDesglosesaldocajeroBE,RequestGralEfectivofondofijoBE> service; 
	
	public RequestGralFacturacionelectronicaIniBE getPostFacturacionelectronicaIniBE (RequestGralFacturacionelectronicaIniBE request) {
		return service.getPostFacturacionelectronicaIniBE(request);
	}
	
	public RequestGralFacturacionelectronicajsBE getPostFacturacionelectronicajsBE (RequestGralFacturacionelectronicajsBE request) {
		return service.getPostFacturacionelectronicajsBE(request);
	}
	
	public RequestGralMonitorcajasBE getPostMonitorcajasBE (RequestGralMonitorcajasBE request) {
		return service.getPostMonitorcajasBE(request);
	}
	
	public RequestGralDesglosesaldocajeroBE getPostDesglosesaldocajeroBE (RequestGralDesglosesaldocajeroBE request) {
		return service.getPostDesglosesaldocajeroBE(request);
	}
	
	public RequestGralEfectivofondofijoBE getPostDetalleFondoAceptar (RequestGralEfectivofondofijoBE request) {
		return service.getPostDetalleFondoAceptar(request);
	}
}
