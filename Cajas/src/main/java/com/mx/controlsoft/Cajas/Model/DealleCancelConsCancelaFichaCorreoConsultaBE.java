package com.mx.controlsoft.Cajas.Model;

public class DealleCancelConsCancelaFichaCorreoConsultaBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumero() {
		return Numero;
	}
	public void setNumero(String numero) {
		Numero = numero;
	}
	public String getSucursal() {
		return Sucursal;
	}
	public void setSucursal(String sucursal) {
		Sucursal = sucursal;
	}
	public String getEjercicio() {
		return Ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		Ejercicio = ejercicio;
	}
	public String getCajaAsignadaCorreo() {
		return CajaAsignadaCorreo;
	}
	public void setCajaAsignadaCorreo(String cajaAsignadaCorreo) {
		CajaAsignadaCorreo = cajaAsignadaCorreo;
	}
	public String getTipoTransaccionCorreo() {
		return TipoTransaccionCorreo;
	}
	public void setTipoTransaccionCorreo(String tipoTransaccionCorreo) {
		TipoTransaccionCorreo = tipoTransaccionCorreo;
	}
	public String getFichaCorreo() {
		return FichaCorreo;
	}
	public void setFichaCorreo(String fichaCorreo) {
		FichaCorreo = fichaCorreo;
	}
	public String getCajaAsignada() {
		return CajaAsignada;
	}
	public void setCajaAsignada(String cajaAsignada) {
		CajaAsignada = cajaAsignada;
	}
	public String getTipoTransaccion() {
		return TipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		TipoTransaccion = tipoTransaccion;
	}
	public String getFicha() {
		return Ficha;
	}
	public void setFicha(String ficha) {
		Ficha = ficha;
	}
	public String getFechaFicha() {
		return FechaFicha;
	}
	public void setFechaFicha(String fechaFicha) {
		FechaFicha = fechaFicha;
	}
	public String getUsuario() {
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		Hora = hora;
	}
	public String getEstadoFicha() {
		return EstadoFicha;
	}
	public void setEstadoFicha(String estadoFicha) {
		EstadoFicha = estadoFicha;
	}
	public String getCancelarFicha() {
		return CancelarFicha;
	}
	public void setCancelarFicha(String cancelarFicha) {
		CancelarFicha = cancelarFicha;
	}
	public String getCancelarFichaHref() {
		return CancelarFichaHref;
	}
	public void setCancelarFichaHref(String cancelarFichaHref) {
		CancelarFichaHref = cancelarFichaHref;
	}
	private long id;
     private String Numero;
     private String Sucursal;
     private String Ejercicio;
     private String CajaAsignadaCorreo;
     private String TipoTransaccionCorreo;
     private String FichaCorreo;
     private String CajaAsignada;
     private String TipoTransaccion;
     private String Ficha;
     private String FechaFicha;
     private String Usuario;
     private String Hora;
     private String EstadoFicha;
     private String CancelarFicha;
     private String CancelarFichaHref;
     
	@Override
	public String toString() {
		return "DealleCancelConsCancelaFichaCorreoConsultaBE [id=" + id + ", Numero=" + Numero + ", Sucursal="
				+ Sucursal + ", Ejercicio=" + Ejercicio + ", CajaAsignadaCorreo=" + CajaAsignadaCorreo
				+ ", TipoTransaccionCorreo=" + TipoTransaccionCorreo + ", FichaCorreo=" + FichaCorreo
				+ ", CajaAsignada=" + CajaAsignada + ", TipoTransaccion=" + TipoTransaccion + ", Ficha=" + Ficha
				+ ", FechaFicha=" + FechaFicha + ", Usuario=" + Usuario + ", Hora=" + Hora + ", EstadoFicha="
				+ EstadoFicha + ", CancelarFicha=" + CancelarFicha + ", CancelarFichaHref=" + CancelarFichaHref + "]";
	}
     
}
