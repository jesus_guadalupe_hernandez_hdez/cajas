package com.mx.controlsoft.Cajas.Model;

public class Encdatos_BuscaSocioBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursal() {
		return INnIDSucursal;
	}
	public void setINnIDSucursal(String iNnIDSucursal) {
		INnIDSucursal = iNnIDSucursal;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINnIDSocioJ() {
		return INnIDSocioJ;
	}
	public void setINnIDSocioJ(String iNnIDSocioJ) {
		INnIDSocioJ = iNnIDSocioJ;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAPaterno() {
		return APaterno;
	}
	public void setAPaterno(String aPaterno) {
		APaterno = aPaterno;
	}
	public String getAMaterno() {
		return AMaterno;
	}
	public void setAMaterno(String aMaterno) {
		AMaterno = aMaterno;
	}
	public String getsNumeroSocio() {
		return sNumeroSocio;
	}
	public void setsNumeroSocio(String sNumeroSocio) {
		this.sNumeroSocio = sNumeroSocio;
	}
	public String getdFechaCreacion() {
		return dFechaCreacion;
	}
	public void setdFechaCreacion(String dFechaCreacion) {
		this.dFechaCreacion = dFechaCreacion;
	}
	public String gettHora() {
		return tHora;
	}
	public void settHora(String tHora) {
		this.tHora = tHora;
	}
	public String getRFC() {
		return RFC;
	}
	public void setRFC(String rFC) {
		RFC = rFC;
	}
	public String getCURP() {
		return CURP;
	}
	public void setCURP(String cURP) {
		CURP = cURP;
	}
	public String getFECHAREGISTRO() {
		return FECHAREGISTRO;
	}
	public void setFECHAREGISTRO(String fECHAREGISTRO) {
		FECHAREGISTRO = fECHAREGISTRO;
	}
	public String getTitulodeberes() {
		return titulodeberes;
	}
	public void setTitulodeberes(String titulodeberes) {
		this.titulodeberes = titulodeberes;
	}
	public String getTitulovencido() {
		return titulovencido;
	}
	public void setTitulovencido(String titulovencido) {
		this.titulovencido = titulovencido;
	}
	public String getsTexto() {
		return sTexto;
	}
	public void setsTexto(String sTexto) {
		this.sTexto = sTexto;
	}
	public String getsTexto1() {
		return sTexto1;
	}
	public void setsTexto1(String sTexto1) {
		this.sTexto1 = sTexto1;
	}
	public String getsTexto2() {
		return sTexto2;
	}
	public void setsTexto2(String sTexto2) {
		this.sTexto2 = sTexto2;
	}
	public String getsTesto3() {
		return sTesto3;
	}
	public void setsTesto3(String sTesto3) {
		this.sTesto3 = sTesto3;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursal;
    private String INsIDUsuario;
    private String respuesta;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INnIDSocioJ;
    private String nombre;
    private String APaterno;
    private String AMaterno;
    private String sNumeroSocio;
    private String dFechaCreacion;
    private String tHora;
    private String RFC;
    private String CURP;
    private String FECHAREGISTRO;
    private String titulodeberes;
    private String titulovencido;
    private String sTexto;
    private String sTexto1;
    private String sTexto2;
    private String sTesto3;
    
	@Override
	public String toString() {
		return "Encdatos_BuscaSocioBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursal=" + INnIDSucursal + ", INsIDUsuario="
				+ INsIDUsuario + ", respuesta=" + respuesta + ", INsUnionSocio=" + INsUnionSocio + ", INsCajaSocio="
				+ INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio + ", INnIDSocioJ=" + INnIDSocioJ + ", nombre="
				+ nombre + ", APaterno=" + APaterno + ", AMaterno=" + AMaterno + ", sNumeroSocio=" + sNumeroSocio
				+ ", dFechaCreacion=" + dFechaCreacion + ", tHora=" + tHora + ", RFC=" + RFC + ", CURP=" + CURP
				+ ", FECHAREGISTRO=" + FECHAREGISTRO + ", titulodeberes=" + titulodeberes + ", titulovencido="
				+ titulovencido + ", sTexto=" + sTexto + ", sTexto1=" + sTexto1 + ", sTexto2=" + sTexto2 + ", sTesto3="
				+ sTesto3 + "]";
	}
}
