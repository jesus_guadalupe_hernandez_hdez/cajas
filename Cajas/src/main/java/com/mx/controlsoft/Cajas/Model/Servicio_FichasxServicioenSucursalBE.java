package com.mx.controlsoft.Cajas.Model;

public class Servicio_FichasxServicioenSucursalBE {
	 public String getIDServicio() {
		return IDServicio;
	}
	public void setIDServicio(String iDServicio) {
		IDServicio = iDServicio;
	}
	public String getAccion() {
		return Accion;
	}
	public void setAccion(String accion) {
		Accion = accion;
	}
	private String IDServicio;
    private String Accion;
    
	@Override
	public String toString() {
		return "Servicio_FichasxServicioenSucursalBE [IDServicio=" + IDServicio + ", Accion=" + Accion + "]";
	}
}
