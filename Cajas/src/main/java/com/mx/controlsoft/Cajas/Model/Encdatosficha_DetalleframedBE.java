package com.mx.controlsoft.Cajas.Model;

public class Encdatosficha_DetalleframedBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getnCajaAsignada() {
		return nCajaAsignada;
	}
	public void setnCajaAsignada(String nCajaAsignada) {
		this.nCajaAsignada = nCajaAsignada;
	}
	public String getsDescripcion() {
		return sDescripcion;
	}
	public void setsDescripcion(String sDescripcion) {
		this.sDescripcion = sDescripcion;
	}
	public String getnFolio() {
		return nFolio;
	}
	public void setnFolio(String nFolio) {
		this.nFolio = nFolio;
	}
	public String getdFechaCreacion() {
		return dFechaCreacion;
	}
	public void setdFechaCreacion(String dFechaCreacion) {
		this.dFechaCreacion = dFechaCreacion;
	}
	public String getThora() {
		return thora;
	}
	public void setThora(String thora) {
		this.thora = thora;
	}
	public String getnCompromisoAhorro() {
		return nCompromisoAhorro;
	}
	public void setnCompromisoAhorro(String nCompromisoAhorro) {
		this.nCompromisoAhorro = nCompromisoAhorro;
	}
	public String getsPeriodo() {
		return sPeriodo;
	}
	public void setsPeriodo(String sPeriodo) {
		this.sPeriodo = sPeriodo;
	}
	public String getsTutor() {
		return sTutor;
	}
	public void setsTutor(String sTutor) {
		this.sTutor = sTutor;
	}
	public String getnImporteAcumParaIDE() {
		return nImporteAcumParaIDE;
	}
	public void setnImporteAcumParaIDE(String nImporteAcumParaIDE) {
		this.nImporteAcumParaIDE = nImporteAcumParaIDE;
	}
	private long  INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String respuesta;
    private String INsTransaccion;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INsNumeroSocio;
    private String nCajaAsignada;
    private String sDescripcion;
    private String nFolio;
    private String dFechaCreacion;
    private String thora;
    private String nCompromisoAhorro;
    private String sPeriodo;
    private String sTutor;
    private String nImporteAcumParaIDE;
    
	@Override
	public String toString() {
		return "Encdatosficha_DetalleframedBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", respuesta=" + respuesta + ", INsTransaccion=" + INsTransaccion + ", INsUnionSocio="
				+ INsUnionSocio + ", INsCajaSocio=" + INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio
				+ ", INsNumeroSocio=" + INsNumeroSocio + ", nCajaAsignada=" + nCajaAsignada + ", sDescripcion="
				+ sDescripcion + ", nFolio=" + nFolio + ", dFechaCreacion=" + dFechaCreacion + ", thora=" + thora
				+ ", nCompromisoAhorro=" + nCompromisoAhorro + ", sPeriodo=" + sPeriodo + ", sTutor=" + sTutor
				+ ", nImporteAcumParaIDE=" + nImporteAcumParaIDE + "]";
	}
}
