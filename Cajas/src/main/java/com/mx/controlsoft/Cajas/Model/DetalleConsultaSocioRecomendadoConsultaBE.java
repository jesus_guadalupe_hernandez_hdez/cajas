package com.mx.controlsoft.Cajas.Model;

public class DetalleConsultaSocioRecomendadoConsultaBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumero() {
		return Numero;
	}
	public void setNumero(String numero) {
		Numero = numero;
	}
	public String getUnione() {
		return Unione;
	}
	public void setUnione(String unione) {
		Unione = unione;
	}
	public String getCajaPopular() {
		return CajaPopular;
	}
	public void setCajaPopular(String cajaPopular) {
		CajaPopular = cajaPopular;
	}
	public String getSucursal() {
		return Sucursal;
	}
	public void setSucursal(String sucursal) {
		Sucursal = sucursal;
	}
	public String getNumerodeSocio() {
		return NumerodeSocio;
	}
	public void setNumerodeSocio(String numerodeSocio) {
		NumerodeSocio = numerodeSocio;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellidoPaterno() {
		return ApellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		ApellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return ApellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		ApellidoMaterno = apellidoMaterno;
	}
	public String getFechaAlta() {
		return FechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		FechaAlta = fechaAlta;
	}
	public String getUnionR() {
		return UnionR;
	}
	public void setUnionR(String unionR) {
		UnionR = unionR;
	}
	public String getCajaPopularR() {
		return CajaPopularR;
	}
	public void setCajaPopularR(String cajaPopularR) {
		CajaPopularR = cajaPopularR;
	}
	public String getSucursalR() {
		return SucursalR;
	}
	public void setSucursalR(String sucursalR) {
		SucursalR = sucursalR;
	}
	public String getNumerodeSocioR() {
		return NumerodeSocioR;
	}
	public void setNumerodeSocioR(String numerodeSocioR) {
		NumerodeSocioR = numerodeSocioR;
	}
	public String getNombreS() {
		return NombreS;
	}
	public void setNombreS(String nombreS) {
		NombreS = nombreS;
	}
	public String getApellidoPaternoS() {
		return ApellidoPaternoS;
	}
	public void setApellidoPaternoS(String apellidoPaternoS) {
		ApellidoPaternoS = apellidoPaternoS;
	}
	public String getApellidoMaternoS() {
		return ApellidoMaternoS;
	}
	public void setApellidoMaternoS(String apellidoMaternoS) {
		ApellidoMaternoS = apellidoMaternoS;
	}
	private long id;
     private String Numero;
     private String Unione;
     private String CajaPopular;
     private String Sucursal;
     private String NumerodeSocio;
     private String Nombre;
     private String ApellidoPaterno;
     private String ApellidoMaterno;
     private String FechaAlta;
     private String UnionR;
     private String CajaPopularR;
     private String SucursalR;
     private String NumerodeSocioR;
     private String NombreS;
     private String ApellidoPaternoS;
     private String ApellidoMaternoS;
     
	@Override
	public String toString() {
		return "DetalleConsultaSocioRecomendadoConsultaBE [id=" + id + ", Numero=" + Numero + ", Unione=" + Unione
				+ ", CajaPopular=" + CajaPopular + ", Sucursal=" + Sucursal + ", NumerodeSocio=" + NumerodeSocio
				+ ", Nombre=" + Nombre + ", ApellidoPaterno=" + ApellidoPaterno + ", ApellidoMaterno=" + ApellidoMaterno
				+ ", FechaAlta=" + FechaAlta + ", UnionR=" + UnionR + ", CajaPopularR=" + CajaPopularR + ", SucursalR="
				+ SucursalR + ", NumerodeSocioR=" + NumerodeSocioR + ", NombreS=" + NombreS + ", ApellidoPaternoS="
				+ ApellidoPaternoS + ", ApellidoMaternoS=" + ApellidoMaternoS + "]";
	}
     
}
