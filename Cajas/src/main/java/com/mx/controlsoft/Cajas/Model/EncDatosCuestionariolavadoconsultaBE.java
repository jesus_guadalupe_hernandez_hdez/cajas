package com.mx.controlsoft.Cajas.Model;

public class EncDatosCuestionariolavadoconsultaBE {
	 public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursal() {
		return iNnIDSucursal;
	}
	public void setiNnIDSucursal(String iNnIDSucursal) {
		this.iNnIDSucursal = iNnIDSucursal;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getIDCajaAsignada() {
		return IDCajaAsignada;
	}
	public void setIDCajaAsignada(String iDCajaAsignada) {
		IDCajaAsignada = iDCajaAsignada;
	}
	public String getIDTipoTransaccion() {
		return IDTipoTransaccion;
	}
	public void setIDTipoTransaccion(String iDTipoTransaccion) {
		IDTipoTransaccion = iDTipoTransaccion;
	}
	public String getIDFicha() {
		return IDFicha;
	}
	public void setIDFicha(String iDFicha) {
		IDFicha = iDFicha;
	}
	public String getEjercicio() {
		return Ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		Ejercicio = ejercicio;
	}
	
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursal;
    private String iNsIDUsuario;
    private String respuesta;
    private String IDCajaAsignada;
    private String IDTipoTransaccion;
    private String IDFicha;
    private String Ejercicio;
    
	@Override
	public String toString() {
		return "EncDatosCuestionariolavadoconsultaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion="
				+ iNnIDUnion + ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursal=" + iNnIDSucursal
				+ ", iNsIDUsuario=" + iNsIDUsuario + ", respuesta=" + respuesta + ", IDCajaAsignada=" + IDCajaAsignada
				+ ", IDTipoTransaccion=" + IDTipoTransaccion + ", IDFicha=" + IDFicha + ", Ejercicio=" + Ejercicio
				+ "]";
	}
}
