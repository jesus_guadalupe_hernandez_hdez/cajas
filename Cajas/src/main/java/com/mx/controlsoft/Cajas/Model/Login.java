package com.mx.controlsoft.Cajas.Model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Login {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getUe() {
		return ue;
	}
	public void setUe(String ue) {
		this.ue = ue;
	}
	public String getsPwd() {
		return sPwd;
	}
	public void setsPwd(String sPwd) {
		this.sPwd = sPwd;
	}
	public int getNtries() {
		return ntries;
	}
	public void setNtries(int ntries) {
		this.ntries = ntries;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getnUnion() {
		return nUnion;
	}
	public void setnUnion(String nUnion) {
		this.nUnion = nUnion;
	}
	public String getnCaja() {
		return nCaja;
	}
	public void setnCaja(String nCaja) {
		this.nCaja = nCaja;
	}
	public String getnSucursal() {
		return nSucursal;
	}
	public void setnSucursal(String nSucursal) {
		this.nSucursal = nSucursal;
	}
	public String getsIDUsuario() {
		return sIDUsuario;
	}
	public void setsIDUsuario(String sIDUsuario) {
		this.sIDUsuario = sIDUsuario;
	}
	public int getCaduco() {
		return caduco;
	}
	public void setCaduco(int caduco) {
		this.caduco = caduco;
	}
	private long id;
	private String token;
	@NotNull(message = "Debes especificar el password")
	@Size(min = 1, max = 50, message = "Introduce la sucursal")
	private String sucursal;
	@NotNull(message = "Debes especificar el Usuario")
	@Size(min = 3, max = 50, message = "El usuario debe tener mas de 3 caracteres")
	private String ue;
	@NotNull(message = "Debes especificar el password")
	@Size(min = 1, max = 50, message = "Introduce el password")
	private String sPwd;
	private int ntries;
	private String mensaje;
	private String nUnion;
	private String nCaja;
	private String nSucursal;
	private String sIDUsuario;
	private int caduco;
	
	
	@Override
	public String toString() {
		return "Login [id=" + id + ", token=" + token + ", sucursal=" + sucursal + ", ue=" + ue + ", sPwd=" + sPwd
				+ ", ntries=" + ntries + ", mensaje=" + mensaje + ", nUnion=" + nUnion + ", nCaja=" + nCaja
				+ ", nSucursal=" + nSucursal + ", sIDUsuario=" + sIDUsuario + ", caduco=" + caduco + "]";
	}

}
