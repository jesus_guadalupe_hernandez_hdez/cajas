package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IFondoFijoBEDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetallefondofijoBE;

@Service
public class FondoFijoBEService {
	
	@Autowired
	IFondoFijoBEDao<RequestGralDetallefondofijoBE> service;
	
	public RequestGralDetallefondofijoBE getPostFondoFijoBE_Ini (RequestGralDetallefondofijoBE request ) {
		return service.getPostFondoFijoBE_Ini(request);
	}
}
