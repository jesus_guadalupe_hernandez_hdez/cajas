package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoConsultaBE;

public class RequestGralConsultaSocioRecomendadoConsultaBE {
	
	private RequestConsultaSocioRecomendadoConsultaBE request;

	public RequestConsultaSocioRecomendadoConsultaBE getRequest() {
		return request;
	}

	public void setRequest(RequestConsultaSocioRecomendadoConsultaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralConsultaSocioRecomendadoConsultaBE [request=" + request + "]";
	}
}
