package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetDatDetFicha_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.DetMensajesoc_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.EncDatDetFicha_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.EncMensaSoc_DetalleFichaBE;
import com.mx.controlsoft.Cajas.Model.Servicios_DetalleFichaBE;

public class RequesDetalleFichaBE {
	public List<EncDatDetFicha_DetalleFichaBE> getTtEncdatdetficha() {
		return ttEncdatdetficha;
	}
	public void setTtEncdatdetficha(List<EncDatDetFicha_DetalleFichaBE> ttEncdatdetficha) {
		this.ttEncdatdetficha = ttEncdatdetficha;
	}
	public List<DetDatDetFicha_DetalleFichaBE> getTtDetdatdetficha() {
		return ttDetdatdetficha;
	}
	public void setTtDetdatdetficha(List<DetDatDetFicha_DetalleFichaBE> ttDetdatdetficha) {
		this.ttDetdatdetficha = ttDetdatdetficha;
	}
	public List<Servicios_DetalleFichaBE> getTtservicios() {
		return ttservicios;
	}
	public void setTtservicios(List<Servicios_DetalleFichaBE> ttservicios) {
		this.ttservicios = ttservicios;
	}
	public List<EncMensaSoc_DetalleFichaBE> getTtEncmensasoc() {
		return ttEncmensasoc;
	}
	public void setTtEncmensasoc(List<EncMensaSoc_DetalleFichaBE> ttEncmensasoc) {
		this.ttEncmensasoc = ttEncmensasoc;
	}
	public List<DetMensajesoc_DetalleFichaBE> getTtdetmensajesoc() {
		return ttdetmensajesoc;
	}
	public void setTtdetmensajesoc(List<DetMensajesoc_DetalleFichaBE> ttdetmensajesoc) {
		this.ttdetmensajesoc = ttdetmensajesoc;
	}
	
	private List<EncDatDetFicha_DetalleFichaBE> ttEncdatdetficha;
	private List<DetDatDetFicha_DetalleFichaBE> ttDetdatdetficha;
	private List<Servicios_DetalleFichaBE> ttservicios;
	private List<EncMensaSoc_DetalleFichaBE> ttEncmensasoc;
	private List<DetMensajesoc_DetalleFichaBE> ttdetmensajesoc;
	
	@Override
	public String toString() {
		return "RequesDetalleFichaBE [ttEncdatdetficha=" + ttEncdatdetficha + ", ttDetdatdetficha=" + ttDetdatdetficha
				+ ", ttservicios=" + ttservicios + ", ttEncmensasoc=" + ttEncmensasoc + ", ttdetmensajesoc="
				+ ttdetmensajesoc + "]";
	}

}
