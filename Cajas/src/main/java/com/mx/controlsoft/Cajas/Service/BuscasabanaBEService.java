package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mx.controlsoft.Cajas.IDao.IBuscasabanaBEDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscasabanaBE;

@Service
public class BuscasabanaBEService {
	
	@Autowired 
	IBuscasabanaBEDao <RequestGralBuscasabanaBE> ser;
	
	public RequestGralBuscasabanaBE getBuscasabanaBE (RequestGralBuscasabanaBE request) {
		return ser.getBuscasabanaBE(request);
	}
	
}
