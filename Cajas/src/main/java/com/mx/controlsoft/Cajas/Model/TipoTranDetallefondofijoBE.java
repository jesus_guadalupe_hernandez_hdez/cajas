package com.mx.controlsoft.Cajas.Model;

public class TipoTranDetallefondofijoBE {
	 public long getTtNID() {
		return ttNID;
	}
	public void setTtNID(long ttNID) {
		this.ttNID = ttNID;
	}
	public String getTtTipTran() {
		return ttTipTran;
	}
	public void setTtTipTran(String ttTipTran) {
		this.ttTipTran = ttTipTran;
	}
	public String getTtDescTran() {
		return ttDescTran;
	}
	public void setTtDescTran(String ttDescTran) {
		this.ttDescTran = ttDescTran;
	}
	private long ttNID;
    private String ttTipTran;
    private String ttDescTran;
	@Override
	public String toString() {
		return "TipoTranDetallefondofijoBE [ttNID=" + ttNID + ", ttTipTran=" + ttTipTran + ", ttDescTran=" + ttDescTran
				+ "]";
	}
}
