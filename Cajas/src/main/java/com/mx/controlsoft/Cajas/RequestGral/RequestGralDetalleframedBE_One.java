package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestDetalleframedBE_One;

public class RequestGralDetalleframedBE_One {
	private RequestDetalleframedBE_One request;

	public RequestDetalleframedBE_One getRequest() {
		return request;
	}

	public void setRequest(RequestDetalleframedBE_One request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralDetalleframedBE_One [request=" + request + "]";
	}
	
}
