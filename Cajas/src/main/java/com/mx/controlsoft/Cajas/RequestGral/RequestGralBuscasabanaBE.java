package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestBuscasabanaBE;

public class RequestGralBuscasabanaBE {
	public RequestBuscasabanaBE getRequest() {
		return request;
	}

	public void setRequest(RequestBuscasabanaBE request) {
		this.request = request;
	}

	private RequestBuscasabanaBE request;

	@Override
	public String toString() {
		return "RequestGralBuscasabanaBE [request=" + request + "]";
	}
	
}
