package com.mx.controlsoft.Cajas.Model;

public class Encdatos_DetalleframedBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getINVerSaldos() {
		return INVerSaldos;
	}
	public void setINVerSaldos(String iNVerSaldos) {
		INVerSaldos = iNVerSaldos;
	}
	public String getINValidoHuella() {
		return INValidoHuella;
	}
	public void setINValidoHuella(String iNValidoHuella) {
		INValidoHuella = iNValidoHuella;
	}
	public String getINDespliegaMensaje() {
		return INDespliegaMensaje;
	}
	public void setINDespliegaMensaje(String iNDespliegaMensaje) {
		INDespliegaMensaje = iNDespliegaMensaje;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String respuesta;
    private String INsTransaccion;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INsNumeroSocio;
    private String INVerSaldos;
    private String INValidoHuella;
    private String INDespliegaMensaje;
    
	@Override
	public String toString() {
		return "Encdatos_DetalleframedBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", respuesta=" + respuesta + ", INsTransaccion=" + INsTransaccion + ", INsUnionSocio="
				+ INsUnionSocio + ", INsCajaSocio=" + INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio
				+ ", INsNumeroSocio=" + INsNumeroSocio + ", INVerSaldos=" + INVerSaldos + ", INValidoHuella="
				+ INValidoHuella + ", INDespliegaMensaje=" + INDespliegaMensaje + "]";
	}
}
