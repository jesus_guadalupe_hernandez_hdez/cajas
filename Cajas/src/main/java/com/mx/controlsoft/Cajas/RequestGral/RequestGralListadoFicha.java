package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestListadoficha;

public class RequestGralListadoFicha {
	private RequestListadoficha request ;

	public RequestListadoficha getRequest() {
		return request;
	}

	public void setRequest(RequestListadoficha request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralListadoFicha [request=" + request + "]";
	}
}
