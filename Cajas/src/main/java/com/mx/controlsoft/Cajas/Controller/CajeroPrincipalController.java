package com.mx.controlsoft.Cajas.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mx.controlsoft.Cajas.Model.EncFacturaElecFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultacortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosMonitorcajasBE;
import com.mx.controlsoft.Cajas.Model.FondoFijoTemporales;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestConsultaCorteCajaBE;
import com.mx.controlsoft.Cajas.Request.RequestDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Request.RequestEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Request.RequestFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.Request.RequestMonitorcajasBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaCorteCajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetallefondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralMonitorcajasBE;
import com.mx.controlsoft.Cajas.Service.BuscacortecajaBEService;
import com.mx.controlsoft.Cajas.Service.CajeroPrincipalBEService;
import com.mx.controlsoft.Cajas.Service.FondoFijoBEService;

@Controller
@RequestMapping(value = "/cajeroPrincipal")
public class CajeroPrincipalController {
	
	@Autowired 
	FondoFijoBEService service;
	
	@Autowired
	CajeroPrincipalBEService serv;
	
	@Autowired
	BuscacortecajaBEService _serv;
	
	@RequestMapping(value = "/detalleDesgloceFondoFijo/{idFondoFijo}",method = {RequestMethod.GET, RequestMethod.POST})
	public String desgloceFondoFijo (@PathVariable ("idFondoFijo") String  fileArchivo, Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		
		if (logginn != null) {
			
			model.addAttribute("usuario", logginn);
			
				        
	        //model.addAttribute("lstRequestGral", serv.getPostDetalleFondoAceptar(requetGralReq));
	        //model.addAttribute("temValores",fondoFijoTemporales);

			return "cajeroPrincipal/vistadetalleFondoFijo";

		}
		return "login";

	}

	@RequestMapping(value = "/detalleFondoFijoBE",method = { RequestMethod.POST})
	public String mostrarDetalleFondoFijoBE (@ModelAttribute FondoFijoTemporales fondoFijoTemporales, Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		
		System.out.println("Transaccion:" + fondoFijoTemporales.getEstadoTransaccion() + " caja : " + fondoFijoTemporales.getCajaCajero() + " importe: " + fondoFijoTemporales.getTxtImporte() );
		
		if (logginn != null) {
			
			model.addAttribute("usuario", logginn);
			
			EncdatosEfectivofondofijoBE detalleFondoFijo = new EncdatosEfectivofondofijoBE();
			detalleFondoFijo.setINid(1);
			detalleFondoFijo.setINnIDUnion(logginn.getnUnion());
	        detalleFondoFijo.setINnIDCajaPopular(logginn.getnCaja());
	        detalleFondoFijo.setINnIDSucursa(logginn.getnSucursal());
	        //detalleFondoFijo.setINsIDUsuario(logginn.getUe());
	        detalleFondoFijo.setINsIDUsuario("fUadqcjiqXGmmdpN");
	        detalleFondoFijo.setiNTransaccion(fondoFijoTemporales.getEstadoTransaccion());
	        detalleFondoFijo.setiNCajaDestino(fondoFijoTemporales.getCajaCajero());
	        detalleFondoFijo.setiNImporte(fondoFijoTemporales.getTxtImporte());
	        detalleFondoFijo.setREQUEST_METHOD("GET");
	        
	        List<EncdatosEfectivofondofijoBE> lstEncDatos = new ArrayList<>();
	        lstEncDatos.add(detalleFondoFijo);
	        
	        RequestGralEfectivofondofijoBE requetGralReq = new RequestGralEfectivofondofijoBE();
	        RequestEfectivofondofijoBE requetFonFij = new RequestEfectivofondofijoBE();
	        requetFonFij.setTtEncdatos(lstEncDatos);
	        requetGralReq.setRequest(requetFonFij);
	        
	        model.addAttribute("lstRequestGral", serv.getPostDetalleFondoAceptar(requetGralReq));
	        model.addAttribute("temValores",fondoFijoTemporales);

			return "cajeroPrincipal/detalleFondoFijo";

		}
		return "login";

	}
	
	@RequestMapping(value = "/corteCaja",method = RequestMethod.GET)
	public String mostrarPrincipal(Model model ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "deposito/depositoLst";
		}
		return "login";
		
	}
	
	@RequestMapping(value = "/sabanaMovimiento",method = RequestMethod.GET)
	public String mostrarSabanaMovimiento(Model model ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			return "cajeroPrincipal/sabanaMovimiento";
		}
		return "login";
		
	}
	
	@RequestMapping(value = "/fondoFijo",method = RequestMethod.GET)
	public String mostrarFondoFijo(Model model ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			
			EncdatosDetallefondofijoBE encDat = new EncdatosDetallefondofijoBE();
			encDat.setINid(1);
			encDat.setINnIDUnion(logginn.getnUnion());
			encDat.setINnIDCajaPopular(logginn.getnCaja());
			encDat.setINnIDSucursa(logginn.getnSucursal());
			encDat.setINsIDUsuario(logginn.getUe());
			encDat.setiNMaintOption("INICIALIZA");
			
			List<EncdatosDetallefondofijoBE> lstEncdatosDetallefondofijoBE = new ArrayList<>();
			lstEncdatosDetallefondofijoBE.add(encDat);
			
			RequestDetallefondofijoBE r = new RequestDetallefondofijoBE();
			r.setTtEncdatos(lstEncdatosDetallefondofijoBE);
			
			RequestGralDetallefondofijoBE  reGral = new RequestGralDetallefondofijoBE();
			reGral.setRequest(r);
			
			RequestGralDetallefondofijoBE resp = service.getPostFondoFijoBE_Ini(reGral); 
			
			model.addAttribute("lstRequestGral", resp);
			model.addAttribute("fondoFijoTemporales", new FondoFijoTemporales());
			return "cajeroPrincipal/fondoFijo";
		}
		return "login";
		
	}
	
	@RequestMapping(value = "/facturacionelectronicaIniPDF_XML",method = RequestMethod.GET)
	public String mostrarfacturacionelectronicaIniBE(Model model ,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			EncFacturaElecFacturacionelectronicaIniBE enc = new EncFacturaElecFacturacionelectronicaIniBE();
			enc.setINid(0);
			enc.setINtoken("");
			enc.setINnIDUnion(logginn.getnUnion());
			enc.setINnIDCajaPopular(logginn.getnCaja());
			enc.setINnIDSucursal(logginn.getSucursal());
			
			List<EncFacturaElecFacturacionelectronicaIniBE> lstEnc = new ArrayList<>();
			lstEnc.add(enc);
			
			RequestFacturacionelectronicaIniBE reqFacturaIni = new RequestFacturacionelectronicaIniBE();
			reqFacturaIni.setTt_EncFacturaElec(lstEnc);
			
			RequestGralFacturacionelectronicaIniBE reqGral = new RequestGralFacturacionelectronicaIniBE();
			reqGral.setRequest(reqFacturaIni);
			
			RequestGralFacturacionelectronicaIniBE reqSetGral = serv.getPostFacturacionelectronicaIniBE(reqGral);
			model.addAttribute("lstRequestGral", reqSetGral);
			
			return "cajeroPrincipal/pdfXmlFacturacionElectronica";
		}
		return "login";
	}
	
	@RequestMapping(value = "/estatusfacturasucursalJS",method = RequestMethod.GET)
	public String mostrarconsultaestatusfacturasucursalJSBE(Model model ,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			EncFacturaElecFacturacionelectronicaIniBE enc = new EncFacturaElecFacturacionelectronicaIniBE();
			enc.setINid(0);
			enc.setINtoken("");
			enc.setINnIDUnion(logginn.getnUnion());
			enc.setINnIDCajaPopular(logginn.getnCaja());
			enc.setINnIDSucursal(logginn.getSucursal());
			
			List<EncFacturaElecFacturacionelectronicaIniBE> lstEnc = new ArrayList<>();
			lstEnc.add(enc);
			
			RequestFacturacionelectronicaIniBE reqFacturaIni = new RequestFacturacionelectronicaIniBE();
			reqFacturaIni.setTt_EncFacturaElec(lstEnc);
			
			RequestGralFacturacionelectronicaIniBE reqGral = new RequestGralFacturacionelectronicaIniBE();
			reqGral.setRequest(reqFacturaIni);
			
			RequestGralFacturacionelectronicaIniBE reqSetGral = serv.getPostFacturacionelectronicaIniBE(reqGral);
			model.addAttribute("lstRequestGral", reqSetGral);
			
			return "cajeroPrincipal/estatusfacturasucursalJS";
		}
		return "login";
	}
	
	@RequestMapping(value = "/monitoreoCaja",method = RequestMethod.GET)
	public String mostrarMonitoreoCaja(Model model ,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			EncdatosMonitorcajasBE enc = new EncdatosMonitorcajasBE();
			enc.setINid(0);
			enc.setINtoken("");
			enc.setINnIDUnion(logginn.getnUnion());
			enc.setINnIDCajaPopular(logginn.getnCaja());
			enc.setINnIDSucursa (logginn.getnSucursal());

			//enc.setINsIDUsuario(logginn.getUe());
			enc.setINsIDUsuario("lClzdbkdcaaFaJkl");
			
			
			List<EncdatosMonitorcajasBE> lstEnc = new ArrayList<>();
			lstEnc.add(enc);
			
			RequestMonitorcajasBE reqMon = new RequestMonitorcajasBE();
			reqMon.setTtEncdatos (lstEnc);
			
			RequestGralMonitorcajasBE reqGral = new RequestGralMonitorcajasBE();
			reqGral.setRequest(reqMon);
			
			RequestGralMonitorcajasBE reqSetGral = serv.getPostMonitorcajasBE (reqGral);
			model.addAttribute("lstRequestGral", reqSetGral);
			model.addAttribute("usuario", logginn);
			
			return "cajeroPrincipal/monitorCaja";
		}
		return "login";
	}
	
	@RequestMapping(value = "/consultarcortaCaja",method = RequestMethod.GET)
	public String mostrarconsultarcorteCajas(Model model ,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
		
			model.addAttribute("usuario", logginn);
			
			return "cajeroPrincipal/consultaCorteCaja";
		}
		return "login";
	}
	
	@RequestMapping(value = "/consultarcortacajaDetalle",params = { "rowidcortecaja" }, method = RequestMethod.GET)
	public String mostrarconsultarcorteCajasDetalle( @RequestParam("rowidcortecaja") String rowidcortecaja,Model model ,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			EncdatosConsultacortecajaBE enc = new EncdatosConsultacortecajaBE();
			enc.setiNid(1);
			enc.setiNnIDUnion(logginn.getnUnion());
			enc.setiNnIDCajaPopular(logginn.getnCaja());
			enc.setiNnIDSucursa(logginn.getnSucursal());
			enc.setiNrowidcortecaja(rowidcortecaja);
			List<EncdatosConsultacortecajaBE> lstEnc = new ArrayList<>();
			lstEnc.add(enc);
			RequestConsultaCorteCajaBE reqCore = new RequestConsultaCorteCajaBE();
			reqCore.setTtEncdatos(lstEnc);
			
			RequestGralConsultaCorteCajaBE reGral = new RequestGralConsultaCorteCajaBE();
			reGral.setRequest(reqCore);
			
			RequestGralConsultaCorteCajaBE reGralFinal =  _serv.getConsultacortecajaBE(reGral);
			
			model.addAttribute("usuario", logginn);
			model.addAttribute("lstRequestGral", reGralFinal);
			
			return "cajeroPrincipal/consultacortecajaDetalle";
		}
		return "login";
	}
}
