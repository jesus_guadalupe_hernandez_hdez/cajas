package com.mx.controlsoft.Cajas.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.controlsoft.Cajas.Model.DealleCancelConsCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Model.DetFicha_FichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Model.DetalleConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.Model.Detficha_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.EncafichasListadoficha;
import com.mx.controlsoft.Cajas.Model.EncdatosCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultasociorecomendadoconsultaBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.ListafichasData;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Model.objectTem;
import com.mx.controlsoft.Cajas.Request.RequestCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.Request.RequestFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.Request.RequestFichasxServicioenSucursalBE_Inicio;
import com.mx.controlsoft.Cajas.Request.RequestReporteListadoficha;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralReporteListadoficha;
import com.mx.controlsoft.Cajas.Service.ReporteFichasService;
import com.mx.controlsoft.Cajas.Service.ReporteFichasxservicioensucursalBE_Service;
import com.mx.controlsoft.Cajas.Utilerias.CrearExcel;

@Controller
@RequestMapping(value = "/ajax_Excel")
public class Ajax_Excel {
	
private static final Logger logger = LogManager.getLogger(Ajax_Excel.class);
	
	@Autowired
	ReporteFichasxservicioensucursalBE_Service service;
	
	@Autowired
	ReporteFichasService service_reportes;
	
	//String temp = "C:\\Users\\winnie\\Desktop\\ejemplo\\";
	String temp = "C:\\Users\\dmx\\Documents\\temporal\\";
	
	@ResponseBody
	@RequestMapping(value = "/downloadListadoFichaPost",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public fechasRecibe mostrarPrincipal(@RequestBody fechasRecibe  encFichaServicio ,final HttpServletRequest request, final HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralReporteListadoficha req = new RequestGralReporteListadoficha();
			
			EncafichasListadoficha enc = new EncafichasListadoficha(); 
			enc.setId(1);
			enc.setnUnion(logginn.getnUnion());
			enc.setnCaja(logginn.getnCaja());
			enc.setnSucursal(logginn.getnSucursal());
			enc.setFechaReporte(encFichaServicio.getInicia());
			enc.setCajaAsignadaReporte(encFichaServicio.getCaja());
			enc.setsCaja("");
			enc.setsSucursal("");
			enc.setTitulo("");
			enc.setFiltro("");
			
			List<EncafichasListadoficha> lstFichasxServicio = new LinkedList<>();
			lstFichasxServicio.add(enc);
			
			RequestReporteListadoficha reqFicha = new RequestReporteListadoficha();
			reqFicha.setTtencafichas  (lstFichasxServicio);
			req.setRequest(reqFicha);
			
			RequestGralReporteListadoficha requestGralFichaSucursal= service_reportes.getConsultaListadoficha(req) ; //(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			
			fechasRecibe temporalName = new fechasRecibe();
			java.util.Date fecha = new Date();
			String strFecha = fecha.toLocaleString();
			strFecha = strFecha.replaceAll ("/", "-");
			strFecha = strFecha.replaceAll (":", "-");
			strFecha = strFecha + "-ReporteListadoFicha.xls";
			
			Map<String, Object[]> datos = new TreeMap<String, Object[]>();
			logger.info("crearo el array para llenado "  );
			
			
			int i=0;
			for (ListafichasData det : requestGralFichaSucursal.getRequest().getTtlistafichas()) {
				if (i == 0) {
					datos.put( String.valueOf(i), new Object[]{"Numero","Ficha ", "Socio", "Nombre","Servicio","Cargos","Abonos"
							, "Saldo Anterior"});
				}
				else
				{
					datos.put( String.valueOf(i), new Object[]{String.valueOf(i) ,det.getsFicha(), det.getsSocio(),
							det.getsNombre(),det.getServicio(),det.getCargo(),det.getAbono(),
							det.getSaldo()});
				}
				i++;
				
			}
			logger.info("Termine de llenar el arreglo y se mandara a la creación del doc "  );
			CrearExcel.getDataExcel(datos, (temp + strFecha));
			logger.info("Ruta donde se creo: " + (temp+strFecha) );
			logger.info("se creo el documento y se crea bufer para descarga "  );
			// return requestGralFichaSucursal;
			temporalName.setNombreArchivo(strFecha);
			//return fe;
			
			return temporalName;
		}
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "downloadCancelarFichaCorreoPost",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public fechasRecibe mostrarDataCancelaFichaCorreo(@RequestBody objectTem  objecttem ,final HttpServletRequest request, final HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			logger.info("Valor recibido caja" + objecttem);
			
			RequestGralCancelaFichaCorreoConsultaBE req = new RequestGralCancelaFichaCorreoConsultaBE();
			EncdatosCancelaFichaCorreoConsultaBE enc = new EncdatosCancelaFichaCorreoConsultaBE(); 
			enc.setINid(1);
			enc.setINtoken("");
			enc.setINnIDUnion(objecttem.getUnion());
			enc.setINnIDCajaPopular(objecttem.getCajaPupular());
			enc.setINnIDSucursal(objecttem.getSucursal());
			enc.setINsIDUsuario("lClzdbkdcaaFaJkl");
			enc.setINnEjercisio(objecttem.getEjercisio());
			enc.setINsAccion("realizarConsulta");
			enc.setINnIDCajaAsignadaCorreo(objecttem.getCajaAsignada());
			enc.setINnIDFichacorreo("");
			enc.setINnIDTipoTransaccionCorreo("");
			enc.setRespuesta("");
			
			List<EncdatosCancelaFichaCorreoConsultaBE> lstcancelaFichaCorreo = new LinkedList<>();
			lstcancelaFichaCorreo.add(enc);
			
			RequestCancelaFichaCorreoConsultaBE reqFicha = new RequestCancelaFichaCorreoConsultaBE();
			reqFicha.setTtEncdatos(lstcancelaFichaCorreo);
			
			req.setRequest(reqFicha);
			
			RequestGralCancelaFichaCorreoConsultaBE requestGralFichaSucursal= service_reportes.getConsultaCancelaFichaCorreoConsultaBE(req) ; //(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			
			fechasRecibe temporalName = new fechasRecibe();
			java.util.Date fecha = new Date();
			String strFecha = fecha.toLocaleString();
			strFecha = strFecha.replaceAll ("/", "-");
			strFecha = strFecha.replaceAll (":", "-");
			strFecha = strFecha + "-CancelarFichaCorreo.xls";
			
			Map<String, Object[]> datos = new TreeMap<String, Object[]>();
			logger.info("crearo el array para llenado "  );
			
			int i=1;
			for (DealleCancelConsCancelaFichaCorreoConsultaBE det : requestGralFichaSucursal.getRequest().getTtDealleCancelCons()) {
				if (i == 1) {
					datos.put( String.valueOf(i), new Object[]{"Numero ", "Sucursal", "Ejercicio","Caja Asignada Correo","Tipo Transacción","Ficha Correo"
							, "Caja Asignada","Tipo Transacción","Ficha","Fecha Ficha","Usuario","Hora","Estado Ficha","Cancelar Ficha"});
				}
				else
				{
					datos.put( String.valueOf(i), new Object[]{String.valueOf(i) ,det.getSucursal(), det.getEjercicio(),
							det.getCajaAsignadaCorreo(),det.getTipoTransaccionCorreo(),det.getFichaCorreo(),det.getCajaAsignada(),
							det.getTipoTransaccion(),det.getFicha(),det.getFechaFicha(),det.getUsuario(),det.getHora(),det.getEstadoFicha(),
							det.getCancelarFicha()});

				}
				i++;
				
			}
			logger.info("Termine de llenar el arreglo y se mandara a la creación del doc "  );
			CrearExcel.getDataExcel(datos, (temp + strFecha));
			logger.info("Ruta donde se creo: " + (temp+strFecha) );
			logger.info("se creo el documento y se crea bufer para descarga "  );
			// return requestGralFichaSucursal;
			temporalName.setNombreArchivo(strFecha);
			//return fe;
			
			return temporalName;
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/downloadConsultaSocioRecomendado",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public fechasRecibe mostrarConsultaSocioRecomendadoConsultaBE(@RequestBody fechasRecibe fe,final HttpServletRequest request, final HttpServletResponse response ) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralConsultaSocioRecomendadoConsultaBE req = new RequestGralConsultaSocioRecomendadoConsultaBE();
			fechasRecibe temporalName = new fechasRecibe();
			EncdatosConsultasociorecomendadoconsultaBE  enc = new EncdatosConsultasociorecomendadoconsultaBE();
			
			System.out.println("valor fecha ini: " + enc.getINsFechaIni());
			System.out.println("valor fecha fin: " + enc.getINsFechaIni());
			
			enc.setINid(1);
			enc.setINtoken("");
			enc.setINnIDUnion("1");
			enc.setINnIDCajaPopular("8");
			enc.setINnIDSucursal("1");
			enc.setINsIDUsuario("root");
			enc.setRespuesta("");
			enc.setINsFechaIni(fe.getInicia());
			enc.setINsFechafin(fe.getTermina());
			//enc.setINsFechaIni("10/01/2020");
			//enc.setINsFechafin("29/04/2020");
			enc.setTitulo("");
			
			List<EncdatosConsultasociorecomendadoconsultaBE> lstConsultasociorecomendadoconsultaBE = new LinkedList<>();
			lstConsultasociorecomendadoconsultaBE.add(enc);
			RequestConsultaSocioRecomendadoConsultaBE reqFicha = new RequestConsultaSocioRecomendadoConsultaBE();
			reqFicha.setTtEncdatos(lstConsultasociorecomendadoconsultaBE);
			req.setRequest(reqFicha);
			RequestGralConsultaSocioRecomendadoConsultaBE requestGralFichaSucursal= service_reportes.getConsultaSocioRecomendadoConsultaBE(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			
			java.util.Date fecha = new Date();
			String strFecha = fecha.toLocaleString();
			strFecha = strFecha.replaceAll ("/", "-");
			strFecha = strFecha.replaceAll (":", "-");
			strFecha = strFecha + "-SocioRecomendado.xls";
			
			Map<String, Object[]> datos = new TreeMap<String, Object[]>();
			logger.info("crearo el array para llenado "  );
			
			int i=1;
			for (DetalleConsultaSocioRecomendadoConsultaBE det : requestGralFichaSucursal.getRequest().getTtDetalle()) {
				if (i == 1) {
					datos.put( String.valueOf(i), new Object[]{"Numero ", "Unión", "Caja popular","Sucursal","Numero de Socio","Nombre","Apellido Paterno","Apellido Materno","Fecha Alta","Unión","Caja Popular","Sucursal","Numero de Socio que recomendo","Nombre","Apellido Paterno","Apellido Materno"});
				}
				else
				{
			
					datos.put( String.valueOf(i), new Object[]{String.valueOf(i) ,det.getNumero(), det.getUnione(),
							det.getCajaPopular(),det.getSucursal(),det.getNumerodeSocio(),det.getNombre(),det.getApellidoPaterno()
							,det.getApellidoMaterno(),det.getFechaAlta(),det.getUnionR(),det.getCajaPopularR(),det.getSucursalR(),
							det.getNumerodeSocioR(),det.getNombreS(),det.getApellidoPaternoS(),det.getApellidoMaternoS()});
				}
				i++;
				
			}
			logger.info("Termine de llenar el arreglo y se mandara a la creación del doc "  );
			CrearExcel.getDataExcel(datos, (temp + strFecha));
			logger.info("Ruta donde se creo: " + (temp+strFecha) );
			logger.info("se creo el documento y se crea bufer para descarga "  );
			// return requestGralFichaSucursal;
			temporalName.setNombreArchivo(strFecha);
			//return fe;
			
			return temporalName;
		}
		return null;
		//return requestGralFichaSucursal;
	}
	
	@ResponseBody
	@RequestMapping(value = "/downloadFichasconHorarioxSucursal",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public fechasRecibe mostrarFichasconHorarios(@RequestBody Encdatos_FichasconHorarioxSucBE  enc ,final HttpServletRequest request, final HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralFichasconHorarioxSucBE req = new RequestGralFichasconHorarioxSucBE();
			fechasRecibe temporalName = new fechasRecibe();
			
			enc.setiNid(1);
			enc.setiNtoken("");
			enc.setiNnIDUnion(logginn.getnUnion());
			enc.setiNnIDCajaPopular(logginn.getnCaja());
			enc.setiNnIDSucursal(logginn.getnSucursal());
			enc.setiNsIDUsuario(logginn.getUe());
			enc.setiNsAccion("Genera");
			enc.setsNombreCaja("");
			enc.setsRFCCaja("");
			enc.setFecha("");
			enc.setRespuesta("");
			
			List<Encdatos_FichasconHorarioxSucBE> lstFichasxHorario = new LinkedList<>();
			lstFichasxHorario.add(enc);
			RequestFichasconHorarioxSucBE reqFicha = new RequestFichasconHorarioxSucBE();
			reqFicha.setTtEncdatos(lstFichasxHorario);
			req.setRequest(reqFicha);
			RequestGralFichasconHorarioxSucBE requestGralFichaSucursal= service_reportes.getFichasconHorarioxSucBE(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			
			java.util.Date fecha = new Date();
			String strFecha = fecha.toLocaleString();
			strFecha = strFecha.replaceAll ("/", "-");
			strFecha = strFecha.replaceAll (":", "-");
			strFecha = strFecha + "-FichasconHorario.xls";
			
			Map<String, Object[]> datos = new TreeMap<String, Object[]>();
			logger.info("crearo el array para llenado "  );
			
			int i=1;
			for (DetFicha_FichasconHorarioxSucBE det : requestGralFichaSucursal.getRequest().getTtDetFicha()) {
				if (i == 1) {
					datos.put( String.valueOf(i), new Object[]{"Sucursal", "Cajero", "Tipo Transacción","Ficha","Fecha","Hora"});
				}
				else
				{
			
					datos.put( String.valueOf(i), new Object[]{det.getiDSucursal() ,det.getiDCajaAsignada(), det.getiDTipotransaccion(),
							det.getiDFicha(),det.getFechaFicha(),det.getHora()});
				}
				i++;
			}
			logger.info("Termine de llenar el arreglo y se mandara a la creación del doc "  );
			CrearExcel.getDataExcel(datos, (temp + strFecha));
			logger.info("Ruta donde se creo: " + (temp+strFecha) );
			logger.info("se creo el documento y se crea bufer para descarga "  );
			// return requestGralFichaSucursal;
			temporalName.setNombreArchivo(strFecha);
			//return fe;
			
			return temporalName;
		}
		return null;
	}
	
	@RequestMapping(value = "/downloadFichas/{fileArchivo}", method = {RequestMethod.GET})
	public void downloadFicha (@PathVariable ("fileArchivo") String  fileArchivo, final HttpServletRequest request, final HttpServletResponse response) {
		File file = new File ((temp + fileArchivo) );
        
        try (InputStream fileInputStream = new FileInputStream(file);
                OutputStream output = response.getOutputStream();) {

            response.reset();

            response.setContentType("application/octet-stream");
            response.setContentLength((int) (file.length()));

            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

            IOUtils.copyLarge(fileInputStream, output);
            output.flush();
            logger.info("Termine de crear el buffer y esta listo para descargar"  );
            file.delete();
        } catch (IOException e) {
        	logger.info("error trono " + e.getMessage() );
        }	
	}
	
	@ResponseBody
	@RequestMapping(value = "/downloadFichasServSuc", method = {RequestMethod.GET,RequestMethod.POST },headers = {"content-type=application/json"})
    public fechasRecibe llenaArchivo( @RequestBody fechasRecibe  fe ,final HttpServletRequest request, final HttpServletResponse response)  {
		logger.info("Paso 1" );
		RequestGralFichasxServicioenSucursalBE req = new RequestGralFichasxServicioenSucursalBE();
		Encdatos_FichasxServicioenSucursalBE  encFichaServicio = new Encdatos_FichasxServicioenSucursalBE();
		HttpSession sesion = request.getSession();
		java.util.Date fecha = new Date();
		String strFecha = fecha.toLocaleString();
		strFecha = strFecha.replaceAll ("/", "-");
		strFecha = strFecha.replaceAll (":", "-");
		strFecha = strFecha + ".xls";
		
		
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			encFichaServicio.setiNid(1);
			encFichaServicio.setiNtoken("");
			encFichaServicio.setiNnIDUnion(logginn.getnUnion());
			encFichaServicio.setiNnIDCajaPopular(logginn.getnCaja());
			encFichaServicio.setiNnIDSucursal(logginn.getnSucursal());
			encFichaServicio.setiNsIDUsuario(logginn.getUe());
			encFichaServicio.setRespuesta("");
			encFichaServicio.setiNGenerar("Generar el Reporte");
			encFichaServicio.setiNMenu("");
			encFichaServicio.setiNCajero(fe.getCaja());
			encFichaServicio.setnIDCajeroInicio("1");
			encFichaServicio.setiNServicio(fe.getServicio());
			encFichaServicio.setiNsFechaInicial(fe.getInicia());
			encFichaServicio.setiNsFechaFinal(fe.getTermina());
			encFichaServicio.setsNombreCaja("");
			encFichaServicio.setsNombreSucursal("");
			encFichaServicio.setFecha("");
			encFichaServicio.setTermino("");
			List<Encdatos_FichasxServicioenSucursalBE> lstFichasxServicio = new LinkedList<>();
			lstFichasxServicio.add(encFichaServicio);
			RequestFichasxServicioenSucursalBE_Inicio reqFicha = new RequestFichasxServicioenSucursalBE_Inicio();
			reqFicha.setTtEncdatos(lstFichasxServicio);
			req.setRequest(reqFicha);
			RequestGralFichasxServicioenSucursalBE requestGralFichaSucursal= service.getFichasxservicioensucursalBE_ReportesInicio(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();

			Map<String, Object[]> datos = new TreeMap<String, Object[]>();
			logger.info("crearo el array para llenado "  );
			
			int i=1;
			for (Detficha_FichasxServicioenSucursalBE det : requestGralFichaSucursal.getRequest().getTtDetficha()) {
				if (i == 1) {
					datos.put( String.valueOf(i), new Object[]{"Id", "Caja Asignada", "Tipo Transacción","Ficha","Usuario","Fecha","Hora",
							"Suc Socio","Id Socio", "Nombre Socio","Servicio","Cargo","Bono"});
				}
				else
				{
					datos.put( String.valueOf(i), new Object[]{det.getIdSucursal(),det.getIdCajaAsignada(), det.getIdTipoTransaccion(),
							det.getiDFicha(),det.getUsuario(),det.getFecha(),det.getHora(),
							det.getSucsocio(),det.getiDSocio(), det.getNomsocio(),det.getServicio(),det.getCargo(),det.getAbono()});
				}
				i++;
			}
			logger.info("Termine de llenar el arreglo y se mandara a la creación del doc "  );
			CrearExcel.getDataExcel(datos, (temp + strFecha));
			logger.info("Ruta donde se creo: " + (temp+strFecha) );
			logger.info("se creo el documento y se crea bufer para descarga "  );
			// return requestGralFichaSucursal;
			fe.setNombreArchivo(strFecha);
			return fe;
		}
	
		logger.info("Paso 1.0 no entre por que no hay usuario logueado" );
		return null;

    }
}

