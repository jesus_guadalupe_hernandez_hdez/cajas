package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DocumentoDocumentoscortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncabezadoDocumentoDocumentoscortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDocumentoscortecajaBE;

public class RequestDocumentoscortecajaBE {
	public List<EncdatosDocumentoscortecajaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosDocumentoscortecajaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DocumentoDocumentoscortecajaBE> getTtDocumento() {
		return ttDocumento;
	}
	public void setTtDocumento(List<DocumentoDocumentoscortecajaBE> ttDocumento) {
		this.ttDocumento = ttDocumento;
	}
	public List<EncabezadoDocumentoDocumentoscortecajaBE> getTtEncabezadoDocumento() {
		return ttEncabezadoDocumento;
	}
	public void setTtEncabezadoDocumento(List<EncabezadoDocumentoDocumentoscortecajaBE> ttEncabezadoDocumento) {
		this.ttEncabezadoDocumento = ttEncabezadoDocumento;
	}
	private List<EncdatosDocumentoscortecajaBE> ttEncdatos;
	private List<DocumentoDocumentoscortecajaBE> ttDocumento;
	private List<EncabezadoDocumentoDocumentoscortecajaBE> ttEncabezadoDocumento;
	@Override
	public String toString() {
		return "RequestDocumentoscortecajaBE [ttEncdatos=" + ttEncdatos + ", ttDocumento=" + ttDocumento
				+ ", ttEncabezadoDocumento=" + ttEncabezadoDocumento + "]";
	}
}
