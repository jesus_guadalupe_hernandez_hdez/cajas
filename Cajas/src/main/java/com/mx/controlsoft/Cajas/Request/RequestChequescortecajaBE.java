package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.BancoChequescortecajaBE;
import com.mx.controlsoft.Cajas.Model.ChequeChequesCorteCajaBE;
import com.mx.controlsoft.Cajas.Model.EncDatosChequesCorteCajaBE;
import com.mx.controlsoft.Cajas.Model.EncabezadoChequeChequescortecajaBE;

public class RequestChequescortecajaBE {
	public List<EncDatosChequesCorteCajaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncDatosChequesCorteCajaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<ChequeChequesCorteCajaBE> getTtCheque() {
		return ttCheque;
	}
	public void setTtCheque(List<ChequeChequesCorteCajaBE> ttCheque) {
		this.ttCheque = ttCheque;
	}
	public List<EncabezadoChequeChequescortecajaBE> getTtEncabezadoCheque() {
		return ttEncabezadoCheque;
	}
	public void setTtEncabezadoCheque(List<EncabezadoChequeChequescortecajaBE> ttEncabezadoCheque) {
		this.ttEncabezadoCheque = ttEncabezadoCheque;
	}
	public List<BancoChequescortecajaBE> getTtBanco() {
		return ttBanco;
	}
	public void setTtBanco(List<BancoChequescortecajaBE> ttBanco) {
		this.ttBanco = ttBanco;
	}
	private List<EncDatosChequesCorteCajaBE> ttEncdatos;
	private List<ChequeChequesCorteCajaBE> ttCheque;
	private List<EncabezadoChequeChequescortecajaBE> ttEncabezadoCheque;
	private List<BancoChequescortecajaBE> ttBanco;
	@Override
	public String toString() {
		return "RequestChequescortecajaBE [ttEncdatos=" + ttEncdatos + ", ttCheque=" + ttCheque
				+ ", ttEncabezadoCheque=" + ttEncabezadoCheque + ", ttBanco=" + ttBanco + "]";
	}
}
