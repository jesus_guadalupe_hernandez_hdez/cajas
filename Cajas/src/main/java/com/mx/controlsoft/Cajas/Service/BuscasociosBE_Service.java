package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IBuscasociosBE_Dao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDepositosBuscaSociosBE;

@Service
public class BuscasociosBE_Service {
	
	@Autowired
	IBuscasociosBE_Dao<RequestGralDepositosBuscaSociosBE> buscaSocio;
	
	public BuscasociosBE_Service(IBuscasociosBE_Dao<RequestGralDepositosBuscaSociosBE> buscaSocio) {
		this.buscaSocio = buscaSocio;
	}
	
	public RequestGralDepositosBuscaSociosBE getListadoSurcusalMenu (RequestGralDepositosBuscaSociosBE request) {
		return  this.buscaSocio.getBuscaSociosBE(request);
	}
	
}
