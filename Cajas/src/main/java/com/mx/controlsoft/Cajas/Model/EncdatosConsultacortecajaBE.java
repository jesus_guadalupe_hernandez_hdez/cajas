package com.mx.controlsoft.Cajas.Model;

public class EncdatosConsultacortecajaBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursa() {
		return iNnIDSucursa;
	}
	public void setiNnIDSucursa(String iNnIDSucursa) {
		this.iNnIDSucursa = iNnIDSucursa;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getiNrowidcortecaja() {
		return iNrowidcortecaja;
	}
	public void setiNrowidcortecaja(String iNrowidcortecaja) {
		this.iNrowidcortecaja = iNrowidcortecaja;
	}
	public String getCheques() {
		return cheques;
	}
	public void setCheques(String cheques) {
		this.cheques = cheques;
	}
	public String getChequesModal() {
		return chequesModal;
	}
	public void setChequesModal(String chequesModal) {
		this.chequesModal = chequesModal;
	}
	public String getDocumentos() {
		return Documentos;
	}
	public void setDocumentos(String documentos) {
		Documentos = documentos;
	}
	public String getDocumentosModal() {
		return DocumentosModal;
	}
	public void setDocumentosModal(String documentosModal) {
		DocumentosModal = documentosModal;
	}
	public String getiDsTitulo() {
		return iDsTitulo;
	}
	public void setiDsTitulo(String iDsTitulo) {
		this.iDsTitulo = iDsTitulo;
	}
	public String getiDTotalefectivo() {
		return iDTotalefectivo;
	}
	public void setiDTotalefectivo(String iDTotalefectivo) {
		this.iDTotalefectivo = iDTotalefectivo;
	}
	public String getiDTotalGeneral() {
		return iDTotalGeneral;
	}
	public void setiDTotalGeneral(String iDTotalGeneral) {
		this.iDTotalGeneral = iDTotalGeneral;
	}
	public String getiDiNIDResultado() {
		return iDiNIDResultado;
	}
	public void setiDiNIDResultado(String iDiNIDResultado) {
		this.iDiNIDResultado = iDiNIDResultado;
	}
	public String getiDSaldoinicial() {
		return iDSaldoinicial;
	}
	public void setiDSaldoinicial(String iDSaldoinicial) {
		this.iDSaldoinicial = iDSaldoinicial;
	}
	public String getiDTotalRecibidoFF() {
		return iDTotalRecibidoFF;
	}
	public void setiDTotalRecibidoFF(String iDTotalRecibidoFF) {
		this.iDTotalRecibidoFF = iDTotalRecibidoFF;
	}
	public String getiDTotalEntregadoFF() {
		return iDTotalEntregadoFF;
	}
	public void setiDTotalEntregadoFF(String iDTotalEntregadoFF) {
		this.iDTotalEntregadoFF = iDTotalEntregadoFF;
	}
	public String getiDDepositosEnFichas() {
		return iDDepositosEnFichas;
	}
	public void setiDDepositosEnFichas(String iDDepositosEnFichas) {
		this.iDDepositosEnFichas = iDDepositosEnFichas;
	}
	public String getiDRetiroEnFichas() {
		return iDRetiroEnFichas;
	}
	public void setiDRetiroEnFichas(String iDRetiroEnFichas) {
		this.iDRetiroEnFichas = iDRetiroEnFichas;
	}
	public String getiDSaldoFinal() {
		return iDSaldoFinal;
	}
	public void setiDSaldoFinal(String iDSaldoFinal) {
		this.iDSaldoFinal = iDSaldoFinal;
	}
	public String getiDNota() {
		return iDNota;
	}
	public void setiDNota(String iDNota) {
		this.iDNota = iDNota;
	}
	public String getiDNotaModal() {
		return iDNotaModal;
	}
	public void setiDNotaModal(String iDNotaModal) {
		this.iDNotaModal = iDNotaModal;
	}
	public String getiDCorteCajaNota() {
		return iDCorteCajaNota;
	}
	public void setiDCorteCajaNota(String iDCorteCajaNota) {
		this.iDCorteCajaNota = iDCorteCajaNota;
	}
	public String getiDFolioInicialDeposito() {
		return iDFolioInicialDeposito;
	}
	public void setiDFolioInicialDeposito(String iDFolioInicialDeposito) {
		this.iDFolioInicialDeposito = iDFolioInicialDeposito;
	}
	public String getiDFolioFinalDeposito() {
		return iDFolioFinalDeposito;
	}
	public void setiDFolioFinalDeposito(String iDFolioFinalDeposito) {
		this.iDFolioFinalDeposito = iDFolioFinalDeposito;
	}
	public String getiDFRealizadasDeposito() {
		return iDFRealizadasDeposito;
	}
	public void setiDFRealizadasDeposito(String iDFRealizadasDeposito) {
		this.iDFRealizadasDeposito = iDFRealizadasDeposito;
	}
	public String getiDFolioInicialRetiros() {
		return iDFolioInicialRetiros;
	}
	public void setiDFolioInicialRetiros(String iDFolioInicialRetiros) {
		this.iDFolioInicialRetiros = iDFolioInicialRetiros;
	}
	public String getiDFolioFinalRetiros() {
		return iDFolioFinalRetiros;
	}
	public void setiDFolioFinalRetiros(String iDFolioFinalRetiros) {
		this.iDFolioFinalRetiros = iDFolioFinalRetiros;
	}
	public String getiDFRealizadasRetiros() {
		return iDFRealizadasRetiros;
	}
	public void setiDFRealizadasRetiros(String iDFRealizadasRetiros) {
		this.iDFRealizadasRetiros = iDFRealizadasRetiros;
	}
	public String getiDFolioInicialTraspasos() {
		return iDFolioInicialTraspasos;
	}
	public void setiDFolioInicialTraspasos(String iDFolioInicialTraspasos) {
		this.iDFolioInicialTraspasos = iDFolioInicialTraspasos;
	}
	public String getiDFolioFinalTraspasos() {
		return iDFolioFinalTraspasos;
	}
	public void setiDFolioFinalTraspasos(String iDFolioFinalTraspasos) {
		this.iDFolioFinalTraspasos = iDFolioFinalTraspasos;
	}
	public String getiDFRealizadasTraspasos() {
		return iDFRealizadasTraspasos;
	}
	public void setiDFRealizadasTraspasos(String iDFRealizadasTraspasos) {
		this.iDFRealizadasTraspasos = iDFRealizadasTraspasos;
	}
	public String getiDFolioInicialCheques() {
		return iDFolioInicialCheques;
	}
	public void setiDFolioInicialCheques(String iDFolioInicialCheques) {
		this.iDFolioInicialCheques = iDFolioInicialCheques;
	}
	public String getiDFolioFinalCheques() {
		return iDFolioFinalCheques;
	}
	public void setiDFolioFinalCheques(String iDFolioFinalCheques) {
		this.iDFolioFinalCheques = iDFolioFinalCheques;
	}
	public String getiDFRealizadasCheques() {
		return iDFRealizadasCheques;
	}
	public void setiDFRealizadasCheques(String iDFRealizadasCheques) {
		this.iDFRealizadasCheques = iDFRealizadasCheques;
	}
	public String getnContCheques() {
		return nContCheques;
	}
	public void setnContCheques(String nContCheques) {
		this.nContCheques = nContCheques;
	}
	public String getnTotalCheques() {
		return nTotalCheques;
	}
	public void setnTotalCheques(String nTotalCheques) {
		this.nTotalCheques = nTotalCheques;
	}
	public String getnContDocumentos() {
		return nContDocumentos;
	}
	public void setnContDocumentos(String nContDocumentos) {
		this.nContDocumentos = nContDocumentos;
	}
	public String getnTotalDocumentos() {
		return nTotalDocumentos;
	}
	public void setnTotalDocumentos(String nTotalDocumentos) {
		this.nTotalDocumentos = nTotalDocumentos;
	}
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursa;
    private String iNsIDUsuario;
    private String respuesta;
    private String iNrowidcortecaja;
    private String cheques;
    private String chequesModal;
    private String Documentos;
    private String DocumentosModal;
    private String iDsTitulo;
    private String iDTotalefectivo;
    private String iDTotalGeneral;
    private String iDiNIDResultado;
    private String iDSaldoinicial;
    private String iDTotalRecibidoFF;
    private String iDTotalEntregadoFF;
    private String iDDepositosEnFichas;
    private String iDRetiroEnFichas;
    private String iDSaldoFinal;
    private String iDNota;
    private String iDNotaModal;
    private String iDCorteCajaNota;
    private String iDFolioInicialDeposito;
    private String iDFolioFinalDeposito;
    private String iDFRealizadasDeposito;
    private String iDFolioInicialRetiros;
    private String iDFolioFinalRetiros;
    private String iDFRealizadasRetiros;
    private String iDFolioInicialTraspasos;
    private String iDFolioFinalTraspasos;
    private String iDFRealizadasTraspasos;
    private String iDFolioInicialCheques;
    private String iDFolioFinalCheques;
    private String iDFRealizadasCheques;
    private String nContCheques;
    private String nTotalCheques;
    private String nContDocumentos;
    private String nTotalDocumentos;
    
	@Override
	public String toString() {
		return "EncdatosConsultacortecajaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursa=" + iNnIDSucursa + ", iNsIDUsuario="
				+ iNsIDUsuario + ", respuesta=" + respuesta + ", iNrowidcortecaja=" + iNrowidcortecaja + ", cheques="
				+ cheques + ", chequesModal=" + chequesModal + ", Documentos=" + Documentos + ", DocumentosModal="
				+ DocumentosModal + ", iDsTitulo=" + iDsTitulo + ", iDTotalefectivo=" + iDTotalefectivo
				+ ", iDTotalGeneral=" + iDTotalGeneral + ", iDiNIDResultado=" + iDiNIDResultado + ", iDSaldoinicial="
				+ iDSaldoinicial + ", iDTotalRecibidoFF=" + iDTotalRecibidoFF + ", iDTotalEntregadoFF="
				+ iDTotalEntregadoFF + ", iDDepositosEnFichas=" + iDDepositosEnFichas + ", iDRetiroEnFichas="
				+ iDRetiroEnFichas + ", iDSaldoFinal=" + iDSaldoFinal + ", iDNota=" + iDNota + ", iDNotaModal="
				+ iDNotaModal + ", iDCorteCajaNota=" + iDCorteCajaNota + ", iDFolioInicialDeposito="
				+ iDFolioInicialDeposito + ", iDFolioFinalDeposito=" + iDFolioFinalDeposito + ", iDFRealizadasDeposito="
				+ iDFRealizadasDeposito + ", iDFolioInicialRetiros=" + iDFolioInicialRetiros + ", iDFolioFinalRetiros="
				+ iDFolioFinalRetiros + ", iDFRealizadasRetiros=" + iDFRealizadasRetiros + ", iDFolioInicialTraspasos="
				+ iDFolioInicialTraspasos + ", iDFolioFinalTraspasos=" + iDFolioFinalTraspasos
				+ ", iDFRealizadasTraspasos=" + iDFRealizadasTraspasos + ", iDFolioInicialCheques="
				+ iDFolioInicialCheques + ", iDFolioFinalCheques=" + iDFolioFinalCheques + ", iDFRealizadasCheques="
				+ iDFRealizadasCheques + ", nContCheques=" + nContCheques + ", nTotalCheques=" + nTotalCheques
				+ ", nContDocumentos=" + nContDocumentos + ", nTotalDocumentos=" + nTotalDocumentos + "]";
	}
}
