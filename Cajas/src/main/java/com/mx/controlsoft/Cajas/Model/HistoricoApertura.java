package com.mx.controlsoft.Cajas.Model;

public class HistoricoApertura {
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSucursal() {
		return Sucursal;
	}

	public void setSucursal(String sucursal) {
		Sucursal = sucursal;
	}

	public String getFecha() {
		return Fecha;
	}

	public void setFecha(String fecha) {
		Fecha = fecha;
	}

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String usuario) {
		Usuario = usuario;
	}

	public String getTipoMovimiento() {
		return TipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		TipoMovimiento = tipoMovimiento;
	}

	public String getFicha() {
		return Ficha;
	}

	public void setFicha(String ficha) {
		Ficha = ficha;
	}

	public String getFichaCancelada() {
		return FichaCancelada;
	}

	public void setFichaCancelada(String fichaCancelada) {
		FichaCancelada = fichaCancelada;
	}

	public String getImporte() {
		return Importe;
	}

	public void setImporte(String importe) {
		Importe = importe;
	}

	public String getCheque() {
		return Cheque;
	}

	public void setCheque(String cheque) {
		Cheque = cheque;
	}

	public String getBlanco() {
		return blanco;
	}

	public void setBlanco(String blanco) {
		this.blanco = blanco;
	}

	private String id;
	private String Sucursal;
	private String Fecha;
	private String Usuario;
	private String TipoMovimiento;
	private String Ficha;
	private String FichaCancelada;
	private String Importe;
	private String Cheque;
	private String blanco;
	
	
	@Override
	public String toString() {
		return "HistoricoApertura [id=" + id + ", Sucursal=" + Sucursal + ", Fecha=" + Fecha + ", Usuario=" + Usuario
				+ ", TipoMovimiento=" + TipoMovimiento + ", Ficha=" + Ficha + ", FichaCancelada=" + FichaCancelada
				+ ", Importe=" + Importe + ", Cheque=" + Cheque + ", blanco=" + blanco + "]";
	}

}
