package com.mx.controlsoft.Cajas.Model;

public class SucursalIdeexpideconstanciaBE {
	public String getiDSucursal() {
		return iDSucursal;
	}
	public void setiDSucursal(String iDSucursal) {
		this.iDSucursal = iDSucursal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	private String iDSucursal;
    private String nombre;
	@Override
	public String toString() {
		return "SucursalIdeexpideconstanciaBE [iDSucursal=" + iDSucursal + ", nombre=" + nombre + "]";
	}
}
