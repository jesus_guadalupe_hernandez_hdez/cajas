package com.mx.controlsoft.Cajas.Model;

public class EncdatosBuscasabanaBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursal() {
		return iNnIDSucursal;
	}
	public void setiNnIDSucursal(String iNnIDSucursal) {
		this.iNnIDSucursal = iNnIDSucursal;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getiNmensaje() {
		return iNmensaje;
	}
	public void setiNmensaje(String iNmensaje) {
		this.iNmensaje = iNmensaje;
	}
	public String getiNsTransaccion() {
		return iNsTransaccion;
	}
	public void setiNsTransaccion(String iNsTransaccion) {
		this.iNsTransaccion = iNsTransaccion;
	}
	public String getiNsUnionSocio() {
		return iNsUnionSocio;
	}
	public void setiNsUnionSocio(String iNsUnionSocio) {
		this.iNsUnionSocio = iNsUnionSocio;
	}
	public String getiNsCajaSocio() {
		return iNsCajaSocio;
	}
	public void setiNsCajaSocio(String iNsCajaSocio) {
		this.iNsCajaSocio = iNsCajaSocio;
	}
	public String getiNsSucursalSocio() {
		return iNsSucursalSocio;
	}
	public void setiNsSucursalSocio(String iNsSucursalSocio) {
		this.iNsSucursalSocio = iNsSucursalSocio;
	}
	public String getiNsNumeroSocio() {
		return iNsNumeroSocio;
	}
	public void setiNsNumeroSocio(String iNsNumeroSocio) {
		this.iNsNumeroSocio = iNsNumeroSocio;
	}
	public String getiNIDCaja() {
		return iNIDCaja;
	}
	public void setiNIDCaja(String iNIDCaja) {
		this.iNIDCaja = iNIDCaja;
	}
	public String getiNsFecha() {
		return iNsFecha;
	}
	public void setiNsFecha(String iNsFecha) {
		this.iNsFecha = iNsFecha;
	}
	public String getiNsFichaInicial() {
		return iNsFichaInicial;
	}
	public void setiNsFichaInicial(String iNsFichaInicial) {
		this.iNsFichaInicial = iNsFichaInicial;
	}
	public String getiNsOperadorInicial() {
		return iNsOperadorInicial;
	}
	public void setiNsOperadorInicial(String iNsOperadorInicial) {
		this.iNsOperadorInicial = iNsOperadorInicial;
	}
	public String getiNsFichaFinal() {
		return iNsFichaFinal;
	}
	public void setiNsFichaFinal(String iNsFichaFinal) {
		this.iNsFichaFinal = iNsFichaFinal;
	}
	public String getiNsOperadorFinal() {
		return iNsOperadorFinal;
	}
	public void setiNsOperadorFinal(String iNsOperadorFinal) {
		this.iNsOperadorFinal = iNsOperadorFinal;
	}
	public String getOUTsTitulo() {
		return OUTsTitulo;
	}
	public void setOUTsTitulo(String oUTsTitulo) {
		OUTsTitulo = oUTsTitulo;
	}
	public String getOUTsNombreCaja() {
		return OUTsNombreCaja;
	}
	public void setOUTsNombreCaja(String oUTsNombreCaja) {
		OUTsNombreCaja = oUTsNombreCaja;
	}
	public String getOUTsFecha() {
		return OUTsFecha;
	}
	public void setOUTsFecha(String oUTsFecha) {
		OUTsFecha = oUTsFecha;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursal;
    private String iNsIDUsuario;
    private String iNmensaje;
    private String iNsTransaccion;
    private String iNsUnionSocio;
    private String iNsCajaSocio;
    private String iNsSucursalSocio;
    private String iNsNumeroSocio;
    private String iNIDCaja;
    private String iNsFecha;
    private String iNsFichaInicial;
    private String iNsOperadorInicial;
    private String iNsFichaFinal;
    private String iNsOperadorFinal;
    private String OUTsTitulo;
    private String OUTsNombreCaja;
    private String OUTsFecha;
    private String respuesta;
	@Override
	public String toString() {
		return "EncdatosBuscasabanaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursal=" + iNnIDSucursal + ", iNsIDUsuario="
				+ iNsIDUsuario + ", iNmensaje=" + iNmensaje + ", iNsTransaccion=" + iNsTransaccion + ", iNsUnionSocio="
				+ iNsUnionSocio + ", iNsCajaSocio=" + iNsCajaSocio + ", iNsSucursalSocio=" + iNsSucursalSocio
				+ ", iNsNumeroSocio=" + iNsNumeroSocio + ", iNIDCaja=" + iNIDCaja + ", iNsFecha=" + iNsFecha
				+ ", iNsFichaInicial=" + iNsFichaInicial + ", iNsOperadorInicial=" + iNsOperadorInicial
				+ ", iNsFichaFinal=" + iNsFichaFinal + ", iNsOperadorFinal=" + iNsOperadorFinal + ", OUTsTitulo="
				+ OUTsTitulo + ", OUTsNombreCaja=" + OUTsNombreCaja + ", OUTsFecha=" + OUTsFecha + ", respuesta="
				+ respuesta + "]";
	}
}
