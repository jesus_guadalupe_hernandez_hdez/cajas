package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestDetallefondofijoBE;

public class RequestGralDetallefondofijoBE {
	private RequestDetallefondofijoBE request;

	public RequestDetallefondofijoBE getRequest() {
		return request;
	}

	public void setRequest(RequestDetallefondofijoBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralDetallefondofijoBE [request=" + request + "]";
	}
}
