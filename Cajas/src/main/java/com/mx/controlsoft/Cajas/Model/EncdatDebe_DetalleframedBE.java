package com.mx.controlsoft.Cajas.Model;

public class EncdatDebe_DetalleframedBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getINsAnclarSaldo() {
		return INsAnclarSaldo;
	}
	public void setINsAnclarSaldo(String iNsAnclarSaldo) {
		INsAnclarSaldo = iNsAnclarSaldo;
	}
	public String getINTranInversion() {
		return INTranInversion;
	}
	public void setINTranInversion(String iNTranInversion) {
		INTranInversion = iNTranInversion;
	}
	public String getINnCajaAsignada() {
		return INnCajaAsignada;
	}
	public void setINnCajaAsignada(String iNnCajaAsignada) {
		INnCajaAsignada = iNnCajaAsignada;
	}
	public String getINFolioFicha() {
		return INFolioFicha;
	}
	public void setINFolioFicha(String iNFolioFicha) {
		INFolioFicha = iNFolioFicha;
	}
	public String getHrefsuceso() {
		return hrefsuceso;
	}
	public void setHrefsuceso(String hrefsuceso) {
		this.hrefsuceso = hrefsuceso;
	}
	public String getImporteSuceso() {
		return importeSuceso;
	}
	public void setImporteSuceso(String importeSuceso) {
		this.importeSuceso = importeSuceso;
	}
	private long  INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String respuesta;
    private String INsTransaccion;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INsNumeroSocio;
    private String INsAnclarSaldo;
    private String INTranInversion;
    private String INnCajaAsignada;
    private String INFolioFicha;
    private String hrefsuceso;
    private String importeSuceso;
    
	@Override
	public String toString() {
		return "EncdatDebe_DetalleframedBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", respuesta=" + respuesta + ", INsTransaccion=" + INsTransaccion + ", INsUnionSocio="
				+ INsUnionSocio + ", INsCajaSocio=" + INsCajaSocio + ", INsSucursalSocio=" + INsSucursalSocio
				+ ", INsNumeroSocio=" + INsNumeroSocio + ", INsAnclarSaldo=" + INsAnclarSaldo + ", INTranInversion="
				+ INTranInversion + ", INnCajaAsignada=" + INnCajaAsignada + ", INFolioFicha=" + INFolioFicha
				+ ", hrefsuceso=" + hrefsuceso + ", importeSuceso=" + importeSuceso + "]";
	}
}
