package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetFacturacionFacturacionelectronicajsBE;
import com.mx.controlsoft.Cajas.Model.EncFacturaElecFacturacionelectronicaIniBE;

public class RequestFacturacionelectronicajsBE {
	public List<EncFacturaElecFacturacionelectronicaIniBE> getTt_EncFacturaElec() {
		return tt_EncFacturaElec;
	}
	public void setTt_EncFacturaElec(List<EncFacturaElecFacturacionelectronicaIniBE> tt_EncFacturaElec) {
		this.tt_EncFacturaElec = tt_EncFacturaElec;
	}
	public List<DetFacturacionFacturacionelectronicajsBE> getTt_DetFacturacion() {
		return tt_DetFacturacion;
	}
	public void setTt_DetFacturacion(List<DetFacturacionFacturacionelectronicajsBE> tt_DetFacturacion) {
		this.tt_DetFacturacion = tt_DetFacturacion;
	}
	
	private List<EncFacturaElecFacturacionelectronicaIniBE> tt_EncFacturaElec;
	private List<DetFacturacionFacturacionelectronicajsBE> tt_DetFacturacion;
	
	@Override
	public String toString() {
		return "RequestFacturacionelectronicajsBE [tt_EncFacturaElec=" + tt_EncFacturaElec + ", tt_DetFacturacion="
				+ tt_DetFacturacion + "]";
	}
}
