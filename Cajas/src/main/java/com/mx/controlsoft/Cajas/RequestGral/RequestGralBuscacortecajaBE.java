package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestBuscacortecajaBE;

public class RequestGralBuscacortecajaBE {
	private RequestBuscacortecajaBE request;

	public RequestBuscacortecajaBE getRequest() {
		return request;
	}

	public void setRequest(RequestBuscacortecajaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralBuscacortecajaBE [request=" + request + "]";
	}
	
}
