package com.mx.controlsoft.Cajas.Controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.core.net.TcpSocketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.controlsoft.Cajas.Model.DetEfecEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncFacturaElecFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.Model.EncdatosBuscacortecajaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Model.FondoFijoTemporales;
import com.mx.controlsoft.Cajas.Model.FondoFijoValores;
import com.mx.controlsoft.Cajas.Model.ListaFondoFijo;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestBuscacortecajaBE;
import com.mx.controlsoft.Cajas.Request.RequestDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.Request.RequestDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Request.RequestEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.Request.RequestFacturacionelectronicajsBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscacortecajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetallefondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicajsBE;
import com.mx.controlsoft.Cajas.Service.BuscacortecajaBEService;
import com.mx.controlsoft.Cajas.Service.CajeroPrincipalBEService;
import com.mx.controlsoft.Cajas.Service.FondoFijoBEService;

@Controller
@RequestMapping(value = "/ajax_CajeroPrincipal")
public class Ajax_CajeroPrincipalBE {
	
	@Autowired
	CajeroPrincipalBEService serv;

	
	@Autowired
	BuscacortecajaBEService repos;
	
	@Autowired 
	FondoFijoBEService service;
	
	@ResponseBody
	@RequestMapping(value = "/getPostFondoFijoAceptarMovimiento",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralEfectivofondofijoBE aceptarMovimientoFondoFijo(@RequestBody  ListaFondoFijo fijoTem ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
			
		if (logginn != null) {

			EncdatosEfectivofondofijoBE detalleFondoFijo = new EncdatosEfectivofondofijoBE();
			detalleFondoFijo.setINid(1);
			detalleFondoFijo.setINnIDUnion(logginn.getnUnion());
	        detalleFondoFijo.setINnIDCajaPopular(logginn.getnCaja());
	        detalleFondoFijo.setINnIDSucursa(logginn.getnSucursal());
	        //detalleFondoFijo.setINsIDUsuario(logginn.getUe());
	        detalleFondoFijo.setINsIDUsuario("fUadqcjiqXGmmdpN");
	        detalleFondoFijo.setiNTransaccion(fijoTem.getTemVal().getEstadoTransaccion());
	        detalleFondoFijo.setiNCajaDestino(fijoTem.getTemVal().getCajaCajero());
	        detalleFondoFijo.setiNImporte(fijoTem.getTemVal().getTxtImporte());
	        detalleFondoFijo.setREQUEST_METHOD("POST");
	        
	        List<EncdatosEfectivofondofijoBE> lstEncDatos = new ArrayList<>();
	        lstEncDatos.add(detalleFondoFijo);
	        
	        List<DetEfecEfectivofondofijoBE> lstDetEfectivoFondo = new ArrayList<>();
	        int ii=1;
	        for ( FondoFijoValores fij : fijoTem.getListaFijo() ) {
	        	DetEfecEfectivofondofijoBE detEfec = new DetEfecEfectivofondofijoBE();
		        detEfec.setTtCantidad(fij.getiNImporte());
		        detEfec.setTtDenominacion(fij.getiNAbono());
		        detEfec.setTtImporte(fij.getiNsImporteSubTotal());
		        detEfec.setTtMovimiento(String.valueOf(ii));
		        detEfec.setTtNid(ii);
		        detEfec.setTtTipoEfec( fij.getTipoMonedaBilleteMoneda() );
		        detEfec.setTtTipoMoneda(fij.getTipoMoneda());
		        lstDetEfectivoFondo.add(detEfec);
		        ii++;
	        }

	        RequestGralEfectivofondofijoBE requetGralReq = new RequestGralEfectivofondofijoBE();
	        RequestEfectivofondofijoBE requetFonFij = new RequestEfectivofondofijoBE();
	        requetFonFij.setTtEncdatos(lstEncDatos);
	        requetFonFij.setTtDetEfec(lstDetEfectivoFondo);
	        requetGralReq.setRequest(requetFonFij);
										  
			return serv.getPostDetalleFondoAceptar(requetGralReq);

		}
		return null;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPostFondoFijoAgregarMovimiento",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralEfectivofondofijoBE agregarMovimientoFondoFijo(@RequestBody  FondoFijoTemporales fondoFijoTemporales ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
			
		if (logginn != null) {

			EncdatosEfectivofondofijoBE detalleFondoFijo = new EncdatosEfectivofondofijoBE();
			detalleFondoFijo.setINid(1);
			detalleFondoFijo.setINnIDUnion(logginn.getnUnion());
	        detalleFondoFijo.setINnIDCajaPopular(logginn.getnCaja());
	        detalleFondoFijo.setINnIDSucursa(logginn.getnSucursal());
	        //detalleFondoFijo.setINsIDUsuario(logginn.getUe());
	        detalleFondoFijo.setINsIDUsuario("fUadqcjiqXGmmdpN");
	        detalleFondoFijo.setiNTransaccion(fondoFijoTemporales.getEstadoTransaccion());
	        detalleFondoFijo.setiNCajaDestino(fondoFijoTemporales.getCajaCajero());
	        detalleFondoFijo.setiNImporte(fondoFijoTemporales.getTxtImporte());
	        detalleFondoFijo.setREQUEST_METHOD("POST");
	        
	        List<EncdatosEfectivofondofijoBE> lstEncDatos = new ArrayList<>();
	        lstEncDatos.add(detalleFondoFijo);
	        
	        List<DetEfecEfectivofondofijoBE> lstDetEfectivoFondo = new ArrayList<>();
	        DetEfecEfectivofondofijoBE detEfec = new DetEfecEfectivofondofijoBE();
	        detEfec.setTtCantidad(fondoFijoTemporales.getTtCantidad());
	        detEfec.setTtDenominacion(fondoFijoTemporales.getTtDenominacion());
	        detEfec.setTtImporte(fondoFijoTemporales.getTtImporte());
	        detEfec.setTtMovimiento(fondoFijoTemporales.getTtMovimiento());
	        detEfec.setTtNid(1);
	        detEfec.setTtTipoEfec(fondoFijoTemporales.getTtTipoEfec());
	        detEfec.setTtTipoMoneda(fondoFijoTemporales.getTtTipoMoneda());
	        	
	        lstDetEfectivoFondo.add(detEfec);
	        
	        RequestGralEfectivofondofijoBE requetGralReq = new RequestGralEfectivofondofijoBE();
	        RequestEfectivofondofijoBE requetFonFij = new RequestEfectivofondofijoBE();
	        requetFonFij.setTtEncdatos(lstEncDatos);
	        requetFonFij.setTtDetEfec(lstDetEfectivoFondo);
	        requetGralReq.setRequest(requetFonFij);
										  
			return serv.getPostDetalleFondoAceptar(requetGralReq);

		}
		return null;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPostFondoFijoBuscar",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralDetallefondofijoBE mostrarFondoFijo(@RequestBody temporal  tem ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			EncdatosDetallefondofijoBE encDat = new EncdatosDetallefondofijoBE();
			encDat.setINid(1);
			encDat.setINnIDUnion(logginn.getnUnion());
			encDat.setINnIDCajaPopular(logginn.getnCaja());
			encDat.setINnIDSucursa(logginn.getnSucursal());
			//encDat.setINsIDUsuario(logginn.getUe());
			encDat.setINsIDUsuario("fUadqcjiqXGmmdpN");
			encDat.setiNMaintOption("BUSCAR");
			encDat.setiNFechaBusca(tem.getiNidFechaI());
			
			List<EncdatosDetallefondofijoBE> lstEncdatosDetallefondofijoBE = new ArrayList<>();
			lstEncdatosDetallefondofijoBE.add(encDat);
			
			RequestDetallefondofijoBE r = new RequestDetallefondofijoBE();
			r.setTtEncdatos(lstEncdatosDetallefondofijoBE);
			
			RequestGralDetallefondofijoBE  reGral = new RequestGralDetallefondofijoBE();
			reGral.setRequest(r);
			
			RequestGralDetallefondofijoBE resp = service.getPostFondoFijoBE_Ini(reGral); 
			
			//model.addAttribute("lstRequestGral", resp);
			return resp;
		}
		return null;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPostFacturacionelectronicajsBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralFacturacionelectronicajsBE mostrarPrincipal(@RequestBody temporal  tem ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralFacturacionelectronicajsBE req = new RequestGralFacturacionelectronicajsBE();
			EncFacturaElecFacturacionelectronicaIniBE enc = new EncFacturaElecFacturacionelectronicaIniBE();
			enc.setINid(1);
			enc.setINtoken("");
			enc.setINnIDUnion(logginn.getnUnion());
			enc.setINnIDCajaPopular(logginn.getnCaja());
			enc.setINnIDSucursal(logginn.getSucursal());
			enc.setINsIDUsuario(logginn.getUe());
			enc.setINsSucursalSocio(tem.getiNsSucursalSocio());
			enc.setINsAccion(tem.getTransaccion());
			enc.setINidFechaI(tem.getiNidFechaI());
			enc.setINidFechaF(tem.getiNidFechaF());
			
			List<EncFacturaElecFacturacionelectronicaIniBE> lstFactElec = new LinkedList<>();
			lstFactElec.add(enc);
			
			RequestFacturacionelectronicajsBE reqFact = new RequestFacturacionelectronicajsBE();
			reqFact.setTt_EncFacturaElec (lstFactElec);
			req.setRequest(reqFact);
			RequestGralFacturacionelectronicajsBE requestGral= serv.getPostFacturacionelectronicajsBE(req) ;  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			return requestGral;
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPostDesgloseSaldoCajeroBE",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralDesglosesaldocajeroBE mostrarDesgloseSaldo(@RequestBody temporal  tem ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralDesglosesaldocajeroBE req = new RequestGralDesglosesaldocajeroBE();
			EncdatosDesglosesaldocajeroBE enc = new EncdatosDesglosesaldocajeroBE();
			enc.setINid(1);
			enc.setINtoken("");
			enc.setINnIDUnion(logginn.getnUnion());
			enc.setINnIDCajaPopular(logginn.getnCaja());
			enc.setINnIDSucursa(logginn.getnSucursal());
			enc.setINsIDUsuario("fUadqcjiqXGmmdpN");
			enc.setINnCajaAsignada("89");

			List<EncdatosDesglosesaldocajeroBE> lsEnc = new LinkedList<>();
			lsEnc.add(enc);
			
			RequestDesglosesaldocajeroBE reqFact = new RequestDesglosesaldocajeroBE();
			reqFact.setTtEncdatos  (lsEnc);
			req.setRequest(reqFact);
			RequestGralDesglosesaldocajeroBE requestGral= serv.getPostDesglosesaldocajeroBE(req) ;  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			return requestGral;
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPostBuscaCortecaja",method = RequestMethod.POST,headers = {"content-type=application/json"})
	public RequestGralBuscacortecajaBE mostrarBuscaCorteCaja(@RequestBody temporal  tem ,HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			RequestGralBuscacortecajaBE req = new RequestGralBuscacortecajaBE();
			EncdatosBuscacortecajaBE enc = new EncdatosBuscacortecajaBE();
			enc.setiNid(1);
			enc.setiNtoken("");
			enc.setiNnIDUnion(logginn.getnUnion());
			enc.setiNnIDCajaPopular(logginn.getnCaja());
			enc.setiNnIDSucursa(logginn.getnSucursal());
			enc.setiNIDUnion(logginn.getnUnion());
			enc.setiNIDCajaPopular(logginn.getnCaja());
			enc.setiNIDSucursal(logginn.getnSucursal());
			
			enc.setiNIDCajaAsignada("135");
			enc.setiNFecha("21/01/2020");

			List<EncdatosBuscacortecajaBE> lsEnc = new LinkedList<>();
			lsEnc.add(enc);
			
			RequestBuscacortecajaBE reqFact = new RequestBuscacortecajaBE();
			reqFact.setTtEncdatos  (lsEnc);
			req.setRequest(reqFact);
			RequestGralBuscacortecajaBE requestGral= repos.getBuscacortecajaBE(req) ;  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			return requestGral;
		}
		return null;
	}
}

class temporal {
	public String getiNidFechaI() {
		return iNidFechaI;
	}
	public void setiNidFechaI(String iNidFechaI) {
		this.iNidFechaI = iNidFechaI;
	}
	public String getiNidFechaF() {
		return iNidFechaF;
	}
	public void setiNidFechaF(String iNidFechaF) {
		this.iNidFechaF = iNidFechaF;
	}
	public String getiNsSucursalSocio() {
		return iNsSucursalSocio;
	}
	public void setiNsSucursalSocio(String iNsSucursalSocio) {
		this.iNsSucursalSocio = iNsSucursalSocio;
	}
	
	private String iNidFechaI;
	private String iNidFechaF;
	private String iNsSucursalSocio;
	private String transaccion;
	
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	@Override
	public String toString() {
		return "temporal [iNidFechaI=" + iNidFechaI + ", iNidFechaF=" + iNidFechaF + ", iNsSucursalSocio="
				+ iNsSucursalSocio + "]";
	}
}
