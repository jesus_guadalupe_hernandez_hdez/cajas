package com.mx.controlsoft.Cajas.Model;

public class EncdatosDetallefondofijoBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getiNMaintOption() {
		return iNMaintOption;
	}
	public void setiNMaintOption(String iNMaintOption) {
		this.iNMaintOption = iNMaintOption;
	}
	public String getiNCajaAsignada() {
		return iNCajaAsignada;
	}
	public void setiNCajaAsignada(String iNCajaAsignada) {
		this.iNCajaAsignada = iNCajaAsignada;
	}
	public String getiNUsuario() {
		return iNUsuario;
	}
	public void setiNUsuario(String iNUsuario) {
		this.iNUsuario = iNUsuario;
	}
	public String getiNFichaFondoFijo() {
		return iNFichaFondoFijo;
	}
	public void setiNFichaFondoFijo(String iNFichaFondoFijo) {
		this.iNFichaFondoFijo = iNFichaFondoFijo;
	}
	public String getiNHora() {
		return iNHora;
	}
	public void setiNHora(String iNHora) {
		this.iNHora = iNHora;
	}
	public String getiNTransaccion() {
		return iNTransaccion;
	}
	public void setiNTransaccion(String iNTransaccion) {
		this.iNTransaccion = iNTransaccion;
	}
	public String getiNFechaBusca() {
		return iNFechaBusca;
	}
	public void setiNFechaBusca(String iNFechaBusca) {
		this.iNFechaBusca = iNFechaBusca;
	}
	public String getiNCajaDestino() {
		return iNCajaDestino;
	}
	public void setiNCajaDestino(String iNCajaDestino) {
		this.iNCajaDestino = iNCajaDestino;
	}
	public String getiNConcepto() {
		return iNConcepto;
	}
	public void setiNConcepto(String iNConcepto) {
		this.iNConcepto = iNConcepto;
	}
	public String getiNImporte() {
		return iNImporte;
	}
	public void setiNImporte(String iNImporte) {
		this.iNImporte = iNImporte;
	}
	public String getImprimeFicha() {
		return ImprimeFicha;
	}
	public void setImprimeFicha(String imprimeFicha) {
		ImprimeFicha = imprimeFicha;
	}
	public String getIDFicha() {
		return IDFicha;
	}
	public void setIDFicha(String iDFicha) {
		IDFicha = iDFicha;
	}
	public String getCajaAsignadaImp() {
		return CajaAsignadaImp;
	}
	public void setCajaAsignadaImp(String cajaAsignadaImp) {
		CajaAsignadaImp = cajaAsignadaImp;
	}
	public String getIDFichaBorrar() {
		return IDFichaBorrar;
	}
	public void setIDFichaBorrar(String iDFichaBorrar) {
		IDFichaBorrar = iDFichaBorrar;
	}
	public String getCancelaFicha() {
		return CancelaFicha;
	}
	public void setCancelaFicha(String cancelaFicha) {
		CancelaFicha = cancelaFicha;
	}
	public String getCajaBorra() {
		return CajaBorra;
	}
	public void setCajaBorra(String cajaBorra) {
		CajaBorra = cajaBorra;
	}
	public String getnFichaParametro() {
		return nFichaParametro;
	}
	public void setnFichaParametro(String nFichaParametro) {
		this.nFichaParametro = nFichaParametro;
	}
	public String getIDFichaParametro() {
		return IDFichaParametro;
	}
	public void setIDFichaParametro(String iDFichaParametro) {
		IDFichaParametro = iDFichaParametro;
	}
	public String getsIDReg() {
		return sIDReg;
	}
	public void setsIDReg(String sIDReg) {
		this.sIDReg = sIDReg;
	}
	public String getNuevaFicha() {
		return NuevaFicha;
	}
	public void setNuevaFicha(String nuevaFicha) {
		NuevaFicha = nuevaFicha;
	}
	public String getDespliegaMensaje() {
		return DespliegaMensaje;
	}
	public void setDespliegaMensaje(String despliegaMensaje) {
		DespliegaMensaje = despliegaMensaje;
	}
	public String getREQUEST_METHOD() {
		return REQUEST_METHOD;
	}
	public void setREQUEST_METHOD(String rEQUEST_METHOD) {
		REQUEST_METHOD = rEQUEST_METHOD;
	}
	public String getFilaActual() {
		return FilaActual;
	}
	public void setFilaActual(String filaActual) {
		FilaActual = filaActual;
	}
	public String getFilas() {
		return Filas;
	}
	public void setFilas(String filas) {
		Filas = filas;
	}
	public String getsTipoCajero() {
		return sTipoCajero;
	}
	public void setsTipoCajero(String sTipoCajero) {
		this.sTipoCajero = sTipoCajero;
	}
	public String getsNombreUsuario() {
		return sNombreUsuario;
	}
	public void setsNombreUsuario(String sNombreUsuario) {
		this.sNombreUsuario = sNombreUsuario;
	}
	public String getnCajaAsignada() {
		return nCajaAsignada;
	}
	public void setnCajaAsignada(String nCajaAsignada) {
		this.nCajaAsignada = nCajaAsignada;
	}
	public String getROWIDCajaAsignada() {
		return ROWIDCajaAsignada;
	}
	public void setROWIDCajaAsignada(String rOWIDCajaAsignada) {
		ROWIDCajaAsignada = rOWIDCajaAsignada;
	}
	public String getSesion() {
		return sesion;
	}
	public void setSesion(String sesion) {
		this.sesion = sesion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursa;
    private String INsIDUsuario;
    private String iNMaintOption;
    private String iNCajaAsignada;
    private String iNUsuario;
    private String iNFichaFondoFijo;
    private String iNHora;
    private String iNTransaccion;
    private String iNFechaBusca;
    private String iNCajaDestino;
    private String iNConcepto;
    private String iNImporte;
    private String ImprimeFicha;
    private String IDFicha;
    private String CajaAsignadaImp;
    private String IDFichaBorrar;
    private String CancelaFicha;
    private String CajaBorra;
    private String nFichaParametro;
    private String IDFichaParametro;
    private String sIDReg;
    private String NuevaFicha;
    private String DespliegaMensaje;
    private String REQUEST_METHOD;
    private String FilaActual;
    private String Filas;
    private String sTipoCajero;
    private String sNombreUsuario;
    private String nCajaAsignada;
    private String ROWIDCajaAsignada;
    private String sesion;
    private String cantidad;
    private String Total;
    private String respuesta;
	@Override
	public String toString() {
		return "EncdatosDetallefondofijoBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", iNMaintOption=" + iNMaintOption + ", iNCajaAsignada=" + iNCajaAsignada
				+ ", iNUsuario=" + iNUsuario + ", iNFichaFondoFijo=" + iNFichaFondoFijo + ", iNHora=" + iNHora
				+ ", iNTransaccion=" + iNTransaccion + ", iNFechaBusca=" + iNFechaBusca + ", iNCajaDestino="
				+ iNCajaDestino + ", iNConcepto=" + iNConcepto + ", iNImporte=" + iNImporte + ", ImprimeFicha="
				+ ImprimeFicha + ", IDFicha=" + IDFicha + ", CajaAsignadaImp=" + CajaAsignadaImp + ", IDFichaBorrar="
				+ IDFichaBorrar + ", CancelaFicha=" + CancelaFicha + ", CajaBorra=" + CajaBorra + ", nFichaParametro="
				+ nFichaParametro + ", IDFichaParametro=" + IDFichaParametro + ", sIDReg=" + sIDReg + ", NuevaFicha="
				+ NuevaFicha + ", DespliegaMensaje=" + DespliegaMensaje + ", REQUEST_METHOD=" + REQUEST_METHOD
				+ ", FilaActual=" + FilaActual + ", Filas=" + Filas + ", sTipoCajero=" + sTipoCajero
				+ ", sNombreUsuario=" + sNombreUsuario + ", nCajaAsignada=" + nCajaAsignada + ", ROWIDCajaAsignada="
				+ ROWIDCajaAsignada + ", sesion=" + sesion + ", cantidad=" + cantidad + ", Total=" + Total
				+ ", respuesta=" + respuesta + "]";
	}
}
