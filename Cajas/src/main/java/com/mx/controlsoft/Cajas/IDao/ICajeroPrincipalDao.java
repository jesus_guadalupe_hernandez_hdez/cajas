package com.mx.controlsoft.Cajas.IDao;

public interface ICajeroPrincipalDao <T,Y,X,Z,A>{
	T getPostFacturacionelectronicaIniBE(T request);
	Y getPostFacturacionelectronicajsBE (Y request);
	X getPostMonitorcajasBE (X request);
	Z getPostDesglosesaldocajeroBE(Z request);
	A getPostDetalleFondoAceptar(A request);
}
