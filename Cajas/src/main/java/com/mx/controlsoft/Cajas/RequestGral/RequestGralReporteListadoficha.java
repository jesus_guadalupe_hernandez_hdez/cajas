package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestReporteListadoficha;

public class RequestGralReporteListadoficha {
	
	private RequestReporteListadoficha request;

	public RequestReporteListadoficha getRequest() {
		return request;
	}

	public void setRequest(RequestReporteListadoficha request) {
		this.request = request;
	}
	
}
