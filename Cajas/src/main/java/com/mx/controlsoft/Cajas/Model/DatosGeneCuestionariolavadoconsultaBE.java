package com.mx.controlsoft.Cajas.Model;

public class DatosGeneCuestionariolavadoconsultaBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getnLugarFecha() {
		return nLugarFecha;
	}
	public void setnLugarFecha(String nLugarFecha) {
		this.nLugarFecha = nLugarFecha;
	}
	public String getnDiaLugFech() {
		return nDiaLugFech;
	}
	public void setnDiaLugFech(String nDiaLugFech) {
		this.nDiaLugFech = nDiaLugFech;
	}
	public String getnMesLugFech() {
		return nMesLugFech;
	}
	public void setnMesLugFech(String nMesLugFech) {
		this.nMesLugFech = nMesLugFech;
	}
	public String getnAnioLugFec() {
		return nAnioLugFec;
	}
	public void setnAnioLugFec(String nAnioLugFec) {
		this.nAnioLugFec = nAnioLugFec;
	}
	public String getnEsSocio() {
		return nEsSocio;
	}
	public void setnEsSocio(String nEsSocio) {
		this.nEsSocio = nEsSocio;
	}
	public String getnMenor() {
		return nMenor;
	}
	public void setnMenor(String nMenor) {
		this.nMenor = nMenor;
	}
	public String getnSocio() {
		return nSocio;
	}
	public void setnSocio(String nSocio) {
		this.nSocio = nSocio;
	}
	public String getnEsCliente() {
		return nEsCliente;
	}
	public void setnEsCliente(String nEsCliente) {
		this.nEsCliente = nEsCliente;
	}
	public String getnApellidoPSocio() {
		return nApellidoPSocio;
	}
	public void setnApellidoPSocio(String nApellidoPSocio) {
		this.nApellidoPSocio = nApellidoPSocio;
	}
	public String getnApellidoMSocio() {
		return nApellidoMSocio;
	}
	public void setnApellidoMSocio(String nApellidoMSocio) {
		this.nApellidoMSocio = nApellidoMSocio;
	}
	public String getnNombreSocio() {
		return nNombreSocio;
	}
	public void setnNombreSocio(String nNombreSocio) {
		this.nNombreSocio = nNombreSocio;
	}
	public String getnFechaNacSocio() {
		return nFechaNacSocio;
	}
	public void setnFechaNacSocio(String nFechaNacSocio) {
		this.nFechaNacSocio = nFechaNacSocio;
	}
	public String getnSexoMSocio() {
		return nSexoMSocio;
	}
	public void setnSexoMSocio(String nSexoMSocio) {
		this.nSexoMSocio = nSexoMSocio;
	}
	public String getnSexoFSocio() {
		return nSexoFSocio;
	}
	public void setnSexoFSocio(String nSexoFSocio) {
		this.nSexoFSocio = nSexoFSocio;
	}
	public String getnEstadoCivSocio() {
		return nEstadoCivSocio;
	}
	public void setnEstadoCivSocio(String nEstadoCivSocio) {
		this.nEstadoCivSocio = nEstadoCivSocio;
	}
	public String getnCurpSocio() {
		return nCurpSocio;
	}
	public void setnCurpSocio(String nCurpSocio) {
		this.nCurpSocio = nCurpSocio;
	}
	public String getnRFCSocio() {
		return nRFCSocio;
	}
	public void setnRFCSocio(String nRFCSocio) {
		this.nRFCSocio = nRFCSocio;
	}
	public String getnDomicilioSocio() {
		return nDomicilioSocio;
	}
	public void setnDomicilioSocio(String nDomicilioSocio) {
		this.nDomicilioSocio = nDomicilioSocio;
	}
	public String getnColoniaSocio() {
		return nColoniaSocio;
	}
	public void setnColoniaSocio(String nColoniaSocio) {
		this.nColoniaSocio = nColoniaSocio;
	}
	public String getnCiudadSocio() {
		return nCiudadSocio;
	}
	public void setnCiudadSocio(String nCiudadSocio) {
		this.nCiudadSocio = nCiudadSocio;
	}
	public String getnIdentifSocio() {
		return nIdentifSocio;
	}
	public void setnIdentifSocio(String nIdentifSocio) {
		this.nIdentifSocio = nIdentifSocio;
	}
	public String getnNumeroSocio() {
		return nNumeroSocio;
	}
	public void setnNumeroSocio(String nNumeroSocio) {
		this.nNumeroSocio = nNumeroSocio;
	}
	public String getnFolioSocio() {
		return nFolioSocio;
	}
	public void setnFolioSocio(String nFolioSocio) {
		this.nFolioSocio = nFolioSocio;
	}
	public String getnApellidoPmenor() {
		return nApellidoPmenor;
	}
	public void setnApellidoPmenor(String nApellidoPmenor) {
		this.nApellidoPmenor = nApellidoPmenor;
	}
	public String getnApellidoMmenor() {
		return nApellidoMmenor;
	}
	public void setnApellidoMmenor(String nApellidoMmenor) {
		this.nApellidoMmenor = nApellidoMmenor;
	}
	public String getnNombreMenor() {
		return nNombreMenor;
	}
	public void setnNombreMenor(String nNombreMenor) {
		this.nNombreMenor = nNombreMenor;
	}
	public String getnFechaNacMenor() {
		return nFechaNacMenor;
	}
	public void setnFechaNacMenor(String nFechaNacMenor) {
		this.nFechaNacMenor = nFechaNacMenor;
	}
	public String getnSexoMmenor() {
		return nSexoMmenor;
	}
	public void setnSexoMmenor(String nSexoMmenor) {
		this.nSexoMmenor = nSexoMmenor;
	}
	public String getnSexoFMenor() {
		return nSexoFMenor;
	}
	public void setnSexoFMenor(String nSexoFMenor) {
		this.nSexoFMenor = nSexoFMenor;
	}
	public String getnEstadoCivMenor() {
		return nEstadoCivMenor;
	}
	public void setnEstadoCivMenor(String nEstadoCivMenor) {
		this.nEstadoCivMenor = nEstadoCivMenor;
	}
	public String getnCURPMeor() {
		return nCURPMeor;
	}
	public void setnCURPMeor(String nCURPMeor) {
		this.nCURPMeor = nCURPMeor;
	}
	public String getnRFCMenor() {
		return nRFCMenor;
	}
	public void setnRFCMenor(String nRFCMenor) {
		this.nRFCMenor = nRFCMenor;
	}
	public String getnDomicilioMenor() {
		return nDomicilioMenor;
	}
	public void setnDomicilioMenor(String nDomicilioMenor) {
		this.nDomicilioMenor = nDomicilioMenor;
	}
	public String getnColoniaMenor() {
		return nColoniaMenor;
	}
	public void setnColoniaMenor(String nColoniaMenor) {
		this.nColoniaMenor = nColoniaMenor;
	}
	public String getnCiudadMenor() {
		return nCiudadMenor;
	}
	public void setnCiudadMenor(String nCiudadMenor) {
		this.nCiudadMenor = nCiudadMenor;
	}
	public String getnIdentifMenor() {
		return nIdentifMenor;
	}
	public void setnIdentifMenor(String nIdentifMenor) {
		this.nIdentifMenor = nIdentifMenor;
	}
	public String getnNumeroMenor() {
		return nNumeroMenor;
	}
	public void setnNumeroMenor(String nNumeroMenor) {
		this.nNumeroMenor = nNumeroMenor;
	}
	public String getnFolioMenor() {
		return nFolioMenor;
	}
	public void setnFolioMenor(String nFolioMenor) {
		this.nFolioMenor = nFolioMenor;
	}
	public String getsMarcaDirectivoSi() {
		return sMarcaDirectivoSi;
	}
	public void setsMarcaDirectivoSi(String sMarcaDirectivoSi) {
		this.sMarcaDirectivoSi = sMarcaDirectivoSi;
	}
	public String getsMarcaDirectivoNo() {
		return sMarcaDirectivoNo;
	}
	public void setsMarcaDirectivoNo(String sMarcaDirectivoNo) {
		this.sMarcaDirectivoNo = sMarcaDirectivoNo;
	}
	public String getsCargoDirectivo() {
		return sCargoDirectivo;
	}
	public void setsCargoDirectivo(String sCargoDirectivo) {
		this.sCargoDirectivo = sCargoDirectivo;
	}
	public String getsMarcaFuncionarioSI() {
		return sMarcaFuncionarioSI;
	}
	public void setsMarcaFuncionarioSI(String sMarcaFuncionarioSI) {
		this.sMarcaFuncionarioSI = sMarcaFuncionarioSI;
	}
	public String getsMarcaFuncionarioNo() {
		return sMarcaFuncionarioNo;
	}
	public void setsMarcaFuncionarioNo(String sMarcaFuncionarioNo) {
		this.sMarcaFuncionarioNo = sMarcaFuncionarioNo;
	}
	public String getsCargoFuncionario() {
		return sCargoFuncionario;
	}
	public void setsCargoFuncionario(String sCargoFuncionario) {
		this.sCargoFuncionario = sCargoFuncionario;
	}
	public String getsMarcaEmpleadoSi() {
		return sMarcaEmpleadoSi;
	}
	public void setsMarcaEmpleadoSi(String sMarcaEmpleadoSi) {
		this.sMarcaEmpleadoSi = sMarcaEmpleadoSi;
	}
	public String getsMarcaEmpleadoNo() {
		return sMarcaEmpleadoNo;
	}
	public void setsMarcaEmpleadoNo(String sMarcaEmpleadoNo) {
		this.sMarcaEmpleadoNo = sMarcaEmpleadoNo;
	}
	public String getsCargoEmpleado() {
		return sCargoEmpleado;
	}
	public void setsCargoEmpleado(String sCargoEmpleado) {
		this.sCargoEmpleado = sCargoEmpleado;
	}
	public String getoServAbono() {
		return oServAbono;
	}
	public void setoServAbono(String oServAbono) {
		this.oServAbono = oServAbono;
	}
	public String getoMontDepRealiz() {
		return oMontDepRealiz;
	}
	public void setoMontDepRealiz(String oMontDepRealiz) {
		this.oMontDepRealiz = oMontDepRealiz;
	}
	public String getoEfectivo() {
		return oEfectivo;
	}
	public void setoEfectivo(String oEfectivo) {
		this.oEfectivo = oEfectivo;
	}
	public String getoCheque() {
		return oCheque;
	}
	public void setoCheque(String oCheque) {
		this.oCheque = oCheque;
	}
	public String getoTransferencia() {
		return oTransferencia;
	}
	public void setoTransferencia(String oTransferencia) {
		this.oTransferencia = oTransferencia;
	}
	public String getoNacioal() {
		return oNacioal;
	}
	public void setoNacioal(String oNacioal) {
		this.oNacioal = oNacioal;
	}
	public String getoInter() {
		return oInter;
	}
	public void setoInter(String oInter) {
		this.oInter = oInter;
	}
	public String getoActEcoAct() {
		return oActEcoAct;
	}
	public void setoActEcoAct(String oActEcoAct) {
		this.oActEcoAct = oActEcoAct;
	}
	public String getoComprobanteSi() {
		return oComprobanteSi;
	}
	public void setoComprobanteSi(String oComprobanteSi) {
		this.oComprobanteSi = oComprobanteSi;
	}
	public String getoComprobanteNo() {
		return oComprobanteNo;
	}
	public void setoComprobanteNo(String oComprobanteNo) {
		this.oComprobanteNo = oComprobanteNo;
	}
	public String getoDocJustifica() {
		return oDocJustifica;
	}
	public void setoDocJustifica(String oDocJustifica) {
		this.oDocJustifica = oDocJustifica;
	}
	public String getoTipOperacion() {
		return oTipOperacion;
	}
	public void setoTipOperacion(String oTipOperacion) {
		this.oTipOperacion = oTipOperacion;
	}
	public String getoDescOperacion() {
		return oDescOperacion;
	}
	public void setoDescOperacion(String oDescOperacion) {
		this.oDescOperacion = oDescOperacion;
	}
	public String getoJustificSocio() {
		return oJustificSocio;
	}
	public void setoJustificSocio(String oJustificSocio) {
		this.oJustificSocio = oJustificSocio;
	}
	public String getoObserva() {
		return oObserva;
	}
	public void setoObserva(String oObserva) {
		this.oObserva = oObserva;
	}
	public String getoNomFirmSocCli() {
		return oNomFirmSocCli;
	}
	public void setoNomFirmSocCli(String oNomFirmSocCli) {
		this.oNomFirmSocCli = oNomFirmSocCli;
	}
	public String getoNomFirmEmp() {
		return oNomFirmEmp;
	}
	public void setoNomFirmEmp(String oNomFirmEmp) {
	this.oNomFirmEmp = oNomFirmEmp;
	}
	
	private long id;
    private String nLugarFecha;
    private String nDiaLugFech;
    private String nMesLugFech;
    private String nAnioLugFec;
    private String nEsSocio;
    private String nMenor;
    private String nSocio;
    private String nEsCliente;
    private String nApellidoPSocio;
    private String nApellidoMSocio;
    private String nNombreSocio;
    private String nFechaNacSocio;
    private String nSexoMSocio;
    private String nSexoFSocio;
    private String nEstadoCivSocio;
    private String nCurpSocio;
    private String nRFCSocio;
    private String nDomicilioSocio;
    private String nColoniaSocio;
    private String nCiudadSocio;
    private String nIdentifSocio;
    private String nNumeroSocio;
    private String nFolioSocio;
    private String nApellidoPmenor;
    private String nApellidoMmenor;
    private String nNombreMenor;
    private String nFechaNacMenor;
    private String nSexoMmenor;
    private String nSexoFMenor;
    private String nEstadoCivMenor;
    private String nCURPMeor;
    private String nRFCMenor;
    private String nDomicilioMenor;
    private String nColoniaMenor;
    private String nCiudadMenor;
    private String nIdentifMenor;
    private String nNumeroMenor;
    private String nFolioMenor;
    private String sMarcaDirectivoSi;
    private String sMarcaDirectivoNo;
    private String sCargoDirectivo;
    private String sMarcaFuncionarioSI;
    private String sMarcaFuncionarioNo;
    private String sCargoFuncionario;
    private String sMarcaEmpleadoSi;
    private String sMarcaEmpleadoNo;
    private String sCargoEmpleado;
    private String oServAbono;
    private String oMontDepRealiz;
    private String oEfectivo;
    private String oCheque;
    private String oTransferencia;
    private String oNacioal;
    private String oInter;
    private String oActEcoAct;
    private String oComprobanteSi;
    private String oComprobanteNo;
    private String oDocJustifica;
    private String oTipOperacion;
    private String oDescOperacion;
    private String oJustificSocio;
    private String oObserva;
    private String oNomFirmSocCli;
    private String oNomFirmEmp;
     
	@Override
	public String toString() {
		return "DatosGeneCuestionariolavadoconsultaBE [id=" + id + ", nLugarFecha=" + nLugarFecha + ", nDiaLugFech="
				+ nDiaLugFech + ", nMesLugFech=" + nMesLugFech + ", nAnioLugFec=" + nAnioLugFec + ", nEsSocio="
				+ nEsSocio + ", nMenor=" + nMenor + ", nSocio=" + nSocio + ", nEsCliente=" + nEsCliente
				+ ", nApellidoPSocio=" + nApellidoPSocio + ", nApellidoMSocio=" + nApellidoMSocio + ", nNombreSocio="
				+ nNombreSocio + ", nFechaNacSocio=" + nFechaNacSocio + ", nSexoMSocio=" + nSexoMSocio
				+ ", nSexoFSocio=" + nSexoFSocio + ", nEstadoCivSocio=" + nEstadoCivSocio + ", nCurpSocio=" + nCurpSocio
				+ ", nRFCSocio=" + nRFCSocio + ", nDomicilioSocio=" + nDomicilioSocio + ", nColoniaSocio="
				+ nColoniaSocio + ", nCiudadSocio=" + nCiudadSocio + ", nIdentifSocio=" + nIdentifSocio
				+ ", nNumeroSocio=" + nNumeroSocio + ", nFolioSocio=" + nFolioSocio + ", nApellidoPmenor="
				+ nApellidoPmenor + ", nApellidoMmenor=" + nApellidoMmenor + ", nNombreMenor=" + nNombreMenor
				+ ", nFechaNacMenor=" + nFechaNacMenor + ", nSexoMmenor=" + nSexoMmenor + ", nSexoFMenor=" + nSexoFMenor
				+ ", nEstadoCivMenor=" + nEstadoCivMenor + ", nCURPMeor=" + nCURPMeor + ", nRFCMenor=" + nRFCMenor
				+ ", nDomicilioMenor=" + nDomicilioMenor + ", nColoniaMenor=" + nColoniaMenor + ", nCiudadMenor="
				+ nCiudadMenor + ", nIdentifMenor=" + nIdentifMenor + ", nNumeroMenor=" + nNumeroMenor
				+ ", nFolioMenor=" + nFolioMenor + ", sMarcaDirectivoSi=" + sMarcaDirectivoSi + ", sMarcaDirectivoNo="
				+ sMarcaDirectivoNo + ", sCargoDirectivo=" + sCargoDirectivo + ", sMarcaFuncionarioSI="
				+ sMarcaFuncionarioSI + ", sMarcaFuncionarioNo=" + sMarcaFuncionarioNo + ", sCargoFuncionario="
				+ sCargoFuncionario + ", sMarcaEmpleadoSi=" + sMarcaEmpleadoSi + ", sMarcaEmpleadoNo="
				+ sMarcaEmpleadoNo + ", sCargoEmpleado=" + sCargoEmpleado + ", oServAbono=" + oServAbono
				+ ", oMontDepRealiz=" + oMontDepRealiz + ", oEfectivo=" + oEfectivo + ", oCheque=" + oCheque
				+ ", oTransferencia=" + oTransferencia + ", oNacioal=" + oNacioal + ", oInter=" + oInter
				+ ", oActEcoAct=" + oActEcoAct + ", oComprobanteSi=" + oComprobanteSi + ", oComprobanteNo="
				+ oComprobanteNo + ", oDocJustifica=" + oDocJustifica + ", oTipOperacion=" + oTipOperacion
				+ ", oDescOperacion=" + oDescOperacion + ", oJustificSocio=" + oJustificSocio + ", oObserva=" + oObserva
				+ ", oNomFirmSocCli=" + oNomFirmSocCli + ", oNomFirmEmp=" + oNomFirmEmp + "]";
	}
     
}
