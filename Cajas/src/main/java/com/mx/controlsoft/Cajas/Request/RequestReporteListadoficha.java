package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncafichasListadoficha;
import com.mx.controlsoft.Cajas.Model.ListafichasData;

public class RequestReporteListadoficha {
	public List<EncafichasListadoficha> getTtencafichas() {
		return ttencafichas;
	}
	public void setTtencafichas(List<EncafichasListadoficha> ttencafichas) {
		this.ttencafichas = ttencafichas;
	}
	public List<ListafichasData> getTtlistafichas() {
		return ttlistafichas;
	}
	public void setTtlistafichas(List<ListafichasData> ttlistafichas) {
		this.ttlistafichas = ttlistafichas;
	}
	private List<EncafichasListadoficha> ttencafichas ;
	private List<ListafichasData> ttlistafichas;
}
