package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncDatos_buscaSocioBE_Inicio;

public class RequestEncdatosBuscaSocioBE_Inicio {
	
	List<EncDatos_buscaSocioBE_Inicio> ttEncdatos;

	public List<EncDatos_buscaSocioBE_Inicio> getTtEncdatos() {
		return ttEncdatos;
	}

	public void setTtEncdatos(List<EncDatos_buscaSocioBE_Inicio> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}

	@Override
	public String toString() {
		return "RequestEncdatosBuscaSocioBE_Inicio [ttEncdatos=" + ttEncdatos + "]";
	}
	
}
