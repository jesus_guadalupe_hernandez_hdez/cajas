package com.mx.controlsoft.Cajas.Controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.controlsoft.Cajas.Model.ConsultaSaldo;
import com.mx.controlsoft.Cajas.Model.Encdatos_BuscaSocioBE;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestBuscaSociosBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDepositosBuscaSociosBE;
import com.mx.controlsoft.Cajas.Service.BuscasociosBE_Service;

@Controller
@RequestMapping(value = "/consulta")
public class ConsultaSaldoController {
	
	@Autowired 
	BuscasociosBE_Service buscasociosService;
	
	@RequestMapping(value = "/saldo",method = RequestMethod.GET)
	public String mostrarAperturaEdit(ConsultaSaldo  consultaSaldo ,Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", (Login)sesion.getAttribute("usuario"));
			return "consulta/saldo";
		}
		return "login";

	}
	
	@RequestMapping(value = "/saldo",method = RequestMethod.POST)
	public String mostrarAperturaEdit(@Valid ConsultaSaldo  consultaSaldo,BindingResult result, Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		if (result.hasErrors()) {	
			model.addAttribute("consultaSaldo", consultaSaldo);
			return "consulta/saldo";
	    }
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", (Login)sesion.getAttribute("usuario"));
			model.addAttribute("requestData", getRequestData());
			
			//buscasociosService
			return "consulta/detalleSaldo";
		}
		return "login";

	}
	
	private RequestGralDepositosBuscaSociosBE getRequestData() {
		RequestGralDepositosBuscaSociosBE request = new RequestGralDepositosBuscaSociosBE();
		RequestBuscaSociosBE reqBuscaSocio = new RequestBuscaSociosBE();
		
		List<Encdatos_BuscaSocioBE> lstEncdatos = new LinkedList<>();
		Encdatos_BuscaSocioBE encBuscaSocioBE = new Encdatos_BuscaSocioBE();
        	 
		encBuscaSocioBE.setINid(1);
		encBuscaSocioBE.setINtoken("");
		encBuscaSocioBE.setINnIDUnion("1");
		encBuscaSocioBE.setINnIDCajaPopular("8");
		encBuscaSocioBE.setINnIDSucursal("1");
		encBuscaSocioBE.setINsIDUsuario("lClzdbkdcaaFaJkl");
		encBuscaSocioBE.setINsUnionSocio("1");
		encBuscaSocioBE.setINsCajaSocio("8");
		encBuscaSocioBE.setINsSucursalSocio("1");
		encBuscaSocioBE.setINnIDSocioJ("128");
		
		lstEncdatos.add(encBuscaSocioBE);
		reqBuscaSocio.setTtEncdatos(lstEncdatos);
		request.setRequest(reqBuscaSocio);
		
		RequestGralDepositosBuscaSociosBE requestGralBuscaSocio =  buscasociosService.getListadoSurcusalMenu(request); // new BuscasociosBE_Service(new BuscasociosBE_Dao()).getListadoSurcusalMenu(request);
		return requestGralBuscaSocio;
	}
}
