package com.mx.controlsoft.Cajas.Model;

public class TotalesCajMonitorcajasBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTotCajeros() {
		return totCajeros;
	}
	public void setTotCajeros(String totCajeros) {
		this.totCajeros = totCajeros;
	}
	public String getTotSaldoIni() {
		return totSaldoIni;
	}
	public void setTotSaldoIni(String totSaldoIni) {
		this.totSaldoIni = totSaldoIni;
	}
	public String getTotDepositos() {
		return totDepositos;
	}
	public void setTotDepositos(String totDepositos) {
		this.totDepositos = totDepositos;
	}
	public String getTotRetiros() {
		return totRetiros;
	}
	public void setTotRetiros(String totRetiros) {
		this.totRetiros = totRetiros;
	}
	public String getTotSaldoDia() {
		return totSaldoDia;
	}
	public void setTotSaldoDia(String totSaldoDia) {
		this.totSaldoDia = totSaldoDia;
	}
	public String getTotSaldoAcum() {
		return totSaldoAcum;
	}
	public void setTotSaldoAcum(String totSaldoAcum) {
		this.totSaldoAcum = totSaldoAcum;
	}
	private long id;
    private String totCajeros;
    private String totSaldoIni;
    private String totDepositos;
    private String totRetiros;
    private String totSaldoDia;
    private String totSaldoAcum;
    
	@Override
	public String toString() {
		return "TotalesCajMonitorcajasBE [id=" + id + ", totCajeros=" + totCajeros + ", totSaldoIni=" + totSaldoIni
				+ ", totDepositos=" + totDepositos + ", totRetiros=" + totRetiros + ", totSaldoDia=" + totSaldoDia
				+ ", totSaldoAcum=" + totSaldoAcum + "]";
	}
}
