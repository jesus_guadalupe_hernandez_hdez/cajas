package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IAperturaCierreDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDatoApercierreBE;


@Service
public class AperturaCierreBeService {

	@Autowired
	IAperturaCierreDao<RequestGralDatoApercierreBE> service;
	
	public RequestGralDatoApercierreBE getDataAperturaCierreSuc (RequestGralDatoApercierreBE request) {
		return  this.service.getDataAperturaCierreSuc(request);
	}
}
