package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IConsultaCorteCajaDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscacortecajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralChequescortecajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaCorteCajaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDocumentoscortecajaBE;

@Service
public class BuscacortecajaBEService {
	
	@Autowired
	private IConsultaCorteCajaDao <RequestGralDocumentoscortecajaBE,RequestGralChequescortecajaBE,RequestGralConsultaCorteCajaBE,RequestGralBuscacortecajaBE> rep;
	
	public RequestGralDocumentoscortecajaBE getDocumentoscortecajaBE(RequestGralDocumentoscortecajaBE request) {
		return rep.getDocumentoscortecajaBE(request);
	} 
	public RequestGralChequescortecajaBE getChequesCorteCajaBE(RequestGralChequescortecajaBE request) {
		return rep.getChequesCorteCajaBE(request);
	} 
	public RequestGralConsultaCorteCajaBE getConsultacortecajaBE(RequestGralConsultaCorteCajaBE request) {
		return rep.getConsultacortecajaBE(request);
	} 
	public RequestGralBuscacortecajaBE getBuscacortecajaBE(RequestGralBuscacortecajaBE request) {
		return rep.getBuscacortecajaBE(request);
	} 
}
