package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestAperturaCierre;

public class RequestGralAperturaCierre {
	
	private RequestAperturaCierre request;

	public RequestAperturaCierre getRequest() {
		return request;
	}

	public void setRequest(RequestAperturaCierre request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralAperturaCierre [request=" + request + "]";
	}
}
