package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestConsultaCorteCajaBE;

public class RequestGralConsultaCorteCajaBE {
	private RequestConsultaCorteCajaBE request;

	public RequestConsultaCorteCajaBE getRequest() {
		return request;
	}

	public void setRequest(RequestConsultaCorteCajaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralConsultaCorteCajaBE [request=" + request + "]";
	}
	
}
