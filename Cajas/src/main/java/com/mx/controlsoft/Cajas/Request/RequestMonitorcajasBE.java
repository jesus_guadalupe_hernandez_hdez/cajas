package com.mx.controlsoft.Cajas.Request;
import java.util.List;

import com.mx.controlsoft.Cajas.Model.EncdatosMonitorcajasBE;
import com.mx.controlsoft.Cajas.Model.MonitorCajasMonitorcajasBE;
import com.mx.controlsoft.Cajas.Model.SaldoServCajMonitorcajasBE;
import com.mx.controlsoft.Cajas.Model.TotalesCajMonitorcajasBE;

public class RequestMonitorcajasBE {
	public List<EncdatosMonitorcajasBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosMonitorcajasBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<MonitorCajasMonitorcajasBE> getTtMonitorCajas() {
		return ttMonitorCajas;
	}
	public void setTtMonitorCajas(List<MonitorCajasMonitorcajasBE> ttMonitorCajas) {
		this.ttMonitorCajas = ttMonitorCajas;
	}
	public List<TotalesCajMonitorcajasBE> getTttotalesCaj() {
		return tttotalesCaj;
	}
	public void setTttotalesCaj(List<TotalesCajMonitorcajasBE> tttotalesCaj) {
		this.tttotalesCaj = tttotalesCaj;
	}
	public List<SaldoServCajMonitorcajasBE> getTtSaldoServCaj() {
		return ttSaldoServCaj;
	}
	public void setTtSaldoServCaj(List<SaldoServCajMonitorcajasBE> ttSaldoServCaj) {
		this.ttSaldoServCaj = ttSaldoServCaj;
	}
	
	private List<EncdatosMonitorcajasBE> ttEncdatos;
	private List<MonitorCajasMonitorcajasBE> ttMonitorCajas;
	private List<TotalesCajMonitorcajasBE> tttotalesCaj;
	private List<SaldoServCajMonitorcajasBE> ttSaldoServCaj;
	
	@Override
	public String toString() {
		return "RequestMonitorcajasBE [ttEncdatos=" + ttEncdatos + ", ttMonitorCajas=" + ttMonitorCajas
				+ ", tttotalesCaj=" + tttotalesCaj + ", ttSaldoServCaj=" + ttSaldoServCaj + "]";
	}
	
}
