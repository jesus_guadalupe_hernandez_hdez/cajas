package com.mx.controlsoft.Cajas.Model;

public class DetEfecEfectivofondofijoBE {
	 public long getTtNid() {
		return ttNid;
	}
	public void setTtNid(long ttNid) {
		this.ttNid = ttNid;
	}
	public String getTtMovimiento() {
		return ttMovimiento;
	}
	public void setTtMovimiento(String ttMovimiento) {
		this.ttMovimiento = ttMovimiento;
	}
	public String getTtTipoMoneda() {
		return ttTipoMoneda;
	}
	public void setTtTipoMoneda(String ttTipoMoneda) {
		this.ttTipoMoneda = ttTipoMoneda;
	}
	public String getTtTipoEfec() {
		return ttTipoEfec;
	}
	public void setTtTipoEfec(String ttTipoEfec) {
		this.ttTipoEfec = ttTipoEfec;
	}
	public String getTtCantidad() {
		return ttCantidad;
	}
	public void setTtCantidad(String ttCantidad) {
		this.ttCantidad = ttCantidad;
	}
	public String getTtDenominacion() {
		return ttDenominacion;
	}
	public void setTtDenominacion(String ttDenominacion) {
		this.ttDenominacion = ttDenominacion;
	}
	public String getTtImporte() {
		return ttImporte;
	}
	public void setTtImporte(String ttImporte) {
		this.ttImporte = ttImporte;
	}
	
	private long ttNid;
    private String ttMovimiento;
    private String ttTipoMoneda;
    private String ttTipoEfec;
    private String ttCantidad;
    private String ttDenominacion;
    private String ttImporte;
    
	@Override
	public String toString() {
		return "DetEfecEfectivofondofijoBE [ttNid=" + ttNid + ", ttMovimiento=" + ttMovimiento + ", ttTipoMoneda="
				+ ttTipoMoneda + ", ttTipoEfec=" + ttTipoEfec + ", ttCantidad=" + ttCantidad + ", ttDenominacion="
				+ ttDenominacion + ", ttImporte=" + ttImporte + "]";
	}
}
