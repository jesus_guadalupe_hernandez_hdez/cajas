package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.ILoginDao;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestLogin;

@Service
public class LoginService {
	
	@Autowired
	ILoginDao<RequestLogin,Login> _resp;
	
	public LoginService(ILoginDao<RequestLogin,Login> _resp) {
		this._resp = _resp;
	}
	
	public RequestLogin getUserLogin (Login log) {
		//return  this._resp.getLoginSuc(log);
		return this._resp.getLoginSuc(log);
	}
}
