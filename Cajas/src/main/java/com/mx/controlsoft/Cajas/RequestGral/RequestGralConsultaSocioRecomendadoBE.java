package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoBE;

public class RequestGralConsultaSocioRecomendadoBE {
	
	private RequestConsultaSocioRecomendadoBE request;

	public RequestConsultaSocioRecomendadoBE getRequest() {
		return request;
	}

	public void setRequest(RequestConsultaSocioRecomendadoBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralConsultaSocioRecomendadoBE [request=" + request + "]";
	}
}
