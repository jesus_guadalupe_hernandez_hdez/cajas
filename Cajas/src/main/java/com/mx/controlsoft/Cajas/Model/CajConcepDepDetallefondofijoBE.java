package com.mx.controlsoft.Cajas.Model;

public class CajConcepDepDetallefondofijoBE {
	public long getTtNidDep() {
		return ttNidDep;
	}
	public void setTtNidDep(long ttNidDep) {
		this.ttNidDep = ttNidDep;
	}
	public String getTtDepCajDep() {
		return ttDepCajDep;
	}
	public void setTtDepCajDep(String ttDepCajDep) {
		this.ttDepCajDep = ttDepCajDep;
	}
	public String getTtCajConcepDep() {
		return ttCajConcepDep;
	}
	public void setTtCajConcepDep(String ttCajConcepDep) {
		this.ttCajConcepDep = ttCajConcepDep;
	}
	private long ttNidDep;
    private String ttDepCajDep;
    private String ttCajConcepDep;
	@Override
	public String toString() {
		return "CajConcepDepDetallefondofijoBE [ttNidDep=" + ttNidDep + ", ttDepCajDep=" + ttDepCajDep
				+ ", ttCajConcepDep=" + ttCajConcepDep + "]";
	}
}
