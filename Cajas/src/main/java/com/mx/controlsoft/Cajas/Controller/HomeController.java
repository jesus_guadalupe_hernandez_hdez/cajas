package com.mx.controlsoft.Cajas.Controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.controlsoft.Cajas.Model.Login;

import com.mx.controlsoft.Cajas.Request.RequestLogin;
import com.mx.controlsoft.Cajas.Service.LoginService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
public class HomeController {
	
	@Autowired 
	private LoginService logServce;
	
	private static final Logger logger = LogManager.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public String goLogueo(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			model.addAttribute("strMensaje", "0");
			return "admin";
		}
		return "login";
	}

	@GetMapping("/login")
	public String getLogin(@Valid Login  login,BindingResult result, Model model,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		return this.goAdministrador(logginn, result, model, request, response);
	}
	
	@GetMapping("/logout")
	public String closeSesion(Login  login,HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		sesion.invalidate();
		return "login"; 
	}
	
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	public String goAdministrador( @Valid Login  login,BindingResult result, Model model,HttpServletRequest request, HttpServletResponse response) {
		try
		{
			if (result.hasErrors()) {	
				model.addAttribute("usuario", login);
				logger.info("hay errores de validaión");
				return "login";
		    }
			
			HttpSession sesion = request.getSession();
			RequestLogin reqLogin = logServce.getUserLogin(login); //new LoginService(new LoginDao()).getUserLogin(user1);
			logger.info("Json Recibido Metodo Logueo " + reqLogin);
			if (reqLogin.getTtlogin().size() >= 0) {
				Login recuLogin = reqLogin.getTtlogin().get(0);
				if (recuLogin.getCaduco() == 1) {
					model.addAttribute("usuario", login);
					return "changeLogin";
				}
				if (recuLogin.getMensaje().equals("SUCESS") && recuLogin.getNtries() == 0 )
				{
					sesion.setAttribute("usuario", recuLogin);
					model.addAttribute("usuario", recuLogin);
					return "admin";
				}
				else if (!recuLogin.getMensaje().equals("SUCESS") && (recuLogin.getNtries() >= 1 && recuLogin.getNtries() <= 3  )  ) {
					logger.info("Usuario no correcto " );
					model.addAttribute("strMensaje", recuLogin.getMensaje());
					login.setMensaje(recuLogin.getMensaje());
					login.setNtries(recuLogin.getNtries());
					return "login";
				}
				else {
					logger.info("Usuario bloqueado " );
					model.addAttribute("strMensaje", recuLogin.getMensaje());
					login.setMensaje(recuLogin.getMensaje());
					login.setNtries(recuLogin.getNtries());
					return "login";
				}
			}
			/*
			logger.info("Usuario no correcto " );
			model.addAttribute("strMensaje", "error");
			return "home";
			*/
			return "home";
		}
		catch(Exception ex) {
			logger.error("Error: En motodo de logueo " + ex.getMessage());
			model.addAttribute("mensaje", "Error en el metodo de logueo no esta arriba el server, favor de reportarlo al sistema ");
			return "error";
		}
	}
	
}

