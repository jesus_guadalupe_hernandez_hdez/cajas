package com.mx.controlsoft.Cajas.Controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.controlsoft.Cajas.Model.DetCajaAsignadaFichas;
import com.mx.controlsoft.Cajas.Model.Encafichas;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.Model.Encdatos_FichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.Model.Listafichas;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.Request.RequestFichasxServicioenSucursalBE_Inicio;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasSucursal;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasxServicioenSucursalBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralListadoFicha;
import com.mx.controlsoft.Cajas.Service.ReporteFichasService;
import com.mx.controlsoft.Cajas.Service.ReporteFichasxservicioensucursalBE_Service;


@Controller
@RequestMapping(value = "/reportes")
public class ReportesController {

	@Autowired
	ReporteFichasxservicioensucursalBE_Service service;
	
	@Autowired
	ReporteFichasService service_report;
	
	@RequestMapping(value = "/reporteslistadoFicha", method=RequestMethod.GET)
	public String goAdministrador( Model model , HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			RequestGralFichasSucursal requestGralSuc=  service_report.getListadoSurcusalMenu();
			List< DetCajaAsignadaFichas> lstF =  requestGralSuc.getRequest().getTtdetcajaasignada();
			
			model.addAttribute("lstF", lstF);
			
			return "reportes/index";	
		}
		return "login";

	}	
	
	@RequestMapping(value = "/fichasconhorarioxsucBE", method=RequestMethod.GET)
	public String goFichasconhorarioxsucBE( Model model , HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			//RequestGralFichasSucursal requestGralSuc=  new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			//List< DetCajaAsignadaFichas> lstF =  requestGralSuc.getRequest().getTtdetcajaasignada();
			//model.addAttribute("lstF", lstF);
			return "reportes/fichasconhorarioxsucBE";	
		}
		return "login";

	}	
	
	@RequestMapping(value = "/consultasociorecomendadoBE", method=RequestMethod.GET)
	public String goConsultaSocioRecomendadoBE( Model model , HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			
			EncdatosConsultaSocioRecomendadoBE ecn = new EncdatosConsultaSocioRecomendadoBE();
			
			ecn.setINid(1);
			ecn.setINtoken("");
			ecn.setINnIDUnion("1");
			ecn.setINnIDCajaPopular("8");
			ecn.setINnIDSucursal("1");
			ecn.setINsIDUsuario("root");
			ecn.setRespuesta("");
			
			List<EncdatosConsultaSocioRecomendadoBE> lstEncConsulta = new LinkedList<>();
			lstEncConsulta.add(ecn);
			RequestConsultaSocioRecomendadoBE reqConsulta = new RequestConsultaSocioRecomendadoBE();
			reqConsulta.setTtEncdatos(lstEncConsulta);
			
			RequestGralConsultaSocioRecomendadoBE reqGral = new RequestGralConsultaSocioRecomendadoBE();
			reqGral.setRequest(reqConsulta);
			
			RequestGralConsultaSocioRecomendadoBE requestGralSuc=  service_report.getConsultasociorecomendadoBE(reqGral);
			
			model.addAttribute("lstRequestGral", requestGralSuc);
			return "reportes/consultasociorecomendadoBE";	
		}
		return "login";

	}	
	//
	
	@RequestMapping(value = "/reportesFichasxServicio", method=RequestMethod.GET)
	public String goReportesFichasxServicio( Model model , HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			System.out.println("valor de la sucursal " + logginn.getnSucursal());

			RequestGralFichasxServicioenSucursalBE req = new RequestGralFichasxServicioenSucursalBE();
			Encdatos_FichasxServicioenSucursalBE encFichaServicio = new Encdatos_FichasxServicioenSucursalBE();
			encFichaServicio.setiNid(1);
			encFichaServicio.setiNtoken("");
			encFichaServicio.setiNnIDUnion("1");
			encFichaServicio.setiNnIDCajaPopular("8");
			encFichaServicio.setiNnIDSucursal("1");
			encFichaServicio.setiNsIDUsuario(logginn.getUe());
			encFichaServicio.setiNsFechaInicial("");
			encFichaServicio.setiNsFechaFinal("");
			encFichaServicio.setRespuesta("");
			encFichaServicio.setiNGenerar("");
			encFichaServicio.setiNMenu("");
			encFichaServicio.setiNCajero("");
			encFichaServicio.setnIDCajeroInicio("");
			encFichaServicio.setiNServicio("");
			encFichaServicio.setsNombreCaja("");
			encFichaServicio.setsNombreSucursal("");
			encFichaServicio.setFecha("");
			encFichaServicio.setTermino("");
			List<Encdatos_FichasxServicioenSucursalBE> lstFichasxServicio = new LinkedList<>();
			lstFichasxServicio.add(encFichaServicio);
			RequestFichasxServicioenSucursalBE_Inicio reqFicha = new RequestFichasxServicioenSucursalBE_Inicio();
			reqFicha.setTtEncdatos(lstFichasxServicio);
			
			req.setRequest(reqFicha);
			RequestGralFichasxServicioenSucursalBE requestGralFichaSucursal= service.getFichasxservicioensucursalBE_ReportesInicio(req);  //new ReporteFichasService(new ReporteFichasDao()).getListadoSurcusalMenu();
			model.addAttribute("lstRequestGral", requestGralFichaSucursal);
			model.addAttribute("usuario", logginn);
			return "reportes/fichasxServicioSucursal";	
		}
		return "login";

	}	
	
	@RequestMapping(value = "/reporteslistadoFichaRep", method=RequestMethod.POST)
	public String goreporteSucursalesList( @ModelAttribute("sucMenu") Encafichas  sucMenu, Model model , HttpServletRequest request, HttpServletResponse response) {
	
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			model.addAttribute("usuario", logginn);
			/*Llenado de objeto*/
			sucMenu.setId("1"); 
			sucMenu.setnUnion("1");
			sucMenu.setnCaja("8"); 
			sucMenu.setnSucursal("1");
			
			//sucMenu.setFechaReporte(fechaReporte); "FechaReporte": "02-01-2020",
	        //"CajaAsignadaReporte": "100",
			sucMenu.setsCaja(""); 
			sucMenu.setsSucursal(""); 
			sucMenu.setTitulo(""); 
			sucMenu.setFiltro(""); 
			/*se termina llenado de objeto*/
			
			RequestGralListadoFicha requestGralSuc =  service_report.getListadoFichaSucursales(sucMenu);
			List<Listafichas> lstFicha =  requestGralSuc.getRequest().getTtlistafichas();

			model.addAttribute("lstListadoFicha", lstFicha);
			
			RequestGralFichasSucursal requestGralFicha=  service_report.getListadoSurcusalMenu();
			List< DetCajaAsignadaFichas> lstF =  requestGralFicha.getRequest().getTtdetcajaasignada();
			
			

			model.addAttribute("lstF", lstF);
			
			return "reportes/index";		
		}
		return "login";
	}	
	
	@RequestMapping(value = "/reportesCancelafichacorreoconsultaBE", method=RequestMethod.GET)
	public String goreporteCancelaFichaCorreoConsultaBEList(  Model model , HttpServletRequest request, HttpServletResponse response) {
	
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			
			model.addAttribute("usuario", logginn);
						
			return "reportes/cancelaFichaCorreo";		
		}
		return "login";
	}	
}
