package com.mx.controlsoft.Cajas.Request;
import java.util.List;

import com.mx.controlsoft.Cajas.Model.Apertura ;
import com.mx.controlsoft.Cajas.Model.Dato;
import com.mx.controlsoft.Cajas.Model.HistoricoApertura;

public class RequestAperturaCierre {
	
	public List<Apertura> getTtserapertura() {
		return ttserapertura;
	}
	public void setTtserapertura(List<Apertura> ttserapertura) {
		this.ttserapertura = ttserapertura;
	}
	public List<HistoricoApertura> getTthistapertura() {
		return tthistapertura;
	}
	public void setTthistapertura(List<HistoricoApertura> tthistapertura) {
		this.tthistapertura = tthistapertura;
	}
	public List<Dato> getTtdato() {
		return ttdato;
	}
	public void setTtdato(List<Dato> ttdato) {
		this.ttdato = ttdato;
	}
	
	private List<Apertura> ttserapertura;
	private List<HistoricoApertura> tthistapertura;
	private List<Dato> ttdato;
	
	@Override
	public String toString() {
		return "RequestAperturaCierre [ttserapertura=" + ttserapertura + ", tthistapertura=" + tthistapertura
				+ ", ttdato=" + ttdato + "]";
	}
}
