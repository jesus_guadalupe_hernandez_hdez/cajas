package com.mx.controlsoft.Cajas.Model;

public class ListafichasData {
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getsFicha() {
		return sFicha;
	}
	public void setsFicha(String sFicha) {
		this.sFicha = sFicha;
	}
	public String getsSocio() {
		return sSocio;
	}
	public void setsSocio(String sSocio) {
		this.sSocio = sSocio;
	}
	public String getsNombre() {
		return sNombre;
	}
	public void setsNombre(String sNombre) {
		this.sNombre = sNombre;
	}
	public String getServicio() {
		return Servicio;
	}
	public void setServicio(String servicio) {
		Servicio = servicio;
	}
	public String getCargo() {
		return Cargo;
	}
	public void setCargo(String cargo) {
		Cargo = cargo;
	}
	public String getAbono() {
		return Abono;
	}
	public void setAbono(String abono) {
		Abono = abono;
	}
	public String getSaldo() {
		return Saldo;
	}
	public void setSaldo(String saldo) {
		Saldo = saldo;
	}
	private long id;
    private String sFicha;
    private String sSocio;
    private String sNombre;
    private String Servicio;
    private String Cargo;
    private String Abono;
    private String Saldo;
    
	@Override
	public String toString() {
		return "ListafichasData [id=" + id + ", sFicha=" + sFicha + ", sSocio=" + sSocio + ", sNombre=" + sNombre
				+ ", Servicio=" + Servicio + ", Cargo=" + Cargo + ", Abono=" + Abono + ", Saldo=" + Saldo + "]";
	}
    
}
