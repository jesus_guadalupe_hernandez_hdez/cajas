package com.mx.controlsoft.Cajas.Controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.mx.controlsoft.Cajas.IDao.IIdeexpideconstanciaBEDao;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosIdeexpideconstanciaBE;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.Request.RequestIdeexpideconstanciaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralIdeexpideconstanciaBE;
import com.mx.controlsoft.Cajas.Service.IdeexpideconstanciaBEService;

@Controller
public class ExpideConstanciaIDEController {
	
	@Autowired
	IdeexpideconstanciaBEService rep;

	@GetMapping ("/expide/constanciaIDE")
	public String getAceptacheqsalvobuencobro(Login  login, Model model, HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			
			EncdatosIdeexpideconstanciaBE ecn = new EncdatosIdeexpideconstanciaBE();
			
			ecn.setiNid(1);
			ecn.setiNtoken("");
			ecn.setiNnIDUnion(logginn.getnUnion());
			ecn.setiNnIDCajaPopular(logginn.getnCaja());
			ecn.setiNnIDSucursa(logginn.getSucursal());
			ecn.setiNsIDUsuario("lClzdbkdcaaFaJkl");
			ecn.setiNPCaja1(logginn.getnCaja());
			ecn.setiNPNumSocio("");
			ecn.setiNListaSucursal("");
			ecn.setiNGeneraReporte("");
			ecn.setiNlmes("");
			ecn.setiNNumSocio("");
			
			List<EncdatosIdeexpideconstanciaBE> lstEncConsulta = new LinkedList<>();
			lstEncConsulta.add(ecn);
			RequestIdeexpideconstanciaBE reqConsulta = new RequestIdeexpideconstanciaBE();
			reqConsulta.setTtEncdatos(lstEncConsulta);
			
			RequestGralIdeexpideconstanciaBE reqGral = new RequestGralIdeexpideconstanciaBE();
			reqGral.setRequest(reqConsulta);
			
			RequestGralIdeexpideconstanciaBE requestGralSuc=  rep.getDataIdeexpideconstanciaBE (reqGral);
			
			model.addAttribute("lstRequestGral", requestGralSuc);
			
			return "expide/constanciaIDE";
		}
		return "login";
		
	}
}
