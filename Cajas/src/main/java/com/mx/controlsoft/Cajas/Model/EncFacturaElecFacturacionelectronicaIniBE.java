package com.mx.controlsoft.Cajas.Model;

public class EncFacturaElecFacturacionelectronicaIniBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursal() {
		return INnIDSucursal;
	}
	public void setINnIDSucursal(String iNnIDSucursal) {
		INnIDSucursal = iNnIDSucursal;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getINmensaje() {
		return INmensaje;
	}
	public void setINmensaje(String iNmensaje) {
		INmensaje = iNmensaje;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}
	public String getINsUnionSocio() {
		return INsUnionSocio;
	}
	public void setINsUnionSocio(String iNsUnionSocio) {
		INsUnionSocio = iNsUnionSocio;
	}
	public String getINsCajaSocio() {
		return INsCajaSocio;
	}
	public void setINsCajaSocio(String iNsCajaSocio) {
		INsCajaSocio = iNsCajaSocio;
	}
	public String getINsSucursalSocio() {
		return INsSucursalSocio;
	}
	public void setINsSucursalSocio(String iNsSucursalSocio) {
		INsSucursalSocio = iNsSucursalSocio;
	}
	public String getINsNumeroSocio() {
		return INsNumeroSocio;
	}
	public void setINsNumeroSocio(String iNsNumeroSocio) {
		INsNumeroSocio = iNsNumeroSocio;
	}
	public String getINsAccion() {
		return INsAccion;
	}
	public void setINsAccion(String iNsAccion) {
		INsAccion = iNsAccion;
	}
	public String getINidFechaI() {
		return INidFechaI;
	}
	public void setINidFechaI(String iNidFechaI) {
		INidFechaI = iNidFechaI;
	}
	public String getINidFechaF() {
		return INidFechaF;
	}
	public void setINidFechaF(String iNidFechaF) {
		INidFechaF = iNidFechaF;
	}
	public String getINidFacturaACancelar() {
		return INidFacturaACancelar;
	}
	public void setINidFacturaACancelar(String iNidFacturaACancelar) {
		INidFacturaACancelar = iNidFacturaACancelar;
	}
	
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursal;
    private String INsIDUsuario;
    private String INmensaje;
    private String INsTransaccion;
    private String INsUnionSocio;
    private String INsCajaSocio;
    private String INsSucursalSocio;
    private String INsNumeroSocio;
    private String INsAccion;
    private String INidFechaI;
    private String INidFechaF;
    private String INidFacturaACancelar;
    
	@Override
	public String toString() {
		return "EncFacturaElecFacturacionelectronicaIniBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion="
				+ INnIDUnion + ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursal=" + INnIDSucursal
				+ ", INsIDUsuario=" + INsIDUsuario + ", INmensaje=" + INmensaje + ", INsTransaccion=" + INsTransaccion
				+ ", INsUnionSocio=" + INsUnionSocio + ", INsCajaSocio=" + INsCajaSocio + ", INsSucursalSocio="
				+ INsSucursalSocio + ", INsNumeroSocio=" + INsNumeroSocio + ", INsAccion=" + INsAccion + ", INidFechaI="
				+ INidFechaI + ", INidFechaF=" + INidFechaF + ", INidFacturaACancelar=" + INidFacturaACancelar + "]";
	}
}
