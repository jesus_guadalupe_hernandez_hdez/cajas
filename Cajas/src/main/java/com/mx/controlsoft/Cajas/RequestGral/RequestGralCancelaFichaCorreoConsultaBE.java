package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestCancelaFichaCorreoConsultaBE;

public class RequestGralCancelaFichaCorreoConsultaBE {
	private RequestCancelaFichaCorreoConsultaBE request;

	public RequestCancelaFichaCorreoConsultaBE getRequest() {
		return request;
	}

	public void setRequest(RequestCancelaFichaCorreoConsultaBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralCancelaFichaCorreoConsultaBE [request=" + request + "]";
	}
	
}
