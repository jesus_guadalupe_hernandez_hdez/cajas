package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestBuscaSociosBE;

public class RequestGralDepositosBuscaSociosBE {
	
	private RequestBuscaSociosBE request;

	public RequestBuscaSociosBE getRequest() {
		return request;
	}

	public void setRequest(RequestBuscaSociosBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestBuscaSociosBE [request=" + request + "]";
	}
}
