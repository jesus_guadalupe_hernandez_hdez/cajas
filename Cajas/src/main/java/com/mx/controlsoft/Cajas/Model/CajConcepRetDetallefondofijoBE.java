package com.mx.controlsoft.Cajas.Model;

public class CajConcepRetDetallefondofijoBE {
	public long getTtNidRet() {
		return ttNidRet;
	}
	public void setTtNidRet(long ttNidRet) {
		this.ttNidRet = ttNidRet;
	}
	public String getTtDepCajRet() {
		return ttDepCajRet;
	}
	public void setTtDepCajRet(String ttDepCajRet) {
		this.ttDepCajRet = ttDepCajRet;
	}
	public String getTtCajConcepRet() {
		return ttCajConcepRet;
	}
	public void setTtCajConcepRet(String ttCajConcepRet) {
		this.ttCajConcepRet = ttCajConcepRet;
	}
	private long ttNidRet;
    private String ttDepCajRet;
    private String ttCajConcepRet;
	@Override
	public String toString() {
		return "CajConcepRetDetallefondofijoBE [ttNidRet=" + ttNidRet + ", ttDepCajRet=" + ttDepCajRet
				+ ", ttCajConcepRet=" + ttCajConcepRet + "]";
	}
}
