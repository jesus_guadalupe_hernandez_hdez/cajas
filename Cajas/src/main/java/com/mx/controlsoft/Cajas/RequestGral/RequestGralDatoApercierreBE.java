package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestDatoApercierreBE;

public class RequestGralDatoApercierreBE {
	
	private RequestDatoApercierreBE request;

	public RequestDatoApercierreBE getRequest() {
		return request;
	}

	public void setRequest(RequestDatoApercierreBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralDatoApercierreBE [request=" + request + "]";
	}
	
}
