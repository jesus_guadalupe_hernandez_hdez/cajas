package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.ICuestionariolavadoconsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralCuestionariolavadoconsultaBE;

@Service
public class CuestionariolavadoconsultaBEService {

	@Autowired
	ICuestionariolavadoconsultaBE<RequestGralCuestionariolavadoconsultaBE> _resp;
	
	public RequestGralCuestionariolavadoconsultaBE getPostCuestionarioLavadoConsulta (RequestGralCuestionariolavadoconsultaBE request) {
		return _resp.getPostCuestionarioLavadoConsulta(request);
	}
}
