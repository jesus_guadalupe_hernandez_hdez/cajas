package com.mx.controlsoft.Cajas.Model;

public class Encdatos_FichasconHorarioxSucBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursal() {
		return iNnIDSucursal;
	}
	public void setiNnIDSucursal(String iNnIDSucursal) {
		this.iNnIDSucursal = iNnIDSucursal;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getiNEjercicio() {
		return iNEjercicio;
	}
	public void setiNEjercicio(String iNEjercicio) {
		this.iNEjercicio = iNEjercicio;
	}
	public String getiNdFechaInicial() {
		return iNdFechaInicial;
	}
	public void setiNdFechaInicial(String iNdFechaInicial) {
		this.iNdFechaInicial = iNdFechaInicial;
	}
	public String getiNdFechaFinal() {
		return iNdFechaFinal;
	}
	public void setiNdFechaFinal(String iNdFechaFinal) {
		this.iNdFechaFinal = iNdFechaFinal;
	}
	public String getiNsAccion() {
		return iNsAccion;
	}
	public void setiNsAccion(String iNsAccion) {
		this.iNsAccion = iNsAccion;
	}
	public String getsNombreCaja() {
		return sNombreCaja;
	}
	public void setsNombreCaja(String sNombreCaja) {
		this.sNombreCaja = sNombreCaja;
	}
	public String getsRFCCaja() {
		return sRFCCaja;
	}
	public void setsRFCCaja(String sRFCCaja) {
		this.sRFCCaja = sRFCCaja;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursal;
    private String iNsIDUsuario;
    private String iNEjercicio;
    private String iNdFechaInicial;
    private String iNdFechaFinal;
    private String iNsAccion;
    private String sNombreCaja;
    private String sRFCCaja;
    private String fecha;
    private String respuesta;
    
	@Override
	public String toString() {
		return "Encdatos_FichasconHorarioxSucBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursal=" + iNnIDSucursal + ", iNsIDUsuario="
				+ iNsIDUsuario + ", iNEjercicio=" + iNEjercicio + ", iNdFechaInicial=" + iNdFechaInicial
				+ ", iNdFechaFinal=" + iNdFechaFinal + ", iNsAccion=" + iNsAccion + ", sNombreCaja=" + sNombreCaja
				+ ", sRFCCaja=" + sRFCCaja + ", fecha=" + fecha + ", respuesta=" + respuesta + "]";
	}

}
