package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestFichasxServicioenSucursalBE_Inicio;

public class RequestGralFichasxServicioenSucursalBE {
	public RequestFichasxServicioenSucursalBE_Inicio getRequest() {
		return request;
	}

	public void setRequest(RequestFichasxServicioenSucursalBE_Inicio request) {
		this.request = request;
	}

	private RequestFichasxServicioenSucursalBE_Inicio request;

	@Override
	public String toString() {
		return "RequestGralFichasxServicioenSucursalBE [request=" + request + "]";
	}
	
}
