package com.mx.controlsoft.Cajas.Model;

public class PrestamosBuscaSocioBE {
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public String getPrestamo() {
		return prestamo;
	}
	public void setPrestamo(String prestamo) {
		this.prestamo = prestamo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getSitua() {
		return situa;
	}
	public void setSitua(String situa) {
		this.situa = situa;
	}
	public String getFechavencimiento() {
		return fechavencimiento;
	}
	public void setFechavencimiento(String fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}
	public String getdMontoEntregado() {
		return dMontoEntregado;
	}
	public void setdMontoEntregado(String dMontoEntregado) {
		this.dMontoEntregado = dMontoEntregado;
	}
	public String getMontopagado() {
		return montopagado;
	}
	public void setMontopagado(String montopagado) {
		this.montopagado = montopagado;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public String getnInteresNormal() {
		return nInteresNormal;
	}
	public void setnInteresNormal(String nInteresNormal) {
		this.nInteresNormal = nInteresNormal;
	}
	public String getnInteresTMoratorio() {
		return nInteresTMoratorio;
	}
	public void setnInteresTMoratorio(String nInteresTMoratorio) {
		this.nInteresTMoratorio = nInteresTMoratorio;
	}
	public String getnIVAMoraDev() {
		return nIVAMoraDev;
	}
	public void setnIVAMoraDev(String nIVAMoraDev) {
		this.nIVAMoraDev = nIVAMoraDev;
	}
	public String getnIVADev() {
		return nIVADev;
	}
	public void setnIVADev(String nIVADev) {
		this.nIVADev = nIVADev;
	}
	public String getnNumAboVen() {
		return nNumAboVen;
	}
	public void setnNumAboVen(String nNumAboVen) {
		this.nNumAboVen = nNumAboVen;
	}
	public String getnCapitalVencido() {
		return nCapitalVencido;
	}
	public void setnCapitalVencido(String nCapitalVencido) {
		this.nCapitalVencido = nCapitalVencido;
	}
	public String getDiasvencidos() {
		return diasvencidos;
	}
	public void setDiasvencidos(String diasvencidos) {
		this.diasvencidos = diasvencidos;
	}
	public String getnDiasMoraC() {
		return nDiasMoraC;
	}
	public void setnDiasMoraC(String nDiasMoraC) {
		this.nDiasMoraC = nDiasMoraC;
	}
	public String getsFecUltAbonoCapC() {
		return sFecUltAbonoCapC;
	}
	public void setsFecUltAbonoCapC(String sFecUltAbonoCapC) {
		this.sFecUltAbonoCapC = sFecUltAbonoCapC;
	}
	public String getsFecUltAbonoIntC() {
		return sFecUltAbonoIntC;
	}
	public void setsFecUltAbonoIntC(String sFecUltAbonoIntC) {
		this.sFecUltAbonoIntC = sFecUltAbonoIntC;
	}
	public String getCal() {
		return cal;
	}
	public void setCal(String cal) {
		this.cal = cal;
	}
	
	private long ID;
    private String prestamo;
    private String tipo;
    private String estado;
    private String situa;
    private String fechavencimiento;
    private String dMontoEntregado;
    private String montopagado;
    private String saldo;
    private String nInteresNormal;
    private String nInteresTMoratorio;
    private String nIVAMoraDev;
    private String nIVADev;
    private String nNumAboVen;
    private String nCapitalVencido;
    private String diasvencidos;
    private String nDiasMoraC;
    private String sFecUltAbonoCapC;
    private String sFecUltAbonoIntC;
    private String cal;
    
	@Override
	public String toString() {
		return "PrestamosBuscaSocioBE [ID=" + ID + ", prestamo=" + prestamo + ", tipo=" + tipo + ", estado=" + estado
				+ ", situa=" + situa + ", fechavencimiento=" + fechavencimiento + ", dMontoEntregado=" + dMontoEntregado
				+ ", montopagado=" + montopagado + ", saldo=" + saldo + ", nInteresNormal=" + nInteresNormal
				+ ", nInteresTMoratorio=" + nInteresTMoratorio + ", nIVAMoraDev=" + nIVAMoraDev + ", nIVADev=" + nIVADev
				+ ", nNumAboVen=" + nNumAboVen + ", nCapitalVencido=" + nCapitalVencido + ", diasvencidos="
				+ diasvencidos + ", nDiasMoraC=" + nDiasMoraC + ", sFecUltAbonoCapC=" + sFecUltAbonoCapC
				+ ", sFecUltAbonoIntC=" + sFecUltAbonoIntC + ", cal=" + cal + "]";
	}
 
}
