package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetalleConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosConsultasociorecomendadoconsultaBE;

public class RequestConsultaSocioRecomendadoConsultaBE {
	public List<EncdatosConsultasociorecomendadoconsultaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosConsultasociorecomendadoconsultaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DetalleConsultaSocioRecomendadoConsultaBE> getTtDetalle() {
		return ttDetalle;
	}
	public void setTtDetalle(List<DetalleConsultaSocioRecomendadoConsultaBE> ttDetalle) {
		this.ttDetalle = ttDetalle;
	}
	List<EncdatosConsultasociorecomendadoconsultaBE> ttEncdatos;
	List<DetalleConsultaSocioRecomendadoConsultaBE> ttDetalle;
	
	@Override
	public String toString() {
		return "RequestConsultaSocioRecomendadoConsultaBE [ttEncdatos=" + ttEncdatos + ", ttDetalle=" + ttDetalle + "]";
	}
}
