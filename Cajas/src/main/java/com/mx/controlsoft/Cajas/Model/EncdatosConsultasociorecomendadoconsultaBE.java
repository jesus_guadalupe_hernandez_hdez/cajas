package com.mx.controlsoft.Cajas.Model;

public class EncdatosConsultasociorecomendadoconsultaBE {
	 public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursal() {
		return INnIDSucursal;
	}
	public void setINnIDSucursal(String iNnIDSucursal) {
		INnIDSucursal = iNnIDSucursal;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getINsFechaIni() {
		return INsFechaIni;
	}
	public void setINsFechaIni(String iNsFechaIni) {
		INsFechaIni = iNsFechaIni;
	}
	public String getINsFechafin() {
		return INsFechafin;
	}
	public void setINsFechafin(String iNsFechafin) {
		INsFechafin = iNsFechafin;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	private long INid;
    private String INtoken;
    private String INnIDUnion;
    private String INnIDCajaPopular;
    private String INnIDSucursal;
    private String INsIDUsuario;
    private String INsFechaIni;
    private String INsFechafin;
    private String respuesta;
    private String titulo;
    
	@Override
	public String toString() {
		return "EncdatosConsultasociorecomendadoconsultaBE [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion="
				+ INnIDUnion + ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursal=" + INnIDSucursal
				+ ", INsIDUsuario=" + INsIDUsuario + ", INsFechaIni=" + INsFechaIni + ", INsFechafin=" + INsFechafin
				+ ", respuesta=" + respuesta + ", titulo=" + titulo + "]";
	}
}
