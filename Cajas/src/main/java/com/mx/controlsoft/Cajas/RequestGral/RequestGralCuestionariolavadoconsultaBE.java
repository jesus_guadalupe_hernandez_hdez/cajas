package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestCuestionariolavadoconsultaBE;

public class RequestGralCuestionariolavadoconsultaBE {
	public RequestCuestionariolavadoconsultaBE getRequest() {
		return request;
	}

	public void setRequest(RequestCuestionariolavadoconsultaBE request) {
		this.request = request;
	}

	private RequestCuestionariolavadoconsultaBE request;

	@Override
	public String toString() {
		return "RequestGralCuestionariolavadoconsultaBE [request=" + request + "]";
	}
}
