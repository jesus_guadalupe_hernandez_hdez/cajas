package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IReporteFichasDao;
import com.mx.controlsoft.Cajas.Model.Encafichas;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasSucursal;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralListadoFicha;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralReporteListadoficha;

@Service
public class ReporteFichasService {
	
	@Autowired
	IReporteFichasDao<RequestGralFichasSucursal,RequestGralListadoFicha,Encafichas,RequestGralFichasconHorarioxSucBE,RequestGralConsultaSocioRecomendadoBE,RequestGralConsultaSocioRecomendadoConsultaBE,RequestGralReporteListadoficha,RequestGralCancelaFichaCorreoConsultaBE> reportFichaSucursal;
	
	public ReporteFichasService(IReporteFichasDao<RequestGralFichasSucursal,RequestGralListadoFicha,Encafichas,RequestGralFichasconHorarioxSucBE,RequestGralConsultaSocioRecomendadoBE,RequestGralConsultaSocioRecomendadoConsultaBE,RequestGralReporteListadoficha,RequestGralCancelaFichaCorreoConsultaBE> reportFichaSucursal) {
		this.reportFichaSucursal = reportFichaSucursal;
	}
	
	public RequestGralFichasSucursal getListadoSurcusalMenu () {
		return  this.reportFichaSucursal.getDataSucursales();
	}
	
	public RequestGralListadoFicha getListadoFichaSucursales (Encafichas ficha) {
		return  this.reportFichaSucursal.getListadoFichaSucursales(ficha);
	}
	
	public RequestGralFichasconHorarioxSucBE getFichasconHorarioxSucBE (RequestGralFichasconHorarioxSucBE request)
	{
		return  this.reportFichaSucursal.getFichasconHorarioxSucBE(request);
	}
	
	public RequestGralConsultaSocioRecomendadoBE getConsultasociorecomendadoBE (RequestGralConsultaSocioRecomendadoBE request)
	{
		
		return  this.reportFichaSucursal.getConsultasociorecomendadoBE(request);
	}

	public RequestGralConsultaSocioRecomendadoConsultaBE getConsultaSocioRecomendadoConsultaBE (RequestGralConsultaSocioRecomendadoConsultaBE request)
	{
		return  this.reportFichaSucursal.getConsultaSocioRecomendadoConsultaBE(request);
	}
	
	public RequestGralReporteListadoficha getConsultaListadoficha (RequestGralReporteListadoficha request)
	{
		return  this.reportFichaSucursal.getConsultaListadoficha(request);
	}
	
	public RequestGralCancelaFichaCorreoConsultaBE getConsultaCancelaFichaCorreoConsultaBE (RequestGralCancelaFichaCorreoConsultaBE request)
	{
		return  this.reportFichaSucursal.getConsultaCancelaFichaCorreoConsultaBE(request);
	}
	
}
