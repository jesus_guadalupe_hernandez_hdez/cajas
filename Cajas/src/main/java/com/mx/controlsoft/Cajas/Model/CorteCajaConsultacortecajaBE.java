package com.mx.controlsoft.Cajas.Model;

public class CorteCajaConsultacortecajaBE {
	 public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public String getSesion() {
		return Sesion;
	}
	public void setSesion(String sesion) {
		Sesion = sesion;
	}
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		Hora = hora;
	}
	public String getCaja() {
		return Caja;
	}
	public void setCaja(String caja) {
		Caja = caja;
	}
	public String getIDUnion() {
		return IDUnion;
	}
	public void setIDUnion(String iDUnion) {
		IDUnion = iDUnion;
	}
	public String getIDCajaPopular() {
		return IDCajaPopular;
	}
	public void setIDCajaPopular(String iDCajaPopular) {
		IDCajaPopular = iDCajaPopular;
	}
	public String getIDSucursal() {
		return IDSucursal;
	}
	public void setIDSucursal(String iDSucursal) {
		IDSucursal = iDSucursal;
	}
	public String getIDCajaAsignada() {
		return IDCajaAsignada;
	}
	public void setIDCajaAsignada(String iDCajaAsignada) {
		IDCajaAsignada = iDCajaAsignada;
	}
	public String getMonto() {
		return Monto;
	}
	public void setMonto(String monto) {
		Monto = monto;
	}
	public String getResultado() {
		return Resultado;
	}
	public void setResultado(String resultado) {
		Resultado = resultado;
	}
	private long id;
    private String Fecha;
    private String Sesion;
    private String Hora;
    private String Caja;
    private String IDUnion;
    private String IDCajaPopular;
    private String IDSucursal;
    private String IDCajaAsignada;
    private String Monto;
    private String Resultado;
	@Override
	public String toString() {
		return "CorteCajaConsultacortecajaBE [id=" + id + ", Fecha=" + Fecha + ", Sesion=" + Sesion + ", Hora=" + Hora
				+ ", Caja=" + Caja + ", IDUnion=" + IDUnion + ", IDCajaPopular=" + IDCajaPopular + ", IDSucursal="
				+ IDSucursal + ", IDCajaAsignada=" + IDCajaAsignada + ", Monto=" + Monto + ", Resultado=" + Resultado
				+ "]";
	}
}
