package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DetCajaAsignadaFichas;
import com.mx.controlsoft.Cajas.Model.Encdatos_Fichas;

public class RequestFichasSucursal {
	public List<Encdatos_Fichas> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<Encdatos_Fichas> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DetCajaAsignadaFichas> getTtdetcajaasignada() {
		return ttdetcajaasignada;
	}
	public void setTtdetcajaasignada(List<DetCajaAsignadaFichas> ttdetcajaasignada) {
		this.ttdetcajaasignada = ttdetcajaasignada;
	}
	
	private List<Encdatos_Fichas> ttEncdatos;
	private List<DetCajaAsignadaFichas> ttdetcajaasignada;
	
	@Override
	public String toString() {
		return "RequestFichasSucursal [ttEncdatos=" + ttEncdatos + ", ttdetcajaasignada=" + ttdetcajaasignada + "]";
	}
}
