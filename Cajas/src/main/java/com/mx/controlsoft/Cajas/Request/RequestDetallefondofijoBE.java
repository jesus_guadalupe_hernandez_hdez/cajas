package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.CajConcepDepDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.CajConcepRetDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.CajRecepDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.DetFichaFondoFijoDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.EncdatosDetallefondofijoBE;
import com.mx.controlsoft.Cajas.Model.TipoTranDetallefondofijoBE;

public class RequestDetallefondofijoBE {
	public List<EncdatosDetallefondofijoBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosDetallefondofijoBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<CajRecepDetallefondofijoBE> getTtCajRecep() {
		return ttCajRecep;
	}
	public void setTtCajRecep(List<CajRecepDetallefondofijoBE> ttCajRecep) {
		this.ttCajRecep = ttCajRecep;
	}
	public List<CajConcepRetDetallefondofijoBE> getTtCajConcepRet() {
		return ttCajConcepRet;
	}
	public void setTtCajConcepRet(List<CajConcepRetDetallefondofijoBE> ttCajConcepRet) {
		this.ttCajConcepRet = ttCajConcepRet;
	}
	public List<CajConcepDepDetallefondofijoBE> getTtCajConcepDep() {
		return ttCajConcepDep;
	}
	public void setTtCajConcepDep(List<CajConcepDepDetallefondofijoBE> ttCajConcepDep) {
		this.ttCajConcepDep = ttCajConcepDep;
	}
	public List<TipoTranDetallefondofijoBE> getTtTipoTran() {
		return ttTipoTran;
	}
	public void setTtTipoTran(List<TipoTranDetallefondofijoBE> ttTipoTran) {
		this.ttTipoTran = ttTipoTran;
	}
	private List< EncdatosDetallefondofijoBE> ttEncdatos;
	private List<CajRecepDetallefondofijoBE> ttCajRecep;
	private List<CajConcepRetDetallefondofijoBE> ttCajConcepRet;
	private List<CajConcepDepDetallefondofijoBE> ttCajConcepDep;
	private List<TipoTranDetallefondofijoBE> ttTipoTran;
	private List<DetFichaFondoFijoDetallefondofijoBE> ttDetFichaFondoFijo;
	public List<DetFichaFondoFijoDetallefondofijoBE> getTtDetFichaFondoFijo() {
		return ttDetFichaFondoFijo;
	}
	public void setTtDetFichaFondoFijo(List<DetFichaFondoFijoDetallefondofijoBE> ttDetFichaFondoFijo) {
		this.ttDetFichaFondoFijo = ttDetFichaFondoFijo;
	}
	@Override
	public String toString() {
		return "RequestDetallefondofijoBE [ttEncdatos=" + ttEncdatos + ", ttCajRecep=" + ttCajRecep
				+ ", ttCajConcepRet=" + ttCajConcepRet + ", ttCajConcepDep=" + ttCajConcepDep + ", ttTipoTran="
				+ ttTipoTran + ", ttDetFichaFondoFijo=" + ttDetFichaFondoFijo + "]";
	}
	
}
