package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.Encafichas;
import com.mx.controlsoft.Cajas.Model.Listafichas;

public class RequestListadoficha {
	public List<Encafichas> getTtencafichas() {
		return ttencafichas;
	}
	public void setTtencafichas(List<Encafichas> ttencafichas) {
		this.ttencafichas = ttencafichas;
	}
	public List<Listafichas> getTtlistafichas() {
		return ttlistafichas;
	}
	public void setTtlistafichas(List<Listafichas> ttlistafichas) {
		this.ttlistafichas = ttlistafichas;
	}
	
	private List<Encafichas> ttencafichas;
	private List<Listafichas> ttlistafichas;
	
	@Override
	public String toString() {
		return "RequestListadoficha [ttencafichas=" + ttencafichas + ", ttlistafichas=" + ttlistafichas + "]";
	}
}
