package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.IDepositosDao;
import com.mx.controlsoft.Cajas.Request.RequestBuscasocioAceptarBE;
import com.mx.controlsoft.Cajas.Request.RequestEncdatosBuscaSocioBE_Inicio;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralBuscaSocioOtraSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleFichaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDetalleframedBE_One;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class DepositosDao implements IDepositosDao <RequestGralBuscaSocioOtraSucBE,RequestEncdatosBuscaSocioBE_Inicio,RequestGralDetalleframedBE_One,RequestGralDetalleFichaBE,RequestBuscasocioAceptarBE>{
	//RequestGralDetalleFichaBE
	
	private static final Logger logger = LogManager.getLogger(DepositosDao.class);
	
	public RequestGralBuscaSocioOtraSucBE getBuscaSocioOtraSucBE(RequestGralBuscaSocioOtraSucBE request) {
		try {
			RequestGralBuscaSocioOtraSucBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/buscasociootrasucBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralBuscaSocioOtraSucBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestEncdatosBuscaSocioBE_Inicio getBuscaSocioBE_Inicio(RequestEncdatosBuscaSocioBE_Inicio request) {
		try {
			RequestEncdatosBuscaSocioBE_Inicio requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/buscasocioBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestEncdatosBuscaSocioBE_Inicio.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralDetalleframedBE_One getDepositoDetalleFramedBE(RequestGralDetalleframedBE_One request) {
		try {
			RequestGralDetalleframedBE_One requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json getDepositoDetalleFramedBE : " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/detalleframedBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralDetalleframedBE_One.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }
	}

	public RequestGralDetalleFichaBE getDepositoDetallefichaBE_Dat(RequestGralDetalleFichaBE request) {
		try {
			RequestGralDetalleFichaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json detallefichaBE: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/detallefichaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralDetalleFichaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	//
	public RequestBuscasocioAceptarBE getBuscaSocioAceptarBE(RequestBuscasocioAceptarBE request) {
		try {
			RequestBuscasocioAceptarBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/buscasocioaceptarBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestBuscasocioAceptarBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }
	}
}
