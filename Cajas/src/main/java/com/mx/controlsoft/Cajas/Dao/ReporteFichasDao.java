package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.IReporteFichasDao;
import com.mx.controlsoft.Cajas.Model.Encafichas;
import com.mx.controlsoft.Cajas.Model.Encdatos_Fichas;
import com.mx.controlsoft.Cajas.Request.RequestFichasSucursal;
import com.mx.controlsoft.Cajas.Request.RequestListadoficha;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralConsultaSocioRecomendadoConsultaBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasSucursal;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasconHorarioxSucBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralListadoFicha;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralReporteListadoficha;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class ReporteFichasDao implements IReporteFichasDao<RequestGralFichasSucursal,RequestGralListadoFicha,Encafichas, RequestGralFichasconHorarioxSucBE,RequestGralConsultaSocioRecomendadoBE,RequestGralConsultaSocioRecomendadoConsultaBE,RequestGralReporteListadoficha,RequestGralCancelaFichaCorreoConsultaBE
> {
	
	private static final Logger logger = LogManager.getLogger(ReporteFichasDao.class);
	
	public RequestGralListadoFicha getListadoFichaSucursales (Encafichas ficha) {
		try {
			RequestGralListadoFicha request = null;
			Gson gson = new Gson();
			RequestGralListadoFicha requestA = new RequestGralListadoFicha();
			RequestListadoficha requestEnvia = new RequestListadoficha();
			
			List<Encafichas> lstListadoFicha = new LinkedList<>();
			
			lstListadoFicha.add(ficha);
			requestEnvia.setTtencafichas (lstListadoFicha);
		
			requestA.setRequest(requestEnvia);
			
			String JSON = gson.toJson(requestA);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/listadoficha");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	
            	request  = gson.fromJson(output,
            			RequestGralListadoFicha.class);
            	logger.info(request);
            }
            
            conn.disconnect();
            return request;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }
	}

	public RequestGralFichasSucursal getDataSucursales () {
		try {
			
			Gson gson = new Gson();
			
			RequestGralFichasSucursal request = null;
			
			RequestGralFichasSucursal datRequest = new RequestGralFichasSucursal();
			Encdatos_Fichas ficha = new Encdatos_Fichas();
			ficha.setINid(1); 
			ficha.setINtoken("");
			ficha.setINnIDUnion("1");
			ficha.setINnIDCajaPopular("8"); 
            ficha.setINnIDSucursal("1"); 
            ficha.setINsIDUsuario(""); 
            ficha.setRespuesta(""); 
            ficha.setINMenu("CAJA"); 
            ficha.setINFechaReporte("31/01/2020"); 
            
            List<Encdatos_Fichas>  lstFichaEncDatos = new LinkedList<>();
            lstFichaEncDatos.add(ficha);
            //ttEncdatos
            RequestFichasSucursal reqq = new RequestFichasSucursal();
            reqq.setTtEncdatos(lstFichaEncDatos);
            
            datRequest.setRequest(reqq);
            
			String json = gson.toJson(datRequest);
			
			
            URL url = new URL( ConfigProperties.getConex() +"/wscajapService/menulistadodefichasBE");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            
            while ((output = br.readLine()) != null) {
            	
            	request  = gson.fromJson(output,
            			RequestGralFichasSucursal.class);
            	
            }
            
            conn.disconnect();
            return request;

        } catch (Exception e) {
        	logger.error("Exception in NetClientGet:- " + e);
            return null;
        }

	}

	public RequestGralFichasconHorarioxSucBE getFichasconHorarioxSucBE(RequestGralFichasconHorarioxSucBE request) {
		try {
			RequestGralFichasconHorarioxSucBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/fichasconhorarioxsucBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralFichasconHorarioxSucBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralConsultaSocioRecomendadoBE getConsultasociorecomendadoBE(RequestGralConsultaSocioRecomendadoBE request) {
		try {
			RequestGralConsultaSocioRecomendadoBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/consultasociorecomendadoBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralConsultaSocioRecomendadoBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}

	public RequestGralConsultaSocioRecomendadoConsultaBE getConsultaSocioRecomendadoConsultaBE(RequestGralConsultaSocioRecomendadoConsultaBE request) {
		try {
			RequestGralConsultaSocioRecomendadoConsultaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/consultasociorecomendadoconsultaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralConsultaSocioRecomendadoConsultaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}

	public RequestGralReporteListadoficha getConsultaListadoficha(RequestGralReporteListadoficha request) {
		try {
			RequestGralReporteListadoficha requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/listadoficha");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralReporteListadoficha.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}

	//
	
	public RequestGralCancelaFichaCorreoConsultaBE getConsultaCancelaFichaCorreoConsultaBE(RequestGralCancelaFichaCorreoConsultaBE request) {
		try {
			RequestGralCancelaFichaCorreoConsultaBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/cancelafichacorreoconsultaBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralCancelaFichaCorreoConsultaBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}

}
