package com.mx.controlsoft.Cajas.Model;

public class DetdatDebe_DetalleframedBE {
	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public long getINiddet() {
		return INiddet;
	}
	public void setINiddet(long iNiddet) {
		INiddet = iNiddet;
	}
	public String getHRef() {
		return HRef;
	}
	public void setHRef(String hRef) {
		HRef = hRef;
	}
	public String getServicioDescripcion() {
		return servicioDescripcion;
	}
	public void setServicioDescripcion(String servicioDescripcion) {
		this.servicioDescripcion = servicioDescripcion;
	}
	public String getSaldoSocio() {
		return saldoSocio;
	}
	public void setSaldoSocio(String saldoSocio) {
		this.saldoSocio = saldoSocio;
	}
	private long  INid;
    private long INiddet;
    private String HRef;
    private String servicioDescripcion;
    private String saldoSocio;
    
	@Override
	public String toString() {
		return "DetdatDebe_DetalleframedBE [INid=" + INid + ", INiddet=" + INiddet + ", HRef=" + HRef
				+ ", servicioDescripcion=" + servicioDescripcion + ", saldoSocio=" + saldoSocio + "]";
	}
}
