package com.mx.controlsoft.Cajas.Model;

public class SucursalConsultaSocioRecomendadoBE {
	public long getIDSucursal() {
		return IDSucursal;
	}
	public void setIDSucursal(long iDSucursal) {
		IDSucursal = iDSucursal;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	private long IDSucursal;
    private String Nombre;
    
	@Override
	public String toString() {
		return "SucursalConsultaSocioRecomendadoBE [IDSucursal=" + IDSucursal + ", Nombre=" + Nombre + "]";
	}
    
}
