package com.mx.controlsoft.Cajas.RequestGral;

import com.mx.controlsoft.Cajas.Request.RequestFacturacionelectronicaIniBE;

public class RequestGralFacturacionelectronicaIniBE {
	private RequestFacturacionelectronicaIniBE request;

	public RequestFacturacionelectronicaIniBE getRequest() {
		return request;
	}

	public void setRequest(RequestFacturacionelectronicaIniBE request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "RequestGralFacturacionelectronicaIniBE [request=" + request + "]";
	}
	
}
