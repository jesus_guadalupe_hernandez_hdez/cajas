package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.ICajeroPrincipalDao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralDesglosesaldocajeroBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralEfectivofondofijoBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicaIniBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFacturacionelectronicajsBE;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralMonitorcajasBE;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class CajeroPrincipalDao implements ICajeroPrincipalDao <RequestGralFacturacionelectronicaIniBE, RequestGralFacturacionelectronicajsBE,RequestGralMonitorcajasBE, RequestGralDesglosesaldocajeroBE
,RequestGralEfectivofondofijoBE> {
	
	private static final Logger logger = LogManager.getLogger(CajeroPrincipalDao.class);
	
	public RequestGralEfectivofondofijoBE getPostDetalleFondoAceptar (RequestGralEfectivofondofijoBE request) {
		try {
			RequestGralEfectivofondofijoBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/efectivofondofijoBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralEfectivofondofijoBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralFacturacionelectronicaIniBE getPostFacturacionelectronicaIniBE (RequestGralFacturacionelectronicaIniBE request) {
		try {
			RequestGralFacturacionelectronicaIniBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/facturacionelectronicaIniBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralFacturacionelectronicaIniBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralFacturacionelectronicajsBE getPostFacturacionelectronicajsBE (RequestGralFacturacionelectronicajsBE request) {
		try {
			RequestGralFacturacionelectronicajsBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/facturacionelectronicajsBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralFacturacionelectronicajsBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralMonitorcajasBE getPostMonitorcajasBE (RequestGralMonitorcajasBE request) {
		try {
			RequestGralMonitorcajasBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/monitorcajasBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralMonitorcajasBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
	
	public RequestGralDesglosesaldocajeroBE getPostDesglosesaldocajeroBE (RequestGralDesglosesaldocajeroBE request) {
		try {
			RequestGralDesglosesaldocajeroBE requestSend = null;
			Gson gson = new Gson();
			
			String JSON = gson.toJson(request);
			logger.info("valor del json: " + JSON);
            URL url = new URL(ConfigProperties.getConex() + "/wscajapService/desglosesaldocajeroBE");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            OutputStream os = conn.getOutputStream();
            os.write(JSON.getBytes("UTF-8"));
            os.close();
            
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
 
            while ((output = br.readLine()) != null) {
            	
            	logger.info("Antes de convertir a objeto: "  + output);
            	requestSend  = gson.fromJson(output,
            			RequestGralDesglosesaldocajeroBE.class);
            	logger.info("Valor del response: "  + requestSend);
            }
            
            conn.disconnect();
            return requestSend;

        } catch (Exception e) {
        	logger.info("Exception in NetClientGet:- " + e);
            return null;
        }
	}
}
