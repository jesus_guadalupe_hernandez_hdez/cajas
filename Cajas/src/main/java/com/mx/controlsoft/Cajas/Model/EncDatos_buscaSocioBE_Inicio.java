package com.mx.controlsoft.Cajas.Model;

public class EncDatos_buscaSocioBE_Inicio {

	public long getINid() {
		return INid;
	}
	public void setINid(long iNid) {
		INid = iNid;
	}
	public String getINtoken() {
		return INtoken;
	}
	public void setINtoken(String iNtoken) {
		INtoken = iNtoken;
	}
	public String getINnIDUnion() {
		return INnIDUnion;
	}
	public void setINnIDUnion(String iNnIDUnion) {
		INnIDUnion = iNnIDUnion;
	}
	public String getINnIDCajaPopular() {
		return INnIDCajaPopular;
	}
	public void setINnIDCajaPopular(String iNnIDCajaPopular) {
		INnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getINnIDSucursa() {
		return INnIDSucursa;
	}
	public void setINnIDSucursa(String iNnIDSucursa) {
		INnIDSucursa = iNnIDSucursa;
	}
	public String getINsIDUsuario() {
		return INsIDUsuario;
	}
	public void setINsIDUsuario(String iNsIDUsuario) {
		INsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getINsTransaccion() {
		return INsTransaccion;
	}
	public void setINsTransaccion(String iNsTransaccion) {
		INsTransaccion = iNsTransaccion;
	}

	
	private long INid;
	private String INtoken;
	private String INnIDUnion;
	private String INnIDCajaPopular;
	private String INnIDSucursa;
	private String INsIDUsuario;
	private String INsTransaccion;
	private String respuesta;
	private String sDescripcionTransaccion;
	
	public String getsDescripcionTransaccion() {
		return sDescripcionTransaccion;
	}
	public void setsDescripcionTransaccion(String sDescripcionTransaccion) {
		this.sDescripcionTransaccion = sDescripcionTransaccion;
	}
	
	@Override
	public String toString() {
		return "EncDatos_buscaSocioBE_Inicio [INid=" + INid + ", INtoken=" + INtoken + ", INnIDUnion=" + INnIDUnion
				+ ", INnIDCajaPopular=" + INnIDCajaPopular + ", INnIDSucursa=" + INnIDSucursa + ", INsIDUsuario="
				+ INsIDUsuario + ", INsTransaccion=" + INsTransaccion + ", respuesta=" + respuesta
				+ ", sDescripcionTransaccion=" + sDescripcionTransaccion + "]";
	}	
}
