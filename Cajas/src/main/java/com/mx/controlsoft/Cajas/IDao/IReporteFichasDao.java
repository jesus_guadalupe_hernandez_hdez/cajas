package com.mx.controlsoft.Cajas.IDao;

public interface IReporteFichasDao <T,U,O,Q,R,S,V,X>{
	
	public T getDataSucursales(); 
	
	public U getListadoFichaSucursales(O ficha);
	
	public Q getFichasconHorarioxSucBE (Q request); 
	
	public R getConsultasociorecomendadoBE (R request); 
	
	public S getConsultaSocioRecomendadoConsultaBE (S request); 
	
	public V getConsultaListadoficha (V request); 
	
	public X getConsultaCancelaFichaCorreoConsultaBE(X request);

}
