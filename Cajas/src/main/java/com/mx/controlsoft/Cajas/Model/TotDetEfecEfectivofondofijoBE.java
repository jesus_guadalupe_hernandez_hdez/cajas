package com.mx.controlsoft.Cajas.Model;

public class TotDetEfecEfectivofondofijoBE {
	public long getTtNid() {
		return ttNid;
	}
	public void setTtNid(long ttNid) {
		this.ttNid = ttNid;
	}
	public String getTtTotEfec() {
		return ttTotEfec;
	}
	public void setTtTotEfec(String ttTotEfec) {
		this.ttTotEfec = ttTotEfec;
	}
	public String getTtTotEntregar() {
		return ttTotEntregar;
	}
	public void setTtTotEntregar(String ttTotEntregar) {
		this.ttTotEntregar = ttTotEntregar;
	}
	public String getTtTotGeneral() {
		return ttTotGeneral;
	}
	public void setTtTotGeneral(String ttTotGeneral) {
		this.ttTotGeneral = ttTotGeneral;
	}
	private long ttNid;
    private String ttTotEfec;
    private String ttTotEntregar;
    private String ttTotGeneral;
}
