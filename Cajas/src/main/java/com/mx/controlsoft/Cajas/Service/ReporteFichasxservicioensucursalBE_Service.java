package com.mx.controlsoft.Cajas.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.controlsoft.Cajas.IDao.IFichasxServicioenSucursalBE_Dao;
import com.mx.controlsoft.Cajas.RequestGral.RequestGralFichasxServicioenSucursalBE;

@Service
public class ReporteFichasxservicioensucursalBE_Service {

	@Autowired
	IFichasxServicioenSucursalBE_Dao<RequestGralFichasxServicioenSucursalBE> req;
	
	public RequestGralFichasxServicioenSucursalBE getFichasxservicioensucursalBE_ReportesInicio (RequestGralFichasxServicioenSucursalBE request) {
		return  this.req.getFichasxservicioensucursalBE_ReportesInicio(request);
	}
}
