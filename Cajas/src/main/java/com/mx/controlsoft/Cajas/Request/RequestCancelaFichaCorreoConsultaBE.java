package com.mx.controlsoft.Cajas.Request;

import java.util.List;

import com.mx.controlsoft.Cajas.Model.DealleCancelConsCancelaFichaCorreoConsultaBE;
import com.mx.controlsoft.Cajas.Model.EncdatosCancelaFichaCorreoConsultaBE;

public class RequestCancelaFichaCorreoConsultaBE {
	
	public List<EncdatosCancelaFichaCorreoConsultaBE> getTtEncdatos() {
		return ttEncdatos;
	}
	public void setTtEncdatos(List<EncdatosCancelaFichaCorreoConsultaBE> ttEncdatos) {
		this.ttEncdatos = ttEncdatos;
	}
	public List<DealleCancelConsCancelaFichaCorreoConsultaBE> getTtDealleCancelCons() {
		return ttDealleCancelCons;
	}
	public void setTtDealleCancelCons(List<DealleCancelConsCancelaFichaCorreoConsultaBE> ttDealleCancelCons) {
		this.ttDealleCancelCons = ttDealleCancelCons;
	}
	
	private List<EncdatosCancelaFichaCorreoConsultaBE>ttEncdatos;
	private List<DealleCancelConsCancelaFichaCorreoConsultaBE> ttDealleCancelCons;
	
	@Override
	public String toString() {
		return "RequestCancelaFichaCorreoConsultaBE [ttEncdatos=" + ttEncdatos + ", ttDealleCancelCons="
				+ ttDealleCancelCons + "]";
	}
}
