package com.mx.controlsoft.Cajas.Model;

public class EncdatosBuscacortecajaBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursa() {
		return iNnIDSucursa;
	}
	public void setiNnIDSucursa(String iNnIDSucursa) {
		this.iNnIDSucursa = iNnIDSucursa;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getiNIDUnion() {
		return iNIDUnion;
	}
	public void setiNIDUnion(String iNIDUnion) {
		this.iNIDUnion = iNIDUnion;
	}
	public String getiNIDCajaPopular() {
		return iNIDCajaPopular;
	}
	public void setiNIDCajaPopular(String iNIDCajaPopular) {
		this.iNIDCajaPopular = iNIDCajaPopular;
	}
	public String getiNIDSucursal() {
		return iNIDSucursal;
	}
	public void setiNIDSucursal(String iNIDSucursal) {
		this.iNIDSucursal = iNIDSucursal;
	}
	public String getiNIDCajaAsignada() {
		return iNIDCajaAsignada;
	}
	public void setiNIDCajaAsignada(String iNIDCajaAsignada) {
		this.iNIDCajaAsignada = iNIDCajaAsignada;
	}
	public String getiNFecha() {
		return iNFecha;
	}
	public void setiNFecha(String iNFecha) {
		this.iNFecha = iNFecha;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getnMinutos() {
		return nMinutos;
	}
	public void setnMinutos(String nMinutos) {
		this.nMinutos = nMinutos;
	}
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursa;
    private String iNsIDUsuario;
    private String iNIDUnion;
    private String iNIDCajaPopular;
    private String iNIDSucursal;
    private String iNIDCajaAsignada;
    private String iNFecha;
    private String respuesta;
    private String nMinutos;
	@Override
	public String toString() {
		return "EncdatosBuscacortecajaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursa=" + iNnIDSucursa + ", iNsIDUsuario="
				+ iNsIDUsuario + ", iNIDUnion=" + iNIDUnion + ", iNIDCajaPopular=" + iNIDCajaPopular + ", iNIDSucursal="
				+ iNIDSucursal + ", iNIDCajaAsignada=" + iNIDCajaAsignada + ", iNFecha=" + iNFecha + ", respuesta="
				+ respuesta + ", nMinutos=" + nMinutos + "]";
	}
}
