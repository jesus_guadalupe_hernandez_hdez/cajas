package com.mx.controlsoft.Cajas.Model;

public class Encdatos_FichasxServicioenSucursalBE {
	public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursal() {
		return iNnIDSucursal;
	}
	public void setiNnIDSucursal(String iNnIDSucursal) {
		this.iNnIDSucursal = iNnIDSucursal;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getiNsFechaInicial() {
		return iNsFechaInicial;
	}
	public void setiNsFechaInicial(String iNsFechaInicial) {
		this.iNsFechaInicial = iNsFechaInicial;
	}
	public String getiNsFechaFinal() {
		return iNsFechaFinal;
	}
	public void setiNsFechaFinal(String iNsFechaFinal) {
		this.iNsFechaFinal = iNsFechaFinal;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getiNGenerar() {
		return iNGenerar;
	}
	public void setiNGenerar(String iNGenerar) {
		this.iNGenerar = iNGenerar;
	}
	public String getiNMenu() {
		return iNMenu;
	}
	public void setiNMenu(String iNMenu) {
		this.iNMenu = iNMenu;
	}
	public String getiNCajero() {
		return iNCajero;
	}
	public void setiNCajero(String iNCajero) {
		this.iNCajero = iNCajero;
	}
	public String getnIDCajeroInicio() {
		return nIDCajeroInicio;
	}
	public void setnIDCajeroInicio(String nIDCajeroInicio) {
		this.nIDCajeroInicio = nIDCajeroInicio;
	}
	public String getiNServicio() {
		return iNServicio;
	}
	public void setiNServicio(String iNServicio) {
		this.iNServicio = iNServicio;
	}
	public String getsNombreCaja() {
		return sNombreCaja;
	}
	public void setsNombreCaja(String sNombreCaja) {
		this.sNombreCaja = sNombreCaja;
	}
	public String getsNombreSucursal() {
		return sNombreSucursal;
	}
	public void setsNombreSucursal(String sNombreSucursal) {
		this.sNombreSucursal = sNombreSucursal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTermino() {
		return termino;
	}
	public void setTermino(String termino) {
		this.termino = termino;
	}
	
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursal;
    private String iNsIDUsuario;
    private String iNsFechaInicial;
    private String iNsFechaFinal;
    private String respuesta;
    private String iNGenerar;
    private String iNMenu;
    private String iNCajero;
    private String nIDCajeroInicio;
    private String iNServicio;
    private String sNombreCaja;
    private String sNombreSucursal;
    private String fecha;
    private String termino;
    
	@Override
	public String toString() {
		return "Encdatos_FichasxServicioenSucursalBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion="
				+ iNnIDUnion + ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursal=" + iNnIDSucursal
				+ ", iNsIDUsuario=" + iNsIDUsuario + ", iNsFechaInicial=" + iNsFechaInicial + ", iNsFechaFinal="
				+ iNsFechaFinal + ", respuesta=" + respuesta + ", iNGenerar=" + iNGenerar + ", iNMenu=" + iNMenu
				+ ", iNCajero=" + iNCajero + ", nIDCajeroInicio=" + nIDCajeroInicio + ", iNServicio=" + iNServicio
				+ ", sNombreCaja=" + sNombreCaja + ", sNombreSucursal=" + sNombreSucursal + ", fecha=" + fecha
				+ ", termino=" + termino + "]";
	}
}
