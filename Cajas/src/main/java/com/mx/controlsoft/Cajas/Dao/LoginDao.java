package com.mx.controlsoft.Cajas.Dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mx.controlsoft.Cajas.IDao.ILoginDao;
import com.mx.controlsoft.Cajas.Model.Login;
import com.mx.controlsoft.Cajas.Request.RequestLogin;
import com.mx.controlsoft.Cajas.Utilerias.ConfigProperties;

@Service
public class LoginDao implements ILoginDao<RequestLogin,Login> {
	
	public RequestLogin getLoginSuc (Login log) {
		
			try {
				RequestLogin request = null;
				Gson gson = new Gson();
				RequestLogin requestEnvia = new RequestLogin();
				Login login = log;
				
				login.setId(1);
				login.setToken("");  
			    
				List<Login> lstLogin = new LinkedList<>();
				
				lstLogin.add(login);
				requestEnvia.setTtlogin(lstLogin);
			
				
				String JSON = gson.toJson(requestEnvia);
				
	            URL url = new URL(ConfigProperties.getConex() +"/wscajapService/loginBE");//your url i.e fetch data from .
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setRequestMethod("POST");
	            conn.setConnectTimeout(5000);
	            //conn.setRequestProperty("Accept", "application/json");
	            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	            conn.setDoInput(true);
	            conn.setDoOutput(true);
	            
	            OutputStream os = conn.getOutputStream();
	            os.write(JSON.getBytes("UTF-8"));
	            os.close();
	            
	            if (conn.getResponseCode() != 200) {
	                throw new RuntimeException("Failed : HTTP Error code : "
	                        + conn.getResponseCode());
	            }
	            InputStreamReader in = new InputStreamReader(conn.getInputStream());
	            BufferedReader br = new BufferedReader(in);
	            String output;
	 
	            while ((output = br.readLine()) != null) {
	            	
	            	
	            	request  = gson.fromJson(output,
	            			RequestLogin.class);
	            	//System.out.println(request);
	            }
	            
	            conn.disconnect();
	            return request;

	        } catch (Exception e) {
	            System.out.println("Exception in NetClientGet:- " + e);
	            return null;
	        }

	}

}
