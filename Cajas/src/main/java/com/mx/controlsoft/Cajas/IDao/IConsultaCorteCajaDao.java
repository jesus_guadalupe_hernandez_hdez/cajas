package com.mx.controlsoft.Cajas.IDao;

public interface IConsultaCorteCajaDao<T,U,X,Z> {
	public T getDocumentoscortecajaBE(T request); 
	public U getChequesCorteCajaBE(U request); 
	public X getConsultacortecajaBE(X request); 
	public Z getBuscacortecajaBE(Z request); 
}
