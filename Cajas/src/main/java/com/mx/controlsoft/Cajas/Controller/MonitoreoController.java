package com.mx.controlsoft.Cajas.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.controlsoft.Cajas.Model.Login;

@Controller
@RequestMapping(value = "/monitoreo")
public class MonitoreoController {

	@RequestMapping(value = "/liberacion",method = RequestMethod.GET)
	public String mostrarPrincipal(Model model , HttpServletRequest request, HttpServletResponse response) {
		HttpSession sesion = request.getSession();
		Login logginn = (Login) sesion.getAttribute("usuario");
		if (logginn != null) {
			model.addAttribute("usuario", logginn);
			//return "monitoreo/monitoreoLiberacion";
			return "construccion";
		}
		return "login";
		
	}
}
