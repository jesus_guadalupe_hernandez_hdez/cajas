package com.mx.controlsoft.Cajas.Model;

public class EncDatosChequesCorteCajaBE {
	 public long getiNid() {
		return iNid;
	}
	public void setiNid(long iNid) {
		this.iNid = iNid;
	}
	public String getiNtoken() {
		return iNtoken;
	}
	public void setiNtoken(String iNtoken) {
		this.iNtoken = iNtoken;
	}
	public String getiNnIDUnion() {
		return iNnIDUnion;
	}
	public void setiNnIDUnion(String iNnIDUnion) {
		this.iNnIDUnion = iNnIDUnion;
	}
	public String getiNnIDCajaPopular() {
		return iNnIDCajaPopular;
	}
	public void setiNnIDCajaPopular(String iNnIDCajaPopular) {
		this.iNnIDCajaPopular = iNnIDCajaPopular;
	}
	public String getiNnIDSucursa() {
		return iNnIDSucursa;
	}
	public void setiNnIDSucursa(String iNnIDSucursa) {
		this.iNnIDSucursa = iNnIDSucursa;
	}
	public String getiNsIDUsuario() {
		return iNsIDUsuario;
	}
	public void setiNsIDUsuario(String iNsIDUsuario) {
		this.iNsIDUsuario = iNsIDUsuario;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getiNrowidcortecaja() {
		return iNrowidcortecaja;
	}
	public void setiNrowidcortecaja(String iNrowidcortecaja) {
		this.iNrowidcortecaja = iNrowidcortecaja;
	}
	public String getsTitulo() {
		return sTitulo;
	}
	public void setsTitulo(String sTitulo) {
		this.sTitulo = sTitulo;
	}
	public String getnBanco() {
		return nBanco;
	}
	public void setnBanco(String nBanco) {
		this.nBanco = nBanco;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getnTotalCheques() {
		return nTotalCheques;
	}
	public void setnTotalCheques(String nTotalCheques) {
		this.nTotalCheques = nTotalCheques;
	}
	public String getnMinutos() {
		return nMinutos;
	}
	public void setnMinutos(String nMinutos) {
		this.nMinutos = nMinutos;
	}
	private long iNid;
    private String iNtoken;
    private String iNnIDUnion;
    private String iNnIDCajaPopular;
    private String iNnIDSucursa;
    private String iNsIDUsuario;
    private String respuesta;
    private String iNrowidcortecaja;
    private String sTitulo;
    private String nBanco;
    private String titulo;
    private String nTotalCheques;
    private String nMinutos;
	@Override
	public String toString() {
		return "EncDatosChequesCorteCajaBE [iNid=" + iNid + ", iNtoken=" + iNtoken + ", iNnIDUnion=" + iNnIDUnion
				+ ", iNnIDCajaPopular=" + iNnIDCajaPopular + ", iNnIDSucursa=" + iNnIDSucursa + ", iNsIDUsuario="
				+ iNsIDUsuario + ", respuesta=" + respuesta + ", iNrowidcortecaja=" + iNrowidcortecaja + ", sTitulo="
				+ sTitulo + ", nBanco=" + nBanco + ", titulo=" + titulo + ", nTotalCheques=" + nTotalCheques
				+ ", nMinutos=" + nMinutos + "]";
	}
}
