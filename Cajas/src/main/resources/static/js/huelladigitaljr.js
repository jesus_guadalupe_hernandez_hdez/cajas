/*
JAIME RENE RAMIREZ OSEGUERA
jr30072@gmail.com
*/
	var ProtocoloHD		= "";
	var IPServidorHD	= "";
	/*var DirPromoHD		= "/class/applet/promo/";*/
	var DirPromoHD		= "http://172.8.1.18:8585/applet/promo/";
	var DirCajaHD		= "/class/applet/caja/";
	var ScriptHD		= "index.html";
	var strParametros	= "center:yes;dialogwidth:720px;dialogheight:590px;resizable:no;scroll:no;location:no;";
	function createCorsObject(){
		var objetoAjax=false;
		if (window.XDomainRequest){ //IE8
			objetoAjax = new XDomainRequest();
		}
		else if (window.XMLHttpRequest){ //FF, Opera, IE7
	    	objetoAjax = new XMLHttpRequest();
  		}
		else if (window.ActiveXObject){ //IE6
			objetoAjax = new ActiveXObject('Microsoft.XMLHTTP')
		}
		else { //No Ajax Compatibles
		   	objetoAjax = null;
		}

		if (!objetoAjax && typeof XMLHttpRequest!='undefined'){
			objetoAjax = new XMLHttpRequest();
		}

	return objetoAjax;
	}


		/* Cancelar Llamadas Call Center en el Reporte */
	var xdr_existeHuellaSocio;
    var err_existeHuellaSocio = function(){alert("XDR onerror");}
    var timeo_existeHuellaSocio = function(){alert("XDR ontimeout");}
    var load_existeHuellaSocio = function(){
        //alert("Got: " + xdr_existeHuellaSocio.responseText);

		var respuesta2 = TrimSec(xdr_existeHuellaSocio.responseText);		
		
    }
    var progres_existeHuellaSocio = function(){/*alert("Progresso");*/}
    var stopdata_existeHuellaSocio = function(){alert("stop");xdr_existeHuellaSocio.abort();}
	
	
	existeHuellaSocio = function(IDUnion,IDCajaPopular,IDSucursal,IDSocio,mostrarAlertas,hacerDespues,hacerNoEncontro){
		var lEntrar = false;
		if(IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){lEntrar=true;}
		else{alert("Los parametros de prueba estan vacios");}
		if(lEntrar){

			var IDSocioAux		= TrimSec(IDSocio);
			var IDSucursalAux	= TrimSec(IDSucursal);


			if(IDSocioAux.length>=12){
				var IDSocioCompleto = TrimSec(IDSocioAux);
				IDSucursalAux=IDSocioCompleto.substring(4,6);
				IDSocioAux=IDSocioCompleto.substring(6);
			}

			var cadena = "";
			var aleatorio = Math.random();
			var url = "http://172.8.1.20/4glhuellas/4glhuellas.php?"+
													"IDUnion="+IDUnion+
													"&IDCajaPopular="+IDCajaPopular+
													"&IDSucursal="+IDSucursalAux+
													"&IDSocio="+IDSocioAux+
													"&ID="+aleatorio;
	        var timeout = 15000;
			var xdr_existeHuellaSocio=createCorsObject();
			
			if (xdr_existeHuellaSocio){
                xdr_existeHuellaSocio.onerror		= err_existeHuellaSocio;
                xdr_existeHuellaSocio.ontimeout		= timeo_existeHuellaSocio;
                xdr_existeHuellaSocio.onprogress	= progres_existeHuellaSocio;
                xdr_existeHuellaSocio.onload		= function(){ 
					//alert(xdr_existeHuellaSocio.responseText);
					//var respuesta = xdr_existeHuellaSocio.responseXML;
					//alert(xdr_existeHuellaSocio.responseText);
					//alert(hacerDespues);
					//alert(hacerNoEncontro);
					//alert(xdr_existeHuellaSocio.responseText);
					var respuesta = loadXMLString(xdr_existeHuellaSocio.responseText);

					var resultado="";
					var lEncontro=false;
					var status="FALSE";
					try{status=respuesta.documentElement.getElementsByTagName("status")[0].firstChild.nodeValue;}catch(e){}
					var error="";
					try{error=respuesta.documentElement.getElementsByTagName("error")[0].firstChild.nodeValue;}catch(e){}
					if(status=="TRUE"){
						var nRegistrosq=respuesta.getElementsByTagName("registrosq");
						var registrosq =nRegistrosq[0];
						if(registrosq.hasChildNodes()){
							if(registrosq.childNodes.length>0){
								var nRegistrosocio=registrosq.getElementsByTagName("registrosocio");
								var registrosocio;
								var a;
								var nNumeroSocio,nNombre,nHabilitado,nEsEmpleado,nDedos;
								var numerosocio,nombre,habilitado,esempleado,dedos;
								var c=0;
								var tmp;
								for(a=0;a<(registrosq.childNodes.length);a++){
									registrosocio=nRegistrosocio[a];
									nNumeroSocio=registrosocio.getElementsByTagName("numerosocio");
									numerosocio=(nNumeroSocio[0].firstChild) ? new String(nNumeroSocio[0].firstChild.nodeValue):"";

									nNombre=registrosocio.getElementsByTagName("nombre");
									nombre=(nNombre[0].firstChild) ? new String(nNombre[0].firstChild.nodeValue):"";

									nHabilitado=registrosocio.getElementsByTagName("habilitado");
									habilitado=(nHabilitado[0].firstChild) ? new String(nHabilitado[0].firstChild.nodeValue):"";

									nEsEmpleado=registrosocio.getElementsByTagName("esempleado");
									esempleado=(nEsEmpleado[0].firstChild) ? new String(nEsEmpleado[0].firstChild.nodeValue):"";

									nDedos=registrosocio.getElementsByTagName("dedos");
									dedos=(nDedos[0].firstChild) ? new String(nDedos[0].firstChild.nodeValue):"";

									lEncontro=true;
								}
								if(lEncontro){
									if(hacerDespues!=""){setTimeout(hacerDespues,500);}
								}
							}
						}
					}
					else if(status=="FALSE"){
						if(mostrarAlertas){
							if(error=="CONEXION")alert("Error de CONEXION con el Servicio de Huellas");
							else if(error=="CONSULTA")alert("SIN REGISTROS DE HUELLAS");
							else if(error=="REGISTROS")alert("NO SE ENCONTRARON REGISTROS DE HUELLAS");
							else if(error=="PARAMETROS")alert("Faltan Parametros para identificar");
							else alert("Error Desconocido: "+error);
						}
					}
					else alert("Estado Desconocido: "+status);
					if(lEncontro==false && hacerNoEncontro!=""){setTimeout(hacerNoEncontro,500);}

				}; 

                xdr_existeHuellaSocio.timeout	= timeout;
                xdr_existeHuellaSocio.open("get", url);
                xdr_existeHuellaSocio.send();
            }
            else{alert('Error crear XDR');}
									
		}
	}


	function loadXMLString(txt){
		if(window.DOMParser){
		  parser=new DOMParser();
		  xmlDoc=parser.parseFromString(txt,"text/xml");
		}
		else // Internet Explorer
		  {
		  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		  xmlDoc.async=false;
		  xmlDoc.loadXML(txt);
		}
	return xmlDoc;
	}

	existeHuellaSocio2 = function(IDUnion,IDCajaPopular,IDSucursal,IDSocio,mostrarAlertas,hacerDespues,hacerNoEncontro){
		alert("::");
		var lEntrar = false;
		if(IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){lEntrar=true;}
		else{alert("Los parametros de prueba estan vacios");}
		if(lEntrar){
			var urlDireccion = "http://172.8.1.20/4glhuellas/4glhuellas.php?"+
													"IDUnion="+IDUnion+
													"&IDCajaPopular="+IDCajaPopular+
													"&IDSucursal="+IDSucursal+
													"&IDSocio="+IDSocio;
			/*esperando*/
			if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp_existeHuellaSocio=new XMLHttpRequest();
			}
			else{// code for IE6, IE5
				xmlhttp_existeHuellaSocio=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp_existeHuellaSocio.onreadystatechange=function(){
				if (xmlhttp_existeHuellaSocio.readyState==4 && xmlhttp_existeHuellaSocio.status==200){
					alert("..");
					var respuesta = xmlhttp_existeHuellaSocio.responseXML;
					var resultado="";
					var lEncontro=false;
					var status="FALSE";
					try{status=respuesta.documentElement.getElementsByTagName("status")[0].firstChild.nodeValue;}catch(e){}
					var error="";
					try{error=respuesta.documentElement.getElementsByTagName("error")[0].firstChild.nodeValue;}catch(e){}
					if(status=="TRUE"){
						var nRegistrosq=respuesta.getElementsByTagName("registrosq");
						var registrosq =nRegistrosq[0];
						if(registrosq.hasChildNodes()){
							if(registrosq.childNodes.length>0){
								var nRegistrosocio=registrosq.getElementsByTagName("registrosocio");
								var registrosocio;
								var a;
								var nNumeroSocio,nNombre,nHabilitado,nEsEmpleado,nDedos;
								var numerosocio,nombre,habilitado,esempleado,dedos;
								var c=0;
								var tmp;
								for(a=0;a<(registrosq.childNodes.length);a++){
									registrosocio=nRegistrosocio[a];
									nNumeroSocio=registrosocio.getElementsByTagName("numerosocio");
									numerosocio=(nNumeroSocio[0].firstChild) ? new String(nNumeroSocio[0].firstChild.nodeValue):"";

									nNombre=registrosocio.getElementsByTagName("nombre");
									nombre=(nNombre[0].firstChild) ? new String(nNombre[0].firstChild.nodeValue):"";

									nHabilitado=registrosocio.getElementsByTagName("habilitado");
									habilitado=(nHabilitado[0].firstChild) ? new String(nHabilitado[0].firstChild.nodeValue):"";

									nEsEmpleado=registrosocio.getElementsByTagName("esempleado");
									esempleado=(nEsEmpleado[0].firstChild) ? new String(nEsEmpleado[0].firstChild.nodeValue):"";

									nDedos=registrosocio.getElementsByTagName("dedos");
									dedos=(nDedos[0].firstChild) ? new String(nDedos[0].firstChild.nodeValue):"";

									lEncontro=true;
								}
								if(lEncontro){
									if(hacerDespues!=""){setTimeout(hacerDespues,500);}
								}
							}
						}
					}
					else if(status=="FALSE"){
						if(mostrarAlertas){
							if(error=="CONEXION")alert("Error de CONEXION con el Servicio de Huellas");
							else if(error=="CONSULTA")alert("SIN REGISTROS DE HUELLAS");
							else if(error=="REGISTROS")alert("NO SE ENCONTRARON REGISTROS DE HUELLAS");
							else if(error=="PARAMETROS")alert("Faltan Parametros para identificar");
							else alert("Error Desconocido: "+error);
						}
					}
					else alert("Estado Desconocido: "+status);

					if(hacerNoEncontro!=""){setTimeout(hacerNoEncontro,500);}
				}
			}
			xmlhttp_existeHuellaSocio.open("GET",urlDireccion,true);
			xmlhttp_existeHuellaSocio.send();
		}
	}

	Codifica = function(cadena){
		var clave="0123456789abcdefghijklmn:ñopqrstuvwxyzABCDEFGHIJKLMN-ÑOPQRSTUVWXYZ.*#$%&/()=?¡*[]";var resultado="",caracter,cont,posic,codigo;
		for(cont=0; cont<cadena.length; cont++){
			caracter=cadena.substring(cont,cont+1);posic=clave.indexOf(caracter);codigo=clave.substring(posic+1,posic+2);resultado+=codigo;
		}
	return TrimSec(resultado);
    }

	Decodifica = function(cadena){
		var clave="0123456789abcdefghijklmn:ñopqrstuvwxyzABCDEFGHIJKLMN-ÑOPQRSTUVWXYZ.*#$%&/()=?¡*[]";var resultado="",caracter,cont,posic,codigo;
		for(cont=0; cont<cadena.length; cont++){
			caracter=cadena.substring(cont,cont+1);posic=clave.indexOf(caracter);codigo=clave.substring(posic-0,posic-1);resultado+=codigo;
		}
	return TrimSec(resultado);
	}

	function LTrimSec(str){
		var whitespace = new String(" \t\n\r");var s = new String(str);
		if (whitespace.indexOf(s.charAt(0)) != -1) {
			var j=0, i = s.length;
			while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
			j++;
			s = s.substring(j, i);
		}
	return s;
	}

	function RTrimSec(str){
		var whitespace = new String(" \t\n\r");var s = new String(str);
		if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
			var i = s.length - 1;       // Get length of string
			while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
			s = s.substring(0, i+1);
		}
	return s;
	}

	function TrimSec(str){return RTrimSec(LTrimSec(str));}

	VerificaCajeroHD = function(IDUsuario,TipoTransaccion,IDUnion,IDCajaPopular,IDSucursal,IDSocio,Continuar){
		if(IDUsuario!=""&&TipoTransaccion!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""&&Continuar!=""){
			var c_IDUsuario=Codifica(IDUsuario);
			var c_TipoTransaccion=Codifica(TipoTransaccion);
			var d=new Date();
			var dd=d.getDate();
			var mm=d.getMonth();
			var yyyy=d.getFullYear();
			var valorFecha=dd+'/'+mm+'/'+yyyy;
			var c_FechaEvento=Codifica(valorFecha);
			var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);
			var c_HashHD=Codifica(valorHash);
			var c_IDUnion=Codifica(IDUnion);
			var c_IDCajaPopular=Codifica(IDCajaPopular);
			var c_IDSucursal=Codifica(IDSucursal);
			var c_IDSocio=Codifica(IDSocio);
			var c_Continuar=Codifica(Continuar);
			if(c_IDUsuario!=""&&c_TipoTransaccion!=""&&c_FechaEvento!=""&&valorHash!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){
				var cSU = "";
				try{cSU=Codifica(document.getElementById(IDSucursal).value);}catch(e){}
				var ventanaHD=window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=N", null, strParametros);
				var vecRespuestaVentana = ventanaHD;
				vecRespuestaVentana = vecRespuestaVentana.split(":");
				var lgRes=vecRespuestaVentana.length;
				if(lgRes>0){
					if(vecRespuestaVentana[0]=="TRUE"){
						try{
							var nVhash = 0;
							try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}catch(e){}
							nVhash=new String(nVhash);
							valorHash=new String(valorHash);
							nVhash = nVhash.valueOf();
							nVhash = nVhash.valueOf();
							if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){
								document.getElementById(IDUnion).value=vecRespuestaVentana[6];
								document.getElementById(IDCajaPopular).value=vecRespuestaVentana[7];
								document.getElementById(IDSucursal).value=vecRespuestaVentana[8];
								document.getElementById(IDSocio).value=vecRespuestaVentana[9];
								document.getElementById("passwordx").value=vecRespuestaVentana[9];
								document.getElementById(Continuar).click();
							}
							else{alert("ERROR EN RESPUESTA");}
						}catch(e){}
					}
				}
			}else{alert("FALTAN DATOS PARA REALIZAR VERIFICACION");}
		}else{alert("NO EXISTE UNA SESION DE USUARIO ACTIVA, VUELVA A ENTRAR AL SISTEMA");}
	}

	visorVerificaCajeroHD = function(IDUsuario,TipoTransaccion,IDUnion,IDCajaPopular,IDSucursal,IDSocio,Continuar){
		if(IDUsuario!=""&&TipoTransaccion!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""&&Continuar!=""){
			var c_IDUsuario=Codifica(IDUsuario);
			var c_TipoTransaccion=Codifica(TipoTransaccion);
			var d=new Date();
			var dd=d.getDate();
			var mm=d.getMonth();
			var yyyy=d.getFullYear();
			var valorFecha=dd+'/'+mm+'/'+yyyy;
			var c_FechaEvento=Codifica(valorFecha);
			var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);
			var c_HashHD=Codifica(valorHash);
			var c_IDUnion=Codifica(IDUnion);
			var c_IDCajaPopular=Codifica(IDCajaPopular);
			var c_IDSucursal=Codifica(IDSucursal);
			var c_IDSocio=Codifica(IDSocio);
			var c_Continuar=Codifica(Continuar);
			var Auto="TRUE";
			if(c_IDUsuario!=""&&c_TipoTransaccion!=""&&c_FechaEvento!=""&&valorHash!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){
				var cSU = "";
				try{cSU=Codifica(document.getElementById(IDSucursal).value);}catch(e){}
				var ventanaHD=window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&AT="+Codifica(Auto)+"&SU="+cSU+"&VS=N", null, strParametros);
//				setTimeout('ventanaHD.close()', 5000);
				var vecRespuestaVentana = ventanaHD;
				vecRespuestaVentana = vecRespuestaVentana.split(":");
				var lgRes=vecRespuestaVentana.length;
				if(lgRes>0){
					if(vecRespuestaVentana[0]=="TRUE"){
						try{
							var nVhash = 0;
							try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}catch(e){}
							nVhash=new String(nVhash);
							valorHash=new String(valorHash);
							nVhash = nVhash.valueOf();
							nVhash = nVhash.valueOf();
							if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){
/*								document.getElementById(IDUnion).value=vecRespuestaVentana[6];
								document.getElementById(IDCajaPopular).value=vecRespuestaVentana[7];
								document.getElementById(IDSucursal).value=vecRespuestaVentana[8];
								document.getElementById(IDSocio).value=vecRespuestaVentana[9];
								document.getElementById("passwordx").value=vecRespuestaVentana[9]; */
								document.getElementById(Continuar).click();
							}
							else{alert("ERROR EN RESPUESTA");}
						}catch(e){}
					}
				}
			}else{alert("FALTAN DATOS PARA REALIZAR VERIFICACION");}
		}else{alert("NO EXISTE UNA SESION DE USUARIO ACTIVA, VUELVA A ENTRAR AL SISTEMA");}
	}

	verFrmSocioCajaHD = function(IDUsuario,TipoTransaccion,IDUnion,IDCajaPopular,IDSucursal,IDSocio,Continuar){
		if(IDUsuario!=""&&TipoTransaccion!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""&&Continuar!=""){
			var c_IDUsuario=Codifica(IDUsuario);var c_TipoTransaccion=Codifica(TipoTransaccion);var d=new Date();var dd=d.getDate();var mm=d.getMonth();var yyyy=d.getFullYear();var valorFecha=dd+'/'+mm+'/'+yyyy;var c_FechaEvento=Codifica(valorFecha);var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);var c_HashHD=Codifica(valorHash);var c_IDUnion=Codifica(IDUnion);var c_IDCajaPopular=Codifica(IDCajaPopular);var c_IDSucursal=Codifica(IDSucursal);var c_IDSocio=Codifica(IDSocio);var c_Continuar=Codifica(Continuar);
			if(c_IDUsuario!=""&&c_TipoTransaccion!=""&&c_FechaEvento!=""&&valorHash!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){
				var cSU = "";
				try{cSU=Codifica(document.getElementById(IDSucursal).value);}catch(e){}
				var ventanaHD=window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=N", null, strParametros);var vecRespuestaVentana = ventanaHD;vecRespuestaVentana = vecRespuestaVentana.split(":");var lgRes=vecRespuestaVentana.length;
				if(lgRes>0){
					if(vecRespuestaVentana[0]=="TRUE"){
						try{
							var nVhash = 0;
							try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}catch(e){}
							nVhash=new String(nVhash);
							valorHash=new String(valorHash);
							nVhash = nVhash.valueOf();
							nVhash = nVhash.valueOf();
							if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){document.getElementById(IDUnion).value=vecRespuestaVentana[6];document.getElementById(IDCajaPopular).value=vecRespuestaVentana[7];document.getElementById(IDSucursal).value=vecRespuestaVentana[8];document.getElementById(IDSocio).value=vecRespuestaVentana[9];document.getElementById(Continuar).click();
							}else{alert("ERROR EN RESPUESTA");}
						}catch(e){}
					}
				}
			}else{alert("FALTAN DATOS PARA REALIZAR VERIFICACION");}
		}else{alert("NO EXISTE UNA SESION DE USUARIO ACTIVA, VUELVA A ENTRAR AL SISTEMA");}
	}

	visorFrmSocioCajaHD = function(IDUsuario,TipoTransaccion,IDUnion,IDCajaPopular,IDSucursal,IDSocio,Continuar){
		if(IDUsuario!=""&&TipoTransaccion!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""&&Continuar!=""){
			var c_IDUsuario=Codifica(IDUsuario);var c_TipoTransaccion=Codifica(TipoTransaccion);var d=new Date();var dd=d.getDate();var mm=d.getMonth();var yyyy=d.getFullYear();var valorFecha=dd+'/'+mm+'/'+yyyy;var c_FechaEvento=Codifica(valorFecha);var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);var c_HashHD=Codifica(valorHash);var c_IDUnion=Codifica(IDUnion);var c_IDCajaPopular=Codifica(IDCajaPopular);var c_IDSucursal=Codifica(IDSucursal);var c_IDSocio=Codifica(IDSocio);var c_Continuar=Codifica(Continuar);
			if(c_IDUsuario!=""&&c_TipoTransaccion!=""&&c_FechaEvento!=""&&valorHash!=""&&IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""){
				var cSU = "";
				try{cSU=Codifica(document.getElementById(IDSucursal).value);}catch(e){}
				var ventanaHD=window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=N", null, strParametros);var vecRespuestaVentana = ventanaHD;vecRespuestaVentana = vecRespuestaVentana.split(":");var lgRes=vecRespuestaVentana.length;
				if(lgRes>0){
					if(vecRespuestaVentana[0]=="TRUE"){
						try{
							var nVhash = 0;
							try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}catch(e){}
							nVhash=new String(nVhash);
							valorHash=new String(valorHash);
							nVhash = nVhash.valueOf();
							nVhash = nVhash.valueOf();
/*							if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){document.getElementById(IDUnion).value=vecRespuestaVentana[6];document.getElementById(IDCajaPopular).value=vecRespuestaVentana[7];document.getElementById(IDSucursal).value=vecRespuestaVentana[8];document.getElementById(IDSocio).value=vecRespuestaVentana[9];document.getElementById(Continuar).click(); */
							if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){document.getElementById(Continuar).click();
							}else{alert("ERROR EN RESPUESTA");}
						}catch(e){}
					}
				}
			}else{alert("FALTAN DATOS PARA REALIZAR VERIFICACION");}
		}else{alert("NO EXISTE UNA SESION DE USUARIO ACTIVA, VUELVA A ENTRAR AL SISTEMA");}
	}

	verFrmSocioCajaHDNumSocio = function(IDUsuario_In,IDTipoTransaccion,IDUnion,IDCajaPopular,IDSucursal,IDSocio,Continuar){
		var lEntrar = false;

		if(IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDSocio!=""&&IDUsuario_In!=""){
			var sUS = document.getElementById(IDUnion).value;
			var sCS = document.getElementById(IDCajaPopular).value;
			var sSS = document.getElementById(IDSucursal).value;
			var sSO = document.getElementById(IDSocio).value;

			if(sUS!=""&&sCS!=""&&sSS!=""&&sSO!=""){
				var vSO=new Array();var vLL=new Array();var cantidadTemporales=0;var c=0,x=0;var IDUsuario = IDUsuario_In;
	
				IDUsuario=IDUsuario.replace(String.fromCharCode(209),"N");
				IDUsuario=IDUsuario.replace(String.fromCharCode(241),"N");

				var lExiste=false;
				var cSU=Codifica(IDSucursal);
				var c_IDUsuario=Codifica(IDUsuario);
				var d=new Date();var dd=d.getDate();var mm=d.getMonth();var yyyy=d.getFullYear();var valorFecha=dd+'/'+mm+'/'+yyyy;
				var c_FechaEvento=Codifica(valorFecha);var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);var c_HashHD=Codifica(valorHash);var c_TipoTransaccion=Codifica(IDTipoTransaccion);var vSO_C=new Array();var vecRespuestaVentana="";var nVhash=0,i=0;
				var compSocio = new String(sUS+":"+sCS+":"+sSS+":"+sSO);

				var ventanaHD = window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=Y"+"&VSOC="+compSocio, null, strParametros);
				vecRespuestaVentana = ventanaHD;
				vecRespuestaVentana = vecRespuestaVentana.split(":");
				var lgRes = vecRespuestaVentana.length;
				var s_SO="";
				
				sSS_ = "";
				sSO_ = "";
				if(lgRes>0){
					if(vecRespuestaVentana[0]=="TRUE"){
						try{
							nVhash = 0;
							try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}
							catch(e){}
							nVhash=new String(nVhash);
							valorHash=new String(valorHash);
							nVhash = nVhash.valueOf();
							nVhash = nVhash.valueOf();
							if(IDUsuario==vecRespuestaVentana[2]&&IDTipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){
								s_SO = new String(vecRespuestaVentana[6]+":"+vecRespuestaVentana[7]+":"+vecRespuestaVentana[8]+":"+vecRespuestaVentana[9]);
								sSS_ = vecRespuestaVentana[8];
								sSO_ = vecRespuestaVentana[9];

								try{sSS = parseInt(sSS,10);}catch(e){}
								try{sSO = parseInt(sSO,10);}catch(e){}

								try{sSS_ = parseInt(sSS_,10);}catch(e){}
								try{sSS_ = parseInt(sSS_,10);}catch(e){}


								if(sSS==sSS_&&sSO==sSO_){lExiste=true;}

							}else{alert("ERROR EN RESPUESTA");}
						}catch(e){}
					}
				}
				if(lExiste){
					if(Continuar!=""){document.getElementById(Continuar).click();}
				}else{alert("NO PASO VERIFICACION");}
			}else{alert("PARA HACER ESTA VERIFICACION, ES NECESARIO COLOCAR EL NUMERO DE SUCURSAL Y SOCIO");}
		}else{alert("NO SE CARGARON PARAMETROS, VUELVA A INICIAR SESION");}
	}

	verFrmBuscarSocioHD = function(IDUsuario,FechaEvento,ElementoHash){
		if(IDUsuario!=""){
			var c_IDUsuario=Codifica(IDUsuario);var c_FechaEvento=Codifica(FechaEvento);var valorHash=ElementoHash;
			try{valorHash			= parseInt(valorHash);valorHash++;}catch(e){}
			valorHash				= new String(valorHash);
			var c_HashHD			= Codifica(valorHash);
			if(c_IDUsuario!=""&&c_FechaEvento!=""&&c_HashHD!=""){
				var strParametrosB = "width=725,height=570,top=5,left=5,status=no,titlebar=no, scrollbars=no, toolbar=no, menubar=no, location=no, directories=no";
				var ventanaHD = window.open(ProtocoloHD+IPServidorHD+DirPromoHD+ScriptHD+"?US="+c_IDUsuario+"&FE="+c_FechaEvento+"&VH="+c_HashHD, "ventanaHD", strParametrosB);
			}else{alert("FALTAN DATOS PARA REALIZAR PROCESO");}
		}else{alert("NO EXISTE UNA SESION DE USUARIO ACTIVA, VUELVA A ENTRAR AL SISTEMA");}
	}

	sociosVerificarFicha = function(IDUnion,IDCajaPopular,IDSucursal,Ejercicio,IDCajaAsignada,IDTipoTransaccion,IDFicha,IDUsuarioEncode,DO_Later){
		var lEntrar = false;
		if(IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&Ejercicio!=""&&IDCajaAsignada!=""&&IDTipoTransaccion!=""&&IDFicha!=""&&IDUsuarioEncode!=""){
			var pkLlave = IDUnion+":"+IDCajaPopular+":"+IDSucursal+":"+Ejercicio+":"+IDCajaAsignada+":"+IDTipoTransaccion+":"+IDFicha;
			var ajax_sociosVerificarFicha=creaAjax();
			ajax_sociosVerificarFicha.open ('POST', '_hd.r', true);
			ajax_sociosVerificarFicha.onreadystatechange = function(){
				if (ajax_sociosVerificarFicha.readyState==1){}
				else if (ajax_sociosVerificarFicha.readyState==4){
					if(ajax_sociosVerificarFicha.status==200){
						var vSO=new Array();var vLL=new Array();var respuesta=ajax_sociosVerificarFicha.responseXML;var status="";var tipoerror="";var cantidadTemporales=0;var c=0,x=0;var IDUsuario="";var lgTmpFicXML=0;var lExiste;
						try{
							status=respuesta.documentElement.getElementsByTagName("status")[0].firstChild.nodeValue;
							tipoerror=respuesta.documentElement.getElementsByTagName("tipoerror")[0].firstChild.nodeValue;
							if(status=="TRUE"&&tipoerror=="NINGUNO"){
								try{
									IDUsuario=respuesta.documentElement.getElementsByTagName("idusuario")[0].firstChild.nodeValue;
									IDUsuario=IDUsuario.replace(String.fromCharCode(209),"N");
									IDUsuario=IDUsuario.replace(String.fromCharCode(241),"N");
									var XML_cantidadTemporales = respuesta.documentElement.getElementsByTagName("cantidadtemporales")[0].firstChild.nodeValue;
									cantidadTemporales = parseInt(XML_cantidadTemporales);
									if(cantidadTemporales>0){
										var nodo;var lUN,lCA,lSU,lEJ,lCS,lTT,lFI,sUS,sCS,sSS,sSO;var pkLlave = IDUnion+":"+IDCajaPopular+":"+IDSucursal+":"+Ejercicio+":"+IDCajaAsignada+":"+IDTipoTransaccion+":"+IDFicha;var xmlSO="";var XML_registrosTemporales	= respuesta.documentElement.getElementsByTagName("detallefichatemp");
										for(c=0; c<cantidadTemporales; c++){
											nodo=XML_registrosTemporales[c].getElementsByTagName("idunion");lUN = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idcajapopular");
											lCA = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idsucursal");
											lSU = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("ejercicio");
											lEJ = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idcajaasignada");
											lCS = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idtipotransaccion");
											lTT = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idficha");
											lFI = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idunionsocio");
											sUS = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idcajapopularsocio");
											sCS = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idsucursalsocio");
											sSS = new String(nodo[0].firstChild.nodeValue);
											nodo=XML_registrosTemporales[c].getElementsByTagName("idsocio");
											sSO = new String(nodo[0].firstChild.nodeValue);
											xmlSO = sUS+":"+sCS+":"+sSS+":"+sSO;
											lExiste=false;
											for(x=0;x<vSO.length;x++){if(xmlSO==vSO[x]){lExiste=true;break;}}
											if(lExiste==false){vLL.push(lUN+":"+lCA+":"+lSU+":"+lEJ+":"+lCS+":"+lTT+":"+lFI);vSO.push(xmlSO);}
										}
									}
								}catch(e){alert("ERROR");}
							}else{alert(tipoerror);}
						}catch(e){alert("MAL FORMADO XML");}
						lgTmpFicXML=vLL.length;
						if(lgTmpFicXML>0&&lgTmpFicXML==vSO.length){
							var cSU=Codifica(IDSucursal);var c_IDUsuario=Codifica(IDUsuario);var d=new Date();var dd=d.getDate();var mm=d.getMonth();var yyyy=d.getFullYear();var valorFecha=dd+'/'+mm+'/'+yyyy;var c_FechaEvento=Codifica(valorFecha);var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);var c_HashHD=Codifica(valorHash);var c_TipoTransaccion=Codifica(IDTipoTransaccion);var vSO_C=new Array();var ventanaHD;var vecRespuestaVentana="";var lgRes=0,nVhash=0,i=0;var sSO="";
							var compSocio = "";
							for(x=0;x<vSO.length;x++){
								if(vSO[x]!="")compSocio=compSocio+"_"+vSO[x];
							}
							if(compSocio!="")compSocio="&VSOC="+compSocio.substring(1);

							alert("PARA PROCEDER CON LA OPERACION CONFIRMAR HUELLA DIGITAL DE LOS SOCIOS A LOS CUALES SE LES GENERA EL CARGO");
							for(x=0; x<lgTmpFicXML; x++){
								ventanaHD = window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=Y"+compSocio, null, strParametros);
								vecRespuestaVentana = ventanaHD;
								vecRespuestaVentana = vecRespuestaVentana.split(":");
								lgRes = vecRespuestaVentana.length;
								if(lgRes>0){
									if(vecRespuestaVentana[0]=="TRUE"){
										try{
											nVhash = 0;
											try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}
											catch(e){}
											nVhash=new String(nVhash);
											valorHash=new String(valorHash);
											nVhash = nVhash.valueOf();
											nVhash = nVhash.valueOf();
											if(IDUsuario==vecRespuestaVentana[2]&&IDTipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){
												sSO = vecRespuestaVentana[6]+":"+vecRespuestaVentana[7]+":"+vecRespuestaVentana[8]+":"+vecRespuestaVentana[9];
												lExiste=false;
												for(i=0;i<vSO_C.length;i++){if(sSO==vSO_C[i]){lExiste=true;break;}}
												if(lExiste==false){vSO_C.push(sSO);}
											}else{alert("ERROR EN RESPUESTA");}
										}catch(e){}
									}
								}
							}
							var cIsTrued = 0;
							for(x=0; x<lgTmpFicXML; x++){if(vLL[x]==pkLlave){for(i=0; i<vSO_C.length; i++){if(vSO[x]==vSO_C[i]){cIsTrued++;break;}}}}
							if(cIsTrued==lgTmpFicXML){if(DO_Later!=""){eval(DO_Later);}}
							else{alert("NO PASO VERIFICACION");}
						}else{alert("NO HAY REGISTROS");}
					}
					else if(ajax_sociosVerificarFicha.status==404){alert("La direccion no existe");}
					else{alert("Error: " + ajax_sociosVerificarFicha.status);}
				}
			}
			ajax_sociosVerificarFicha.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			ajax_sociosVerificarFicha.send("sAccion=xmlDetalleFichaTemp"+"&IDUnion="+IDUnion+"&IDCajaPopular="+IDCajaPopular+"&IDSucursal="+IDSucursal+"&Ejercicio="+Ejercicio+"&IDCajaAsignada="+IDCajaAsignada+"&IDTipoTransaccion="+IDTipoTransaccion+"&IDFicha="+IDFicha+"&IDUsuarioEncode="+IDUsuarioEncode);
			return;
		}
	}

	sociosVerificarPrestamo = function(IDUnion,IDCajaPopular,IDSucursal,IDPrestamo,IDUsuarioEncode,TipoTransaccion){
		var lEntrar = false;var xmlDoc;var respuesta;
		if(IDUnion!=""&&IDCajaPopular!=""&&IDSucursal!=""&&IDPrestamo!=""&&IDUsuarioEncode!=""&&TipoTransaccion!=""){
			var pathSub = location.pathname;
			if(pathSub.lastIndexOf("/")>0){pathSub = location.host+pathSub.substring(0, pathSub.lastIndexOf("/"))+"/"};
			xmlDoc = "http://"+pathSub+"_hd.r?"+"sAccion=xmlDatosPrestamo"+"&IDUnion="+IDUnion+"&IDCajaPopular="+IDCajaPopular+"&IDSucursal="+IDSucursal+"&IDPrestamo="+IDPrestamo+"&IDUsuarioEncode="+IDUsuarioEncode;
			try{respuesta = AbrirFichero(xmlDoc);lEntrar=true;}catch(e){}
		}
		if(lEntrar){
			var status="";var tipoerror="";var cantidadTemporales=0;var c=0,x=0;var IDUsuario="";var lgTmpFicXML=0;var lExiste;var pkLlave=IDUnion+":"+IDCajaPopular+":"+IDSucursal+":"+IDPrestamo;var vSO=new Array();var vLL=new Array();var status="";var tipoerror="";var cantidadTemporales=0;var c=0,x=0;var IDUsuario="";var lgTmpFicXML=0;var lExiste;
			try{
				status=respuesta.documentElement.getElementsByTagName("status")[0].firstChild.nodeValue;
				tipoerror=respuesta.documentElement.getElementsByTagName("tipoerror")[0].firstChild.nodeValue;
				if(status=="TRUE"&&tipoerror=="NINGUNO"){
					try{
						IDUsuario=respuesta.documentElement.getElementsByTagName("idusuario")[0].firstChild.nodeValue;
						IDUsuario=IDUsuario.replace(String.fromCharCode(209),"N");
						IDUsuario=IDUsuario.replace(String.fromCharCode(241),"N");
						var nodo;var lUN,lCA,lSU,lPR,sUS,sCS,sSS,sSO;var pkLlave=IDUnion+":"+IDCajaPopular+":"+IDSucursal+":"+IDPrestamo;var xmlSO="";var XML_registrosPR= respuesta.documentElement.getElementsByTagName("prestamo");var cantidadTemporales = XML_registrosPR.length;
						for(c=0; c<cantidadTemporales; c++){
							nodo=XML_registrosPR[c].getElementsByTagName("idunion");
							lUN = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idcajapopular");
							lCA = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idsucursal");
							lSU = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idprestamo");
							lPR = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idunionsocio");
							sUS = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idcajapopularsocio");
							sCS = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idsucursalsocio");
							sSS = new String(nodo[0].firstChild.nodeValue);
							nodo=XML_registrosPR[c].getElementsByTagName("idsocio");
							sSO = new String(nodo[0].firstChild.nodeValue);
							xmlSO = sUS+":"+sCS+":"+sSS+":"+sSO;
							lExiste=false;
							for(x=0;x<vSO.length;x++){if(xmlSO==vSO[x]){lExiste=true;break;}}
							if(lExiste==false){vLL.push(lUN+":"+lCA+":"+lSU+":"+lPR);vSO.push(xmlSO);}
						}
					}catch(e){alert("ERROR");}
				}else{alert(tipoerror);}
			}catch(e){alert("MAL FORMADO XML");}
			lgTmpFicXML=vLL.length;
			if(lgTmpFicXML>0&&lgTmpFicXML==vSO.length){
				var cSU=Codifica(IDSucursal);var c_IDUsuario=Codifica(IDUsuario);var d=new Date();var dd=d.getDate();var mm=d.getMonth();var yyyy=d.getFullYear();var valorFecha=dd+'/'+mm+'/'+yyyy;var c_FechaEvento=Codifica(valorFecha);var valorHash=Math.floor(Math.random()*99999);valorHash=new String(valorHash);var c_HashHD=Codifica(valorHash);var c_TipoTransaccion=Codifica(TipoTransaccion);var vSO_C=new Array();var ventanaHD;var vecRespuestaVentana="";var lgRes=0,nVhash=0,i=0;var sSO="";

				var compSocio = "";
				for(x=0;x<vSO.length;x++){
					if(vSO[x]!="")compSocio=compSocio+"_"+vSO[x];
				}
				if(compSocio!="")compSocio="&VSOC="+compSocio.substring(1);
				
				alert("PARA PROCEDER CON LA OPERACION CONFIRMAR HUELLA DIGITAL DE LOS SOCIOS A LOS CUALES SE LES GENERA EL CARGO");
				for(x=0; x<lgTmpFicXML; x++){
					ventanaHD = window.showModalDialog(ProtocoloHD+IPServidorHD+DirCajaHD+ScriptHD+"?US="+c_IDUsuario+"&TT="+c_TipoTransaccion+"&FE="+c_FechaEvento+"&VH="+c_HashHD+"&SU="+cSU+"&VS=Y"+compSocio, null, strParametros);
					vecRespuestaVentana = ventanaHD;
					vecRespuestaVentana = vecRespuestaVentana.split(":");
					lgRes = vecRespuestaVentana.length;
					if(lgRes>0){
						if(vecRespuestaVentana[0]=="TRUE"){
							try{
								nVhash = 0;
								try{nVhash=parseInt(vecRespuestaVentana[5]);nVhash--;}
								catch(e){}
								nVhash=new String(nVhash);
								valorHash=new String(valorHash);
								nVhash = nVhash.valueOf();
								nVhash = nVhash.valueOf();
								if(IDUsuario==vecRespuestaVentana[2]&&TipoTransaccion==vecRespuestaVentana[3]&&valorFecha==vecRespuestaVentana[4]&&nVhash==valorHash){
									sSO = vecRespuestaVentana[6]+":"+vecRespuestaVentana[7]+":"+vecRespuestaVentana[8]+":"+vecRespuestaVentana[9];
									lExiste=false;
									for(i=0;i<vSO_C.length;i++){if(sSO==vSO_C[i]){lExiste=true;break;}}
									if(lExiste==false){vSO_C.push(sSO);}
								}else{alert("ERROR EN RESPUESTA");}
							}catch(e){}
						}
					}
				}
				var cIsTrued = 0;
				for(x=0; x<lgTmpFicXML; x++){if(vLL[x]==pkLlave){for(i=0; i<vSO_C.length; i++){if(vSO[x]==vSO_C[i]){cIsTrued++;break;}}}}
				if(cIsTrued==lgTmpFicXML){alert("CONFIRMADO");return true;}
				else{alert("NO PASO HUELLA");}
			}else{alert("NO HAY REGISTROS");}
		}
	return false;
	}