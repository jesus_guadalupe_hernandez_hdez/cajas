var Tables = {};
$(document).ready(function () {
    //ACCIONES AL DAR CLIC FUERA DE LOS MENUS DE NOTIFICACIONES
    //clears notifications panel
    $(document).click(function () {
        var NotPanel = $(".notifications");
        if (NotPanel[0].className == 'notifications active') {
            $(".panelNotif").toggle("fast");
            NotPanel.toggleClass("active");
        }

        var PendPanel = $(".message");
        if (PendPanel[0].className == 'message active') {
            $(".panelpend").toggle("fast");
            PendPanel.toggleClass("active");
        }

        var AlertPanel = $(".comment");
        if (AlertPanel[0].className == 'comment active') {
            $(".panelalert").toggle("fast");
            AlertPanel.toggleClass("active");
        }

        var InfoPanel = $(".user");
        if (InfoPanel[0].className == 'user active') {
            $(".panelinfo").toggle("fast");
            InfoPanel.toggleClass("active");
        }

    });
    // -----------------------------------------------

    // ACCIONES AL DAR CLIC DENTRO DE LOS MENUS DE NOTIFICACIONES
    //ioen notifications
    $(".notifications").click(function () {
        $(".panelNotif").toggle("fast");
        $(this).toggleClass("active");
    });
    $(".message").click(function () {
        $(".panelpend").toggle("fast");
        $(this).toggleClass("active");
    });
    $(".comment").click(function () {
        $(".panelalert").toggle("fast");
        $(this).toggleClass("active");
    });
    $(".user").click(function () {
        $(".panelinfo").toggle("fast");
        $(this).toggleClass("active");
    });
    // -----------------------------------------------

    //ACCIONES AL DAR CLIC FUERA DE LOS MENUS DE NOTIFICACIONES
    //stop propagation
    $(".notifications").click(function (e) {
        e.stopPropagation();
        var PendPanel = $(".message");
        if (PendPanel[0].className == 'message active') {
            $(".panelpend").toggle("fast");
            PendPanel.toggleClass("active");
        }
        var AlertPanel = $(".comment");
        if (AlertPanel[0].className == 'comment active') {
            $(".panelalert").toggle("fast");
            AlertPanel.toggleClass("active");
        }

        var InfoPanel = $(".user");
        if (InfoPanel[0].className == 'user active') {
            $(".panelinfo").toggle("fast");
            InfoPanel.toggleClass("active");
        }
    })
    $(".message").click(function (e) {
        e.stopPropagation();
        var NotPanel = $(".notifications");
        if (NotPanel[0].className == 'notifications active') {
            $(".panelNotif").toggle("fast");
            NotPanel.toggleClass("active");
        }
        var AlertPanel = $(".comment");
        if (AlertPanel[0].className == 'comment active') {
            $(".panelalert").toggle("fast");
            AlertPanel.toggleClass("active");
        }

        var InfoPanel = $(".user");
        if (InfoPanel[0].className == 'user active') {
            $(".panelinfo").toggle("fast");
            InfoPanel.toggleClass("active");
        }
    })
    $(".comment").click(function (e) {
        e.stopPropagation();
        var NotPanel = $(".notifications");
        if (NotPanel[0].className == 'notifications active') {
            $(".panelNotif").toggle("fast");
            NotPanel.toggleClass("active");
        }

        var PendPanel = $(".message");
        if (PendPanel[0].className == 'message active') {
            $(".panelpend").toggle("fast");
            PendPanel.toggleClass("active");
        }

        var InfoPanel = $(".user");
        if (InfoPanel[0].className == 'user active') {
            $(".panelinfo").toggle("fast");
            InfoPanel.toggleClass("active");
        }
    })
    $(".user").click(function (e) {
        e.stopPropagation();
        var NotPanel = $(".notifications");
        if (NotPanel[0].className == 'notifications active') {
            $(".panelNotif").toggle("fast");
            NotPanel.toggleClass("active");
        }

        var PendPanel = $(".message");
        if (PendPanel[0].className == 'message active') {
            $(".panelpend").toggle("fast");
            PendPanel.toggleClass("active");
        }

        var AlertPanel = $(".comment");
        if (AlertPanel[0].className == 'comment active') {
            $(".panelalert").toggle("fast");
            AlertPanel.toggleClass("active");
        }
    })

});


