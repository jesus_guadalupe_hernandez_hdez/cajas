function ValidaFechaNac(Fecha) {
    if (moment(Fecha, 'DD/MM/YYYY', true).isValid() == true) {
        var from = Fecha.split("/");
        var date1 = new Date(from[2], from[1] - 1, from[0]);
        var date2 = new Date();

        if (date1 > date2)
            return false;

        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays >= 36500)
            return false;

        return true;
    }
    else {
        return false;
    }
}

function ValidaRetroactividadVigencia(Fecha) {
    if (moment(Fecha, 'DD/MM/YYYY', true).isValid() == true) {
        var dateToday = moment(new Date()).format("DD/MM/YYYY");

        var formatFecP1 = Fecha.split("/");
        var dateParamC1 = new Date(formatFecP1[2], formatFecP1[1] - 1, formatFecP1[0]);;
        var dateTodayC1 = new Date();
        dateParamC1 = dateParamC1.setHours(0, 0, 0, 0);
        dateTodayC1 = dateTodayC1.setHours(0, 0, 0, 0);

        if (dateParamC1 < dateTodayC1)
            return false;

        var from = Fecha.split("/");
        var date1 = new Date(from[2], from[1] - 1, from[0]);
        var date2 = new Date();

        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 30)
            return false;

        return true;
    }
    else {
        return false;
    }
}

function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

//ENVÍA MENSAJE GENERICO
//----------------------------------------------------
function fAlertMsg(Texto1, Texto2, Tipo, ShowMessage) {

    if (ShowMessage == undefined || ShowMessage == true) {
        swal({
            html: true,
            title: Texto1,
            text: Texto2,
            type: Tipo,
            showCancelButton: false,
            confirmButtonColor: "#ff1821",
            confirmButtonText: "Aceptar",
            closeOnConfirm: true
        },
                        function () {
                            return false;
                        });

    }
};

function fAlertTerminado(){
	//swal("La pagina se cargo correctamente");
	swal.close();
}

function fAlertShowPoputMsg() {
    swal({
        title: "Estimado Socio Comercial",
        html: true,
        text: "Con el objetivo de brindarte soluciones digitales, ponemos a tu disposición la nueva Firma Autógrafa Digital(FAD). Una vez que hayas completado la solicitud podrás elegir la opción &quot;enviar solicitud con FAD&quot; y con ello recabar las firmas digitales a través de la aplicación &quot;FAD&quot;.    <br>Contamos con tu apoyo para utilizar esta nueva funcionalidad que te permitirá: obtener la firma digital del contratante y/o titular desde cualquier lugar, además de disminuir los revires por solicitud y brindarte una atención más ágil y oportuna<br> Consulta <a href='" + Liga + "MISC/PDF_Solicitudes/Manual_SISE_FAD.pdf' download='manual.pdf'>aquí</a > <br><br><div align='justify'>El manual de usuario.</div>",
        showCancelButton: true,
        cancelButtonColor: "#green",
        cancelButtonText: "Aceptar",
        closeOnConfirm: false,
        showConfirmButton: false
    });
    //$(".sweet-alert").css({ 'margin-left': '-350px', 'width': '900px' });
};

function fAlertMsgRFC() {
    //fAlertMsg('Aviso', 'Recuerda que el RFC debe de contar con la homoclave correcta ya que será utilizado para generar el CFDI de la póliza. Si no la tienes puedes continuar con el llenado de la solicitud', 'warning');

    swal({
        title: "Aviso",
        html: true,
        type: "warning",
        text: "Recuerda que el RFC debe de contar con la homoclave correcta ya que será utilizado para generar el CFDI de la póliza. Si no la tienes puedes continuar con el llenado de la solicitud",
        showCancelButton: true,
        cancelButtonColor: "#green",
        cancelButtonText: "Continuar",
        closeOnConfirm: false,
        showConfirmButton: false
    });
}
//ENVÍA MENSAJE DE ENVIO / GUARDADO EN PROCESO
//----------------------------------------------------
function fAlertSending() {
    var opts = {
        lines: 10, // The number of lines to draw
        length: 0, // The length of each line
        width: 25, // The line thickness
        radius: 50, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 16, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#00CC00', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: '999', // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };

    var previousWindowKeyDown = window.onkeydown;
    swal({
        title: 'En proceso...',
        text: "<div>Por favor espera un momento.</div></br></br></br></br></br></br></br></br></br></br><div id='spin'></div>",
        type: "info",
        allowOutsideClick: false,
        showConfirmButton: false,
        showCancelButton: false,
        html: true

    },
	function (isConfirm) {
	});
    var target = document.getElementById('spin');
    var spinner = new Spinner(opts).spin(target);
    //$(".sweet-alert").css({ 'margin-left': '-256px', 'width': '478' });
};


function fAlertMsgConfirmSentPoput(Texto1, temporalArray) {
	
	var tem = "";
	temporalArray.forEach(function(temporalArray, index) {
		tem += "<tr>";
		
		 if (temporalArray.chrColor.trim() == "AMARILLO") {
			 tem += "<td class='table-warning'>" + temporalArray.sTexto + "</td>";
		 }	 
		 else
			 {
			 tem += "<td class='table-success'>" + temporalArray.sTexto + "</td>";
			 }
		 tem +="</tr>";
	 });
	
	var temp = '<table class="table table-bordered table-hover table-responsive" id="tableUserList" >' +
            '<thead class="thead-dark">' +
            '<tr>' + 
            '<td>Santoral: </td>' +
            '</tr>' + 
            '</thead>' + 
            '<tbody>' + 
            tem+ 
          '</tbody>' + 
          '</table>';
	
    swal({
        title: Texto1,
        text: temp,
        html: true,
        //type: 'info',
        showCancelButton: false,
        confirmButtonColor: "#420609",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    },
    	function (isConfirm) {
    	/*
    	    if (isConfirm) {
    	        window.location.href = Liga + "Distribuidores/Seguimiento/Enviadas.aspx"
    	    } else {

    	    }*/
        });
    //$(".sweet-alert").css({ 'margin-left': '-256px', 'width': '478' });
};

function fAlertMsgConfirmSent(Texto1, Texto2, Tipo) {
    swal({
        title: Texto1,
        text: Texto2,
        //type: Tipo,
        showCancelButton: false,
        confirmButtonColor: "#ff1821",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    },
    	function (isConfirm) {
    	/*
    	    if (isConfirm) {
    	        window.location.href = Liga + "Distribuidores/Seguimiento/Enviadas.aspx"
    	    } else {

    	    }*/
        });
    //$(".sweet-alert").css({ 'margin-left': '-256px', 'width': '478' });
};

function fAlertReceiving() {
    var opts = {
        lines: 10, // The number of lines to draw
        length: 0, // The length of each line
        width: 25, // The line thickness
        radius: 50, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 16, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#00CC00', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: '999', // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };

    swal({
        title: 'Cargando datos...',
        text: "<div>Por favor espera un momento.</div></br></br></br></br></br></br></br></br></br></br><div id='spin'></div>",
        type: "info",
        allowOutsideClick: false,
        showConfirmButton: false,
        showCancelButton: false,
        html: true

    },
	function () {
	    return false;
	});
    var target = document.getElementById('spin');
    var spinner = new Spinner(opts).spin(target);
    //$(".sweet-alert").css({ 'margin-left': '-256px', 'width': '478' });
};

//FUncion que despliega un mensaje confirmando que la solicitud se guardó exitosamente
function fAlertConfirmSave(Data) {
    swal({
        title: "¡Solicitud guardada!",
        text: Data,
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#ff1821",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    },
                function (isConfirm) {

        });
    //$(".sweet-alert").css({ 'margin-left': '-256px', 'width': '478' });
};


//Revisa si una dirección de correo tiene formato valido
function validaMAIL(value) {
    var isValid = true;
    if (typeof value == 'undefined' || value.length <= 0) {
        isValid = false
        return isValid;
    }
    var Correos;
    Correos = value.split(';');

    for (i = 0; i < Correos.length; i++) {
        if (!Correos[i] && Correos[i].length <= 0) {
            isValid = false
            return isValid;
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (!reg.test(Correos[i])) {
            isValid = false
            return isValid;
        }
    }
    return isValid;
}


//Obtiene los parametros enviados a traves de URL.
function GetQueryString() {
    return (function (a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
            var p = a[i].split(/=(.+)/);
            if (p.length == 1)
                b[p[0]] = "";
            else
                b[p[0]] = decodeURIComponent(p[1]);
        }
        return b;
    })(window.location.search.substr(1).split('&'));
}


//CONVIERTE A BASE 64
function setsending(val) {
    if (val != '') {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(val), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
        //val = val.toString().replace(/\r\n/g, "\n");
        //return Base64.encode(val);}
    }
    else {
        return '';
    }

}
// CONVIERTE A TEXTO
//----------------------------------------------------
function setreceiving(val) {
    if (val != '') {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');


        var decrypted = CryptoJS.AES.decrypt(
          val,
          key,
          {
              iv: iv,
              mode: CryptoJS.mode.CBC,
              padding: CryptoJS.pad.Pkcs7
          }
        );
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
    else {
        return '';
    }
}


var Base64 = {


    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },
    _utf8_encode: function (string) {
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}



// INICIALIZACION DE TABLAS FILTROS
// -------------------------------------------------------
// -------------------------------------------------------
function InitializeTableFilters(TableFilters) {
    var footableFilter;
    var FilterArray = TableFilters.split(',')
    var fs = [];
    var filterText = '';
    // ESTAS FUNCIONES INICIALIZAN LOS FILTROS PERSONALIZADOS, (Se inicializan 5 filtros, aunque no se contemplen los 5 en html
    $(FilterArray).each(function (index, value) {
        $('.filter-' + (index + 1) + '').change(function (e) {
            e.preventDefault();
            fs = [];
            filterText = '';
            $(FilterArray).each(function (index, value) {
                fs.push(variable = (typeof $('.filter-' + (index + 1) + '').val() != 'undefined' && $('.filter-' + (index + 1) + '').val() != null) ? $('.filter-' + (index + 1) + '').val() : '');
            });
            $(fs).each(function (index, value) {
                filterText = filterText + value + ' ';
            });
            filterText = filterText + $('#filter').val();
            footableFilter = $('table').data('footable-filter');
            footableFilter.filter(filterText);
            PopulateFilters(TableFilters);
        });
    });
    $('#filter2').on('input', function (e) {
        e.preventDefault();
        fs = [];
        filterText = '';
        $(FilterArray).each(function (index, value) {
            fs.push(variable = (typeof $('.filter-' + (index + 1) + '').val() != 'undefined' && $('.filter-' + (index + 1) + '').val() != null) ? $('.filter-' + (index + 1) + '').val() : '');
        });
        $(fs).each(function (index, value) {
            if (value != '') {
                filterText = filterText + value + ' ';
            }

        });
        filterText = filterText + $('#filter2').val();
        footableFilter = $('.table2').data('footable-filter');
        footableFilter.filter(filterText);
        PopulateFilters(TableFilters);
    });

    $(".clear-filter").click(function (e) {
        e.preventDefault();
        $(FilterArray).each(function (index, value) {
            $('.filter-' + (index + 1) + '').val('');
        });
        $('#filter').val('');
        footableFilter = $('.table').data('footable-filter');
        footableFilter.filter('');
        PopulateFilters(TableFilters);
    });

    $(".clear-filter2").click(function (e) {
        e.preventDefault();
        $(FilterArray).each(function (index, value) {
            $('.filter-' + (index + 1) + '').val('');
        });
        $('#filter2').val('');
        footableFilter = $('.table2').data('footable-filter');
        footableFilter.filter('');
        PopulateFilters(TableFilters);
    });
    $('#filter3').on('input', function (e) {
        e.preventDefault();
        fs = [];
        filterText = '';
        $(FilterArray).each(function (index, value) {
            fs.push(variable = (typeof $('.filter-' + (index + 1) + '').val() != 'undefined' && $('.filter-' + (index + 1) + '').val() != null) ? $('.filter-' + (index + 1) + '').val() : '');
        });
        $(fs).each(function (index, value) {
            if (value != '') {
                filterText = filterText + value + ' ';
            }

        });
        filterText = filterText + $('#filter3').val();
        footableFilter = $('.table3').data('footable-filter');
        footableFilter.filter(filterText);
        PopulateFilters(TableFilters);
    });

    $(".clear-filter3").click(function (e) {
        e.preventDefault();
        $(FilterArray).each(function (index, value) {
            $('.filter-' + (index + 1) + '').val('');
        });
        $('#filter3').val('');
        footableFilter = $('.table3').data('footable-filter');
        footableFilter.filter('');
        PopulateFilters(TableFilters);
    });
    window.onkeydown = null;
}

function PopulateFilters(Filters) {
    var items = [], options = [];
    var FilterArray = Filters.split(',')
    var CurSelection;
    var idx;
    $(FilterArray).each(function (index, value) {
        CurSelection = variable = (typeof $('.filter-' + (index + 1) + '').val() != 'undefined' && $('.filter-' + (index + 1) + '').val() != null) ? $('.filter-' + (index + 1) + '').val() : '';
        items = [], options = [];
        idx = $('.table thead th:Contains("' + value + '")')[0].cellIndex + 1;
        $('.table tbody tr:not(.footable-filtered) td:nth-child(' + idx + ')').each(function () {
            var a = this
            var b = a.innerText;
            if (b != '' && jQuery.inArray(b, items) == -1) { items.push(b); }
        });
        items = $.unique(items);
        options.push('<option value="">Seleccione..</option>');
        $.each(items, function (i, item) {
            options.push('<option value="' + item + '">' + item + '</option>');
        })
        try {
            $('.filter-' + (index + 1) + '').empty().append(options.join());
        }
        catch (ex) { }
        if ($('.filter-' + (index + 1) + ' option[value="' + CurSelection + '"]').length > 0) {
            $('.filter-' + (index + 1) + '').val(CurSelection);
            $('.filter-' + (index + 1) + '').removeClass('FilterOut');
        }
        else {
            $('.filter-' + (index + 1) + '').val('');
            $('.filter-' + (index + 1) + '').addClass('FilterOut');
        }

    });
    window.onkeydown = null;
}

function InitializeTable(Filters) {
    $('table').footable();
    $('.sort-column').click(function (e) {
        e.preventDefault();
        //get the footable sort object
        var footableSort = $('table').data('footable-sort');
        //get the index we are wanting to sort by
        var index = $(this).data('index');
        footableSort.doSort(index, 'toggle');
    });

    $('#change-page-size').change(function (e) {
        e.preventDefault();
        var pageSize = $(this).val();
        $('.footable').data('page-size', pageSize);
        $('.footable').trigger('footable_initialized');
    });

    $('.clear-filter').click(function (e) {
        e.preventDefault();
        $('table').trigger('footable_clear_filter');
        $('.filter-status').val('');
        PopulateFilters(Filters);
    });
    window.onkeydown = null;

}
// -------------------------------------------------------
// -------------------------------------------------------
//FIN INICIALIZACION DE TABLAS FILTROS



function ValidaRfc(rfcStr, tipo) {

    var strCorrecta;
    strCorrecta = rfcStr;
    if (rfcStr.length < 12 || rfcStr.length > 13) {
        return false;
    }
    if (rfcStr.length == 12 && tipo != 'Persona_moral') {
        return false;
    }
    if (rfcStr.length == 13 && tipo == 'Persona_moral') {
        return false;
    }

    if (rfcStr.length == 12) {
        var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    if (rfcStr.length == 13) {
        var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    var validRfc = new RegExp(valid);
    var matchArray = strCorrecta.match(validRfc);
    if (matchArray == null) {
        return false;
    }
    else {
        return true;
    }
}


function GenerarCURP(Tipo) {
    var DatosCURP = {};

    $('#fcEdoCURPContra').removeClass('MissingField')
    $('#fcCURPContra')[0].value = '';


    try {
        $('#fcEdoCURPSol').removeClass('MissingField')
        $('#fcCURPSol')[0].value = '';
        $('#fcEdoCURPManco').removeClass('MissingField')
        $('#fcCURPManco')[0].value = '';
    }
    catch (err) { }


    if (Tipo == 'CONTRATANTE') {

        DatosCURP.F_Nac = moment($('#fdFechaNacimientoContra')[0].value, 'DD/MM/YYYY', true).format('DD/MM/YYYY');
        DatosCURP.Edo = $('#fcEdoCURPContra')[0].value;
        DatosCURP.Nombre = $('#fcNombreContra')[0].value;
        DatosCURP.Ap_P = $('#fcPaternoContra')[0].value;
        DatosCURP.Ap_M = $('#fcMaternoContra')[0].value;
        DatosCURP.Sexo = $('#Contratante').find("input[name='Sexo_Contratante']:checked").val();

        if (DatosCURP.Edo == '0') {
            fAlertMsg('Aviso', 'Para poder generar el CURP es necesario que selecciones el estado de nacimiento.', 'warning', true);
            $('#fcEdoCURPContra').addClass('MissingField')
            return;
        }
    }
    if (Tipo == 'SOLICITANTE') {
        DatosCURP.F_Nac = moment($('#fdFechaNacimientoSol')[0].value).format('DD/MM/YYYY');
        DatosCURP.Edo = $('#fcEdoCURPSol')[0].value;
        DatosCURP.Nombre = $('#fcNombreSol')[0].value;
        DatosCURP.Ap_P = $('#fcPaternoSol')[0].value;
        DatosCURP.Ap_M = $('#fcMaternoSol')[0].value;
        DatosCURP.Sexo = $('#Solicitante').find("input[name='Sexo_Solicitante']:checked").val();
        if (DatosCURP.Edo == '0') {
            fAlertMsg('Aviso', 'Para poder generar el CURP es necesario que selecciones el estado de nacimiento.', 'warning', true);
            $('#fcEdoCURPSol').addClass('MissingField')
            return;
        }
    }
    if (Tipo == 'MANCOMUNADO') {
        DatosCURP.F_Nac = moment($('#fdFechaNacimientoManco')[0].value).format('DD/MM/YYYY');
        DatosCURP.Edo = $('#fcEdoCURPManco')[0].value;
        DatosCURP.Nombre = $('#fcNombreManco')[0].value;
        DatosCURP.Ap_P = $('#fcPaternoManco')[0].value;
        DatosCURP.Ap_M = $('#fcMaternoManco')[0].value;
        DatosCURP.Sexo = $('#SolicitanteMancomunado').find("input[name='Sexo_SolicitanteM']:checked").val();
        if (DatosCURP.Edo == '0') {
            fAlertMsg('Aviso', 'Para poder generar el CURP es necesario que selecciones el estado de nacimiento.', 'warning', true);
            $('#fcEdoCURPManco').addClass('MissingField')
            return;
        }
    }
    if (Tipo == 'MENOR') {

    }

    $.ajax({
        type: "POST",
        url: Liga + "AJAX_Agentes.aspx/GeneraCURP",
        data: '{ObjCURP: ' + JSON.stringify(DatosCURP) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d != 'error') {
                if (Tipo == 'CONTRATANTE') {
                    $('#fcCURPContra')[0].value = response.d;
                }
                if (Tipo == 'SOLICITANTE') {
                    $('#fcCURPSol')[0].value = response.d;
                }

                if (Tipo == 'MANCOMUNADO') {
                    $('#fcCURPManco')[0].value = response.d;
                }

                fAlertMsg('Listo!', 'El CURP se generó exitosamente.', 'success')
            }
            else {
                fAlertMsg('Oops..', 'No se pudo generar el CURP de forma automática.', 'error')
            }
        },
        error: function (response) {
            window.location.href = Liga + "ErrorPage.aspx"
        }
    });

}



//VALIDA QUE LA TARJETA SEA UN NUMERO EXISTENTE
//----------------------------------------------------

function isCreditCard(CC) {
    if (CC == 'MISMA') {
        return (true)
    }
    else {
        if (CC.length > 19 || CC.length == 0) return (false);
        sum = 0;
        mul = 1;
        l = CC.length;
        for (i = 0; i < l; i++) {
            digit = CC.substring(l - i - 1, l - i);
            tproduct = parseInt(digit, 10) * mul;
            if (tproduct >= 10) sum += (tproduct % 10) + 1;
            else sum += tproduct;
            if (mul == 1) mul++;
            else mul--;
        }
        if ((sum % 10) == 0) return (true);
        else return (false);
    }
}



//----------------------------------------------------

//VALIDA INEGRIDAD DE CAMPOS DE TEXTO
//SIN CARACTERES ESPECIALES
function TextFieldValidation(evt) { // 1
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
 ((evt.which) ? evt.which : 0));
    //Punto decimal
    if (charCode == 46)
        return false;
    //Numneros 0-9
    if (charCode >= 48 && charCode <= 57) {
        return true
    }
    if ((evt.shiftKey && evt.keyCode == 38) || charCode == 44 || charCode == 45 || charCode == 46) {
        return true
    }
    if ((evt.shiftKey && evt.keyCode == 47) || (evt.keyCode == 47)) {
        return true
    }

    if ((charCode > 32 && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode <= 192 || charCode >= 255)) && (charCode != 12)) {
        return false;
    }
    //comma 188
    //guion 189
    //punto 190
    //ampersand (e.shiftKey && e.keyCode === 55) 
    return true;

}
function soloNumerosyLetras(evt) { // 1

    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
 ((evt.which) ? evt.which : 0));
    //Numneros 0-9
    if (charCode >= 48 && charCode <= 57) {
        return true
    }

    if ((evt.shiftKey && evt.keyCode == 47) || (evt.keyCode == 47)) {
        return true
    }
    if ((evt.shiftKey && evt.keyCode == 45) || (evt.keyCode == 45)) {
        return true
    }

    if ((charCode > 32 && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode <= 192 || charCode >= 255)) && (charCode != 12) || (evt.altKey && evt.keyCode == 64)) {
        return false;
    }
    //comma 188
    //guion 189
    //punto 190
    //ampersand (e.shiftKey && e.keyCode === 55) 
    return true;

}

//VALIDA SOLO NUMEROS CON 2 DECIMALES
function isDecimalNumberKey(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    // backspace
    if (key == 8) return true
    // 0-9
    if (key > 47 && key < 58) {
        if (field.value == "") return true
        regexp = /.[0-9]{2}$/
        return !(regexp.test(field.value))
    }
    // .
    if (key == 46) {
        if (field.value == "") return false
        regexp = /^[0-9]+$/
        return regexp.test(field.value)
    }
    // other key
    return false

}

function isVencimiento(e, field) {

    key = e.keyCode ? e.keyCode : e.which
    // backspace
    if (key == 8) return true
    if (key == 47) return true
    // 0-9
    if (key > 47 && key < 58) {
        if (field.value == "") return true
        regexp = /[0-9]{2}\/[0-9]{2}/
        return !(regexp.test(field.value))
    }
    // .
    if (key == 46) {
        if (field.value == "") return false
        regexp = /[0-9]{2}\/[0-9]{2}"/
        return regexp.test(field.value)
    }
    // other key
    return false

}

//Funcion para validar el Importe solo te deja escribir numeros y un punto
function soloNumerosYPunto(e, ctrl) {
    // capturamos la tecla pulsada
    var teclaPulsada = window.event ? window.event.keyCode : e.which;

    // capturamos el contenido del input
    var valor = ctrl.value;

    // 45 = tecla simbolo menos (-)
    // Si el usuario pulsa la tecla menos, y no se ha pulsado anteriormente
    // Modificamos el contenido del mismo añadiendo el simbolo menos al
    // inicio
    //if (teclaPulsada == 45 && valor.indexOf("-") == -1) {
    //    ctrl.value = "-" + valor;
    //}

    // 13 = tecla enter
    // 46 = tecla punto (.)
    // Si el usuario pulsa la tecla enter o el punto y no hay ningun otro
    // punto
    if (teclaPulsada == 13 || (teclaPulsada == 46 && valor.indexOf(".") == -1)) {
        return true;
    }

    // devolvemos true o false dependiendo de si es numerico o no
    return /\d/.test(String.fromCharCode(teclaPulsada));
}

function EstructuraDateSelector(evt, ctrl) {
    // capturamos la tecla pulsada
    var charCode = window.event ? window.event.keyCode : e.which;

    // capturamos el contenido del input
    var valor = ctrl.value + String.fromCharCode(charCode);;

    if (valor.length == 10 || (valor.split('/').length == 3 && valor.split('/')[2].length > 3)) {
        if (valor.indexOf('0000') != -1 || moment(valor, 'DD/MM/YYYY', true).isValid() == false) {
            fAlertMsg('Fecha inválida', 'Por favor verifica que la fecha introducida esté en el formato dd/mm/aaaa.', 'warning', true);
            ctrl.value = '';
        }
    }

    if (charCode >= 48 && charCode <= 57) {
        return true
    }
    if ((evt.shiftKey && evt.keyCode == 47) || (evt.keyCode == 47)) {
        return true
    }

    if (charCode > 32 && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode <= 192 || charCode >= 255)) {
        return false;
    }



    //comma 188
    //guion 189
    //punto 190
    //ampersand (e.shiftKey && e.keyCode === 55) 
    return true;
}



//ENMASCARA DATOS DE TARJETA
//----------------------------------------------------
function maskdata(ctrl) {
    if (ctrl[0].value != '' && ctrl[0].readOnly != true) {
        ctrl[0].value = '*****' + ctrl[0].value.substr(ctrl[0].value.length - 4);
    }
}

//VALIDA SOLO NUMEROS
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // Punto charCode != 46
    if (charCode > 31
       && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function SinEspacios(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    // Punto charCode != 46
    if (charCode == 32)
        return false;

    return true;
}


function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var CellValue = '';
    var CSV = '\uFEFF';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            if (isNaN(arrData[i][index]) && moment(arrData[i][index], ['YYYY-MM-DDTHH:mm:ss.SSS', 'YYYY-MM-DDTHH:mm:ss.SS', 'YYYY-MM-DDTHH:mm:ss.S'], true).isValid()) {
                CellValue = moment(arrData[i][index], ['YYYY-MM-DDTHH:mm:ss.SSS', 'YYYY-MM-DDTHH:mm:ss.SS', 'YYYY-MM-DDTHH:mm:ss.S'], true).format('DD/MM/YYYY HH:mm:ss');
            }
            else {
                CellValue = 'Invalid date';
            }

            CellValue = (CellValue === 'Invalid date' ? arrData[i][index] : CellValue)
            row += '"' + CellValue + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }


    var blob = new Blob([CSV], { type: "text/csv;charset=utf-8" });
    saveAs(blob, ReportTitle + ".csv");

}



function JSONToTXTConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var CellValue = '';
    var CSV = '';

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            if (isNaN(arrData[i][index]) && moment(arrData[i][index], ['YYYY-MM-DDTHH:mm:ss.SSS', 'YYYY-MM-DDTHH:mm:ss.SS', 'YYYY-MM-DDTHH:mm:ss.S'], true).isValid()) {
                CellValue = moment(arrData[i][index], ['YYYY-MM-DDTHH:mm:ss.SSS', 'YYYY-MM-DDTHH:mm:ss.SS', 'YYYY-MM-DDTHH:mm:ss.S'], true).format('DD/MM/YYYY HH:mm:ss');
            }
            else {
                CellValue = 'Invalid date';
            }

            CellValue = (CellValue === 'Invalid date' ? arrData[i][index] : CellValue)
            row += '' + CellValue + '|';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    var blob = new Blob([CSV], { type: "text/csv;charset=utf-8" });
    saveAs(blob, ReportTitle + ".txt");


}



var saveAs = saveAs || function (e) { "use strict"; if (typeof e === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) { return } var t = e.document, n = function () { return e.URL || e.webkitURL || e }, r = t.createElementNS("http://www.w3.org/1999/xhtml", "a"), o = "download" in r, i = function (e) { var t = new MouseEvent("click"); e.dispatchEvent(t) }, a = /constructor/i.test(e.HTMLElement), f = /CriOS\/[\d]+/.test(navigator.userAgent), u = function (t) { (e.setImmediate || e.setTimeout)(function () { throw t }, 0) }, d = "application/octet-stream", s = 1e3 * 40, c = function (e) { var t = function () { if (typeof e === "string") { n().revokeObjectURL(e) } else { e.remove() } }; setTimeout(t, s) }, l = function (e, t, n) { t = [].concat(t); var r = t.length; while (r--) { var o = e["on" + t[r]]; if (typeof o === "function") { try { o.call(e, n || e) } catch (i) { u(i) } } } }, p = function (e) { if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)) { return new Blob([String.fromCharCode(65279), e], { type: e.type }) } return e }, v = function (t, u, s) { if (!s) { t = p(t) } var v = this, w = t.type, m = w === d, y, h = function () { l(v, "writestart progress write writeend".split(" ")) }, S = function () { if ((f || m && a) && e.FileReader) { var r = new FileReader; r.onloadend = function () { var t = f ? r.result : r.result.replace(/^data:[^;]*;/, "data:attachment/file;"); var n = e.open(t, "_blank"); if (!n) e.location.href = t; t = undefined; v.readyState = v.DONE; h() }; r.readAsDataURL(t); v.readyState = v.INIT; return } if (!y) { y = n().createObjectURL(t) } if (m) { e.location.href = y } else { var o = e.open(y, "_blank"); if (!o) { e.location.href = y } } v.readyState = v.DONE; h(); c(y) }; v.readyState = v.INIT; if (o) { y = n().createObjectURL(t); setTimeout(function () { r.href = y; r.download = u; i(r); h(); c(y); v.readyState = v.DONE }); return } S() }, w = v.prototype, m = function (e, t, n) { return new v(e, t || e.name || "download", n) }; if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) { return function (e, t, n) { t = t || e.name || "download"; if (!n) { e = p(e) } return navigator.msSaveOrOpenBlob(e, t) } } w.abort = function () { }; w.readyState = w.INIT = 0; w.WRITING = 1; w.DONE = 2; w.error = w.onwritestart = w.onprogress = w.onwrite = w.onabort = w.onerror = w.onwriteend = null; return m }(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content); if (typeof module !== "undefined" && module.exports) { module.exports.saveAs = saveAs } else if (typeof define !== "undefined" && define !== null && define.amd !== null) { define([], function () { return saveAs }) }


function goProcOp(NoSol) {
    window.location.href = Liga + "Operacion/ViewerOp/DetSolProc.aspx?S=" + NoSol
}

function goFinOp(NoSol) {
    window.location.href = Liga + "Operacion/ViewerOp/DetSolFin.aspx?S=" + NoSol
}

function goSentDist(NoSol) {
    window.location.href = Liga + "Distribuidores/Viewer/DetSolSent.aspx?S=" + NoSol
}




function goProcOpNOT(NoSol, id, type, Readed) {
    if (Readed == '0') {
        SetRead(id, type);
    }
    window.location.href = Liga + "Operacion/ViewerOp/DetSolProc.aspx?S=" + NoSol
}

function goFinOpNOT(NoSol, id, type, Readed) {
    if (Readed == '0') {
        SetRead(id, type);
    }
    window.location.href = Liga + "Operacion/ViewerOp/DetSolFin.aspx?S=" + NoSol
}

function goSentDistNOT(NoSol, id, type, Readed) {
    if (Readed == '0') {
        SetRead(id, type);
    }
    window.location.href = Liga + "Distribuidores/Viewer/DetSolSent.aspx?S=" + NoSol
}

function goFinDistNOT(NoSol, id, type, Readed) {
    if (Readed == '0') {
        SetRead(id, type);
    }
    window.location.href = Liga + "Distribuidores/Viewer/DetSolFin.aspx?S=" + NoSol
}


function SetRead(id, type) {
    $.ajax({
        type: "POST",
        url: Liga + "AJAX_Admin.aspx/MarkRead",
        data: '{id: ' + JSON.stringify(id) + ',type: ' + JSON.stringify(type) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
        },
        error: function (response) {
        }

    });

}